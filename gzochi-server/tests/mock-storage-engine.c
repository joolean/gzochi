/* mock-storage-engine.c: Storage engine impl. with introspection for testing
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "gzochid-storage.h"
#include "mock-storage-engine.h"
#include "storage-mem.h"

/* Mapping from `char *' to `gzochid_storage_context *'. */

static GHashTable *context_by_name;

/* Mapping from `gzochid_storage_context *' to `GHashTable *'. */

static GHashTable *stores_by_context;

/* A `GHRFunc' implementation for lookup up `GHashTable' entries by value (and
   removing them. */

static gboolean
find_value (gpointer key, gpointer value, gpointer user_data)
{
  return value == user_data;
}

/* Decorator function for mem store `initialize' - register created environments
   in the static tables. */   

static gzochid_storage_context *
initialize (char *path)
{
  gzochid_storage_context *context =
    gzochid_storage_engine_interface_mem.initialize (path);

  g_hash_table_insert (context_by_name, strdup (path), context);
  g_hash_table_insert
    (stores_by_context, context,
     g_hash_table_new_full (g_str_hash, g_str_equal, free, NULL));

  return context;
}

/* Decorator function for mem store `close_context' - remove environment from
   the static tables. */

static void
close_context (gzochid_storage_context *context)
{
  g_hash_table_remove (stores_by_context, context);
  g_hash_table_foreach_remove (context_by_name, find_value, context);

  gzochid_storage_engine_interface_mem.close_context (context);
}

/* Decorator function for mem store `open' - register opened store with the
   static tables. */

static gzochid_storage_store *
open (gzochid_storage_context *context, char *name, unsigned int flags)
{
  gzochid_storage_store *store =
    gzochid_storage_engine_interface_mem.open (context, name, flags);
  GHashTable *stores = g_hash_table_lookup (stores_by_context, context);
  
  g_hash_table_insert
    (g_hash_table_lookup (stores_by_context, context), strdup (name), store);

  return store;
}

/* Decorator function for mem store `close_store' - remove store from the static
   tables. */

static void
close_store (gzochid_storage_store *store)
{
  GHashTable *store_by_name = g_hash_table_lookup
    (stores_by_context, store->context);

  g_hash_table_foreach_remove (store_by_name, find_value, store);
  
  gzochid_storage_engine_interface_mem.close_store (store);
}

gzochid_storage_engine_interface gzochid_mock_storage_engine_interface;

void
gzochid_mock_storage_engine_initialize ()
{
  context_by_name = g_hash_table_new_full
    (g_str_hash, g_str_equal, free, NULL);
  stores_by_context = g_hash_table_new_full
    (g_direct_hash, g_direct_equal, NULL,
     (GDestroyNotify) g_hash_table_destroy);

  gzochid_mock_storage_engine_interface = (gzochid_storage_engine_interface)
    {
      "mock",
      
      initialize,
      close_context,
      gzochid_storage_engine_interface_mem.destroy_context,
      
      open,
      close_store,
      gzochid_storage_engine_interface_mem.destroy_store,
      
      gzochid_storage_engine_interface_mem.transaction_begin,
      gzochid_storage_engine_interface_mem.transaction_begin_timed,
      gzochid_storage_engine_interface_mem.transaction_commit,
      gzochid_storage_engine_interface_mem.transaction_rollback,
      gzochid_storage_engine_interface_mem.transaction_prepare,
      
      gzochid_storage_engine_interface_mem.transaction_get,
      gzochid_storage_engine_interface_mem.transaction_get_for_update,
      gzochid_storage_engine_interface_mem.transaction_put,
      gzochid_storage_engine_interface_mem.transaction_delete,
      gzochid_storage_engine_interface_mem.transaction_first_key,
      gzochid_storage_engine_interface_mem.transaction_next_key
    };
}

gzochid_storage_context *
gzochid_mock_storage_engine_get_context (const char *path)
{
  return g_hash_table_lookup (context_by_name, path);
}

gzochid_storage_store *
gzochid_mock_storage_engine_get_store (gzochid_storage_context *context,
				       const char *name)
{
  return g_hash_table_lookup
    (g_hash_table_lookup (stores_by_context, context), name);
}
