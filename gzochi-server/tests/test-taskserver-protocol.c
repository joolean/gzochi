/* test-taskserver-protocol.c: Test routines for taskserver-protocol.c.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "resolver.h"
#include "taskserver-protocol.h"
#include "taskserver.h"
#include "socket.h"

struct _GzochiMetadTaskServer
{
  GObject parent_instance;
};

G_DEFINE_TYPE (GzochiMetadTaskServer, gzochi_metad_task_server, G_TYPE_OBJECT);

static void
gzochi_metad_task_server_class_init (GzochiMetadTaskServerClass *klass)
{
}

static void
gzochi_metad_task_server_init (GzochiMetadTaskServer *self)
{
}

static GList *activity_log = NULL;

static void
clear_activity_log ()
{
  g_list_free_full (activity_log, g_free);
  activity_log = NULL;
}

void
gzochi_metad_taskserver_server_disconnected (GzochiMetadTaskServer *taskserver,
					     int node_id, GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf ("SERVER %d DISCONNECTED", node_id));
}

void
gzochi_metad_taskserver_application_started (GzochiMetadTaskServer *taskserver,
					     int node_id, const char *app,
					     GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf
     ("SERVER %d STARTED APPLICATION %s", node_id, app));
}

void
gzochi_metad_taskserver_application_resubmitted
(GzochiMetadTaskServer *taskserver, int node_id, const char *app, GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf
     ("SERVER %d RESUBMITTED APPLICATION %s", node_id, app));
}

void
gzochi_metad_taskserver_task_submitted (GzochiMetadTaskServer *taskserver,
					int node_id, const char *app,
					guint64 task_id, GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf
     ("SERVER %d SUBMITTED TASK %s/%" G_GUINT64_FORMAT, node_id, app, task_id));
}

void
gzochi_metad_taskserver_task_completed (GzochiMetadTaskServer *taskserver,
					int node_id, const char *app,
					guint64 task_id, GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf
     ("SERVER %d COMPLETED TASK %s/%" G_GUINT64_FORMAT, node_id, app, task_id));
}

void
gzochi_metad_taskserver_task_canceled (GzochiMetadTaskServer *taskserver,
				       int node_id, const char *app,
				       guint64 task_id, GError **err)
{
  activity_log = g_list_append
    (activity_log, g_strdup_printf
     ("SERVER %d CANCELED TASK %s/%" G_GUINT64_FORMAT, node_id, app, task_id));
}

struct _metaserver_wrapper_client
{
  gzochi_metad_taskserver_client *taskserver_client;
};

typedef struct _metaserver_wrapper_client metaserver_wrapper_client;

static gboolean
can_dispatch_wrapper (const GByteArray *bytes, gpointer data)
{
  metaserver_wrapper_client *wrapper_client = data;

  return gzochi_metad_taskserver_client_protocol.can_dispatch
    (bytes, wrapper_client->taskserver_client);
}

static unsigned int
dispatch_wrapper (const GByteArray *bytes, gpointer data)
{
  metaserver_wrapper_client *wrapper_client = data;

  return gzochi_metad_taskserver_client_protocol.dispatch
    (bytes, wrapper_client->taskserver_client);
}

static void
error_wrapper (gpointer data)
{
  metaserver_wrapper_client *wrapper_client = data;

  gzochi_metad_taskserver_client_protocol.error
    (wrapper_client->taskserver_client);
}

static void
free_wrapper (gpointer data)
{
  metaserver_wrapper_client *wrapper_client = data;

  gzochi_metad_taskserver_client_protocol.free
    (wrapper_client->taskserver_client);
  free (wrapper_client);
}

static gzochid_client_protocol wrapper_protocol =
  { can_dispatch_wrapper, dispatch_wrapper, error_wrapper, free_wrapper };

struct _taskserver_protocol_fixture
{
  GzochidSocketServer *socket_server;
  GzochiMetadTaskServer *taskserver;
  metaserver_wrapper_client *client;

  GIOChannel *read_channel;
  GIOChannel *write_channel;
};

typedef struct _taskserver_protocol_fixture taskserver_protocol_fixture;

static void
taskserver_protocol_fixture_set_up (taskserver_protocol_fixture *fixture,
				    gconstpointer user_data)
{
  int socketfd[2] = { 0 };
  int socket_fd = socketpair (PF_LOCAL, SOCK_STREAM, 0, socketfd);
  GIOChannel *write_channel = g_io_channel_unix_new (socketfd[1]);
  metaserver_wrapper_client *wrapper_client =
    malloc (sizeof (metaserver_wrapper_client));
  gzochid_client_socket *client_socket = gzochid_client_socket_new
    (write_channel, "", wrapper_protocol, wrapper_client);
  
  fixture->socket_server = gzochid_resolver_require
    (GZOCHID_TYPE_SOCKET_SERVER, NULL);
  fixture->taskserver = gzochid_resolver_require
    (GZOCHI_METAD_TYPE_TASK_SERVER, NULL);

  fixture->read_channel = g_io_channel_unix_new (socketfd[0]);
  fixture->write_channel = write_channel;
  fixture->client = wrapper_client;

  wrapper_client->taskserver_client = gzochi_metad_taskserver_client_new
    (fixture->taskserver, client_socket, 0);
  
  g_io_channel_set_flags (fixture->read_channel, G_IO_FLAG_NONBLOCK, NULL);  
  g_io_channel_set_encoding (write_channel, NULL, NULL);
  g_io_channel_set_flags (write_channel, G_IO_FLAG_NONBLOCK, NULL);
  
  gzochid_client_socket_listen (fixture->socket_server, client_socket);
  gzochid_client_socket_unref (client_socket);
}

static void
taskserver_protocol_fixture_tear_down (taskserver_protocol_fixture *fixture,
				       gconstpointer user_data)
{
  g_object_unref (fixture->socket_server);
  g_object_unref (fixture->taskserver);
  
  g_io_channel_unref (fixture->read_channel);
  
  clear_activity_log ();
}

static void
test_protocol_can_dispatch_true (taskserver_protocol_fixture *fixture,
				 gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x80""foo\x00", 7);

  g_assert (wrapper_protocol.can_dispatch (bytes, fixture->client));

  g_byte_array_unref (bytes);
}

static void
test_protocol_can_dispatch_false (taskserver_protocol_fixture *fixture,
				  gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x80""foo", 6);

  g_assert (! wrapper_protocol.can_dispatch (bytes, fixture->client));

  g_byte_array_unref (bytes);
}

static void
test_protocol_dispatch_one_start_app (taskserver_protocol_fixture *fixture,
				      gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x80""foo\x00", 7);
  
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);
  
  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr
    ((char *) activity_log->data, ==, "SERVER 0 STARTED APPLICATION foo");
}

static void
test_protocol_dispatch_one_complete_resubmission
(taskserver_protocol_fixture *fixture, gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x82""foo\x00", 7);
  
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);
  
  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr
    ((char *) activity_log->data, ==, "SERVER 0 RESUBMITTED APPLICATION foo");
}

static void
test_protocol_dispatch_one_submit_task (taskserver_protocol_fixture *fixture,
					gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0c\x84""foo\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 15);
  
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);
  
  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr
    ((char *) activity_log->data, ==, "SERVER 0 SUBMITTED TASK foo/123");
}

static void
test_protocol_dispatch_one_task_completed (taskserver_protocol_fixture *fixture,
					   gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0c\x86""foo\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 15);
  
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);
  
  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr
    ((char *) activity_log->data, ==, "SERVER 0 COMPLETED TASK foo/123");
}

static void
test_protocol_dispatch_one_cancel_task (taskserver_protocol_fixture *fixture,
				    gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0c\x88""foo\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 15);
  
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);
  
  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr
    ((char *) activity_log->data, ==, "SERVER 0 CANCELED TASK foo/123");
}

static void
test_protocol_dispatch_multiple (taskserver_protocol_fixture *fixture,
				 gconstpointer user_data)
{
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x80""foo\x00", 7);
  g_byte_array_append (bytes, (const guint8 *) "\x00\x04\x82""foo\x00", 7);
  wrapper_protocol.dispatch (bytes, fixture->client);
  g_byte_array_unref (bytes);

  g_assert_cmpint (g_list_length (activity_log), ==, 2);
  g_assert_cmpstr
    (g_list_nth_data (activity_log, 0), ==, "SERVER 0 STARTED APPLICATION foo");
  g_assert_cmpstr (g_list_nth_data (activity_log, 1), ==,
		   "SERVER 0 RESUBMITTED APPLICATION foo");
}

static void
test_protocol_error (taskserver_protocol_fixture *fixture,
		     gconstpointer user_data)
{
  wrapper_protocol.error (fixture->client);

  g_assert_cmpint (g_list_length (activity_log), ==, 1);
  g_assert_cmpstr ((char *) activity_log->data, ==, "SERVER 0 DISCONNECTED");
}

int
main (int argc, char *argv[])
{
#if GLIB_CHECK_VERSION (2, 36, 0)
  /* No need for `g_type_init'. */
#else
  g_type_init ();
#endif /* GLIB_CHECK_VERSION */

  g_test_init (&argc, &argv, NULL);

  g_test_add
    ("/taskserver/protocol/can_dispatch/true", taskserver_protocol_fixture,
     NULL, taskserver_protocol_fixture_set_up, test_protocol_can_dispatch_true,
     taskserver_protocol_fixture_tear_down);
  
  g_test_add
    ("/taskserver/protocol/can_dispatch/false", taskserver_protocol_fixture,
     NULL, taskserver_protocol_fixture_set_up, test_protocol_can_dispatch_false,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/dispatch/one/start-app", taskserver_protocol_fixture,
     NULL, taskserver_protocol_fixture_set_up,
     test_protocol_dispatch_one_start_app,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/dispatch/one/complete-resubmission",
     taskserver_protocol_fixture, NULL, taskserver_protocol_fixture_set_up,
     test_protocol_dispatch_one_complete_resubmission,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/dispatch/one/submit-task",
     taskserver_protocol_fixture, NULL, taskserver_protocol_fixture_set_up,
     test_protocol_dispatch_one_submit_task,
     taskserver_protocol_fixture_tear_down);
  
  g_test_add
    ("/taskserver/protocol/dispatch/one/task-completed",
     taskserver_protocol_fixture, NULL, taskserver_protocol_fixture_set_up,
     test_protocol_dispatch_one_task_completed,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/dispatch/one/cancel-task",
     taskserver_protocol_fixture, NULL, taskserver_protocol_fixture_set_up,
     test_protocol_dispatch_one_cancel_task,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/dispatch/multiple", taskserver_protocol_fixture,
     NULL, taskserver_protocol_fixture_set_up, test_protocol_dispatch_multiple,
     taskserver_protocol_fixture_tear_down);

  g_test_add
    ("/taskserver/protocol/error", taskserver_protocol_fixture, NULL,
     taskserver_protocol_fixture_set_up, test_protocol_error,
     taskserver_protocol_fixture_tear_down);
  
  return g_test_run ();
}
