;; gzochi/test-data.scm: Scheme unit tests for gzochi data API
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (gzochi app))
(import (gzochi data))
(import (gzochi io))
(import (gzochi mock-data))
(import (gzochi private data))
(import (gzochi srfi-64-support))
(import (rnrs io ports))
(import (srfi :1))
(import (srfi :8))
(import (srfi :64))

(initialize-mock-data)

(gzochi:define-managed-record-type test-record
  (fields (mutable foo (serialization gzochi:string-serialization))))

(define int-serialization (g:<-> gzochi:write-integer gzochi:read-integer))
(define str-serialization (g:<-> gzochi:write-string gzochi:read-string))

(define default-max-bucket-size 10)

(test-runner-current (gzochi:test-runner))

(test-begin "gzochi:managed-vector")

(test-group "ref"
  (let ((vec (gzochi:make-managed-vector 2)))
    (test-eqv #f (gzochi:managed-vector-ref vec 0))
    (test-eqv #f (gzochi:managed-vector-ref vec 1))
	      
    (gzochi:managed-vector-set! vec 0 (make-test-record "foo"))
    (gzochi:managed-vector-set! vec 1 "bar" #:serialization str-serialization)

    (test-equal "foo" (test-record-foo (gzochi:managed-vector-ref vec 0)))
    (test-equal "bar" (gzochi:managed-vector-ref vec 1))))

(test-group "->list"
  (let ((vec (gzochi:make-managed-vector 2)))
    (gzochi:managed-vector-set! vec 0 (make-test-record "foo"))
    (gzochi:managed-vector-set! vec 1 "bar" #:serialization str-serialization)
    (let ((lst (gzochi:managed-vector->list vec)))
      (test-eqv 2 (length lst))
      (test-equal "foo" (test-record-foo (car lst)))
      (test-equal "bar" (cadr lst)))))

(test-group "set!"
  (let ((vec (gzochi:make-managed-vector 2)))
    (gzochi:managed-vector-set! vec 0 (make-test-record "foo"))
    (gzochi:managed-vector-set! vec 1 "bar" #:serialization str-serialization)
    (gzochi:managed-vector-set! vec 0 "foo" #:serialization str-serialization)
    (gzochi:managed-vector-set! vec 1 (make-test-record "bar"))
    (test-equal "foo" (gzochi:managed-vector-ref vec 0))
    (test-equal "bar" (test-record-foo (gzochi:managed-vector-ref vec 1)))))
	    
(test-group "literal"
  (let ((vec (gzochi:managed-vector (make-test-record "foo") "bar"
				    #:serialization str-serialization)))
    (test-eqv 2 (gzochi:managed-vector-length vec))
    (test-equal "foo" (test-record-foo (gzochi:managed-vector-ref vec 0)))
    (test-equal "bar" (gzochi:managed-vector-ref vec 1))))

(test-end "gzochi:managed-vector")

(test-begin "gzochi:managed-sequence")

(test-group "->list"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq 1 #:serialization int-serialization)
    (gzochi:managed-sequence-add! seq 2 #:serialization int-serialization)
    (gzochi:managed-sequence-add! seq 3 #:serialization int-serialization)
    (test-equal '(1 2 3) (gzochi:managed-sequence->list seq))))

(test-group "add!"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq "foo" #:serialization str-serialization)
    (test-eqv 1 (gzochi:managed-sequence-size seq))
    (test-equal "foo" (gzochi:managed-sequence-ref seq 0))))

(test-group "contains?"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq 123 #:serialization int-serialization)
    (test-eqv #t (gzochi:managed-sequence-contains? seq 123 eqv?))
    (test-eqv #f (gzochi:managed-sequence-contains? seq 456 eqv?))))

(test-group "insert-at!"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-insert! seq 0 3 #:serialization int-serialization)
    (gzochi:managed-sequence-insert! seq 0 2 #:serialization int-serialization)
    (gzochi:managed-sequence-insert! seq 0 1 #:serialization int-serialization)
    (test-eqv 3 (gzochi:managed-sequence-size seq))
    (test-eqv 1 (gzochi:managed-sequence-ref seq 0))
    (test-eqv 2 (gzochi:managed-sequence-ref seq 1))
    (test-eqv 3 (gzochi:managed-sequence-ref seq 2))))

(test-group "delete-at!"
  (let ((seq (gzochi:make-managed-sequence))
	(rec (make-test-record "foo")))
    
    (gzochi:managed-sequence-add! seq rec)
    (gzochi:managed-sequence-delete-at! seq 0)
    (test-eqv 0 (gzochi:managed-sequence-size seq))))

(test-group "fold-left"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq "a" #:serialization str-serialization)
    (gzochi:managed-sequence-add! seq "b" #:serialization str-serialization)
    (gzochi:managed-sequence-add! seq "c" #:serialization str-serialization)
    (test-equal
     "cba" (gzochi:managed-sequence-fold-left seq string-append ""))))

(test-group "fold-right"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq "a" #:serialization str-serialization)
    (gzochi:managed-sequence-add! seq "b" #:serialization str-serialization)
    (gzochi:managed-sequence-add! seq "c" #:serialization str-serialization)
    (test-equal 
     "abc" (gzochi:managed-sequence-fold-right seq string-append ""))))

(test-group "force-split"
  (let ((seq (gzochi:make-managed-sequence)))
    (let loop ((i 0))
      (if (<= i default-max-bucket-size)
	  (begin
	    (gzochi:managed-sequence-add!
	     seq i #:serialization int-serialization)
	    (loop (+ i 1)))))
    (test-eqv (+ default-max-bucket-size 1) 
	      (gzochi:managed-sequence-size seq))))

(test-group "force-prune"
  (let ((seq (gzochi:make-managed-sequence)))
    (let loop ((i 0))
      (if (<= i default-max-bucket-size)
	  (begin
	    (gzochi:managed-sequence-add!
	     seq i #:serialization int-serialization)
	    (loop (+ i 1)))))
    (let loop ((i 0))
      (if (<= i (/ default-max-bucket-size 2))
	  (begin (gzochi:managed-sequence-delete-at! seq 0) (loop (+ i 1)))))
      
    (test-eqv (- (+ default-max-bucket-size 1) 
		 (+ (/ default-max-bucket-size 2) 1))
	      (gzochi:managed-sequence-size seq))))

(test-group "insert!"
  (let ((seq (gzochi:make-managed-sequence)))
    (gzochi:managed-sequence-add! seq 1 #:serialization int-serialization)
    (gzochi:managed-sequence-add! seq 3 #:serialization int-serialization)
    (gzochi:managed-sequence-insert! seq 1 2 #:serialization int-serialization)
    (test-equal '(1 2 3) (gzochi:managed-sequence->list seq))))

(test-end "gzochi:managed-sequence")

(test-begin "gzochi:managed-hashtable")

(define test-hash-cb (g:@ string-hash))
(define test-equiv-cb (g:@ string=?))
(define test-serialization (g:<-> gzochi:write-string gzochi:read-string))

(test-group "size"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eqv 0 (gzochi:managed-hashtable-size ht))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (test-eqv 1 (gzochi:managed-hashtable-size ht))))

(test-group "ref"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (test-equal "bar" (gzochi:managed-hashtable-ref ht "foo" #f))
    (test-eqv #f (gzochi:managed-hashtable-ref ht "bar" #f))
    (test-equal "baz" (gzochi:managed-hashtable-ref ht "bar" "baz"))))

(test-group "set!"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-set! ht "foo" "baz"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (test-equal "baz" (gzochi:managed-hashtable-ref ht "foo" #f))))

(test-group "delete!"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-delete! ht "foo")
    (test-eqv #f (gzochi:managed-hashtable-contains? ht "foo"))))

(test-group "contains?"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eqv #f (gzochi:managed-hashtable-contains? ht "foo"))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (test-eqv #t (gzochi:managed-hashtable-contains? ht "foo"))))

(test-group "update!"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eqv #f (gzochi:managed-hashtable-contains? ht "foo"))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-update!
     ht "foo" (lambda (val) (string-append val "-2")) #f
     #:key-serialization test-serialization
     #:value-serialization test-serialization)
    (test-equal "bar-2" (gzochi:managed-hashtable-ref ht "foo" #f))
    (gzochi:managed-hashtable-update!
     ht "baz" (lambda (val) (string-append val)) "quux"
     #:key-serialization test-serialization
     #:value-serialization test-serialization)
    (test-equal "quux" (gzochi:managed-hashtable-ref ht "baz" #f))))

(test-group "clear!"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eqv #f (gzochi:managed-hashtable-contains? ht "foo"))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-set! ht "baz" "quux"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-clear! ht)
    (test-eqv 0 (gzochi:managed-hashtable-size ht))
    (gzochi:managed-hashtable-clear! ht)
    (test-eqv 0 (gzochi:managed-hashtable-size ht))))

(test-group "keys"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-set! ht "baz" "quux"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (let* ((keys (gzochi:managed-hashtable-keys ht))
	   (keylist (gzochi:managed-vector->list keys)))
      (test-eqv 2 (gzochi:managed-vector-length keys))
      (test-assert (member "foo" keylist))
      (test-assert (member "baz" keylist))))) 

(test-group "entries"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (gzochi:managed-hashtable-set! ht "foo" "bar"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (gzochi:managed-hashtable-set! ht "baz" "quux"
				   #:key-serialization test-serialization
				   #:value-serialization test-serialization)
    (receive (keys values) (gzochi:managed-hashtable-entries ht)
      (let ((keylist (gzochi:managed-vector->list keys))
	    (valuelist (gzochi:managed-vector->list values)))
	(test-eqv 2 (gzochi:managed-vector-length keys))
	(test-eqv 2 (gzochi:managed-vector-length values))
	(let ((foo-idx (list-index (lambda (x) (equal? x "foo")) keylist))
	      (baz-idx (list-index (lambda (x) (equal? x "baz")) keylist)))
	  (test-assert foo-idx)
	  (test-assert baz-idx)
	  (test-equal "bar" (list-ref valuelist foo-idx))
	  (test-equal "quux" (list-ref valuelist baz-idx)))))))	      
	
(test-group "hash-function"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eq test-hash-cb (gzochi:managed-hashtable-hash-function ht))))

(test-group "equivalence-function"
  (let ((ht (gzochi:make-managed-hashtable test-hash-cb test-equiv-cb)))
    (test-eq test-equiv-cb (gzochi:managed-hashtable-equivalence-function ht))))

(test-end "gzochi:managed-hashtable")
