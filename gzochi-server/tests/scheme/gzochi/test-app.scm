;; gzochi/test-app.scm: Scheme unit tests for gzochi app API
;; Copyright (C) 2017 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (gzochi app))
(import (gzochi io))
(import (gzochi srfi-64-support))
(import (gzochi test-module))
(import (srfi :64))

(test-runner-current (gzochi:test-runner))

(test-begin "g:@")

(test-group "implicit-module"
  (let ((callback (g:@ gzochi:write-string)))
    (test-equal 'gzochi:write-string (gzochi:callback-procedure callback))
    (test-equal '(gzochi io) (gzochi:callback-module callback))))

  (test-equal 'gzochi:write-integer (gzochi:callback-procedure test-callback))
  (test-equal '(gzochi io) (gzochi:callback-module test-callback))

(test-end "g:@")
