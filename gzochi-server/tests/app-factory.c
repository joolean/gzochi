/* app-factory.c: A factory for test application contexts.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "app.h"
#include "app-factory.h"
#include "config.h"
#include "descriptor.h"
#include "fmemopen.h"
#include "resolver.h"

const char TEST_DESCRIPTOR[] = "<?xml version=\"1.0\" ?>\n"
  "<game name=\"test\">\n"
  "  <description>Test application</description>\n"
  "  <load-paths />\n"
  "  <initialized>\n"
  "    <callback module=\"test\" procedure=\"initialized\" />\n"
  "  </initialized>\n"
  "  <logged-in>\n"
  "    <callback module=\"test\" procedure=\"logged-in\" />\n"
  "  </logged-in>\n"
  "</game>\n";

GzochidApplicationContext *
gzochid_create_test_application_with_context (GzochidResolutionContext *context)
{
  char cwd[PATH_MAX];
  FILE *descriptor_file = fmemopen
    ((char *) TEST_DESCRIPTOR, strlen (TEST_DESCRIPTOR), "r");
  GzochidApplicationDescriptor *descriptor =
    gzochid_config_parse_application_descriptor (descriptor_file);
  GzochidApplicationContext *app_context = NULL;

  assert (getcwd (cwd, sizeof (cwd)) != NULL);    
  descriptor->deployment_root = strdup (cwd);
  gzochid_resolver_provide (context, G_OBJECT (descriptor), NULL);
  
  app_context = gzochid_resolver_require_full
    (context, GZOCHID_TYPE_APPLICATION_CONTEXT, NULL);

  g_object_unref (descriptor);  
  fclose (descriptor_file);

  return app_context;
}

GzochidApplicationContext *
gzochid_create_test_application_with_config (GzochidConfiguration *config)
{
  GzochidResolutionContext *resolution_context = g_object_new
    (GZOCHID_TYPE_RESOLUTION_CONTEXT, NULL);
  GzochidApplicationContext *app_context = NULL;
  
  gzochid_resolver_provide (resolution_context, G_OBJECT (config), NULL);
  app_context = gzochid_create_test_application_with_context
    (resolution_context);
  
  g_object_unref (resolution_context);
  return app_context;
}

GzochidApplicationContext *
gzochid_create_test_application (void)
{
  GKeyFile *key_file = g_key_file_new ();
  GzochidConfiguration *config = g_object_new
    (GZOCHID_TYPE_CONFIGURATION, "key_file", key_file, NULL);
  GzochidApplicationContext *app_context =
    gzochid_create_test_application_with_config (config);
  
  g_object_unref (config);
  g_key_file_unref (key_file);

  return app_context;
}
