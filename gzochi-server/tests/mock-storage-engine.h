/* mock-storage-engine.h: Prototypes and declarations for mock-storage-engine.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_MOCK_STORAGE_ENGINE_H
#define GZOCHID_MOCK_STORAGE_ENGINE_H

#include "gzochid-storage.h"

/* This is a "mock" storage engine interface. It wraps the "mem" storage engine
   to add functionality for retrieving handles to stores and storage 
   environments from within test code. */

extern gzochid_storage_engine_interface gzochid_mock_storage_engine_interface;

/* Initialize the static book-keeping for the mock storage engine. This function
   must be called before the API below - or any of the storage engine's 
   interface functions - can be used. */

void gzochid_mock_storage_engine_initialize (void);

/* Returns a handle to the storage environment associated with the specified
   path string, or `NULL' if no such environment was created. */

gzochid_storage_context *gzochid_mock_storage_engine_get_context (const char *);

/* Returns a handle to the store within the specified environment with the
   specified name, or `NULL' if no such store was opened. */

gzochid_storage_store *gzochid_mock_storage_engine_get_store
(gzochid_storage_context *, const char *);

#endif /* GZOCHID_MOCK_STORAGE_ENGINE_H */
