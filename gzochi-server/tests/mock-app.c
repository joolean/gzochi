/* mock-app.c: Dynamically extensible stub application context implementation.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>

#include "app.h"

struct _GzochidApplicationContext
{
  GObject parent_object;
  GHashTable *properties;
};

typedef struct _GzochidApplicationContext GzochidApplicationContext;

G_DEFINE_TYPE (GzochidApplicationContext, gzochid_application_context,
               G_TYPE_OBJECT);

extern void gzochid_mock_application_install_properties (GObjectClass *);

static void
gzochid_application_context_init (GzochidApplicationContext *context)
{
  context->properties = g_hash_table_new (g_direct_hash, g_direct_equal);
}

static void
finalize (GObject *object)
{
  GzochidApplicationContext *context = GZOCHID_APPLICATION_CONTEXT (object);
  guint i = 0, n_properties = 0;
  GParamSpec **pspecs = g_object_class_list_properties
    (G_OBJECT_GET_CLASS (context), &n_properties);
  
  while (i < n_properties)
    {
      GParamSpec *pspec = pspecs[i];
      gpointer store_value = g_hash_table_lookup
	(context->properties, g_param_spec_get_name (pspec));
      
      if (store_value != NULL)
	{
	  if (G_IS_PARAM_SPEC_BOXED (pspec))
	    g_boxed_free (G_PARAM_SPEC_VALUE_TYPE (pspec), store_value);
	  else if (G_IS_PARAM_SPEC_UINT64 (pspec))
	    free (store_value);
	}

      i++;
    }

  free (pspecs);

  g_hash_table_destroy (context->properties);

  G_OBJECT_CLASS (gzochid_application_context_parent_class)->finalize (object);
}

static void
dispose (GObject *object)
{
  GzochidApplicationContext *context = GZOCHID_APPLICATION_CONTEXT (object);
  guint i = 0, n_properties = 0;
  GParamSpec **pspecs = g_object_class_list_properties
    (G_OBJECT_GET_CLASS (context), &n_properties);
  
  while (i < n_properties)
    {
      GParamSpec *pspec = pspecs[i];
      gpointer store_value = g_hash_table_lookup
	(context->properties, g_param_spec_get_name (pspec));

      if (store_value != NULL)
	{
	  if (G_IS_PARAM_SPEC_OBJECT (pspec))
	    g_object_unref (store_value);
	}

      i++;
    }

  free (pspecs);
  
  G_OBJECT_CLASS (gzochid_application_context_parent_class)->dispose (object);
}

static void
get_property (GObject *object, guint property_id, GValue *value,
	      GParamSpec *pspec)
{
  GzochidApplicationContext *context = GZOCHID_APPLICATION_CONTEXT (object);

  if (g_hash_table_contains (context->properties,
			     g_param_spec_get_name (pspec)))
    {
      gpointer store_value = g_hash_table_lookup
	(context->properties, g_param_spec_get_name (pspec));
      
      if (G_IS_PARAM_SPEC_POINTER (pspec))
	g_value_set_pointer (value, store_value);
      else if (G_IS_PARAM_SPEC_OBJECT (pspec))
	g_value_set_object (value, store_value);
      else if (G_IS_PARAM_SPEC_BOXED (pspec))
	g_value_set_boxed (value, store_value);
      else if (G_IS_PARAM_SPEC_UINT64 (pspec))
	g_value_set_uint64 (value, *((guint64 *) store_value));
      else assert (1 == 0);
    }
}

static void
set_property (GObject *object, guint property_id, const GValue *value,
	      GParamSpec *pspec)
{
  GzochidApplicationContext *context = GZOCHID_APPLICATION_CONTEXT (object);
  gpointer store_value = NULL;
  
  if (G_IS_PARAM_SPEC_POINTER (pspec))
    store_value = g_value_get_pointer (value);
  else if (G_IS_PARAM_SPEC_OBJECT (pspec))
    store_value = g_object_ref (g_value_get_object (value));
  else if (G_IS_PARAM_SPEC_BOXED (pspec))
    store_value = g_value_dup_boxed (value);
  else if (G_IS_PARAM_SPEC_UINT64 (pspec))
    {
      guint64 val = g_value_get_uint64 (value);
      store_value = g_memdup (&val, sizeof (guint64));
    }
  else assert (1 == 0);

  g_hash_table_insert
    (context->properties, (char *) g_param_spec_get_name (pspec), store_value);
}

static void
gzochid_application_context_class_init (GzochidApplicationContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = dispose;
  object_class->finalize = finalize;
  object_class->get_property = get_property;
  object_class->set_property = set_property;

  gzochid_mock_application_install_properties (object_class);
}

static gpointer
get_optional_property (GzochidApplicationContext *app_context, const char *name)
{
  GObjectClass *object_class = G_OBJECT_GET_CLASS (app_context);  
  GParamSpec *pspec = g_object_class_find_property (object_class, name);

  if (pspec != NULL)
    {
      gpointer value = NULL;
      g_object_get (app_context, name, &value, NULL);
      return value;
    }
  else return NULL;
}

const char *
gzochid_application_context_get_name (GzochidApplicationContext *app_context)
{
  return get_optional_property (app_context, "name");
}

const char *
gzochid_application_context_get_log_domain
(GzochidApplicationContext *app_context)
{
  return get_optional_property (app_context, "log_domain");
}

GzochidMetaClient *
gzochid_application_context_get_metaclient
(GzochidApplicationContext *app_context)
{
  return get_optional_property (app_context, "metaclient");
}
