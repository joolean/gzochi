/* mock-data.c: Test-time replacements for data.c routines.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "data.h"
#include "io.h"

static guint64 next_oid;
static GHashTable *oids = NULL;
static GHashTable *oids_to_references = NULL;
static GHashTable *bindings = NULL;

static void
free_reference (gzochid_data_managed_reference *reference)
{
  if (reference->obj != NULL)
    reference->serialization->finalizer (reference->context, reference->obj);

  g_object_unref (reference->context);
  free (reference);
}

static gzochid_data_managed_reference *
create_empty_reference (GzochidApplicationContext *context, guint64 oid, 
			gzochid_io_serialization *serialization)
{
  guint64 *oid_ptr = g_memdup (&oid, sizeof (guint64));
  gzochid_data_managed_reference *reference = 
    calloc (1, sizeof (gzochid_data_managed_reference));

  reference->context = g_object_ref (context);
  reference->oid = oid;
  reference->state = GZOCHID_MANAGED_REFERENCE_STATE_EMPTY;
  reference->serialization = serialization;

  g_hash_table_insert (oids_to_references, oid_ptr, reference);
  
  return reference;
}

gzochid_data_managed_reference *
gzochid_data_create_reference_to_oid (GzochidApplicationContext *context,
				      gzochid_io_serialization *serialization,
				      guint64 oid)
{
  gzochid_data_managed_reference *ref = NULL;
  
  if (g_hash_table_contains (oids_to_references, &oid))
    ref = g_hash_table_lookup (oids_to_references, &oid);
  else ref = create_empty_reference (context, oid, serialization);

  return ref;
}

gzochid_data_managed_reference *
gzochid_data_create_reference (GzochidApplicationContext *context,
			       gzochid_io_serialization *serialization,
			       gpointer data, GError **err)
{
  guint64 oid;
  gzochid_data_managed_reference *ref = NULL;
  GList *values = g_hash_table_get_values (oids_to_references);
  GList *values_ptr = values;

  while (values_ptr != NULL)
    {
      gzochid_data_managed_reference *value_ref = values_ptr->data;

      if (value_ref->obj == data)
	{
	  ref = value_ref;
	  break;
	}
	
      values_ptr = values_ptr->next;
    }

  g_list_free (values);
  
  if (ref != NULL)
    return ref;
  
  oid = next_oid++;
      
  ref = create_empty_reference (context, oid, serialization);
  ref->obj = data;

  return ref;
}

void
gzochid_data_mark (GzochidApplicationContext *context,
		   gzochid_io_serialization *serialization, gpointer data,
		   GError **error)
{
}

gboolean
gzochid_data_binding_exists (GzochidApplicationContext *context, char *name,
			     GError **err)
{
  return g_hash_table_contains (bindings, name);
}

char *
gzochid_data_next_binding_oid (GzochidApplicationContext *context, char *name,
			       guint64 *oid_ptr, GError **err)
{
  char *closest_name = NULL;
  GList *keys = g_hash_table_get_keys (bindings);
  GList *keys_ptr = keys;

  while (keys_ptr != NULL)
    {
      char *binding = keys_ptr->data;

      if (strcmp (binding, name) > 0)
	{
	  if (closest_name == NULL || strcmp (name, closest_name) < 0)
	    closest_name = binding;
	}
      
      keys_ptr = keys_ptr->next;
    }
  
  g_list_free (keys);

  if (closest_name != NULL)
    {
      guint64 *value = g_hash_table_lookup (bindings, closest_name);
      
      *oid_ptr = *value;
      return strdup (closest_name);
    }
  else return NULL;
}

void
gzochid_data_remove_binding (GzochidApplicationContext *context, char *name,
			     GError **err)
{
  if (!g_hash_table_remove (bindings, name))
    g_set_error (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND,
		 "No data found for binding '%s'.", name);
}

void
gzochid_data_remove_object (gzochid_data_managed_reference *ref, GError **err)
{
}

void 
gzochid_data_set_binding_to_oid (GzochidApplicationContext *context, char *name,
				 guint64 oid, GError **error)
{
  g_hash_table_insert
    (bindings, strdup (name), g_memdup (&oid, sizeof (guint64)));
}

void *
gzochid_data_dereference (gzochid_data_managed_reference *reference,
			  GError **err)
{
  GByteArray *in = NULL;
  GString *data = NULL;

  if (reference->obj != NULL
      || reference->state == GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_EMPTY)
    return reference->obj;

  data = g_hash_table_lookup (oids, &reference->oid); 

  if (data == NULL)
    {
      g_set_error
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND,
	 "No data found for reference '%" G_GUINT64_FORMAT "'.",
	 reference->oid);
      return NULL;
    }
  
  in = g_byte_array_sized_new (data->len);
  g_byte_array_append (in, (const guint8 *) data->str, data->len);
  
  reference->obj = reference->serialization->deserializer 
    (reference->context, in, NULL);
  g_byte_array_unref (in);

  return reference->obj;
}

void *
gzochid_data_dereference_for_update (gzochid_data_managed_reference *reference,
				     GError **error)
{
  return gzochid_data_dereference (reference, error);
}

void
gzochid_test_mock_data_store (GzochidApplicationContext *context,
			      gzochid_io_serialization *serialization,
			      gpointer data, guint64 oid)
{
  GByteArray *out = g_byte_array_new ();
  guint64 *oid_ptr = NULL;

  oid = next_oid++;
  oid_ptr = g_memdup (&oid, sizeof (guint64));
 
  serialization->serializer (context, data, out, NULL);
  g_hash_table_insert (oids, oid_ptr, out);
}

void
gzochid_test_mock_data_clear ()
{
  g_hash_table_remove_all (oids);
  g_hash_table_remove_all (oids_to_references);
  g_hash_table_remove_all (bindings);
}

void 
gzochid_test_mock_data_initialize ()
{
  oids = g_hash_table_new_full (g_int64_hash, g_int64_equal, g_free, NULL);
  oids_to_references = g_hash_table_new_full
    (g_int64_hash, g_int64_equal, g_free, (GDestroyNotify) free_reference);
  bindings = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
}

GQuark
gzochid_data_error_quark (void)
{
  return g_quark_from_static_string ("gzochid-data-error-quark");
}
