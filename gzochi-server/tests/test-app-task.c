/* test-app-task.c: Test routines for app-task.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>

#include "app.h"
#include "app-task.h"
#include "auth_int.h"
#include "event.h"
#include "game.h"
#include "gzochid-auth.h"
#include "schedule.h"
#include "task.h"
#include "tx.h"

void
gzochid_mock_application_install_properties (GObjectClass *object_class)
{ 
  g_object_class_install_property
    (object_class, 1, g_param_spec_boxed
     ("event-source", "event-source", "event-source", G_TYPE_SOURCE,
      G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 2, g_param_spec_object
     ("task-queue", "task-queue", "task-queue", G_TYPE_OBJECT,
      G_PARAM_READWRITE));
}

static void
null_worker (GzochidApplicationContext *context,
	     gzochid_auth_identity *identity, gpointer data)
{
}

static void
test_application_task_ref ()
{
  GzochidApplicationContext *context = g_object_new
    (GZOCHID_TYPE_APPLICATION_CONTEXT, NULL);
  gzochid_auth_identity *identity = gzochid_auth_identity_new ("test");
  gzochid_application_task *task =
    gzochid_application_task_new (context, identity, null_worker, NULL, NULL);

  g_assert (task == gzochid_application_task_ref (task));

  gzochid_application_task_unref (task);
  gzochid_application_task_unref (task);
  gzochid_auth_identity_unref (identity);

  g_object_unref (context);
}

guint64
gzochid_schedule_submit_task (GzochidTaskQueue *task_queue, GzochidTask *task)
{
  task->worker (task->data, NULL);
  g_object_unref (task);
  return 0;
}

void
gzochid_schedule_execute_task (GzochidTask *task)
{
  task->worker (task->data, NULL);
}

struct _app_task_fixture
{
  int task_attempts;
  int catch_invocations;
  int cleanup_invocations;

  gzochid_application_task *success_task;
  gzochid_application_task *failure_task;
  gzochid_application_task *catch_task;
  gzochid_application_task *cleanup_task;

  GzochidApplicationContext *context;
  gzochid_auth_identity *identity;
};

typedef struct _app_task_fixture app_task_fixture;

static gboolean
prepare (gpointer data)
{
  return TRUE;
}

static void
commit (gpointer data)
{
}

static void
rollback (gpointer data)
{
}

static gzochid_transaction_participant test_participant =
  { "test-participant", prepare, commit, rollback };

static void
execute_success (GzochidApplicationContext *context,
		 gzochid_auth_identity *identity, gpointer data)
{
  app_task_fixture *fixture = data;

  fixture->task_attempts++;
}

static void
execute_failure (GzochidApplicationContext *context,
		 gzochid_auth_identity *identity, gpointer data)
{
  app_task_fixture *fixture = data;

  fixture->task_attempts++;
  gzochid_transaction_join (&test_participant, NULL);
  gzochid_transaction_mark_for_rollback (&test_participant, TRUE);
}

static void
catch (GzochidApplicationContext *context, gzochid_auth_identity *identity,
       gpointer data)
{
  app_task_fixture *fixture = data;

  fixture->catch_invocations++;
}

static void
cleanup (GzochidApplicationContext *context, gzochid_auth_identity *identity,
	 gpointer data)
{
  app_task_fixture *fixture = data;

  fixture->cleanup_invocations++;
}

static void
app_task_fixture_set_up (app_task_fixture *fixture, gconstpointer user_data)
{
  gzochid_event_source *event_source = gzochid_event_source_new ();
  GObject *mock_task_queue = g_object_new (G_TYPE_OBJECT, NULL);
  
  fixture->task_attempts = 0;
  fixture->catch_invocations = 0;
  fixture->cleanup_invocations = 0;

  fixture->context = g_object_new
    (GZOCHID_TYPE_APPLICATION_CONTEXT,
     "event-source", event_source,
     "task-queue", mock_task_queue,
     NULL);
  
  fixture->identity = gzochid_auth_identity_new ("[TEST]");
  
  fixture->success_task = gzochid_application_task_new
    (fixture->context, fixture->identity, execute_success, NULL, fixture);
  fixture->failure_task = gzochid_application_task_new
    (fixture->context, fixture->identity, execute_failure, NULL, fixture);
  fixture->catch_task = gzochid_application_task_new
    (fixture->context, fixture->identity, catch, NULL, fixture);
  fixture->cleanup_task = gzochid_application_task_new
    (fixture->context, fixture->identity, cleanup, NULL, fixture);

  g_source_unref ((GSource *) event_source);
  g_object_unref (mock_task_queue);
}

static void
app_task_fixture_tear_down (app_task_fixture *fixture, gconstpointer user_data)
{
  gzochid_application_task_free (fixture->success_task);
  gzochid_application_task_free (fixture->failure_task);
  gzochid_application_task_free (fixture->catch_task);
  gzochid_application_task_free (fixture->cleanup_task);

  gzochid_auth_identity_unref (fixture->identity);
  g_object_unref (fixture->context);
}

static void
test_task_execution_reexecute_success (app_task_fixture *fixture,
				       gconstpointer user_data)
{
  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (fixture->success_task, fixture->catch_task, fixture->cleanup_task);
  
  gzochid_application_reexecuting_transactional_task_worker
    (fixture->context, fixture->identity, execution);

  g_assert_cmpint (fixture->task_attempts, ==, 1);
  g_assert_cmpint (fixture->catch_invocations, ==, 0);
  g_assert_cmpint (fixture->cleanup_invocations, ==, 1);

  gzochid_transactional_application_task_execution_free (execution);
}

static void
test_task_execution_reexecute_failure (app_task_fixture *fixture,
				       gconstpointer user_data)
{
  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (fixture->failure_task, fixture->catch_task, fixture->cleanup_task);
  
  gzochid_application_reexecuting_transactional_task_worker
    (fixture->context, fixture->identity, execution);

  g_assert_cmpint (fixture->task_attempts, ==, 3);
  g_assert_cmpint (fixture->catch_invocations, ==, 1);
  g_assert_cmpint (fixture->cleanup_invocations, ==, 1);

  gzochid_transactional_application_task_execution_free (execution);
}

static void
test_task_execution_resubmit_success (app_task_fixture *fixture,
				      gconstpointer user_data)
{
  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (fixture->success_task, fixture->catch_task, fixture->cleanup_task);
  
  gzochid_application_resubmitting_transactional_task_worker
    (fixture->context, fixture->identity, execution);

  g_assert_cmpint (fixture->task_attempts, ==, 1);
  g_assert_cmpint (fixture->catch_invocations, ==, 0);
  g_assert_cmpint (fixture->cleanup_invocations, ==, 1);  

  gzochid_transactional_application_task_execution_free (execution);
}

static void
test_task_execution_resubmit_failure (app_task_fixture *fixture,
				      gconstpointer user_data)
{
  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (fixture->failure_task, fixture->catch_task, fixture->cleanup_task);
  
  gzochid_application_resubmitting_transactional_task_worker
    (fixture->context, fixture->identity, execution);

  g_assert_cmpint (fixture->task_attempts, ==, 3);
  g_assert_cmpint (fixture->catch_invocations, ==, 1);
  g_assert_cmpint (fixture->cleanup_invocations, ==, 1);

  gzochid_transactional_application_task_execution_free (execution);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/application-task/ref", test_application_task_ref);
  
  g_test_add ("/task-execution/reexecute/success", app_task_fixture, NULL,
	      app_task_fixture_set_up, test_task_execution_reexecute_success,
	      app_task_fixture_tear_down);
  g_test_add ("/task-execution/reexecute/failure", app_task_fixture, NULL,
	      app_task_fixture_set_up, test_task_execution_reexecute_failure,
	      app_task_fixture_tear_down);
  g_test_add ("/task-execution/resubmit/success", app_task_fixture, NULL,
	      app_task_fixture_set_up, test_task_execution_resubmit_success,
	      app_task_fixture_tear_down);
  g_test_add ("/task-execution/resubmit/failure", app_task_fixture, NULL,
	      app_task_fixture_set_up, test_task_execution_resubmit_failure,
	      app_task_fixture_tear_down);
  
  return g_test_run ();
}
