/* test-scheme-task.c: Test routines for scheme-task.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>

#include "app.h"
#include "app-factory.h"
#include "app-store.h"
#include "app-task.h"
#include "callback.h"
#include "config.h"
#include "descriptor.h"
#include "game.h"
#include "guile.h"
#include "gzochid-auth.h"
#include "oids.h"
#include "oids-storage.h"
#include "resolver.h"
#include "scheme.h"
#include "scheme-task.h"
#include "storage-mem.h"
#include "session.h"
#include "tx.h"
#include "util.h"

struct test_scheme_task_fixture
{
  GzochidResolutionContext *resolution_context;
  GzochidGameServer *game_server;
  
  GzochidApplicationContext *app_context;
  gzochid_auth_identity *identity;
};

static gboolean
ignore_warnings (const gchar *log_domain, GLogLevelFlags log_level,
		 const gchar *message, gpointer user_data)
{
  if (log_level & G_LOG_LEVEL_CRITICAL
      || log_level & G_LOG_LEVEL_WARNING)
    return FALSE;
  else return log_level & G_LOG_FLAG_FATAL;
}

static void
test_scheme_task_fixture_setup (struct test_scheme_task_fixture *fixture,
				gconstpointer user_data)
{
  GzochidResolutionContext *context = g_object_new
    (GZOCHID_TYPE_RESOLUTION_CONTEXT, NULL);
  GKeyFile *key_file = g_key_file_new ();
  GzochidConfiguration *config = g_object_new
    (GZOCHID_TYPE_CONFIGURATION, "key_file", key_file, NULL);
  
  gzochid_resolver_provide (context, G_OBJECT (config), NULL);
  
  fixture->app_context = gzochid_create_test_application_with_context (context);
  fixture->identity = gzochid_auth_identity_new ("[TEST]");
  fixture->game_server = gzochid_resolver_require_full
    (context, GZOCHID_TYPE_GAME_SERVER, NULL);  

  g_object_unref (config);
  g_key_file_unref (key_file);
  g_object_unref (context);
}

static void
test_scheme_task_fixture_teardown (struct test_scheme_task_fixture *fixture,
				   gconstpointer user_data)
{
  g_object_unref (fixture->app_context);
  g_object_unref (fixture->game_server);
  gzochid_auth_identity_unref (fixture->identity);
}

static void
persist_client_session (struct test_scheme_task_fixture *fixture,
			gzochid_client_session *session)
{
  gzochid_application_store *app_store = NULL;
  GByteArray *out = g_byte_array_new ();
  gzochid_storage_transaction *tx = NULL;

  g_object_get (fixture->app_context, "app-store", &app_store, NULL);

  tx = app_store->iface->transaction_begin (app_store->storage_context);
  
  gzochid_client_session_serialization.serializer 
    (fixture->app_context, session, out, NULL);
  
  app_store->iface->transaction_put
    (tx, app_store->oids, "1", 2, out->data, out->len);
  app_store->iface->transaction_put
    (tx, app_store->names, "s.session.1", 12, (unsigned char *) "1", 2);

  app_store->iface->transaction_prepare (tx);
  app_store->iface->transaction_commit (tx);

  g_byte_array_unref (out);
}

static void 
test_disconnected_worker_no_handler_inner (gpointer data)
{
  struct test_scheme_task_fixture *fixture = data;
  
  g_test_log_set_fatal_handler (ignore_warnings, NULL);
  
  gzochid_scheme_application_disconnected_worker
    (fixture->app_context, fixture->identity, "1");
}

static void 
test_disconnected_worker_no_handler (struct test_scheme_task_fixture *fixture,
				     gconstpointer user_data)
{
  guint64 one = gzochid_util_encode_oid (1);
  gzochid_application_store *app_store = NULL;
  gzochid_storage_transaction *tx = NULL;
  gzochid_client_session *session =
    gzochid_client_session_new (fixture->identity);
  
  persist_client_session (fixture, session);

  gzochid_transaction_execute
    (test_disconnected_worker_no_handler_inner, fixture);

  g_object_get (fixture->app_context, "app-store", &app_store, NULL);
  
  tx = app_store->iface->transaction_begin (app_store->storage_context);

  g_assert
    (app_store->iface->transaction_get
     (tx, app_store->oids, (char *) &one, sizeof (guint64), NULL) == NULL);

  app_store->iface->transaction_rollback (tx);

  gzochid_client_session_free (session);
}

static SCM 
raise_condition (SCM session)
{
  scm_throw (scm_from_locale_symbol ("test-tag"), SCM_EOL);
  return SCM_BOOL_F;
}

static SCM 
return_unspecified (SCM session)
{
  return SCM_UNSPECIFIED;
}

static gzochid_application_callback *
make_callback (char *name, GList *module)
{
  return gzochid_application_callback_new (name, module, -1);
}

static void 
test_logged_in_worker_throws_exception_inner (gpointer data)
{
  struct test_scheme_task_fixture *fixture = data;
  GzochidApplicationDescriptor *descriptor = NULL;
  
  SCM module = scm_c_resolve_module ("test");
  SCM logged_in = scm_c_make_gsubr 
    ("logged-in-exception", 1, 0, 0, raise_condition);
  GList *test_module = g_list_append (NULL, "test");
  
  scm_c_module_define (module, "logged-in-exception", logged_in);

  /* Replace the default logged-in callback. */
  
  g_object_get (fixture->app_context, "descriptor", &descriptor, NULL);
  if (descriptor->logged_in != NULL)
    gzochid_application_callback_free (descriptor->logged_in);
  
  descriptor->logged_in = make_callback ("logged-in-exception", test_module);
  g_object_unref (descriptor);
  
  gzochid_scheme_application_logged_in_worker
    (fixture->app_context, fixture->identity, "1");

  g_list_free (test_module);
}

static void 
test_logged_in_worker_throws_exception
(struct test_scheme_task_fixture *fixture, gconstpointer user_data)
{
  gzochid_client_session *session =
    gzochid_client_session_new (fixture->identity);

  persist_client_session (fixture, session);

  gzochid_transaction_execute 
    (test_logged_in_worker_throws_exception_inner, fixture);

  gzochid_client_session_free (session);
}

static void 
test_logged_in_worker_returns_unspecified_inner (gpointer data)
{
  struct test_scheme_task_fixture *fixture = data;
  GzochidApplicationDescriptor *descriptor = NULL;
  
  SCM module = scm_c_resolve_module ("test");
  SCM logged_in = scm_c_make_gsubr 
    ("logged-in-unspecified", 1, 0, 0, return_unspecified);
  GList *test_module = g_list_append (NULL, "test");

  scm_c_module_define (module, "logged-in-unspecified", logged_in);

  /* Replace the default logged-in callback. */

  g_object_get (fixture->app_context, "descriptor", &descriptor, NULL);
  if (descriptor->logged_in != NULL)
    gzochid_application_callback_free (descriptor->logged_in);
  
  descriptor->logged_in = make_callback ("logged-in-unspecified", test_module);
  g_object_unref (descriptor);

  gzochid_scheme_application_logged_in_worker
    (fixture->app_context, fixture->identity, "1");

  g_list_free (test_module);
}

static void 
test_logged_in_worker_returns_unspecified
(struct test_scheme_task_fixture *fixture, gconstpointer data)
{
  gzochid_client_session *session =
    gzochid_client_session_new (fixture->identity);
  persist_client_session (fixture, session);

  gzochid_transaction_execute 
    (test_logged_in_worker_returns_unspecified_inner, fixture);

  gzochid_client_session_free (session);
}

static void 
test_ready_throws_exception ()
{
  GzochidApplicationContext *context = gzochid_create_test_application ();
  gzochid_auth_identity *identity = gzochid_auth_identity_new ("[TEST]");
  GzochidApplicationDescriptor *descriptor = NULL;
  
  SCM module = scm_c_resolve_module ("test");
  SCM ready = scm_c_make_gsubr ("ready", 1, 0, 0, raise_condition);
  GList *test_module = g_list_append (NULL, "test");

  GError *tmp_err = NULL;

  scm_c_module_define (module, "ready", ready);

  g_object_get (context, "descriptor", &descriptor, NULL);
  descriptor->ready = make_callback ("ready", test_module);
  g_object_unref (descriptor);
  
  gzochid_scheme_application_ready (context, identity, &tmp_err);

  g_assert_error (tmp_err, GZOCHID_SCHEME_ERROR, GZOCHID_SCHEME_ERROR_FAILED);

  g_error_free (tmp_err);
  g_list_free (test_module);
  gzochid_auth_identity_unref (identity);
  g_object_unref (context);
}

void gzochid_api_channel_init () { }
void gzochid_api_data_init () { }
void gzochid_api_log_init () { }
void gzochid_api_session_init () { }
void gzochid_api_task_init () { }
void gzochid_api_tx_init () { }
void gzochid_api_util_init () { }

static void
inner_main (void *data, int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add
    ("/scheme/worker/disconnected/no-handler", struct test_scheme_task_fixture,
     NULL, test_scheme_task_fixture_setup, test_disconnected_worker_no_handler,
     test_scheme_task_fixture_teardown);
  g_test_add
    ("/scheme/worker/logged-in/exception", struct test_scheme_task_fixture,
     NULL, test_scheme_task_fixture_setup,
     test_logged_in_worker_throws_exception, test_scheme_task_fixture_teardown);
  g_test_add
    ("/scheme/worker/logged-in/unspecified", struct test_scheme_task_fixture,
     NULL, test_scheme_task_fixture_setup,
     test_logged_in_worker_returns_unspecified,
     test_scheme_task_fixture_teardown);

  g_test_add_func ("/scheme/ready/exception", test_ready_throws_exception);

  gzochid_guile_init ();
  gzochid_scheme_initialize_bindings ();
  gzochid_scheme_task_initialize_bindings ();
  exit (g_test_run ());
}

int
main (int argc, char *argv[])
{
  scm_boot_guile (argc, argv, inner_main, NULL);

  return 0;
}
