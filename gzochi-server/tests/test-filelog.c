/* test-filelog.c: Test routines for filelog.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>

#include "filelog.h"

#ifndef HAVE_FMEMOPEN
#include "fmemopen.h"
#endif /* HAVE_FMEMOPEN */

static void
test_filelog_handler ()
{
  char buf[1024] = { 0 };
  FILE *memfile = fmemopen (buf, 1024, "a");

  guint handler_id = g_log_set_handler_full
    ("testdomain",
     G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
     gzochid_file_log_handler, gzochid_file_log_context_new (memfile),
     gzochid_file_log_context_free);

  g_log ("testdomain", G_LOG_LEVEL_INFO, "Hello, world!");

  fflush (memfile);

  g_assert (g_str_has_prefix (buf, "** INFO"));
  g_assert (g_str_has_suffix (buf, "Hello, world!\n"));

  g_log_remove_handler ("testdomain", handler_id);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/filelog/handler", test_filelog_handler);
  
  return g_test_run ();
}
