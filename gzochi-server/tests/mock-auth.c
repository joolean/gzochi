/* mock-auth.c: Test-time replacements for auth.c routines.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "gzochid-auth.h"

struct _gzochid_auth_identity
{
  char *name;
  guint ref_count;

  /* Indicates whether or not this identity is reserved. (I.e., is this the 
     system identity?) */
  
  gboolean reserved; 
};

static gzochid_auth_identity system_identity = { "[SYSTEM]", 1, TRUE };

gzochid_auth_identity *
gzochid_auth_system_identity ()
{
  return &system_identity;
}

gzochid_auth_identity *
gzochid_auth_identity_new (char *name)
{
  gzochid_auth_identity *identity = calloc (1, sizeof (gzochid_auth_identity));

  identity->name = strdup (name);
  identity->ref_count = 1;

  return identity;
}

gzochid_auth_identity *
gzochid_application_context_resolve_identity
(GzochidApplicationContext *app_context, char *name)
{
  return gzochid_auth_identity_new (name);
}

gzochid_auth_identity *
gzochid_auth_identity_ref (gzochid_auth_identity *identity)
{
  if (!identity->reserved)  
    g_atomic_int_inc (&identity->ref_count);

  return identity;
}

void
gzochid_auth_identity_unref (gzochid_auth_identity *identity)
{
  if (identity->reserved)
    return;
  
  if (g_atomic_int_dec_and_test (&identity->ref_count)) {
    free (identity->name);
    free (identity);
  }
}

void 
gzochid_auth_identity_serializer 
(GzochidApplicationContext *context, void *ptr, GByteArray *out, GError **err)
{
  gzochid_auth_identity *identity = ptr;

  g_byte_array_append
    (out, (unsigned char *) identity->name, strlen (identity->name) + 1);
}

void *
gzochid_auth_identity_deserializer
(GzochidApplicationContext *context, GByteArray *in, GError **err)
{
  char *name = strndup ((char *) in->data, in->len);
  gzochid_auth_identity *identity =
    gzochid_application_context_resolve_identity (context, name);

  g_byte_array_remove_range (in, 0, strlen (name) + 1);

  free (name);

  return identity;
}

void
gzochid_auth_identity_finalizer (GzochidApplicationContext *context, void *ptr)
{
  gzochid_auth_identity_unref (ptr);
}
