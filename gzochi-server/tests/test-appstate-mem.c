/* test-appstate-mem.c: Test routines for appstate-mem.c in gzochi-metad.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <stddef.h>

#include "appstate.h"
#include "appstate-mem.h"

static void
test_appstate_mem_lookup_simple ()
{
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  g_assert_cmpint (iface->lookup_application_state (appstate, "test"), ==,
		   GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER);

  gzochi_metad_appstate_mem_free (appstate);
}

static void
test_appstate_mem_begin_resubmission_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->begin_resubmission (appstate, "test", 1, &err);
  
  g_assert_no_error (err);
  g_assert_cmpint (iface->lookup_application_state (appstate, "test"), ==,
		   GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING);
  g_assert_cmpint
    (iface->lookup_resubmission_leader (appstate, "test", NULL), ==, 1);
  
  gzochi_metad_appstate_mem_free (appstate);
}

static void
test_appstate_mem_begin_resubmission_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->begin_resubmission (appstate, "test", 1, NULL);
  iface->begin_resubmission (appstate, "test", 2, &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_ALREADY_RESUBMITTING);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);
}

static void
test_appstate_mem_cancel_resubmission_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->begin_resubmission (appstate, "test", 1, NULL);
  iface->cancel_resubmission (appstate, "test", &err);

  g_assert_no_error (err);
  g_assert_cmpint (iface->lookup_application_state (appstate, "test"), ==,
		   GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER);
  
  gzochi_metad_appstate_mem_free (appstate);
}

static void
test_appstate_mem_cancel_resubmission_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->cancel_resubmission (appstate, "test", &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_lookup_resubmission_leader_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);
  int resubmission_leader = 0;
  
  iface->begin_resubmission (appstate, "test", 1, NULL);
  resubmission_leader = iface->lookup_resubmission_leader
    (appstate, "test", &err);
  
  g_assert_no_error (err);
  g_assert_cmpint (resubmission_leader, ==, 1);
  
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_lookup_resubmission_leader_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->lookup_resubmission_leader (appstate, "test", &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_start_application_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->begin_resubmission (appstate, "test", 1, NULL);
  iface->start_application (appstate, "test", &err);

  g_assert_no_error (err);
  g_assert_cmpint (iface->lookup_application_state (appstate, "test"), ==,
		   GZOCHI_METAD_APPLICATION_STATE_STARTED);  
  
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_start_application_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->begin_resubmission (appstate, "test", 1, NULL);
  iface->start_application (appstate, "test", NULL);
  iface->start_application (appstate, "test", &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_ALREADY_STARTED);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_add_participating_node_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);
  GArray *participating_nodes = NULL;

  iface->add_participating_node (appstate, "test", 1, &err);
  participating_nodes = iface->lookup_participating_nodes (appstate, "test");
  
  g_assert_no_error (err);
  g_assert_cmpint (participating_nodes->len, ==, 1);
  g_assert_cmpint (g_array_index (participating_nodes, int, 0), ==, 1);
  
  g_array_unref (participating_nodes);  
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_add_participating_node_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->add_participating_node (appstate, "test", 1, NULL);
  iface->add_participating_node (appstate, "test", 1, &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_ALREADY_PARTICIPATING);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_remove_participating_node_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);
  GArray *participating_nodes = NULL;
  
  iface->add_participating_node (appstate, "test", 1, NULL);
  iface->remove_participating_node (appstate, "test", 1, &err);
  participating_nodes = iface->lookup_participating_nodes (appstate, "test");
  
  g_assert_no_error (err);
  g_assert_cmpint (participating_nodes->len, ==, 0);

  g_array_unref (participating_nodes);  
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_remove_participating_node_error ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);

  iface->remove_participating_node (appstate, "test", 1, &err);

  g_assert_error (err, GZOCHI_METAD_APPSTATE_ERROR,
		  GZOCHI_METAD_APPSTATE_ERROR_NOT_PARTICIPATING);

  g_error_free (err);
  gzochi_metad_appstate_mem_free (appstate);  
}

static void
test_appstate_mem_lookup_participating_nodes_simple ()
{
  GError *err = NULL;
  gzochi_metad_appstate *appstate = gzochi_metad_appstate_mem_new ();
  gzochi_metad_appstate_iface *iface = GZOCHI_METAD_APPSTATE_IFACE (appstate);
  GArray *participating_nodes = NULL;
  
  iface->add_participating_node (appstate, "test", 1, NULL);
  iface->add_participating_node (appstate, "test", 2, NULL);

  participating_nodes = iface->lookup_participating_nodes (appstate, "test");

  g_assert_cmpint (participating_nodes->len, ==, 2);
  g_assert_cmpint (g_array_index (participating_nodes, int, 0), ==, 1);
  g_assert_cmpint (g_array_index (participating_nodes, int, 1), ==, 2);

  g_array_unref (participating_nodes);  
  gzochi_metad_appstate_mem_free (appstate);  
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/appstate/mem/lookup/simple",
		   test_appstate_mem_lookup_simple);
  g_test_add_func ("/appstate/mem/begin-resubmission/simple",
		   test_appstate_mem_begin_resubmission_simple);
  g_test_add_func ("/appstate/mem/begin-resubmission/error",
		   test_appstate_mem_begin_resubmission_error);
  g_test_add_func ("/appstate/mem/cancel-resubmission/simple",
		   test_appstate_mem_cancel_resubmission_simple);
  g_test_add_func ("/appstate/mem/cancel-resubmission/error",
		   test_appstate_mem_cancel_resubmission_error);
  g_test_add_func ("/appstate/mem/lookup-resubmission-leader/simple",
		   test_appstate_mem_lookup_resubmission_leader_simple);
  g_test_add_func ("/appstate/mem/lookup-resubmission-leader/error",
		   test_appstate_mem_lookup_resubmission_leader_error);
  g_test_add_func ("/appstate/mem/start-application/simple",
		   test_appstate_mem_start_application_simple);
  g_test_add_func ("/appstate/mem/start-application/error",
		   test_appstate_mem_start_application_error);

  g_test_add_func ("/appstate/mem/add-participating-node/simple",
		   test_appstate_mem_add_participating_node_simple);
  g_test_add_func ("/appstate/mem/add-participating-node/error",
		   test_appstate_mem_add_participating_node_error);
  g_test_add_func ("/appstate/mem/remove-participating-node/simple",
		   test_appstate_mem_remove_participating_node_simple);
  g_test_add_func ("/appstate/mem/remove-participating-node/error",
		   test_appstate_mem_remove_participating_node_error);
  g_test_add_func ("/appstate/mem/lookup-participating-nodes/simple",
		   test_appstate_mem_lookup_participating_nodes_simple);
  
  return g_test_run ();
}
