/* test-taskserver.c: Tests for taskserver.c in gzochi-metad.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>

#include "resolver.h"
#include "taskserver.h"
#include "socket.h"

struct _gzochid_client_socket
{
  GByteArray *bytes_written;
};

void
gzochid_client_socket_write (gzochid_client_socket *sock, const
			     unsigned char *data, size_t len)
{
  g_byte_array_append (sock->bytes_written, data, len);
}

static gzochid_client_socket *
gzochid_test_client_socket_new (void)
{
  gzochid_client_socket *socket = malloc (sizeof (gzochid_client_socket));

  socket->bytes_written = g_byte_array_new ();
  
  return socket;
}

static void
gzochid_test_client_socket_free (gzochid_client_socket *socket)
{
  g_byte_array_unref (socket->bytes_written);
  free (socket);
}

struct _taskserver_fixture
{
  GzochiMetadTaskServer *server;
  gzochid_client_socket *client_socket;
  gzochid_client_socket *other_client_1;
  gzochid_client_socket *other_client_2; 
};

typedef struct _taskserver_fixture taskserver_fixture;

static void
setup_taskserver (taskserver_fixture *fixture, gconstpointer user_data)
{
  fixture->server = gzochid_resolver_require
    (GZOCHI_METAD_TYPE_TASK_SERVER, NULL);
  fixture->client_socket = gzochid_test_client_socket_new ();
  fixture->other_client_1 = gzochid_test_client_socket_new ();
  fixture->other_client_2 = gzochid_test_client_socket_new ();
}

static void
teardown_taskserver (taskserver_fixture *fixture, gconstpointer user_data)
{
  g_object_unref (fixture->server);
  gzochid_test_client_socket_free (fixture->client_socket);
  gzochid_test_client_socket_free (fixture->other_client_1);
  gzochid_test_client_socket_free (fixture->other_client_2);
}

static void
test_server_connected (taskserver_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, &err);
  g_assert_no_error (err);
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_ALREADY_CONNECTED);

  g_error_free (err);
}

static void
test_server_disconnected (taskserver_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  
  gzochi_metad_taskserver_server_disconnected (fixture->server, 1, &err);
  g_assert_no_error (err);

  gzochi_metad_taskserver_server_disconnected (fixture->server, 1, &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_NOT_CONNECTED);

  g_error_free (err);  
}

static inline GBytes *
byte_array_to_bytes (const GByteArray *byte_array)
{
  return g_bytes_new_static (byte_array->data, byte_array->len);
}

static void
test_server_disconnected_resubmitting (taskserver_fixture *fixture,
				       gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *expected = g_bytes_new_static ("\x00\x04\x81""foo\x00", 7);
  GBytes *actual = NULL;
    
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, NULL);

  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);
  
  gzochi_metad_taskserver_server_disconnected (fixture->server, 1, NULL);

  actual = byte_array_to_bytes (fixture->other_client_1->bytes_written);
  
  g_assert (g_bytes_equal (expected, actual));

  g_bytes_unref (expected);
  g_bytes_unref (actual);
}

static void
test_server_disconnected_started (taskserver_fixture *fixture,
				  gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *expected = g_bytes_new_static
    ("\x00\x04\x83""foo\x00"
     "\x00\x0c\x85""foo\x00\x00\x00\x00\x00\x00\x00\x00\x7b", 22);
  GBytes *actual = NULL;
    
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, NULL);
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_server_disconnected (fixture->server, 1, NULL);

  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);
  
  actual = byte_array_to_bytes (fixture->other_client_1->bytes_written);
  
  g_assert (g_bytes_equal (expected, actual));

  g_bytes_unref (expected);
  g_bytes_unref (actual);
}

static void
test_server_application_started (taskserver_fixture *fixture,
				 gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", &err);
  g_assert_no_error (err);
  
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE);

  g_error_free (err);
}

static void
test_server_application_resubmitted (taskserver_fixture *fixture,
				     gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);

  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);

  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 2, "foo", &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE);
  g_clear_error (&err);
  
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", &err);

  g_assert_no_error (err);
  
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE);

  g_error_free (err);
}

static void
test_server_task_submitted_resubmitting (taskserver_fixture *fixture,
					 gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *expected = g_bytes_new_static ("\x00\x04\x81""foo\x00", 7);
  GBytes *actual = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);

  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, &err);
  g_assert_no_error (err);

  gzochi_metad_taskserver_server_disconnected (fixture->server, 1, NULL);
  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);

  actual = byte_array_to_bytes (fixture->other_client_1->bytes_written);  
  g_assert (g_bytes_equal (expected, actual));

  g_bytes_unref (actual);
  g_bytes_unref (expected);
}

static void
test_server_task_submitted_started (taskserver_fixture *fixture,
				    gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", NULL);

  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, &err);
  g_assert_no_error (err);

  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_MAPPING);
  
  g_error_free (err);
}

static void
test_server_task_completed (taskserver_fixture *fixture,
			    gconstpointer user_data)
{
  GError *err = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", NULL);

  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);

  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, NULL);

  /* Should have no effect. */
  
  gzochi_metad_taskserver_task_completed (fixture->server, 2, "foo", 123, &err);
  g_assert_no_error (err);
  
  gzochi_metad_taskserver_task_completed (fixture->server, 1, "foo", 123, &err);
  g_assert_no_error (err); 

  gzochi_metad_taskserver_task_completed (fixture->server, 1, "foo", 123, &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_MAPPING);

  g_error_free (err);  
}

static void
test_server_task_canceled (taskserver_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *expected = g_bytes_new_static
    ("\x00\x04\x81""foo\x00"
     "\x00\x04\x83""foo\x00"
     "\x00\x0c\x87""foo\x00\x00\x00\x00\x00\x00\x00\x00\x7b", 29);
  GBytes *actual = NULL;
  
  gzochi_metad_taskserver_server_connected
    (fixture->server, 1, fixture->client_socket, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 1, "foo", NULL);
  gzochi_metad_taskserver_application_resubmitted
    (fixture->server, 1, "foo", NULL);

  gzochi_metad_taskserver_task_submitted (fixture->server, 1, "foo", 123, NULL);

  gzochi_metad_taskserver_server_connected
    (fixture->server, 2, fixture->other_client_1, NULL);
  gzochi_metad_taskserver_application_started (fixture->server, 2, "foo", NULL);
  gzochi_metad_taskserver_task_canceled (fixture->server, 2, "foo", 123, &err);

  g_assert_no_error (err);
  
  actual = byte_array_to_bytes (fixture->client_socket->bytes_written);  
  g_assert (g_bytes_equal (expected, actual));
  
  gzochi_metad_taskserver_task_canceled (fixture->server, 2, "foo", 123, &err);
  g_assert_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
		  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_MAPPING);

  g_error_free (err);

  g_bytes_unref (expected);
  g_bytes_unref (actual);
}

int
main (int argc, char *argv[])
{
#if GLIB_CHECK_VERSION (2, 36, 0)
  /* No need for `g_type_init'. */
#else
  g_type_init ();
#endif /* GLIB_CHECK_VERSION */

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/taskserver/server-connected", taskserver_fixture, NULL,
	      setup_taskserver, test_server_connected, teardown_taskserver);
  g_test_add ("/taskserver/server-disconnected", taskserver_fixture, NULL,
	      setup_taskserver, test_server_disconnected, teardown_taskserver);
  g_test_add ("/taskserver/server-disconnected/resubmitting",
	      taskserver_fixture, NULL, setup_taskserver,
	      test_server_disconnected_resubmitting, teardown_taskserver);
  g_test_add ("/taskserver/server-disconnected/started", taskserver_fixture,
	      NULL, setup_taskserver, test_server_disconnected_started,
	      teardown_taskserver);
  g_test_add ("/taskserver/application-started", taskserver_fixture, NULL,
	      setup_taskserver, test_server_application_started,
	      teardown_taskserver);
  g_test_add ("/taskserver/application-resubmitted", taskserver_fixture, NULL,
	      setup_taskserver, test_server_application_resubmitted,
	      teardown_taskserver);
  g_test_add ("/taskserver/task-submitted/resubmitting", taskserver_fixture,
	      NULL, setup_taskserver, test_server_task_submitted_resubmitting,
	      teardown_taskserver);
  g_test_add ("/taskserver/task-submitted/started", taskserver_fixture,
	      NULL, setup_taskserver, test_server_task_submitted_started,
	      teardown_taskserver);
  g_test_add ("/taskserver/task-completed", taskserver_fixture, NULL,
	      setup_taskserver, test_server_task_completed,
	      teardown_taskserver);
  g_test_add ("/taskserver/task-canceled", taskserver_fixture, NULL,
	      setup_taskserver, test_server_task_canceled, teardown_taskserver);
  
  return g_test_run ();
}
