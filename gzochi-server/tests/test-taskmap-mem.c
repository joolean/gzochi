/* test-taskmap-mem.c: Test routines for taskmap-mem.c in gzochi-metad.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <stddef.h>

#include "taskmap.h"
#include "taskmap-mem.h"

static void
test_taskmap_mem_map_simple ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  
  iface->map_task (taskmap, "foo", 1, 1, &err);
  g_assert_no_error (err);
  
  iface->map_task (taskmap, "bar", 1, 1, &err);
  g_assert_no_error (err);  
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_map_previously_unmapped ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  GArray *unmapped_tasks = NULL;
  
  iface->map_task (taskmap, "foo", 1, 1, NULL);
  iface->unmap_task (taskmap, "foo", 1, NULL);
  iface->map_task (taskmap, "foo", 1, 2, &err);

  g_assert_no_error (err);

  unmapped_tasks = iface->lookup_unmapped_tasks (taskmap, "foo");
  g_assert_cmpint (unmapped_tasks->len, ==, 0);

  g_array_unref (unmapped_tasks);
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_map_error ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);

  iface->map_task (taskmap, "foo", 1, 1, &err);
  g_assert_no_error (err);
  iface->map_task (taskmap, "foo", 1, 2, &err);
  g_assert_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		  GZOCHI_METAD_TASKMAP_ERROR_ALREADY_MAPPED);
  g_error_free (err);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_unmap_simple ()
{
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  GArray *mapped_task_ids = NULL;
  GArray *unmapped_task_ids = NULL;
  
  iface->map_task (taskmap, "foo", 1, 1, NULL);
  iface->unmap_task (taskmap, "foo", 1, NULL);

  mapped_task_ids = iface->lookup_mapped_tasks (taskmap, "foo", 1);
  g_assert_cmpint (mapped_task_ids->len, ==, 0);

  unmapped_task_ids = iface->lookup_unmapped_tasks (taskmap, "foo");

  g_assert_cmpint (unmapped_task_ids->len, ==, 1);
  g_assert_cmpint (g_array_index (unmapped_task_ids, guint64, 0), ==, 1);
  
  g_array_unref (mapped_task_ids);
  g_array_unref (unmapped_task_ids);
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_unmap_error ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);

  iface->unmap_task (taskmap, "foo", 1, &err);
  g_assert_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		  GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED);
  g_error_free (err);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_remove_simple ()
{
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  GArray *mapped_task_ids = NULL;
  GArray *unmapped_task_ids = NULL;
  
  iface->map_task (taskmap, "foo", 1, 1, NULL);
  iface->remove_task (taskmap, "foo", 1, NULL);

  mapped_task_ids = iface->lookup_mapped_tasks (taskmap, "foo", 1);
  g_assert_cmpint (mapped_task_ids->len, ==, 0);

  unmapped_task_ids = iface->lookup_unmapped_tasks (taskmap, "foo");

  g_assert_cmpint (unmapped_task_ids->len, ==, 0);
  
  g_array_unref (mapped_task_ids);
  g_array_unref (unmapped_task_ids);
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_remove_error ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);

  iface->remove_task (taskmap, "foo", 1, &err);
  g_assert_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		  GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED);
  g_error_free (err);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_lookup_node_simple ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  
  iface->map_task (taskmap, "foo", 1, 2, NULL);

  g_assert_cmpint (iface->lookup_node (taskmap, "foo", 1, &err), ==, 2);
  g_assert_no_error (err);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_lookup_node_error ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  
  g_assert_cmpint (iface->lookup_node (taskmap, "foo", 1, &err), ==, -1);
  g_assert_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		  GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED);
  g_error_free (err);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_lookup_mapped_tasks_simple ()
{
  GArray *task_ids = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  
  iface->map_task (taskmap, "foo", 1, 2, NULL);

  task_ids = iface->lookup_mapped_tasks (taskmap, "foo", 2);

  g_assert_cmpint (task_ids->len, ==, 1);
  g_assert_cmpint (g_array_index (task_ids, guint64, 0), ==, 1);

  g_array_unref (task_ids);  
  gzochi_metad_taskmap_mem_free (taskmap);
}

static void
test_taskmap_mem_unmap_all_simple ()
{
  GError *err = NULL;
  gzochi_metad_taskmap *taskmap = gzochi_metad_taskmap_mem_new ();
  gzochi_metad_taskmap_iface *iface = GZOCHI_METAD_TASKMAP_IFACE (taskmap);
  GArray *foo_tasks = NULL;
  GArray *bar_tasks = NULL;
  GArray *unmapped_foo_tasks = NULL;
  GArray *unmapped_bar_tasks = NULL;
  
  iface->map_task (taskmap, "foo", 1, 1, NULL);
  iface->map_task (taskmap, "foo", 2, 1, NULL);
  iface->map_task (taskmap, "bar", 1, 1, NULL);

  iface->unmap_all (taskmap, 1);

  foo_tasks = iface->lookup_mapped_tasks (taskmap, "foo", 1);
  bar_tasks = iface->lookup_mapped_tasks (taskmap, "bar", 1);

  g_assert_cmpint (foo_tasks->len, ==, 0);
  g_assert_cmpint (bar_tasks->len, ==, 0);

  unmapped_foo_tasks = iface->lookup_unmapped_tasks (taskmap, "foo");
  unmapped_bar_tasks = iface->lookup_unmapped_tasks (taskmap, "bar");

  g_assert_cmpint (unmapped_foo_tasks->len, ==, 2);
  g_assert_cmpint (g_array_index (unmapped_foo_tasks, guint64, 0), ==, 1);
  g_assert_cmpint (g_array_index (unmapped_foo_tasks, guint64, 1), ==, 2);
  g_assert_cmpint (unmapped_bar_tasks->len, ==, 1);
  g_assert_cmpint (g_array_index (unmapped_bar_tasks, guint64, 0), ==, 1);
  
  g_array_unref (foo_tasks);
  g_array_unref (bar_tasks);
  g_array_unref (unmapped_foo_tasks);
  g_array_unref (unmapped_bar_tasks);
  
  gzochi_metad_taskmap_mem_free (taskmap);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/taskmap/mem/map/simple", test_taskmap_mem_map_simple);
  g_test_add_func ("/taskmap/mem/map/previously-unmapped",
		   test_taskmap_mem_map_previously_unmapped);
  g_test_add_func ("/taskmap/mem/map/error", test_taskmap_mem_map_error);
  g_test_add_func ("/taskmap/mem/unmap/simple", test_taskmap_mem_unmap_simple);
  g_test_add_func ("/taskmap/mem/unmap/error", test_taskmap_mem_unmap_error);
  g_test_add_func ("/taskmap/mem/remove/simple",
		   test_taskmap_mem_remove_simple);
  g_test_add_func ("/taskmap/mem/remove/error", test_taskmap_mem_remove_error);
  g_test_add_func
    ("/taskmap/mem/lookup-node/simple", test_taskmap_mem_lookup_node_simple);
  g_test_add_func
    ("/taskmap/mem/lookup-node/error", test_taskmap_mem_lookup_node_error);
  g_test_add_func ("/taskmap/mem/lookup-mapped-tasks/simple",
		   test_taskmap_mem_lookup_mapped_tasks_simple);
  g_test_add_func
    ("/taskmap/mem/unmap-all/simple", test_taskmap_mem_unmap_all_simple);
    
  return g_test_run ();
}
