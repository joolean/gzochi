/* test-seqhash.c: Test routines for seqhash.c in gzochid.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "seqhash.h"

static void
test_seq_hash_table_is_empty ()
{
  gzochid_seq_hash_table *seq_hash_table = gzochid_seq_hash_table_new
    ((GCompareFunc) strcmp, g_str_hash, g_str_equal, NULL, NULL);

  g_assert_true (gzochid_seq_hash_table_is_empty (seq_hash_table));
  gzochid_seq_hash_table_insert (seq_hash_table, "foo", "bar");
  g_assert_false (gzochid_seq_hash_table_is_empty (seq_hash_table));
  
  gzochid_seq_hash_table_free (seq_hash_table);
}

static void
test_seq_hash_table_first_key ()
{
  gzochid_seq_hash_table *seq_hash_table = gzochid_seq_hash_table_new
    ((GCompareFunc) strcmp, g_str_hash, g_str_equal, NULL, NULL);

  g_assert_null (gzochid_seq_hash_table_first_key (seq_hash_table));
 
  gzochid_seq_hash_table_insert (seq_hash_table, "foo", "bar");
  gzochid_seq_hash_table_insert (seq_hash_table, "bar", "baz");
  gzochid_seq_hash_table_insert (seq_hash_table, "baz", "quux");

  g_assert_cmpstr
    (gzochid_seq_hash_table_first_key (seq_hash_table), ==, "bar");
  
  gzochid_seq_hash_table_free (seq_hash_table);
}

static void
test_seq_hash_table_lookup ()
{
  gzochid_seq_hash_table *seq_hash_table = gzochid_seq_hash_table_new
    ((GCompareFunc) strcmp, g_str_hash, g_str_equal, NULL, NULL);

  g_assert_null (gzochid_seq_hash_table_lookup (seq_hash_table, "foo"));
  
  gzochid_seq_hash_table_insert (seq_hash_table, "foo", "bar");
  gzochid_seq_hash_table_insert (seq_hash_table, "bar", "baz");

  g_assert_cmpstr
    (gzochid_seq_hash_table_lookup (seq_hash_table, "foo"), ==, "bar");
  g_assert_cmpstr
    (gzochid_seq_hash_table_lookup (seq_hash_table, "bar"), ==, "baz");
  
  gzochid_seq_hash_table_free (seq_hash_table);
}

static void
test_seq_hash_table_insert ()
{
  gzochid_seq_hash_table *seq_hash_table = gzochid_seq_hash_table_new
    ((GCompareFunc) strcmp, g_str_hash, g_str_equal, NULL, NULL);

  g_assert_true (gzochid_seq_hash_table_insert (seq_hash_table, "foo", "bar"));
  g_assert_false (gzochid_seq_hash_table_insert (seq_hash_table, "foo", "baz"));
  g_assert_false (gzochid_seq_hash_table_insert
		  (seq_hash_table, "foo", "quux"));

  g_assert_cmpstr
    (gzochid_seq_hash_table_lookup (seq_hash_table, "foo"), ==, "quux");
  
  gzochid_seq_hash_table_free (seq_hash_table);
}

static void
test_seq_hash_table_remove ()
{
  gzochid_seq_hash_table *seq_hash_table = gzochid_seq_hash_table_new
    ((GCompareFunc) strcmp, g_str_hash, g_str_equal, NULL, NULL);

  gzochid_seq_hash_table_insert (seq_hash_table, "foo", "bar");
  gzochid_seq_hash_table_insert (seq_hash_table, "bar", "baz");
  gzochid_seq_hash_table_insert (seq_hash_table, "baz", "quux");

  g_assert_true (gzochid_seq_hash_table_remove (seq_hash_table, "bar"));
  g_assert_false (gzochid_seq_hash_table_remove (seq_hash_table, "bar"));

  g_assert_cmpstr
    (gzochid_seq_hash_table_first_key (seq_hash_table), ==, "baz");
  
  gzochid_seq_hash_table_free (seq_hash_table);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/seq-hash-table/is-empty", test_seq_hash_table_is_empty);
  g_test_add_func ("/seq-hash-table/first-key", test_seq_hash_table_first_key);
  g_test_add_func ("/seq-hash-table/lookup", test_seq_hash_table_lookup);
  g_test_add_func ("/seq-hash-table/insert", test_seq_hash_table_insert);
  g_test_add_func ("/seq-hash-table/remove", test_seq_hash_table_remove);
    
  return g_test_run ();
}
