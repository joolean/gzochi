/* taskserver.h: Prototypes and declarations for taskserver.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHI_METAD_TASK_SERVER_H
#define GZOCHI_METAD_TASK_SERVER_H

#include <glib.h>
#include <glib-object.h>

#include "socket.h"

/* The core task server type definitions. */

#define GZOCHI_METAD_TYPE_TASK_SERVER gzochi_metad_task_server_get_type ()

G_DECLARE_FINAL_TYPE (GzochiMetadTaskServer, gzochi_metad_task_server,
		      GZOCHI_METAD, TASK_SERVER, GObject);

enum
  {
    /* Indicates a failure to service a request because the application server
       node is already connected per the task server's internal state. */
    
    GZOCHI_METAD_TASKSERVER_ERROR_ALREADY_CONNECTED,

    /* Indicates a failure to service a request because the application server
       node is not connected per the task server's internal state. */

    GZOCHI_METAD_TASKSERVER_ERROR_NOT_CONNECTED,

    /* Indicates a failure to service a request because the disposition of the
       target task per the task server's internal state is not consistent with 
       requested operation - e.g., it is not mapped, or it is mapped to a
       different application server node. */

    GZOCHI_METAD_TASKSERVER_ERROR_WRONG_MAPPING,
    
    /* Indicates a failure to service a request because the target application 
       is not in the right lifecycle state. */

    GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE,

    /* Generic task server failure. */
    
    GZOCHI_METAD_TASKSERVER_ERROR_FAILED 
  };

#define GZOCHI_METAD_TASKSERVER_ERROR gzochi_metad_taskserver_error_quark ()

GQuark gzochi_metad_taskserver_error_quark (void);

/*
  Notifies the task server that the specified application server node is ready
  to begin executing tasks for the specified application.

  If the target application has not begun (or completed) the task resubmission
  phase of its lifecycle, the task server will respond by electing the specified
  node to resubmit all durable tasks.

  Signals an error if the specified node has already requested to start the
  specified application.
*/

void gzochi_metad_taskserver_application_started (GzochiMetadTaskServer *,
						  int, const char *, GError **);

/*
  Notifies the task server that the specified application server node (which
  must have previously been elected the task resubmission leader) has completed
  task resubmission for the specified application.

  Signals an error if the specified node is not the task resubmission leader, or
  if the application is not in the task resubmission phase of its lifecycle.
*/

void gzochi_metad_taskserver_application_resubmitted (GzochiMetadTaskServer *,
						      int, const char *,
						      GError **);

/*
  Notifies the task server that the specified gzochi application server node
  (identified by its node id and `gzochid_client_socket') has connected to the
  meta server. 
  
  An error is signaled if a server with the specified node id is already 
  connected. 
*/

void gzochi_metad_taskserver_server_connected (GzochiMetadTaskServer *,
					       int, gzochid_client_socket *,
					       GError **);

/*
  Notifies the task server that the specified gzochi application server node
  (identified by its node id) has disconnected from the meta server. 

  If the specified node was acting as the task resubmission leader for any
  applications, the task server will attempt to elect a new resubmission leader
  for each application. If this cannot be done for some application, that
  application will wait for another node to attempt to start the application.
  Any tasks mapped to the specified node will be mapped to other participating
  applications or go into an "unmapped" state.
  
  An error is signaled if a server the specified node id is not currently 
  connected. 
*/

void gzochi_metad_taskserver_server_disconnected (GzochiMetadTaskServer *,
						  int, GError **);

/*
  Notifies the task server that the specified task has been submitted to the
  specified application server node.

  Signals an error if the task cannot be mapped to the target node, e.g., 
  because it is already mapped.
*/

void gzochi_metad_taskserver_task_submitted (GzochiMetadTaskServer *, int,
					     const char *, guint64, GError **);

/*
  Notifies the task server that the specified task has been completed on the
  specified application server node and can be unmapped and removed.

  Signals an error if the task cannot be unmapped to the target node, e.g.,
  because it does not exist, or because it did not complete on the node it was
  mapped to.
*/

void gzochi_metad_taskserver_task_completed (GzochiMetadTaskServer *, int,
					     const char *, guint64, GError **);

/*
  Notifies the task server that the specified task has been canceled by the
  specified application server node and should be unmapped from the node to 
  which it is currently mapped and removed.

  Signals an error if the task cannot be unmapped, e.g., because it does not 
  exist.
*/

void gzochi_metad_taskserver_task_canceled (GzochiMetadTaskServer *, int,
					    const char *, guint64, GError **);

#endif /* GZOCHI_METAD_TASK_SERVER_H */
