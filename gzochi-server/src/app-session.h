/* app-session.h: Prototypes and declarations for app-session.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_APPLICATION_SESSION_MAP_H
#define GZOCHID_APPLICATION_SESSION_MAP_H

#include <glib.h>

/* The application session map struct encapsulates the aspects of an 
   application's state related to the set of locally connected sessions,
   including synchronization primitives to support safe, concurrent 
   modifications. */

struct _gzochid_application_session_map
{
  GMutex client_mapping_lock; /* Protects the session oid mappings. */

  /* A mapping of session oid pointer to `gzochid_game_client *'. */
  
  GHashTable *oids_to_clients;

  /* A mapping of `gzochid_game_client *' to session oid pointer. */

  GHashTable *clients_to_oids;

  GMutex channel_mapping_lock; /* Protects the channel oid mapping. */

  /* A mapping of channel oid strings to `GSequences' of session oid strings.*/

  GHashTable *channel_oids_to_local_session_oids; 
};

typedef struct _gzochid_application_session_map gzochid_application_session_map;

/* Construct and return a pointer to a new, empty 
   `gzochid_application_session_map' structure. The resources used by the map
   should be released via `gzochid_application_session_map_free' when no longer
   needed. */

gzochid_application_session_map *gzochid_application_session_map_new ();

/* Frees the memory associated with the specified 
   `gzochid_application_session_map'. */

void gzochid_application_session_map_free (gzochid_application_session_map *);

#endif /* GZOCHID_APPLICATION_SESSION_MAP_H */
