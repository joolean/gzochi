/* stats.h: Prototypes and declarations for stats.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_STATS_H
#define GZOCHID_STATS_H

#include "event.h"

#define GZOCHID_TYPE_APPLICATION_STATS gzochid_application_stats_get_type ()

GType gzochid_application_stats_get_type (void);

/* A boxed type for keeping track of useful application metrics. The contents of
   this data structure are updated in response to application events, and are
   read primarily by the rendering code of the HTTP administrative console. */

struct _GzochidApplicationStats
{
  /* Number of messages received from locally-connected application clients. */

  unsigned int num_messages_received;

  /* Number of messages sent to locally-connected application clients, including
     channel messages. */
  
  unsigned int num_messages_sent;

  /* Number of application transactions started. */
  
  unsigned int num_transactions_started;

  /* Number of application transactions committed. */
  
  unsigned int num_transactions_committed;

  /* Number of application transactions rolled back. */
  
  unsigned int num_transactions_rolled_back;

  /* The maximum transaction duration (in milliseconds) across all 
     successfully-committed application transactions. */
  
  unsigned long max_transaction_duration;

  /* The minimum transaction duration (in milliseconds) across all 
     successfully-committed application transactions. */
  
  unsigned long min_transaction_duration;

  /* The average transaction duration (in milliseconds) across all 
     successfully-committed application transactions. */
  
  double average_transaction_duration;

  /* The total number of bytes retrieved from the application's local object 
     store. */
  
  unsigned long bytes_read;

  /* The total number of bytes written to the application's local object 
     store. */

  unsigned long bytes_written;

  /* The total number of durable tasks submitted locally for the application. */
  
  unsigned long tasks_submitted;

  /* The total number of durable tasks for the application assigned to the
     local server. */
  
  unsigned long tasks_assigned;

  /* The total number of durable tasks completed locally for the application. */
  
  unsigned long tasks_completed;

  /* The total number of durable tasks for the application unassigned from the
     local server. */
  
  unsigned long tasks_unassigned;
};

typedef struct _GzochidApplicationStats GzochidApplicationStats;

/* Updates the state of the specified stats object based on the specified event.
   Intended for use as part of an event handler implementation via
   `gzochid_event_attach'. */

void gzochid_stats_update_from_event (GzochidApplicationStats *,
				      GzochidEvent *);

#endif /* GZOCHID_STATS_H */
