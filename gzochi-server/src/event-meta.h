/* event-meta.h: Prototypes and declarations for event-meta.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHI_METAD_EVENT_H
#define GZOCHI_METAD_EVENT_H

#include <glib-object.h>

#include "event.h"

/* Enumeration of client event types. */

enum _gzochi_metad_client_event_type
  {
    /* A gzochid application node has connected to the meta server. The 
       `connection-description' and (conditionally) the `admin-server-base-url'
       properties will be set. */
    
    CLIENT_CONNECTED,

    /* A gzochid application node has disconnected from the meta server. The
       `node-id' property can be used to identify the client. */
    
    CLIENT_DISCONNECTED
  };

typedef enum _gzochi_metad_client_event_type gzochi_metad_client_event_type;

/*
  The client event sub-type. The following properties are available:
  
  node-id: meta server-assigned id for the connected client
  connection-description: string giving the address of the client
  admin-server-base-url: string giving the URL of the client's admin web 
    console, if available; `NULL' otherwise  
*/

/* The core client event type definitions. */

#define GZOCHI_METAD_TYPE_CLIENT_EVENT gzochi_metad_client_event_get_type ()

G_DECLARE_FINAL_TYPE (GzochiMetadClientEvent, gzochi_metad_client_event,
		      GZOCHI_METAD, CLIENT_EVENT, GzochidEvent);

/* Enumeration of session event types. */

enum _gzochi_metad_session_event_type
  {
    /* A client session has been established on a gzochid application node. */
    
    SESSION_CONNECTED,

    /* A client session has disconnected from a gzochid application node. */
    
    SESSION_DISCONNECTED
  };

typedef enum _gzochi_metad_session_event_type gzochi_metad_session_event_type;

/*
  The session event sub-type. The following properties are available:
  
  application: the name of the associated gzochi game application
  node-id: meta server-assigned id for the target application node
*/

/* The core session event type definitions. */

#define GZOCHI_METAD_TYPE_SESSION_EVENT gzochi_metad_session_event_get_type ()

G_DECLARE_FINAL_TYPE (GzochiMetadSessionEvent, gzochi_metad_session_event,
		      GZOCHI_METAD, SESSION_EVENT, GzochidEvent);

/* Enumeration of task event types. */

enum _gzochi_metad_task_event_type
  {
    TASK_ASSIGNED, /* A task has been assigned to a gzochid application node. */
    TASK_CANCELED, /* A task has been canceled on a gzochid application node. */
    
    /* A task has been completed by a gzochid application node. */

    TASK_COMPLETED,

    /* A task has been submitted to the meta server from a gzochid application 
       node. */
    
    TASK_SUBMITTED,

    /* A task has been unassigned from a gzochid application node. */
    
    TASK_UNASSIGNED
  };

typedef enum _gzochi_metad_task_event_type gzochi_metad_task_event_type;

/*
  The task event sub-type. The following properties are available:
  
  application: the name of the associated gzochi game application
  node-id: meta server-assigned id for the target application node
*/

/* The core task event type definitions. */

#define GZOCHI_METAD_TYPE_TASK_EVENT gzochi_metad_task_event_get_type ()

G_DECLARE_FINAL_TYPE (GzochiMetadTaskEvent, gzochi_metad_task_event,
		      GZOCHI_METAD, TASK_EVENT, GzochidEvent);

#endif /* GZOCHI_METAD_EVENT_H */
