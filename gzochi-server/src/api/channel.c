/* channel.c: Primitive functions for user-facing gzochid channel API
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <libguile.h>
#include <stddef.h>

#include "../app.h"
#include "../channel.h"
#include "../data.h"
#include "../scheme.h"
#include "../session.h"

#include "channel.h"
#include "util.h"

SCM
primitive_create_channel (SCM name)
{
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  char *cname = scm_to_locale_string (name);
  gzochid_channel *channel = gzochid_channel_create (context, cname);  
  gzochid_data_managed_reference *scm_reference = NULL;
  SCM ret = SCM_BOOL_F;

  /* `gzochid_channel_create' may return `NULL' if the transaction has gotten
     into a bad state. */
  
  if (channel != NULL)
    {
      guint64 scm_oid = gzochid_channel_scm_oid (channel);
  
      scm_reference = gzochid_data_create_reference_to_oid 
	(context, &gzochid_scheme_data_serialization, scm_oid);
  
      gzochid_data_dereference (scm_reference, NULL);      
      ret = scm_reference->obj;
    }

  free (cname);
  gzochid_api_check_transaction ();
  return ret;
}

SCM
primitive_get_channel (SCM name)
{
  GError *err = NULL;
  GzochidApplicationContext *context = 
    gzochid_api_ensure_current_application_context ();
  char *cname = scm_to_locale_string (name);
  gzochid_channel *channel = gzochid_channel_get (context, cname);
  SCM ret = SCM_BOOL_F;
  
  free (cname);
  if (channel != NULL)
    {
      guint64 scm_oid = gzochid_channel_scm_oid (channel);
      gzochid_data_managed_reference *scm_reference =
	gzochid_data_create_reference_to_oid 
	(context, &gzochid_scheme_data_serialization, scm_oid);

      gzochid_data_dereference (scm_reference, &err);

      if (err == NULL)
	ret = scm_reference->obj;
      else gzochid_api_check_not_found (err);
    }

  gzochid_api_check_transaction ();

  return ret;
}

SCM
primitive_join_channel (SCM channel, SCM session)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  guint64 channel_oid = gzochid_scheme_channel_oid (channel);
  guint64 session_oid = gzochid_scheme_client_session_oid (session);
  gzochid_data_managed_reference *channel_reference =
    gzochid_data_create_reference_to_oid 
    (context, &gzochid_channel_serialization, channel_oid);
  gzochid_data_managed_reference *session_reference =
    gzochid_data_create_reference_to_oid
    (context, &gzochid_client_session_serialization, session_oid);

  gzochid_data_dereference (channel_reference, &err);
  if (err == NULL)
    {
      gzochid_data_dereference (session_reference, &err);

      if (err == NULL)
	gzochid_channel_join 
	  (context, channel_reference->obj, session_reference->obj);
      else gzochid_api_check_not_found (err);
    }
  else gzochid_api_check_not_found (err);
  
  gzochid_api_check_transaction ();

  return SCM_UNSPECIFIED;
}

SCM
primitive_leave_channel (SCM channel, SCM session)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  guint64 channel_oid = gzochid_scheme_channel_oid (channel);
  guint64 session_oid = gzochid_scheme_client_session_oid (session);  
  gzochid_data_managed_reference *channel_reference =
    gzochid_data_create_reference_to_oid 
    (context, &gzochid_channel_serialization, channel_oid);
  gzochid_data_managed_reference *session_reference =
    gzochid_data_create_reference_to_oid
    (context, &gzochid_client_session_serialization, session_oid);
  
  gzochid_data_dereference (channel_reference, &err);
  
  if (err == NULL)
    {
      gzochid_data_dereference (session_reference, &err);
      if (err == NULL)
	gzochid_channel_leave
	  (context, channel_reference->obj, session_reference->obj);
      else gzochid_api_check_not_found (err);
    }
  else gzochid_api_check_not_found (err);
  
  gzochid_api_check_transaction ();

  return SCM_UNSPECIFIED;
}

SCM
primitive_send_channel_message (SCM channel, SCM bv)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();

  short len = (short) SCM_BYTEVECTOR_LENGTH (bv);
  unsigned char *msg = (unsigned char *) SCM_BYTEVECTOR_CONTENTS (bv);

  guint64 channel_oid = gzochid_scheme_channel_oid (channel);
  gzochid_data_managed_reference *channel_reference =
    gzochid_data_create_reference_to_oid 
    (context, &gzochid_channel_serialization, channel_oid);

  gzochid_data_dereference (channel_reference, &err);

  if (err == NULL)
    gzochid_channel_send (context, channel_reference->obj, msg, len);
  else gzochid_api_check_not_found (err);

  gzochid_api_check_transaction ();

  return SCM_UNSPECIFIED;
}

SCM
primitive_close_channel (SCM channel, SCM msg)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  guint64 channel_oid = gzochid_scheme_channel_oid (channel);  
  gzochid_data_managed_reference *channel_reference =
    gzochid_data_create_reference_to_oid 
    (context, &gzochid_channel_serialization, channel_oid);

  gzochid_data_dereference (channel_reference, &err);

  if (err == NULL)
    gzochid_channel_close (context, channel_reference->obj);
  else gzochid_api_check_not_found (err);

  gzochid_api_check_transaction ();

  return SCM_UNSPECIFIED;
}

void gzochid_api_channel_init (void)
{
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-create-channel!"),
     scm_c_make_gsubr ("primitive-create-channel", 1, 0, 0,
		       primitive_create_channel));

  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-get-channel!"),
     scm_c_make_gsubr ("primitive-get-channel", 1, 0, 0,
		       primitive_get_channel));
  
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-join-channel!"),
     scm_c_make_gsubr ("primitive-join-channel", 2, 0, 0,
		       primitive_join_channel));
  
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-leave-channel!"),
     scm_c_make_gsubr ("primitive-leave-channel", 2, 0, 0,
		       primitive_leave_channel));
  
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-send-channel-message!"),
     scm_c_make_gsubr ("primitive-send-channel-message", 2, 0, 0,
		       primitive_send_channel_message));
  
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-create-channel!"),
     scm_c_make_gsubr ("primitive-create-channel", 1, 0, 0,
		       primitive_create_channel));
  
  scm_call_1
    (scm_c_public_ref
     ("gzochi private channel", "gzochi:set-primitive-close-channel!"),
     scm_c_make_gsubr ("primitive-close-channel", 1, 0, 0,
		       primitive_close_channel));
}
