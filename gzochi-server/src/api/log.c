/* log.c: Primitive functions for user-facing gzochid transactional log API
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libguile.h>
#include <string.h>
#include <syslog.h>

#include "../app.h"
#include "../scheme.h"
#include "../txlog.h"

#include "util.h"

SCM
primitive_log_internal (SCM priority, SCM msg)
{
  char *cpriority = scm_to_locale_string (scm_symbol_to_string (priority));
  char *cmsg = scm_to_locale_string (msg);

  if (strcmp (cpriority, "err") == 0)
    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_CRITICAL, "%s", cmsg);
  else if (strcmp (cpriority, "warning") == 0)
    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, "%s", cmsg);
  else if (strcmp (cpriority, "notice") == 0)
    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, "%s", cmsg);
  else if (strcmp (cpriority, "info") == 0)
    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_INFO, "%s", cmsg);
  else if (strcmp (cpriority, "debug") == 0)
    g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s", cmsg);

  free (cpriority);
  free (cmsg);

  return SCM_UNSPECIFIED;
}

SCM
primitive_log (SCM priority, SCM msg)
{
  GzochidApplicationContext *context = 
    gzochid_api_ensure_current_application_context ();
  char *cpriority = scm_to_locale_string (scm_symbol_to_string (priority));
  char *cmsg = scm_to_locale_string (msg);

  if (strcmp (cpriority, "err") == 0)
    gzochid_tx_log (context, G_LOG_LEVEL_CRITICAL, cmsg);
  else if (strcmp (cpriority, "warning") == 0)
    gzochid_tx_log (context, G_LOG_LEVEL_WARNING, cmsg);
  else if (strcmp (cpriority, "notice") == 0)
    gzochid_tx_log (context, G_LOG_LEVEL_MESSAGE, cmsg);
  else if (strcmp (cpriority, "info") == 0)
    gzochid_tx_log (context, G_LOG_LEVEL_INFO, cmsg);
  else if (strcmp (cpriority, "debug") == 0)
    gzochid_tx_log (context, G_LOG_LEVEL_DEBUG, cmsg);

  free (cpriority);
  free (cmsg);

  gzochid_api_check_transaction ();
  
  return SCM_UNSPECIFIED;
}

void gzochid_api_log_init (void)
{
  scm_call_1
    (scm_c_public_ref
     ("gzochi private log", "gzochi:set-primitive-log!"),
     scm_c_make_gsubr ("primitive-log", 2, 0, 0, primitive_log));

  scm_call_1
    (scm_c_public_ref
     ("gzochi private log", "gzochi:set-primitive-log-internal!"),
     scm_c_make_gsubr ("primitive-log-internal", 2, 0, 0,
		       primitive_log_internal));
}
