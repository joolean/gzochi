/* app-session.c: Application session map-management routines.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>
#include <stdlib.h>

#include "app-session.h"

gzochid_application_session_map *
gzochid_application_session_map_new (void)
{
  gzochid_application_session_map *session_map =
    calloc (1, sizeof (gzochid_application_session_map));

  g_mutex_init (&session_map->client_mapping_lock);

  session_map->oids_to_clients = g_hash_table_new_full
    (g_int64_hash, g_int64_equal, g_free, NULL);
  session_map->clients_to_oids = g_hash_table_new_full
    (g_direct_hash, g_direct_equal, NULL, g_free);

  g_mutex_init (&session_map->channel_mapping_lock);

  session_map->channel_oids_to_local_session_oids = g_hash_table_new_full
    (g_int64_hash, g_int64_equal, free, (GDestroyNotify) g_sequence_free);

  return session_map;
}

void
gzochid_application_session_map_free
(gzochid_application_session_map *session_map)
{
  g_mutex_clear (&session_map->client_mapping_lock);

  g_hash_table_destroy (session_map->oids_to_clients);
  g_hash_table_destroy (session_map->clients_to_oids);

  g_mutex_clear (&session_map->channel_mapping_lock);

  g_hash_table_destroy (session_map->channel_oids_to_local_session_oids);

  free (session_map);
}
