/* app.c: Application context routines for gzochid
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <glib.h>
#include <glib-object.h>
#include <glib/gstdio.h>
#include <libguile.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "app.h"
#include "app-session.h"
#include "app-store.h"
#include "app-task.h"
#include "config.h"
#include "dataclient.h"
#include "descriptor.h"
#include "event.h"
#include "filelog.h"
#include "guile.h"
#include "gzochid-auth.h"
#include "metaclient.h"
#include "oids-dataclient.h"
#include "oids-storage.h"
#include "schedule.h"
#include "scheme-task.h"
#include "session.h"
#include "storage-dataclient.h"
#include "storage-mem.h"
#include "storageengine.h"
#include "stats.h"
#include "taskclient.h"

#define DEFAULT_TX_TIMEOUT_MS 100
#define SERVER_FS_DATA_DEFAULT "/var/gzochid/data"

G_LOCK_DEFINE_STATIC (load_path);

static GPrivate thread_application_context_key;
static GPrivate thread_identity_key;

/* The application context struct definition. */

struct _GzochidApplicationContext
{
  GObject parent_object; /* The parent object, for casting. */
  
  GzochidConfiguration *configuration; /* The global configuration object. */
  GzochidApplicationDescriptor *descriptor; /* The application descriptor. */
  
  /* The folder containing the application descriptor file. */

  char *deployment_root;
  
  GList *load_paths; /* Complete set of module load paths. */

  /* The log domain, off the form "app/[app name]" for convenience use by 
     app-specific logging facilities. */

  char *log_domain; 
  
  /* The global authentication plugin registry. */

  GzochidAuthPluginRegistry *auth_plugin_registry; 

  /* The application's authentication function. */

  gzochid_auth_identity *(*authenticator) 
    (unsigned char *, short, gpointer, GError **);

  gpointer auth_data; /* Contextual data for the authentication plugin. */
  gzochid_auth_identity_cache *identity_cache; /* The local identity cache. */

  gzochid_application_session_map *session_map; /* The local session map. */

  /* The global storage engine container. */
  
  GzochidStorageEngineContainer *storage_engine_container;

  /* The oid strategy to use with the application store. */
  
  gzochid_oid_allocation_strategy *oid_strategy; 

  gzochid_application_store *store; /* The application store. */
  gzochid_application_state *state; /* The application state struct. */
  GzochidTaskQueue *task_queue; /* The global task scheduling queue. */
  guint64 tx_timeout_ms; /* The default transaction timeout. */
  
  gzochid_event_source *event_source; /* The local application event source. */
  GzochidApplicationStats *stats; /* The local application stats struct. */
  
  /* The global metaclient container. */

  GzochidMetaClientContainer *metaclient_container; 
};

typedef struct _GzochidApplicationContext GzochidApplicationContext;

static void 
initialize_auth (GzochidApplicationContext *app_context)
{  
  app_context->identity_cache = gzochid_auth_identity_cache_new ();
  
  if (app_context->descriptor->auth_type != NULL)
    {
      gzochid_auth_plugin *plugin = gzochid_auth_plugin_registry_lookup
	(app_context->auth_plugin_registry,
	 app_context->descriptor->auth_type);

      if (plugin != NULL)
	{
	  GError *error = NULL;

	  g_debug 
	    ("Initializing auth plugin '%s' for application '%s'.",
	     app_context->descriptor->auth_type,
	     app_context->descriptor->name);

	  app_context->auth_data = plugin->info->initialize 
	    (app_context->descriptor->auth_properties, &error);
	  if (error != NULL)
	    g_critical
	      ("Auth plugin '%s' failed to initialize "
	       "for application '%s': %s",
	       app_context->descriptor->auth_type,
	       app_context->descriptor->name,
	       error->message);
	  
	  g_clear_error (&error);
	  app_context->authenticator = plugin->info->authenticate;
	}
      else g_critical 
	     ("Unknown auth type '%s' for application '%s'.", 
	      app_context->descriptor->auth_type,
	      app_context->descriptor->name);
    }
  else
    {
      g_message 
	("No auth plugin specified for application '%s'; " 
	 "pass-thru authentication will be used.", 
	 app_context->descriptor->name);
      app_context->authenticator = gzochid_auth_function_pass_thru;
    }
}

/* Returns `TRUE' if the specified `gzochid_storage_engine_interface' is 
   "normal" - i.e., not the in-memory storage engine or the distributed storage
   engine - `FALSE' otherwise. */

static gboolean
needs_durable_storage (gzochid_storage_engine_interface *iface)
{
  return iface != &gzochid_storage_engine_interface_mem
    && iface != &gzochid_storage_engine_interface_dataclient;
}

static void 
initialize_data (GzochidApplicationContext *app_context)
{
  char *data_dir = NULL, *work_dir = NULL;
  GHashTable *config = gzochid_configuration_extract_group
    (app_context->configuration, "game");
  gzochid_storage_engine *storage_engine = NULL;
  gzochid_storage_engine_interface *iface = NULL;
  
  g_object_get (app_context->storage_engine_container,
		"storage-engine", &storage_engine,
		NULL);

  iface = storage_engine->interface;
  
  if (g_hash_table_contains (config, "server.fs.data"))
    work_dir = strdup (g_hash_table_lookup (config, "server.fs.data"));
  else work_dir = strdup (SERVER_FS_DATA_DEFAULT);

  data_dir = g_strconcat
    (work_dir, "/", app_context->descriptor->name, NULL);

  if (needs_durable_storage (iface))
    {
      if (!g_file_test (data_dir, G_FILE_TEST_EXISTS))
	{
	  g_message 
	    ("Work directory %s does not exist; creating...", data_dir);
	  if (g_mkdir_with_parents (data_dir, 493) != 0)
	    {
	      g_critical ("Unable to create work directory %s.", data_dir);
	      exit (EXIT_FAILURE);
	    }
	}
      else if (!g_file_test (data_dir, G_FILE_TEST_IS_DIR))
	{
	  g_critical ("%s is not a directory.", data_dir);
	  exit (EXIT_FAILURE);
	}
    }
  
  /* If we're using the dataclient storage engine, we need to use the dataclient
     oid allocation strategy. */
  
  if (iface == &gzochid_storage_engine_interface_dataclient)
    {
      GzochidMetaClient *metaclient = NULL;
      GzochidDataClient *dataclient = NULL;

      g_object_get (app_context->metaclient_container,
		    "metaclient", &metaclient,
		    NULL);

      assert (metaclient != NULL);
      
      /* Grab the metaclient's managed data client to inject into the oid
	 strategy. */
      
      g_object_get (metaclient, "data-client", &dataclient, NULL);

      app_context->oid_strategy = gzochid_dataclient_oid_strategy_new
	(dataclient, app_context->descriptor->name);

      g_object_unref (dataclient);
      g_object_unref (metaclient);
    }
  else app_context->oid_strategy =
	 gzochid_storage_oid_strategy_new (iface, data_dir);

  app_context->store = gzochid_application_store_open
    (iface, data_dir, app_context->oid_strategy);
  
  if (iface == &gzochid_storage_engine_interface_dataclient)
    {
      GzochidMetaClient *metaclient = NULL;
      GzochidDataClient *dataclient = NULL;

      g_object_get (app_context->metaclient_container,
		    "metaclient", &metaclient,
		    NULL);
      
      assert (metaclient != NULL);

      /* Grab the metaclient's managed data client to inject it into storage
	 context. */
      
      g_object_get (metaclient, "data-client", &dataclient, NULL);
      
      gzochid_dataclient_storage_context_set_dataclient
	(app_context->store->storage_context, dataclient);
      
      g_object_unref (dataclient);
      g_object_unref (metaclient);
    }

  free (work_dir);
  free (data_dir);
  
  g_hash_table_destroy (config);  
}

static void *
initialize_load_paths_guile_worker (void *data)
{
  GList *load_path_ptr = data;

  while (load_path_ptr != NULL)
    {
      gzochid_guile_add_to_load_path (load_path_ptr->data);
      load_path_ptr = load_path_ptr->next;
    }

  return NULL;
}

static void 
initialize_load_paths (GzochidApplicationContext *app_context)
{
  assert (app_context->descriptor->deployment_root != NULL);
  
  app_context->load_paths = g_list_prepend 
    (g_list_copy_deep
     (app_context->descriptor->load_paths, (GCopyFunc) strdup, NULL),
     strdup (app_context->descriptor->deployment_root));
  
  G_LOCK (load_path);
  scm_with_guile
    (initialize_load_paths_guile_worker, app_context->load_paths);
  G_UNLOCK (load_path);
}

static void
initialize_logging (GzochidApplicationContext *app_context)
{
  app_context->log_domain = g_strdup_printf
    ("app/%s", app_context->descriptor->name);

  if (app_context->descriptor->log_filename != NULL)
    {
      FILE *log_file = g_fopen (app_context->descriptor->log_filename, "a");

      if (log_file == NULL)
	{
	  g_critical
	    ("Failed to open log file '%s' for writing for application %s: %s",
	     app_context->descriptor->log_filename,
	     app_context->descriptor->name, g_strerror (errno));
	  exit (EXIT_FAILURE);
	}
      
      g_log_set_handler_full
	(app_context->log_domain,
	 G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION,
	 gzochid_file_log_handler, gzochid_file_log_context_new (log_file),
	 gzochid_file_log_context_free);
    }
}

/*
  Completes the initialization of an application running on a server in a 
  single-node configuration.

  This function should be run inside a transaction.
*/

static void 
initialize_async_transactional (gpointer data)
{
  GError *err = NULL;
  GzochidApplicationContext *context = data;

  gzochid_sweep_client_sessions (context, &err);
  
  if (err != NULL)
    {
      g_warning
	("Failed to sweep sessions for application '%s'; stopping: %s.",
	 context->descriptor->name, err->message);
      
      g_error_free (err);
    }

  /* ...and then resubmit durable tasks and restart task processing. */
      
  else gzochid_restart_tasks (context);
}

static gboolean
ready_async (GzochidApplicationContext *context)
{
  GError *tmp_err = NULL;

  if (context->descriptor->ready != NULL)
    gzochid_scheme_application_ready
      (context, gzochid_auth_system_identity (), &tmp_err);
  
  if (tmp_err != NULL)
    {
      g_clear_error (&tmp_err);
      return FALSE;
    }
  else return TRUE;
}

static void *
run_async_guile (void *data)
{
  GzochidApplicationContext *app_context = data;
  GzochidMetaClient *metaclient = NULL;
  
  if (!ready_async (app_context))
    return NULL;

  metaclient = gzochid_application_context_get_metaclient (app_context);

  if (metaclient == NULL)
    {
      /* If we're running in a single-node configuration, launch a task to
	 finish boostrapping the application. */
      
      gzochid_application_transaction_execute 
	(data, initialize_async_transactional, app_context);
      gzochid_start_task_processing (app_context);
    }
  else
    {
      GzochidTaskClient *taskclient = NULL;
      const char *app_name = gzochid_application_context_get_name (app_context);
      
      g_object_get (metaclient, "task-client", &taskclient, NULL);

      /* If we're running in distributed mode, tell the task server we're
	 running this application and let the task server protocol take care of
	 the rest of the bootstrap process. */
      
      gzochid_taskclient_start_application (taskclient, app_name);

      g_object_unref (taskclient);
      g_object_unref (metaclient);
    }
  
  return NULL;
}

static void 
run_async (gpointer data, gpointer user_data)
{
  scm_with_guile (run_async_guile, data);
}

static void 
run (GzochidApplicationContext *app_context)
{
  gzochid_schedule_submit_task
    (app_context->task_queue,
     gzochid_task_immediate_new (run_async, NULL, app_context));
}

static void 
update_stats (GzochidEvent *event, gpointer data)
{
  gzochid_stats_update_from_event (data, event);
}

/* Boilerplate setup for the application context object. */

G_DEFINE_TYPE (GzochidApplicationContext, gzochid_application_context,
	       G_TYPE_OBJECT);

enum gzochid_application_context_properties
  {
    PROP_CONFIGURATION = 1,
    PROP_DESCRIPTOR,
    PROP_META_CLIENT_CONTAINER,
    PROP_STORAGE_ENGINE_CONTAINER,
    PROP_AUTH_PLUGIN_REGISTRY,
    PROP_EVENT_LOOP,
    PROP_TASK_QUEUE,
    PROP_APPLICATION_STATE,
    PROP_APPLICATION_STORE,
    PROP_SESSION_MAP,
    PROP_EVENT_SOURCE,
    PROP_APPLICATION_STATS,
    PROP_TX_TIMEOUT_MS,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
gzochid_application_context_constructed (GObject *object)
{
  GzochidApplicationContext *app_context = GZOCHID_APPLICATION_CONTEXT (object);
  GHashTable *config = gzochid_configuration_extract_group
    (app_context->configuration, "game");
  
  initialize_auth (app_context);
  initialize_data (app_context);
  initialize_load_paths (app_context);
  initialize_logging (app_context);

  gzochid_event_attach
    (app_context->event_source, update_stats, app_context->stats);
  
  app_context->tx_timeout_ms = gzochid_config_to_long
    (g_hash_table_lookup (config, "tx.timeout"), DEFAULT_TX_TIMEOUT_MS);
  g_hash_table_destroy (config);
  
  run (app_context);
}

static void
gzochid_application_context_get_property (GObject *object, guint property_id,
					  GValue *value, GParamSpec *pspec)
{
  GzochidApplicationContext *context = GZOCHID_APPLICATION_CONTEXT (object);

  switch (property_id)
    {
    case PROP_DESCRIPTOR:
      g_value_set_object (value, context->descriptor);
      break;
      
    case PROP_META_CLIENT_CONTAINER:
      g_value_set_object (value, context->metaclient_container);
      break;

    case PROP_TASK_QUEUE:
      g_value_set_object (value, context->task_queue);
      break;
      
    case PROP_APPLICATION_STATE:
      g_value_set_pointer (value, context->state);
      break;

    case PROP_APPLICATION_STORE:
      g_value_set_pointer (value, context->store);
      break;

    case PROP_SESSION_MAP:
      g_value_set_pointer (value, context->session_map);
      break;
      
    case PROP_EVENT_SOURCE:
      g_value_set_boxed (value, context->event_source);
      break;

    case PROP_APPLICATION_STATS:
      g_value_set_boxed (value, context->stats);
      break;
      
    case PROP_TX_TIMEOUT_MS:
      g_value_set_uint64 (value, context->tx_timeout_ms);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochid_application_context_set_property (GObject *object, guint property_id,
					  const GValue *value,
					  GParamSpec *pspec)
{
  GzochidApplicationContext *self = GZOCHID_APPLICATION_CONTEXT (object);

  switch (property_id)
    {
    case PROP_CONFIGURATION:
      self->configuration = g_object_ref (g_value_get_object (value));
      break;

    case PROP_DESCRIPTOR:
      self->descriptor = g_object_ref (g_value_get_object (value));
      break;
      
    case PROP_META_CLIENT_CONTAINER:
      self->metaclient_container =
	g_object_ref (g_value_get_object (value));
      break;

    case PROP_STORAGE_ENGINE_CONTAINER:
      self->storage_engine_container = g_object_ref
	(g_value_get_object (value));
      break;
      
    case PROP_AUTH_PLUGIN_REGISTRY:
      self->auth_plugin_registry = g_object_ref
	(g_value_get_object (value));
      break;

    case PROP_EVENT_LOOP:

      /* Don't need to store a reference to the event loop, just attach the
	 event source to it. */

      gzochid_event_source_attach
	(g_value_get_object (value), self->event_source);
      break;
      
    case PROP_TASK_QUEUE:
      self->task_queue = g_object_ref (g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochid_application_context_init (GzochidApplicationContext *context)
{
  context->session_map = gzochid_application_session_map_new ();
  context->state = gzochid_application_state_new ();
  context->event_source = gzochid_event_source_new ();
  context->stats = calloc (1, sizeof (GzochidApplicationStats));
}

static void
gzochid_application_context_dispose (GObject *gobject)
{
  GzochidApplicationContext *app_context =
    GZOCHID_APPLICATION_CONTEXT (gobject);

  g_object_unref (app_context->configuration);
  g_object_unref (app_context->descriptor);
  g_object_unref (app_context->metaclient_container);
  g_object_unref (app_context->auth_plugin_registry);
  g_object_unref (app_context->storage_engine_container);
  g_object_unref (app_context->task_queue);
  
  G_OBJECT_CLASS (gzochid_application_context_parent_class)->dispose (gobject);
}

static void
gzochid_application_context_finalize (GObject *gobject)
{
  GzochidApplicationContext *app_context =
    GZOCHID_APPLICATION_CONTEXT (gobject);

  gzochid_auth_identity_cache_destroy (app_context->identity_cache);
  g_list_free_full (app_context->load_paths, free);
  g_free (app_context->log_domain);
  gzochid_oid_allocation_strategy_free (app_context->oid_strategy);
  gzochid_application_session_map_free (app_context->session_map);
  gzochid_application_state_free (app_context->state);
  gzochid_application_store_close (app_context->store);
  
  g_source_destroy ((GSource *) app_context->event_source);
  g_source_unref ((GSource *) app_context->event_source);

  free (app_context->stats);

  G_OBJECT_CLASS (gzochid_application_context_parent_class)->finalize (gobject);
}

static void
gzochid_application_context_class_init (GzochidApplicationContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = gzochid_application_context_constructed;
  object_class->dispose = gzochid_application_context_dispose;
  object_class->finalize = gzochid_application_context_finalize;
  object_class->get_property = gzochid_application_context_get_property;
  object_class->set_property = gzochid_application_context_set_property;

  obj_properties[PROP_CONFIGURATION] = g_param_spec_object
    ("configuration", "config", "The global configuration object",
     GZOCHID_TYPE_CONFIGURATION, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_DESCRIPTOR] = g_param_spec_object
    ("descriptor", "descriptor", "The application descriptor",
     GZOCHID_TYPE_APPLICATION_DESCRIPTOR,
     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_META_CLIENT_CONTAINER] = g_param_spec_object
    ("metaclient-container", "metaclient-container", "The metaclient container",
     GZOCHID_TYPE_META_CLIENT_CONTAINER,
     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_STORAGE_ENGINE_CONTAINER] = g_param_spec_object
    ("storage-engine-container", "storage-engine-container",
     "The global storage engine container",
     GZOCHID_TYPE_STORAGE_ENGINE_CONTAINER,
     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_AUTH_PLUGIN_REGISTRY] = g_param_spec_object
    ("auth-plugin-registry", "auth-plugin-registry",
     "The global authentication plugin registry",
     GZOCHID_TYPE_AUTH_PLUGIN_REGISTRY,
     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_EVENT_LOOP] = g_param_spec_object
    ("event-loop", "event-loop", "The global event loop",
     GZOCHID_TYPE_EVENT_LOOP, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_TASK_QUEUE] = g_param_spec_object
    ("task-queue", "task-queue", "The global task queue",
     GZOCHID_TYPE_TASK_QUEUE, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_APPLICATION_STATE] = g_param_spec_pointer
    ("app-state", "app-state", "The application state", G_PARAM_READABLE);

  obj_properties[PROP_APPLICATION_STORE] = g_param_spec_pointer
    ("app-store", "app-store", "The application store", G_PARAM_READABLE);

  obj_properties[PROP_SESSION_MAP] = g_param_spec_pointer
    ("session-map", "session-map", "The application session map",
     G_PARAM_READABLE);
  
  obj_properties[PROP_EVENT_SOURCE] = g_param_spec_boxed
    ("event-source", "event-source", "The application event source",
     G_TYPE_SOURCE, G_PARAM_READABLE);

  obj_properties[PROP_APPLICATION_STATS] = g_param_spec_boxed
    ("app-stats", "app-stats", "The application statistics object",
     GZOCHID_TYPE_APPLICATION_STATS, G_PARAM_READABLE);
  
  obj_properties[PROP_TX_TIMEOUT_MS] = g_param_spec_uint64
    ("tx-timeout", "tx-timeout", "The default transaction timeout (ms)", 0,
     G_MAXUINT64, 30, G_PARAM_READABLE);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

/* End boilerplate. */

void *
gzochid_with_application_context (GzochidApplicationContext *context, 
				  gzochid_auth_identity *identity,
				  void *(*worker) (gpointer), gpointer data)
{
  gpointer ret = NULL;
  gboolean private_needs_context = 
    g_private_get (&thread_application_context_key) == NULL;
  SCM application_root_fluid = 
    scm_variable_ref
    (scm_c_module_lookup 
     (scm_c_resolve_module("gzochi app"), "%gzochi:application-root"));

  if (private_needs_context)
    {
      g_private_set (&thread_application_context_key, context);
      g_private_set (&thread_identity_key, identity);

      assert (context->descriptor->deployment_root != NULL);
      
      scm_fluid_set_x
	(application_root_fluid, 
	 scm_from_locale_string (context->descriptor->deployment_root));
    }
  
  ret = worker (data);

  if (private_needs_context)
    {
      g_private_set (&thread_application_context_key, NULL);
      g_private_set (&thread_identity_key, NULL);

      scm_fluid_set_x (application_root_fluid, SCM_UNSPECIFIED);
    }

  return ret;
}

GzochidApplicationContext *
gzochid_get_current_application_context (void)
{
  return (GzochidApplicationContext *)
    g_private_get (&thread_application_context_key);
}

gzochid_auth_identity *
gzochid_get_current_identity (void)
{
  return (gzochid_auth_identity *) g_private_get (&thread_identity_key);
}

const char *
gzochid_application_context_get_name (GzochidApplicationContext *app_context)
{
  return app_context->descriptor->name;
}

const char *
gzochid_application_context_get_log_domain
(GzochidApplicationContext *app_context)
{
  return app_context->log_domain;
}

const char *
gzochid_application_context_get_description
(GzochidApplicationContext *app_context)
{
  return app_context->descriptor->description;
}

GzochidMetaClient *
gzochid_application_context_get_metaclient (GzochidApplicationContext *context)
{
  GzochidMetaClient *metaclient = NULL;
  g_object_get (context->metaclient_container, "metaclient", &metaclient, NULL);
  return metaclient;
}

gzochid_auth_identity *
gzochid_application_context_resolve_identity
(GzochidApplicationContext *context, char *name)
{
  return gzochid_auth_identity_from_name (context->identity_cache, name);
}

gzochid_auth_identity *
gzochid_application_context_authenticate (GzochidApplicationContext *context,
					  unsigned char *cred, short cred_len,
					  GError **err)
{
  return context->authenticator (cred, cred_len, context->auth_data, err);
}
