/* storageengine.c: Storage engine bootstrapping routines for gzochid.
 * Copyright (C) 2019 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>

#include "config.h"
#include "gzochid-storage.h"
#include "metaclient.h"
#include "storage-dataclient.h"
#include "storage-mem.h"
#include "storage.h"
#include "storageengine.h"

/* Boilerplate setup for the storage engine container object. */

/* The storage engine container object. */

struct _GzochidStorageEngineContainer
{
  GObject parent_instance; /* The parent struct, for casting. */

  GzochidConfiguration *configuration;  /* The global configuration object. */

  /* The metaclient container, for accessing the data client in distributed 
     mode. */

  GzochidMetaClientContainer *metaclient_container; 

  /* Whether an attemp has been made to fetch the storage engine. */

  gboolean storage_engine_initialized; 

  gzochid_storage_engine *storage_engine; /* The storage engine handle. */
};

G_DEFINE_TYPE (GzochidStorageEngineContainer, gzochid_storage_engine_container,
	       G_TYPE_OBJECT);

enum gzochid_storage_engine_container
  {
    PROP_CONFIGURATION = 1,
    PROP_META_CLIENT_CONTAINER,
    PROP_STORAGE_ENGINE,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

/* 
   A lazy accessor that bootstraps the storage engine to be used by all hosted 
   applications. If the gzochid application server is running in distributed 
   mode, the "dataclient" storage engine is used; otherwise the engine named in
   the `gzochid.conf' file is loaded. If the requested storage engine cannot be
   loaded, the server will fall back to a non-durable "in-memory" storage 
   engine. 
   
   This proess is necessarily lazy; the resolver will recurse infinitely trying
   to resolve the metaclient. 
*/

static gzochid_storage_engine *
lazy_init_storage_engine (GzochidStorageEngineContainer *container)
{
  GHashTable *config = NULL;
  GzochidMetaClient *metaclient = NULL;

  if (container->storage_engine_initialized)
    return container->storage_engine;
  
  config = gzochid_configuration_extract_group
    (container->configuration, "game");

  g_object_get
    (container->metaclient_container, "metaclient", &metaclient, NULL);

  if (metaclient != NULL)
    {
      char *conf_storage_engine = g_hash_table_lookup
	(config, "storage.engine");

      if (conf_storage_engine != NULL)
	g_message
	  ("Meta server client configuration detected; ignoring configured "
	   "storage engine '%s'.", conf_storage_engine);
      else g_message
	     ("Meta server client configuration detected; using data client "
	      "storage engine.");

      container->storage_engine = calloc (1, sizeof (gzochid_storage_engine));
      container->storage_engine->interface =
	&gzochid_storage_engine_interface_dataclient;

      g_object_unref (metaclient);
    }
  else if (g_hash_table_contains (config, "storage.engine"))
    {
      char *dir = NULL;
      char *env = getenv ("GZOCHID_STORAGE_ENGINE_DIR");
      
      if (env != NULL)
	dir = env;
      else 
	{
	  char *conf_dir = g_hash_table_lookup (config, "storage.engine.dir");
	  dir = conf_dir == NULL ? GZOCHID_STORAGE_ENGINE_DIR : conf_dir;
	}

      container->storage_engine = gzochid_storage_load_engine 
	(dir, g_hash_table_lookup (config, "storage.engine"));
    }
  else g_message 
	 ("No durable storage engine configured; memory engine will be used.");

  if (container->storage_engine == NULL)
    {
      g_message
	("Using in-memory storage for application data. THIS CONFIGURATION IS "
	 "NOT SAFE FOR PRODUCTION USE.");

      container->storage_engine = calloc (1, sizeof (gzochid_storage_engine));
      container->storage_engine->interface =
	&gzochid_storage_engine_interface_mem;
    }

  g_hash_table_destroy (config);

  container->storage_engine_initialized = TRUE;
  return container->storage_engine;
}

static void
storage_engine_container_dispose (GObject *obj)
{
  GzochidStorageEngineContainer *container =
    GZOCHID_STORAGE_ENGINE_CONTAINER (obj);

  g_object_unref (container->configuration);
  g_object_unref (container->metaclient_container);

  G_OBJECT_CLASS (gzochid_storage_engine_container_parent_class)->dispose (obj);
}

static void
storage_engine_container_finalize (GObject *obj)
{
  GzochidStorageEngineContainer *container =
    GZOCHID_STORAGE_ENGINE_CONTAINER (obj);

  if (container->storage_engine != NULL)
    free (container->storage_engine);

  G_OBJECT_CLASS (gzochid_storage_engine_container_parent_class)
    ->finalize (obj);
}

static void
storage_engine_container_get_property (GObject *obj, guint property_id,
				       GValue *value, GParamSpec *pspec)
{
  GzochidStorageEngineContainer *container =
    GZOCHID_STORAGE_ENGINE_CONTAINER (obj);

  switch (property_id)
    {
    case PROP_STORAGE_ENGINE:
      g_value_set_pointer (value, lazy_init_storage_engine (container));
      break;
      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
    }
}

static void
storage_engine_container_set_property (GObject *obj, guint property_id,
				       const GValue *value, GParamSpec *pspec)
{
  GzochidStorageEngineContainer *container =
    GZOCHID_STORAGE_ENGINE_CONTAINER (obj);

  switch (property_id)
    {
    case PROP_CONFIGURATION:
      container->configuration = g_object_ref (g_value_get_object (value));
      break;

    case PROP_META_CLIENT_CONTAINER:
      container->metaclient_container = g_object_ref
	(g_value_get_object (value));
      break;
      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
    }
}

void
gzochid_storage_engine_container_class_init
(GzochidStorageEngineContainerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = storage_engine_container_dispose;
  object_class->finalize = storage_engine_container_finalize;
  object_class->get_property = storage_engine_container_get_property;
  object_class->set_property = storage_engine_container_set_property;

  obj_properties[PROP_CONFIGURATION] = g_param_spec_object
    ("configuration", "config", "The global configuration object",
     GZOCHID_TYPE_CONFIGURATION, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  obj_properties[PROP_META_CLIENT_CONTAINER] = g_param_spec_object
    ("metaclient-container", "metaclient-container", "The metaclient container",
     GZOCHID_TYPE_META_CLIENT_CONTAINER,
     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
  
  obj_properties[PROP_STORAGE_ENGINE] = g_param_spec_pointer
    ("storage-engine", "storage-engine", "The global storage engine",
     G_PARAM_READABLE);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

void
gzochid_storage_engine_container_init (GzochidStorageEngineContainer *container)
{
}
