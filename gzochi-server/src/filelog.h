/* filelog.h: Prototypes and declarations for filelog.c
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_FILELOG_H
#define GZOCHID_FILELOG_H

#include <glib.h>
#include <stdio.h>

typedef struct _gzochid_file_log_context gzochid_file_log_context;

/*
  Construct and return a new `gzochid_file_log_context' record using the
  specified file stream. The returned log context takes ownership of the file
  stream, which should not be subsequently modified or closed by the caller.

  The resources used by the returned log context should be freed by a call to
  `gzochid_file_log_context_free when no longer needed.
*/

gzochid_file_log_context *gzochid_file_log_context_new (FILE *);

/*
  A `GDestroyNotify' implementation `gzochid_file_log_context' records. Calls 
  `fclose' on the associated file stream. 
*/

void gzochid_file_log_context_free (gpointer);

/*
  A `GLogFunc' implementation that formats and writes log messages to an
  associated file stream.
*/

void gzochid_file_log_handler (const gchar *, GLogLevelFlags, const gchar *,
			       gpointer);

#endif /* GZOCHID_FILELOG_H */
