/* debug.h: Prototypes and declarations for debug.c
 * Copyright (C) 2019 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_DEBUG_H
#define GZOCHID_DEBUG_H

#include <glib.h>
#include <glib-object.h>

/* The core embedded debugging server type definitions. */

#define GZOCHID_TYPE_DEBUG_SERVER gzochid_debug_server_get_type ()

G_DECLARE_FINAL_TYPE (GzochidDebugServer, gzochid_debug_server, GZOCHID,
		      DEBUG_SERVER, GObject);

enum GzochidDebugServerError
  { 
    GZOCHID_DEBUG_SERVER_ERROR_FAILED /* Generic debugging server failure. */
  };

/*
  The error domain for debugging server-related errors. Error codes for errors
  in this domain will be values from the `GzochidDebugServerError' enum 
  above.
*/

#define GZOCHID_DEBUG_SERVER_ERROR gzochid_debug_server_error_quark ()

/*
  Starts the debugging server listening on the specified port. A thread 
  (managed by the server) will be launched to accept connections and respond to
  commands. A port number of `0' may specified to direct the server to listen 
  on an arbitrary available port.
*/

void gzochid_debug_server_start (GzochidDebugServer *, guint, GError **);

/* Stops the specified debugging server. */

void gzochid_debug_server_stop (GzochidDebugServer *);

GQuark gzochid_debug_server_error_quark ();

#endif /* GZOCHID_DEBUG_H */
