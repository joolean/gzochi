/* task.c: Application task management routines for gzochid
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/time.h>

#include "task.h"
#include "threads.h"

G_DEFINE_TYPE (GzochidTask, gzochid_task, G_TYPE_INITIALLY_UNOWNED);

static void
task_finalize (GObject *object)
{
  GzochidTask *task = GZOCHID_TASK (object);

  if (task->destroy_notify != NULL)
    task->destroy_notify (task->data);
  
  G_OBJECT_CLASS (gzochid_task_parent_class)->finalize (object);
}

static void
gzochid_task_class_init (GzochidTaskClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = task_finalize;
}

static void
gzochid_task_init (GzochidTask *task)
{
}

GzochidTask *
gzochid_task_new (gzochid_thread_worker worker, GDestroyNotify destroy_notify,
		  gpointer data, struct timeval target_execution_time)
{
  GzochidTask *task = g_object_new (GZOCHID_TYPE_TASK, NULL);
  
  task->worker = worker;
  task->destroy_notify = destroy_notify;
  task->data = data;
  task->target_execution_time = target_execution_time;
  
  return task;
}

GzochidTask *
gzochid_task_immediate_new (gzochid_thread_worker worker,
			    GDestroyNotify destroy_notify, gpointer data)
{
  struct timeval now;
  gettimeofday (&now, NULL);

  return gzochid_task_new (worker, destroy_notify, data, now);
}
