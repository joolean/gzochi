/* seqhash.h: Prototypes and declarations for seqhash.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_SEQHASH_H
#define GZOCHID_SEQHASH_H

#include <glib.h>

/* The data structures and functions below support the creation of a 
   "sequential" hash table that offers ordered access to keys. */

/* The sequential hash table struct typedef. */

typedef struct _gzochid_seq_hash_table gzochid_seq_hash_table;

/* Create and return a new sequential hash table that uses the specified
   `GCompareFunc' to order keys, uses the specified `GHashFunc' and `GEqualFunc'
   for hashing, and optional `GDestroyNotify' callbacks for cleaning up keys and
   values respectively. The table should be freed via 
   `gzochid_seq_hash_table_free' when no longer needed. */

gzochid_seq_hash_table *gzochid_seq_hash_table_new (GCompareFunc, GHashFunc,
						    GEqualFunc, GDestroyNotify,
						    GDestroyNotify);

/* Releases the memory used by the specified sequential hash table. */

void gzochid_seq_hash_table_free (gzochid_seq_hash_table *);

/* Returns `TRUE' if the specified sequential hash table has no elements, 
   `FALSE' otherwise. */

gboolean gzochid_seq_hash_table_is_empty (gzochid_seq_hash_table *);

/* Returns the "least" key in the sequential hash table according to the table's
   configured `GCompareFunc', or `NULL' if the table is empty. */

gpointer gzochid_seq_hash_table_first_key (gzochid_seq_hash_table *);

/* Returns the value associated with the specified key in the specified 
   sequential hash table. */

gpointer gzochid_seq_hash_table_lookup (gzochid_seq_hash_table *, gpointer);

/* Inserts the specified key and value into the specified sequential hash table.
   Returns `TRUE' if there was no value previously associated with this key,
   otherwise calls the key and value destroy callbacks (if any) for the
   existing values and returns `FALSE'. */

gboolean gzochid_seq_hash_table_insert (gzochid_seq_hash_table *, gpointer,
					gpointer);

/* Removes the value mapped to the specified key in the specified sequential
   hash table, calling the keya nd value destroy callbacks (if any) for the 
   existing values (if any). Returns `TRUE' if a value was actually removed from
   the table, `FALSE' otherwise. */

gboolean gzochid_seq_hash_table_remove (gzochid_seq_hash_table *, gpointer);

#endif /* GZOCHID_SEQHASH_H */
