;; gzochi/admin.scm: Administrative interface to gzochid
;; Copyright (C) 2020 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi admin)
  (export gzochi:application-context?
	  gzochi:application-context-name

          gzochi:applications
	  gzochi:current-application
	  gzochi:with-application

	  gzochi:set-primitive-applications!
	  gzochi:set-primitive-current-application!
	  gzochi:set-primitive-with-application!
	  )
  (import (only (guile) make-undefined-variable thunk? variable-ref
		        variable-set!)
	  (rnrs base)
	  (rnrs conditions)
	  (rnrs exceptions)
	  (rnrs records syntactic))

  (define-record-type (gzochi:application-context 
		       gzochi:make-application-context 
		       gzochi:application-context?)
    (fields name)
    (opaque #t)
    (sealed #t))

  (define primitive-applications (make-undefined-variable))
  (define primitive-current-application (make-undefined-variable))
  (define primitive-with-application (make-undefined-variable))

  (define (gzochi:set-primitive-applications! proc)
    (variable-set! primitive-applications proc))
  (define (gzochi:set-primitive-current-application! proc)
    (variable-set! primitive-current-application proc))
  (define (gzochi:set-primitive-with-application! proc)
    (variable-set! primitive-with-application proc))
  
  (define (gzochi:applications) ((variable-ref primitive-applications)))

  (define (gzochi:current-application)
    ((variable-ref primitive-current-application)))

  (define (gzochi:with-application context thunk)
    (or (gzochi:application-context? context)
	(assertion-violation
	 'gzochi:with-application "Expected application context." context))
    (or (thunk? thunk)
	(assertion-violation 'gzochi:with-application "Expected thunk." thunk))

    ((variable-ref primitive-with-application) context thunk))
)
