;; gzochi/private/reflect.scm: Exports for common binding reflection procedures
;; Copyright (C) 2017 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi private reflect)
  (export gzochi:check-syntax-module gzochi:resolve-procedure)
  (import (guile)
	  (ice-9 format)
	  (ice-9 threads)
	  (rnrs base)
	  (rnrs conditions)
	  (rnrs exceptions)
	  (only (srfi :1) find))

  ;; This function exists to work around non-deterministic behavior in the
  ;; behavior of `syntax-module' in the Guile 2.x series, specifically that
  ;; the value returned by that function will be different at different compiler
  ;; lifecycle stages and for local symbols versus symbols exported from modules
  ;; imported by the current "compilation unit." This function attempts to
  ;; identify and correct cases in which `syntax-module' may be reporting the
  ;; wrong module for an exported symbol.
  
  (define (gzochi:check-syntax-module proc syntax-module-name)
    (let ((stx-module (resolve-module syntax-module-name)))

      ;; Is the module returned by `syntax-module' the current module? If so,
      ;; it's possible that module dependencies are still be resolved and that
      ;; `syntax-module' is confused; make sure that the symbol is exported from
      ;; the current module's public interface.
      
      (if (if (eq? stx-module (current-module))
	      (module-variable (module-public-interface stx-module) proc)
	      (module-variable stx-module proc))

	  ;; We found the symbol where `syntax-module' said it would be.
	  
	  syntax-module-name

	  ;; Search the interfaces imported by the module reported by
	  ;; `syntax-module' - it might have accidentally attributed an
	  ;; imported symbol to the local module.
	  
	  (or (and=> (find (lambda (iface) (module-variable iface proc))
			   (module-uses stx-module))
		     module-name)
	      (assertion-violation
	       #f "Unable to resolve module for procedure." proc)))))

  ;; Guards against concurrency-related non-determinism inherent in Guile's
  ;; module loader. Needs to be recursive since procedure references can be
  ;; resolved during module loading / expansion.
  
  (define module-resolution-mutex (make-recursive-mutex))

  (define (gzochi:resolve-procedure procedure module-name)
    (with-mutex module-resolution-mutex

      ;; resolve-module is not thread-safe, at least as of Guile 2.0.6.
      ;; If multiple threads attempt to resolve the same module name at once,
      ;; some of them may get false negatives. Protecting this function with a
      ;; a mutex seems to prevent that from happening, but there are surely
      ;; performance implications.

      (let ((module (resolve-module module-name #:ensure #f)))
	(or module (assertion-violation
		    'gzochi:resolve-procedure
		    (format #f "Unable to resolve ~A." module-name)))

	(let ((variable (module-variable module procedure)))
	  (or variable
	      (assertion-violation
	       'gzochi:resolve-procedure
	       (format #f "No binding for ~A in ~A" procedure module-name)))
	
	  (variable-ref variable)))))
)
