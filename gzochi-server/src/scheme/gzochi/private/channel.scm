;; gzochi/private/channel.scm: Private infrastructure for gzochi channel API 
;; Copyright (C) 2020 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi private channel)
  (export gzochi:create-channel
	  gzochi:get-channel

          gzochi:make-channel
	  gzochi:channel?
	  gzochi:channel-name
	  gzochi:channel-oid

	  gzochi:join-channel
	  gzochi:leave-channel
	  gzochi:close-channel
	  gzochi:send-channel-message

	  gzochi:set-primitive-create-channel!
	  gzochi:set-primitive-get-channel!
	  gzochi:set-primitive-join-channel!
	  gzochi:set-primitive-leave-channel!
	  gzochi:set-primitive-send-channel-message!
	  gzochi:set-primitive-close-channel!
	  )

  (import (only (guile) make-undefined-variable variable-ref variable-set!)
	  (gzochi session)
	  (gzochi conditions)
	  (gzochi private data)
	  (gzochi io)
	  (rnrs))

  (gzochi:define-managed-record-type
   (gzochi:channel gzochi:make-channel gzochi:channel?)
   
   (fields (immutable oid (serialization gzochi:integer-serialization))
	   (immutable name (serialization gzochi:string-serialization))))

  (define (gzochi:create-channel name)
    (or (string? name)
	(assertion-violation
	 'gzochi:create-channel "Channel name must be a string." name))

    (and ((variable-ref primitive-get-channel) name)
	 (raise (gzochi:make-name-exists-condition name)))
    ((variable-ref primitive-create-channel) name))

  (define (gzochi:get-channel name)
    (or (string? name)
	(assertion-violation
	 'gzochi:get-channel "Channel name must be a string." name))

    (or ((variable-ref primitive-get-channel) name) 
	(raise (gzochi:make-name-not-bound-condition name))))

  (define (gzochi:join-channel channel session)
    (or (gzochi:channel? channel)
	(assertion-violation
	 'gzochi:join-channel "Expected gzochi:channel." channel))
    (or (gzochi:client-session? session)
	(assertion-violation
	 'gzochi:join-channel "Expeted gzochi:client-session." session))

    ((variable-ref primitive-join-channel) channel session))

  (define (gzochi:leave-channel channel session)
    (or (gzochi:channel? channel)
	(assertion-violation
	 'gzochi:leave-channel "Expected gzochi:channel." channel))
    (or (gzochi:client-session? session)
	(assertion-violation
	 'gzochi:leave-channel "Expeted gzochi:client-session." session))

    ((variable-ref primitive-leave-channel) channel session))

  (define (gzochi:send-channel-message channel msg)
    (or (gzochi:channel? channel)
	(assertion-violation
	 'gzochi:send-channel-message "Expected gzochi:channel." channel))
    (or (bytevector? msg)
	(assertion-violation
	 'gzochi:send-channel-message "Expected bytevector." msg))
    
    ((variable-ref primitive-send-channel-message) channel msg))

  (define (gzochi:close-channel channel)
    (or (gzochi:channel? channel)
	(assertion-violation
	 'gzochi:close-channel "Expected gzochi:channel." channel))

    ((variable-ref primitive-close-channel) channel))
  
  (define primitive-create-channel (make-undefined-variable))
  (define primitive-get-channel (make-undefined-variable))
  (define primitive-join-channel (make-undefined-variable))
  (define primitive-leave-channel (make-undefined-variable))
  (define primitive-send-channel-message (make-undefined-variable))
  (define primitive-close-channel (make-undefined-variable))

  (define (gzochi:set-primitive-create-channel! proc)
    (variable-set! primitive-create-channel proc))
  (define (gzochi:set-primitive-get-channel! proc)
    (variable-set! primitive-get-channel proc))
  (define (gzochi:set-primitive-join-channel! proc)
    (variable-set! primitive-join-channel proc))
  (define (gzochi:set-primitive-leave-channel! proc)
    (variable-set! primitive-leave-channel proc))
  (define (gzochi:set-primitive-send-channel-message! proc)
    (variable-set! primitive-send-channel-message proc))
  (define (gzochi:set-primitive-close-channel! proc)
    (variable-set! primitive-close-channel proc))
)
