;; gzochi/io.scm: Public exports for common I/O and serialization procedures
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi io)
  (export gzochi:read-integer
	  gzochi:read-boolean
	  gzochi:read-string
	  gzochi:read-symbol
	  gzochi:read-bytevector

	  gzochi:write-integer
	  gzochi:write-boolean
	  gzochi:write-string
	  gzochi:write-symbol
	  gzochi:write-bytevector

	  gzochi:integer-serialization
	  gzochi:boolean-serialization
	  gzochi:string-serialization
	  gzochi:symbol-serialization
	  gzochi:bytevector-serialization

	  gzochi:make-uniform-list-serialization
	  gzochi:make-uniform-vector-serialization

	  gzochi:serialization
	  gzochi:serialization?
	  gzochi:make-serialization
	  gzochi:serialization-serializer
	  gzochi:serialization-deserializer

	  gzochi:serialization-reference
	  gzochi:serialization-reference?
	  gzochi:make-serialization-reference
	  gzochi:serialization-reference-serializer-procedure
	  gzochi:serialization-reference-serializer-module-name
	  gzochi:serialization-reference-deserializer-procedure
	  gzochi:serialization-reference-deserializer-module-name
	  g:<->)

  (import (gzochi private reflect)
	  (rnrs)
	  (system syntax))

  (define-record-type (gzochi:serialization 
		       gzochi:make-serialization
		       gzochi:serialization?)
    (fields serializer deserializer))

  ;; A serialization reference is a sub-type of `gzochi:serialization' that
  ;; includes a module-qualified, serializable reference to the serializer and
  ;; deserializer procedures. 
  
  (define-record-type (gzochi:serialization-reference
		       gzochi:make-serialization-reference
		       gzochi:serialization-reference?)

    (parent gzochi:serialization)
    (fields serializer-procedure serializer-module-name
	    deserializer-procedure deserializer-module-name)

    (protocol (lambda (n)
		(lambda (serializer-procedure
			 serializer-module-name
			 deserializer-procedure
			 deserializer-module-name)

		  ;; Resolve the serializer and deserializer to construct the
		  ;; parent type.
		  
		  (let* ((serializer (gzochi:resolve-procedure
				      serializer-procedure
				      serializer-module-name))
			 (deserializer (gzochi:resolve-procedure
					deserializer-procedure
					serializer-module-name))
			 (p (n serializer deserializer)))
		    
		    (p serializer-procedure serializer-module-name
		       deserializer-procedure deserializer-module-name))))))

  ;; Convenience macro for creating new `gzochi:serialization-reference'
  ;; records, with optional "module guessing."
  
  (define-syntax g:<->
    (lambda (stx)
      (syntax-case stx ()
        ((_ (serializer-module-name ...) serializer-procedure
	    (deserializer-module-name ...) deserializer-procedure)
         #'(gzochi:make-serialization-reference
	    (quote serializer-procedure) (quote (serializer-module-name ...))
	    (quote deserializer-procedure)
	    (quote (deserializer-module-name ...))))
        ((_ serializer-procedure deserializer-procedure)
	 (let ((serializer-module
		(datum->syntax
		 stx (gzochi:check-syntax-module
		      (syntax->datum #'serializer-procedure)
		      (syntax-module #'serializer-procedure))))
	       (deserializer-module
		(datum->syntax
		 stx (gzochi:check-syntax-module
		      (syntax->datum #'deserializer-procedure)
		      (syntax-module #'deserializer-procedure)))))
         #`(gzochi:make-serialization-reference
	    (quote serializer-procedure)
	    (quote #,serializer-module)
	    (quote deserializer-procedure)
	    (quote #,deserializer-module)))))))
  
  (define (read-varint port)
    (define (read-varint-inner port tally septets)
      (let* ((b (get-u8 port))
             (tally (bitwise-ior (bitwise-arithmetic-shift-left 
                                  (bitwise-bit-field b 0 7) (* septets 7)) 
                                 tally)))
        (if (bitwise-bit-set? b 7)
            (read-varint-inner port tally (+ septets 1))
            tally)))
    (read-varint-inner port 0 0))
  
  (define (write-varint port varint)
    (let ((b (bitwise-bit-field varint 0 7)))
      (if (> varint 127)
          (begin (put-u8
		  port (bitwise-ior (bitwise-arithmetic-shift-left 1 7) b))
                 (write-varint port (bitwise-arithmetic-shift-right varint 7)))
          (put-u8 port b))))

  (define gzochi:read-integer read-varint)
  (define gzochi:write-integer write-varint)
  (define gzochi:integer-serialization 
    (gzochi:make-serialization gzochi:write-integer gzochi:read-integer))

  (define (gzochi:read-boolean port) (if (eqv? (read-varint port) 0) #f #t))
  (define (gzochi:write-boolean port val) (write-varint port (if val 1 0)))
  (define gzochi:boolean-serialization
    (gzochi:make-serialization gzochi:write-boolean gzochi:read-boolean))

  (define (gzochi:read-string port)
    (utf8->string (get-bytevector-n port (read-varint port))))
  (define (gzochi:write-string port str)
    (write-varint port (string-length str))
    (put-bytevector port (string->utf8 str)))
  (define gzochi:string-serialization
    (gzochi:make-serialization gzochi:write-string gzochi:read-string))
 
  (define (gzochi:read-symbol port) (string->symbol (gzochi:read-string port)))
  (define (gzochi:write-symbol port sym) 
    (gzochi:write-string port (symbol->string sym)))
  (define gzochi:symbol-serialization
    (gzochi:make-serialization gzochi:write-symbol gzochi:read-symbol))

  (define (gzochi:read-bytevector port)
    (get-bytevector-n port (read-varint port)))
  (define (gzochi:write-bytevector port bv)
    (write-varint port (bytevector-length bv))
    (put-bytevector port bv))
  (define gzochi:bytevector-serialization
    (gzochi:make-serialization gzochi:write-bytevector gzochi:read-bytevector))

  (define (gzochi:make-uniform-list-serialization serialization)
    (define serializer (gzochi:serialization-serializer serialization))
    (define deserializer (gzochi:serialization-deserializer serialization))

    (gzochi:make-serialization
     (lambda (port lst)
       (gzochi:write-integer port (length lst))
       (for-each (lambda (l) (serializer port l)) lst))
     (lambda (port)
       (let ((lst-length (gzochi:read-integer port)))
	 (let loop ((i 0) (lst '()))
	   (if (< i lst-length)
	       (loop (+ i 1) (cons (deserializer port) lst))
	       (reverse lst)))))))

  ;; A companion to `gzochi:make-uniform-list-serialization' for vectors. The
  ;; serialization format is a bit more complex to account for sparseness. The
  ;; format:
  ;;
  ;; - vector length (integer)
  ;; - number of non-empty elements (integer)
  ;; - for each non-empty element:
  ;;   - index of element (integer)
  ;;   - serialized element data
  ;;
  ;; The optional "fill" value (default `#f') is used to detect empty indices in
  ;; the vector during serialization. Empty indices in the deserialized vector
  ;; will be filled with this value.
  
  (define (gzochi:make-uniform-vector-serialization serialization . rest)
    (define serializer (gzochi:serialization-serializer serialization))
    (define deserializer (gzochi:serialization-deserializer serialization))

    (let ((fill (if (null? rest) #f (car rest))))
      (gzochi:make-serialization

       ;; Serializer wrapping.
       
       (lambda (port vec)
	 (let* ((vec-length (vector-length vec))
		(entry-count (let loop ((i 0) (entry-count 0))
			       (if (< i vec-length)
				   (loop (+ i 1)
					 (if (eqv? (vector-ref vec i) fill)
					     entry-count
					     (+ entry-count 1)))
				   entry-count))))
	   
	   (gzochi:write-integer port vec-length)
	   (gzochi:write-integer port entry-count)
	   
	   (let loop ((i 0) (entries-written 0))
	     (if (and (< i vec-length) (< entries-written entry-count))
		 (let ((v (vector-ref vec i)))
		   (if (not (eqv? v fill))
		       (begin (gzochi:write-integer port i)
			      (serializer port v)
			      (loop (+ i 1) (+ entries-written 1)))
		       (loop (+ i 1) entries-written)))))))

       ;; Deserializer wrapping.
       
       (lambda (port)
	 (let* ((vec-length (gzochi:read-integer port))
		(entry-count (gzochi:read-integer port))
		(vec (make-vector vec-length fill)))
	   (let loop ((i 0))
	     (if (< i entry-count)
		 (begin (vector-set!
			 vec (gzochi:read-integer port) (deserializer port))
			(loop (+ i 1)))
		 vec)))))))
)
