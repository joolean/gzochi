/* app.h: Prototypes and declarations for app.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_APP_H
#define GZOCHID_APP_H

#include <glib.h>
#include <glib-object.h>

#include "auth_int.h"
#include "event.h"
#include "gzochid-auth.h"
#include "metaclient.h"

enum
  {
    GZOCHID_APPLICATION_STATE_PENDING,
    GZOCIHD_APPLICATION_STATE_RESUBMITTING,
    GZOCHID_APPLICATION_STATE_STARTED
  };

/* 
   The `GzochidApplicationContext' type encapsulates the configuration and
   current state of an application hosted by the gzochid application container,
   including client connections and connections to the storage layer.
   
   Instances of this type must be constructed with an instance of 
   `GzochidConfiguration' and an instance of `GzochidApplicationDescriptor'.
   Neither of these types can be automatically constructed by the gzochid
   dependency injection framework, and so must be supplied explicitly, or via
   `gzochid_resolver_provide'.

   The following readable properties are available:

   - "app-state" returns a pointer to the application's 
   `gzochid_application_state' structure.
   - "app-stats" returns a snapshot copy of the application's 
   `gzochid_application_stats' boxed type.
   - "app-store" returns a pointer to the application's 
   `gzochid_application_store' structure.
   - "descriptor" returns a reference to the application's 
   `GzochidApplicationDescriptor' object.
   - "event-source" returns a reference to the application's 
   `gzochid_event_source' boxed `GSource' type, for dispatching
   application-specific events.
   - "metaclient-container" returns a reference to the global
    `GzochidMetaClientContainer' object.
   - "session-map" returns a pointer to the application's 
   `gzochid_application_session_map' structure, where the session mapping tables
   reside.
   - "task-queue" returns a reference to the global `GzochidTaskQueue' object.
   - "tx-timeout" returns a 64-bit unsigned integer giving the default
   transaction timeout in milliseconds.
*/

/* The core application context type definitions. */

#define GZOCHID_TYPE_APPLICATION_CONTEXT gzochid_application_context_get_type ()

G_DECLARE_FINAL_TYPE (GzochidApplicationContext, gzochid_application_context,
                      GZOCHID, APPLICATION_CONTEXT, GObject);

/* 
   Calls the specified function pointer with the specified data, making the
   specified application context and identity "current" for the duration of the
   execution of function's execution, with respect to 
   `gzochid_get_current_application_context' and `gzochid_get_current_identity'.
   
   Calls to this function may be safely nested; the current application and
   identity (if any) when this function is called will be restored when the
   function returns locally. The state of the current application and identity
   are unspecified if the target function exits non-locally (e.g., via 
   `longjmp').
*/

void *gzochid_with_application_context (GzochidApplicationContext *, 
					gzochid_auth_identity *,
					void *(*) (gpointer), 
					gpointer);

/* Returns the current application context, as established by a call to 
   `gzochid_with_application_context' further up the call stack, or `NULL' if 
   there is no current application context. */

GzochidApplicationContext *gzochid_get_current_application_context (void);

/* Returns the current identity, as established by a call to 
   `gzochid_with_application_context' further up the call stack, or `NULL' if 
   there is no current identity. */   

gzochid_auth_identity *gzochid_get_current_identity (void);

/* 
   Authenticates the principal specified by the specified byte buffer against
   the specified application's configured authentication plugin. Returns a
   pointer to a `gzochid_auth_identity' object on success; returns `NULL' and
   sets the specified `GError *' pointer on an authentication failure. 

   The `gzochid_auth_identity' pointer returned on success should be released 
   via `gzochid_auth_identity_unref' when no longer needed. 
*/

gzochid_auth_identity *gzochid_application_context_authenticate
(GzochidApplicationContext *, unsigned char *, short, GError **);

/* 
   Returns the short name of the specified application, as given by its
   descriptor. 

   The returned string is owned by the application and should not be freed or
   otherwise modified.
*/

const char *gzochid_application_context_get_name (GzochidApplicationContext *);

/* 
   Returns the GLib log domain for the specified application, as constructed by
   prepending "app/" to the application name given by the appication 
   descriptor. 

   The returned string is owned by the application and should not be freed or
   otherwise modified.
*/

const char *gzochid_application_context_get_log_domain
(GzochidApplicationContext *);

/* 
   Returns the descriptive name of the specified application, as given by its
   descriptor. 

   The returned string is owned by the application and should not be freed or
   otherwise modified.
*/

const char *gzochid_application_context_get_description
(GzochidApplicationContext *);

/* Returns a `gzochid_auth_identity' instance for the specified name using the
   specified application's identity cache. The returned identity should be
   released via `gzochid_auth_identity_unref' when no longer in use. */

gzochid_auth_identity *gzochid_application_context_resolve_identity
(GzochidApplicationContext *, char *);

/* Returns an instance of the metaclient if this application node is connected
   to a metaserver, `NULL' otherwise. When this function returns a pointer to
   a `GzochidMetaClient' object, the object should be released via 
   `g_object_unref' when no longer in use. */

GzochidMetaClient *gzochid_application_context_get_metaclient
(GzochidApplicationContext *);

#endif /* GZOCHID_APP_H */
