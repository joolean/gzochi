/* taskmap-mem.c: In-memory taskmap implementation for gzochi-metad
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "taskmap.h"
#include "taskmap-mem.h"

/*
  The functions and data structures defined below form an implementation of
  `gzochi_metad_taskmap' in which the task-to-server mappings are held in
  memory. As such, it may be used in gzochi server cluster configurations in 
  which there is only a single meta server.
  
  This implementation is not thread-safe; only a single thread should interact
  with the map at a time.
*/

/* Memory-backed nodemap struct definition. */

struct _gzochi_metad_application_taskmap_mem
{
  GHashTable *task_to_node; /* Map of `guint64 *' -> `int *'. */

  /* Map of `int *' to a `GSequence *' of `guint64 *'. */
  
  GHashTable *node_to_tasks;

  /* Sorted set of unmapped tasks (as `guint64' pointers). */
  
  GSequence *unmapped_tasks;
};

typedef struct _gzochi_metad_application_taskmap_mem
gzochi_metad_application_taskmap_mem;

struct _gzochi_metad_taskmap_mem
{
  gzochi_metad_taskmap base; /* The base taskmap type. */

  /* Map of `char *' to `gzochid_metad_application_taskmap_mem *'. */
  
  GHashTable *application_taskmaps;
};

typedef struct _gzochi_metad_taskmap_mem gzochi_metad_taskmap_mem;

/* Construct a return a new `gzochi_metad_application_taskmap_mem'. The memory
   used by this object should be freed via 
   `gzochi_metad_application_taskmap_mem_free' when no longer in use. */

gzochi_metad_application_taskmap_mem *
gzochi_metad_application_taskmap_mem_new (void)
{
  gzochi_metad_application_taskmap_mem *taskmap =
    malloc (sizeof (gzochi_metad_application_taskmap_mem));

  taskmap->task_to_node = g_hash_table_new_full
    (g_int64_hash, g_int64_equal, free, free);
  taskmap->node_to_tasks = g_hash_table_new_full
    (g_int_hash, g_int_equal, free, (GDestroyNotify) g_sequence_free);
  taskmap->unmapped_tasks = g_sequence_new (free);
  
  return taskmap;
}

/* Frees the specified `gzochi_metad_application_taskmap_mem. */

void gzochi_metad_application_taskmap_mem_free
(gzochi_metad_application_taskmap_mem *taskmap)
{
  g_hash_table_destroy (taskmap->task_to_node);
  g_hash_table_destroy (taskmap->node_to_tasks);
  g_sequence_free (taskmap->unmapped_tasks);

  free (taskmap);
}

/* Returns a heap-allocated pointer to a four-byte buffer containing the 
   specified int value. The buffer should be freed via `free' when no longer in
   use. */

static inline int *
copy_int (const int v)
{
  return g_memdup (&v, sizeof (int));
}

/* Returns a heap-allocated pointer to an eight-byte buffer containing the 
   specified guint64 value. The buffer should be freed via `free' when no longer
   in use. */

static inline guint64 *
copy_int64 (const guint64 v)
{
  return g_memdup (&v, sizeof (guint64));
}

/* A `GCompareDataFunc' for guint64s. */

static gint
compare_int64_data_func (gconstpointer a_ptr, gconstpointer b_ptr,
			 gpointer user_data)
{
  const guint64 a = *((guint64 *) a_ptr);
  const guint64 b = *((guint64 *) b_ptr);

  return a < b ? -1 : b < a ? 1 : 0;
}

/* The `map_task' implementation for the `gzochi_metad_taskmap_mem' 
   functional interface. */

static void
map_task (gzochi_metad_taskmap *taskmap, const char *app, guint64 task_id,
	  int node_id, GError **err)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = NULL;

  if (g_hash_table_contains (taskmap_mem->application_taskmaps, app))
    app_taskmap = g_hash_table_lookup (taskmap_mem->application_taskmaps, app);
  else
    {
      app_taskmap = gzochi_metad_application_taskmap_mem_new ();
      g_hash_table_insert (taskmap_mem->application_taskmaps, strdup (app),
			   app_taskmap);
    }

  if (g_hash_table_contains (app_taskmap->task_to_node, &task_id))
    
    g_set_error
      (err, GZOCHI_METAD_TASKMAP_ERROR,
       GZOCHI_METAD_TASKMAP_ERROR_ALREADY_MAPPED,
       "Mapping for task already exists.");
  else
    {
      GSequence *tasks = NULL;
      GSequenceIter *unmapped_task_iter = g_sequence_lookup
	(app_taskmap->unmapped_tasks, &task_id, compare_int64_data_func, NULL);
      
      /* Is there a task sequence already mapped to the target node id? */
      
      if (g_hash_table_contains (app_taskmap->node_to_tasks, &node_id))
	tasks = g_hash_table_lookup (app_taskmap->node_to_tasks, &node_id);
      else
	{
	  tasks = g_sequence_new (free);
	  g_hash_table_insert (app_taskmap->node_to_tasks, copy_int (node_id),
			       tasks);
	}
      
      g_sequence_insert_sorted
	(tasks, copy_int64 (task_id), compare_int64_data_func, NULL);
      
      g_hash_table_insert
	(app_taskmap->task_to_node, copy_int64 (task_id), copy_int (node_id));

      if (unmapped_task_iter != NULL)
	g_sequence_remove (unmapped_task_iter);
    }
}

static gboolean
unmap_task_inner (gzochi_metad_application_taskmap_mem *app_taskmap,
		  guint64 task_id)
{
  if (g_hash_table_contains (app_taskmap->task_to_node, &task_id))
    {
      int *node_id = g_hash_table_lookup (app_taskmap->task_to_node, &task_id);
      GSequence *tasks = g_hash_table_lookup
	(app_taskmap->node_to_tasks, node_id);
      
      g_sequence_remove
	(g_sequence_lookup (tasks, &task_id, compare_int64_data_func, NULL));
      
      /* If the task sequence is now empty, remove it from the map. */
      
      if (g_sequence_get_begin_iter (tasks) == g_sequence_get_end_iter (tasks))
	g_hash_table_remove (app_taskmap->node_to_tasks, node_id);
      
      g_hash_table_remove (app_taskmap->task_to_node, &task_id);

      /* Note that we don't remove the application taskmap object, since 
	 creating and tearing down `GHashTable' instances is not that cheap, and
	 it would likely happen a lot as task counts flip between 0 and 1. */
      
      return TRUE;
    }
  else return FALSE;
}

/* The `unmap_task' implementation for the `gzochi_metad_taskmap_mem' 
   functional interface. */

static void
unmap_task (gzochi_metad_taskmap *taskmap, const char *app, guint64 task_id,
	    GError **err)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = g_hash_table_lookup
    (taskmap_mem->application_taskmaps, app);

  if (app_taskmap != NULL && unmap_task_inner (app_taskmap, task_id))
    g_sequence_insert_sorted
      (app_taskmap->unmapped_tasks, copy_int64 (task_id),
       compare_int64_data_func, NULL);

  else g_set_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		    GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED,
		    "No mapping for task exists.");
}

/* The `unmap_task' implementation for the `gzochi_metad_taskmap_mem' 
   functional interface. */

static void
remove_task (gzochi_metad_taskmap *taskmap, const char *app, guint64 task_id,
	     GError **err)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = g_hash_table_lookup
    (taskmap_mem->application_taskmaps, app);

  if (app_taskmap != NULL)
    {
      if (!unmap_task_inner (app_taskmap, task_id))
	{
	  GSequenceIter *unmapped_task_iter = g_sequence_lookup
	    (app_taskmap->unmapped_tasks, &task_id, compare_int64_data_func,
	     NULL);

	  if (unmapped_task_iter != NULL)
	    g_sequence_remove (unmapped_task_iter);
	  else g_set_error (err, GZOCHI_METAD_TASKMAP_ERROR,
			    GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED,
			    "No mapping for task exists.");
	}
    }
  else g_set_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		    GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED,
		    "No mapping for task exists.");
}

/* The `lookup_node' implementation for the `gzochi_metad_taskmap_mem'
   functional interface. */

static int
lookup_node (gzochi_metad_taskmap *taskmap, const char *app, guint64 task_id,
	     GError **err)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = g_hash_table_lookup
    (taskmap_mem->application_taskmaps, app);

  if (app_taskmap != NULL
      && g_hash_table_contains (app_taskmap->task_to_node, &task_id))
    {    
      int *node_id = g_hash_table_lookup (app_taskmap->task_to_node, &task_id);
      return *node_id;
    }
  else
    {
      g_set_error (err, GZOCHI_METAD_TASKMAP_ERROR,
		   GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED,
		   "No mapping for task exists.");
      return -1;
    }
}

/* Convenience function to copy the contents of a `GSequence' of 64-bit integers
   into a new `GArray'. Used to take "snapshots" of various task id 
   sequences. */

static GArray *
int64_sequence_to_array (GSequence *sequence)
{
  GArray *ret = g_array_sized_new
    (FALSE, FALSE, sizeof (guint64), g_sequence_get_length (sequence));
  GSequenceIter *iter = g_sequence_get_begin_iter (sequence);
  int i = 0;

  while (! g_sequence_iter_is_end (iter))
    {
      guint64 *task_id = g_sequence_get (iter);
      
      g_array_insert_val (ret, i++, *task_id);
      iter = g_sequence_iter_next (iter);
    }
  
  return ret;      
}

/* The `lookup_mapped_tasks' implementation for the `gzochi_metad_taskmap_mem' 
   functional interface. */

static GArray *
lookup_mapped_tasks (gzochi_metad_taskmap *taskmap, const char *app,
		     int node_id)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = g_hash_table_lookup
    (taskmap_mem->application_taskmaps, app);

  if (app_taskmap == NULL ||
      ! g_hash_table_contains (app_taskmap->node_to_tasks, &node_id))
    return g_array_new (FALSE, FALSE, sizeof (guint64));
  else return int64_sequence_to_array
	 (g_hash_table_lookup (app_taskmap->node_to_tasks, &node_id));
}

/* The `lookup_unmapped_tasks' implementation for the 
   `gzochi_metad_taskmap_mem' functional interface. */

static GArray *
lookup_unmapped_tasks (gzochi_metad_taskmap *taskmap, const char *app)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;
  gzochi_metad_application_taskmap_mem *app_taskmap = g_hash_table_lookup
    (taskmap_mem->application_taskmaps, app);

  if (app_taskmap == NULL)
    return g_array_new (FALSE, FALSE, sizeof (guint64));
  else return int64_sequence_to_array (app_taskmap->unmapped_tasks);
}

/* A `GFunc' implementation to remove a task-to-node mapping when a task id 
   is removed from a node's task sequence. */

static void
unmap_task_func (gpointer data, gpointer user_data)
{
  gzochi_metad_application_taskmap_mem *app_taskmap = user_data;

  g_sequence_insert_sorted
    (app_taskmap->unmapped_tasks, g_memdup (data, sizeof (guint64)),
     compare_int64_data_func, NULL);
  g_hash_table_remove (app_taskmap->task_to_node, data);
}

/* A `GHFunc' implementation for unmapping all tasks assigned to each node
   participating in an application deployment. */

static void
unmap_app_ghfunc (gpointer key, gpointer value, gpointer user_data)
{
  gzochi_metad_application_taskmap_mem *app_taskmap = value;
  GSequence *tasks = g_hash_table_lookup
    (app_taskmap->node_to_tasks, user_data);

  if (tasks != NULL)
    {
      g_sequence_foreach (tasks, unmap_task_func, app_taskmap);      
      g_hash_table_remove (app_taskmap->node_to_tasks, user_data);
    }
}

/* The `unmap_all' implementation for the `gzochi_metad_taskmap_mem' 
   functional interface. */

static void
unmap_all (gzochi_metad_taskmap *taskmap, int node_id)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;

  g_hash_table_foreach
    (taskmap_mem->application_taskmaps, unmap_app_ghfunc, &node_id);
}

static gzochi_metad_taskmap_iface mem_iface =
  {
    map_task,
    unmap_task,
    remove_task,
    lookup_node,
    lookup_mapped_tasks,
    lookup_unmapped_tasks,
    unmap_all
  };

gzochi_metad_taskmap *
gzochi_metad_taskmap_mem_new ()
{
  gzochi_metad_taskmap_mem *taskmap =
    malloc (sizeof (gzochi_metad_taskmap_mem));

  taskmap->base.iface = &mem_iface;
  
  taskmap->application_taskmaps = g_hash_table_new_full
    (g_str_hash, g_str_equal,
     free, (GDestroyNotify) gzochi_metad_application_taskmap_mem_free);

  return (gzochi_metad_taskmap *) taskmap;
}

void
gzochi_metad_taskmap_mem_free (gzochi_metad_taskmap *taskmap)
{
  gzochi_metad_taskmap_mem *taskmap_mem = (gzochi_metad_taskmap_mem *) taskmap;

  g_hash_table_destroy (taskmap_mem->application_taskmaps);

  free (taskmap);
}

GQuark
gzochi_metad_taskmap_error_quark ()
{
  return g_quark_from_static_string ("gzochi-metad-taskmap-error-quark");
}
