/* taskserver.c: Task server for gzochi-metad
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <gzochi-common.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "appstate-mem.h"
#include "appstate.h"
#include "event.h"
#include "event-meta.h"
#include "log.h"
#include "meta-protocol.h"
#include "socket.h"
#include "taskmap-mem.h"
#include "taskmap.h"
#include "taskserver.h"

#ifdef G_LOG_DOMAIN
#undef G_LOG_DOMAIN
#endif /* G_LOG_DOMAIN */
#define G_LOG_DOMAIN "gzochi-metad.taskserver"

/* A container for taskserver client state. */

struct _connected_server_state
{
  int node_id; /* The application server node id. */
  gzochid_client_socket *socket; /* The associated client socket. */

  /* A list of names of the applications the client has attempted to start. */

  GList *started_applications; 

  /* Sparse mapping of application name to resubmitted task buffer, in the form
     of a `GArray *' of `guint64'. */
  
  GHashTable *resubmitted_tasks;
};

typedef struct _connected_server_state connected_server_state;

/* Construct and return a new `connected_server_state' with the specified node
   id and client socket. The returned pointer should be freed via 
   `connected_server_state_free' when no longer needed. */

static connected_server_state *
connected_server_state_new (int node_id, gzochid_client_socket *socket)
{
  connected_server_state *state = malloc (sizeof (connected_server_state));

  state->node_id = node_id;
  state->socket = socket;
  state->started_applications = NULL;
  state->resubmitted_tasks = g_hash_table_new_full
    (g_str_hash, g_str_equal, free, (GDestroyNotify) g_array_unref);
  
  return state;
}

/* Frees the memory used by the specified `connected_server_state'. */

static void
connected_server_state_free (connected_server_state *state)
{
  g_list_free_full (state->started_applications, free);
  g_hash_table_destroy (state->resubmitted_tasks);
  
  free (state);
}

/* Boilerplate setup for the task server object. */

/* The task server object. */

struct _GzochiMetadTaskServer
{
  GObject parent; /* The parent struct, for casting. */

  gzochi_metad_taskmap *taskmap; /* The task-to-node map. */
  gzochi_metad_appstate *appstate; /* The application state tracker. */
  
  /* Mapping of node id -> `connected_server_state *'. */
  
  GHashTable *connected_servers;

  gzochid_event_source *event_source; /* Event source for task events. */
};

G_DEFINE_TYPE (GzochiMetadTaskServer, gzochi_metad_task_server, G_TYPE_OBJECT);

enum gzochi_metad_task_server_properties
  {
    PROP_EVENT_LOOP = 1,
    PROP_EVENT_SOURCE,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
gzochi_metad_task_server_get_property (GObject *object, guint property_id,
				       GValue *value, GParamSpec *pspec)
{
  GzochiMetadTaskServer *self = GZOCHI_METAD_TASK_SERVER (object);

  switch (property_id)
    {
    case PROP_EVENT_SOURCE:
      g_value_set_boxed (value, self->event_source);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochi_metad_task_server_set_property (GObject *object, guint property_id,
				       const GValue *value, GParamSpec *pspec)
{
  GzochiMetadTaskServer *self = GZOCHI_METAD_TASK_SERVER (object);

  switch (property_id)
    {
    case PROP_EVENT_LOOP:

      /* Don't need to store a reference to the event loop, just attach the
	 event source to it. */

      gzochid_event_source_attach
	(g_value_get_object (value), self->event_source);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochi_metad_task_server_init (GzochiMetadTaskServer *self)
{
  self->taskmap = gzochi_metad_taskmap_mem_new ();
  self->appstate = gzochi_metad_appstate_mem_new ();

  /* Don't install a value destroy notification function, since it's convenient
     to be able to remove a connected server before freeing it. */
  
  self->connected_servers = g_hash_table_new_full
    (g_int_hash, g_int_equal, free, NULL);

  self->event_source = gzochid_event_source_new ();
}

/* A `GHFunc' implementation to use for invoking `connected_server_state_free'
   during finalization. */

static void
connected_server_state_free_ghfunc (gpointer key, gpointer value,
				    gpointer user_data)
{
  connected_server_state_free (value);
}

static void
gzochi_metad_task_server_finalize (GObject *gobject)
{
  GzochiMetadTaskServer *server = GZOCHI_METAD_TASK_SERVER (gobject);

  gzochi_metad_taskmap_mem_free (server->taskmap);
  gzochi_metad_appstate_mem_free (server->appstate);

  /* Free all of the `connected_server_state' objects. */
  
  g_hash_table_foreach (server->connected_servers,
			connected_server_state_free_ghfunc, NULL);
  
  g_hash_table_destroy (server->connected_servers);
  
  g_source_destroy ((GSource *) server->event_source);
  g_source_unref ((GSource *) server->event_source);

  G_OBJECT_CLASS (gzochi_metad_task_server_parent_class)->finalize (gobject);
}

static void
gzochi_metad_task_server_class_init (GzochiMetadTaskServerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gzochi_metad_task_server_finalize;
  object_class->get_property = gzochi_metad_task_server_get_property;
  object_class->set_property = gzochi_metad_task_server_set_property;

  obj_properties[PROP_EVENT_LOOP] = g_param_spec_object
    ("event-loop", "event-loop", "The global event loop",
     GZOCHID_TYPE_EVENT_LOOP, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  obj_properties[PROP_EVENT_SOURCE] = g_param_spec_boxed
    ("event-source", "event-source", "The task server event source",
     G_TYPE_SOURCE, G_PARAM_READABLE);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

/* Sends a `GZOCHID_TASK_PROTOCOL_RESUBMIT_TASKS' message for the specified
   application over the specified `gzochid_client_socket'. */

static void
send_resubmit_tasks (gzochid_client_socket *sock, const char *app)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length prefix, plus 
     opcode. */
  
  size_t total_len = app_len + 4;
  unsigned char *buf = malloc (sizeof (unsigned char) * total_len);
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_RESUBMIT_TASKS;
  memcpy (buf + 3, app, app_len + 1);
  
  gzochid_client_socket_write (sock, buf, total_len);
  
  free (buf);
}

/* Sends a `GZOCHID_TASK_PROTOCOL_APP_STARTED' message for the specified
   application over the specified `gzochid_client_socket'. */

static void
send_app_started (gzochid_client_socket *sock, const char *app)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length prefix, plus 
     opcode. */
  
  size_t total_len = app_len + 4;
  unsigned char *buf = malloc (sizeof (unsigned char) * total_len);
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_APP_STARTED;
  memcpy (buf + 3, app, app_len + 1);
  
  gzochid_client_socket_write (sock, buf, total_len);
  
  free (buf);
}

/* Sends a `GZOCHID_TASK_PROTOCOL_ASSIGN_TASK' message for the specified
   application-qualified task id over the specified `gzochid_client_socket'. */

static void
send_assign_task (gzochid_client_socket *sock, const char *app, guint64 task_id)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length
     prefix, plus opcode, plus 8-byte task oid. */
  
  size_t total_len = app_len + 12;
  unsigned char *buf = malloc (sizeof (unsigned char) * (app_len + 12));
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_ASSIGN_TASK;
  memcpy (buf + 3, app, app_len + 1);
  gzochi_common_io_write_long (task_id, buf, app_len + 4);
  
  gzochid_client_socket_write (sock, buf, total_len);
  
  free (buf);
}

/* Sends a `GZOCHID_TASK_PROTOCOL_UNASSIGN_TASK' message for the specified
   application-qualified task id over the specified `gzochid_client_socket'. */

static void
send_unassign_task (gzochid_client_socket *sock, const char *app,
		    guint64 task_id)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length
     prefix, plus opcode, plus 8-byte task oid. */
  
  size_t total_len = app_len + 12;
  unsigned char *buf = malloc (sizeof (unsigned char) * (app_len + 12));
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_UNASSIGN_TASK;
  memcpy (buf + 3, app, app_len + 1);
  gzochi_common_io_write_long (task_id, buf, app_len + 4);
  
  gzochid_client_socket_write (sock, buf, total_len);
  
  free (buf);
}

/* Translates the specified `GError' from the taskmap API (which must be
   non-`NULL' and have error domain `GZOCHI_METAD_TASKMAP_ERROR') to an
   equivalent `GError' in the `GZOCHI_METAD_TASKSERVER_ERROR' domain, setting
   the specified output error object if it is non-`NULL'. Like 
   `g_propagate_error', this function frees the input error. */

static void
propagate_taskmap_error (GError **err, GError *taskmap_err)
{
  assert (taskmap_err != NULL);
  assert (taskmap_err->domain == GZOCHI_METAD_TASKMAP_ERROR);

  int taskserver_code;
  
  switch (taskmap_err->code)
    {
    case GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED:
    case GZOCHI_METAD_TASKMAP_ERROR_ALREADY_MAPPED:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_WRONG_MAPPING;
      break;
    default: taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_FAILED;
    }

  g_set_error
    (err, GZOCHI_METAD_TASKSERVER_ERROR, taskserver_code,
     "Task mapping error: %s", taskmap_err->message);

  g_error_free (taskmap_err);
}

/* Translates the specified `GError' from the appstate API (which must be
   non-`NULL' and have error domain `GZOCHI_METAD_APPSTATE_ERROR') to an
   equivalent `GError' in the `GZOCHI_METAD_TASKSERVER_ERROR' domain, setting
   the specified output error object if it is non-`NULL'. Like 
   `g_propagate_error', this function frees the input error. */

static void
propagate_appstate_error (GError **err, GError *appstate_err)
{
  assert (appstate_err != NULL);
  assert (appstate_err->domain == GZOCHI_METAD_APPSTATE_ERROR);

  int taskserver_code;
  
  switch (appstate_err->code)
    {
    case GZOCHI_METAD_APPSTATE_ERROR_ALREADY_RESUBMITTING:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE;
      break;
    case GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE;
      break;
    case GZOCHI_METAD_APPSTATE_ERROR_ALREADY_STARTED:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE;
      break;
    case GZOCHI_METAD_APPSTATE_ERROR_ALREADY_PARTICIPATING:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_ALREADY_CONNECTED;
      break;      
    case GZOCHI_METAD_APPSTATE_ERROR_NOT_PARTICIPATING:
      taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_NOT_CONNECTED;
      break;
      
    default: taskserver_code = GZOCHI_METAD_TASKSERVER_ERROR_FAILED;
    }

  g_set_error
    (err, GZOCHI_METAD_TASKSERVER_ERROR, taskserver_code,
     "Application state error: %s", appstate_err->message);

  g_error_free (appstate_err);
}

/* Convenience function for mapping a task to a specified application server
   node and sending a message to notify that node of the assignment. */

static void
assign_and_notify (GzochiMetadTaskServer *taskserver,
		   connected_server_state *server_state, const char *app,
		   guint64 task_id)
{
  gzochi_metad_taskmap_iface *taskmap_iface =
    GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
  GError *taskmap_err = NULL;

  gzochid_trace ("Assigning task %" G_GUINT64_FORMAT " to node %d/%s.",
		 task_id, server_state->node_id, app);
  
  taskmap_iface->map_task
    (taskserver->taskmap, app, task_id, server_state->node_id, &taskmap_err);
  
  assert (taskmap_err == NULL);

  send_assign_task (server_state->socket, app, task_id);
  gzochid_event_dispatch
    (taskserver->event_source,
     g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
		   "application", app,
		   "node-id", server_state->node_id,
		   "type", TASK_ASSIGNED,
		   NULL));
}

/* Returns `TRUE' if the specified application server has requested to start 
   the specified application, `FALSE' otherwise. */

static inline gboolean
has_started_application (connected_server_state *server_state, const char *app)
{
  return g_list_find_custom
    (server_state->started_applications, app, (GCompareFunc) strcmp) != NULL;
}

/* Returns `TRUE' if the specified application has reached the 
   `GZOCHI_METAD_APPLICATION_STATE_STARTED' state, `FALSE' otherwise. */

static inline gboolean
is_application_started (GzochiMetadTaskServer *taskserver, const char *app)
{
  gzochi_metad_appstate_iface *iface =
    GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);

  return iface->lookup_application_state (taskserver->appstate, app) ==
    GZOCHI_METAD_APPLICATION_STATE_STARTED;
}

void
gzochi_metad_taskserver_application_started (GzochiMetadTaskServer *taskserver,
					     int node_id, const char *app,
					     GError **err)
{
  gzochi_metad_appstate_iface *appstate_iface =
    GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);
  gzochi_metad_application_state application_state =
    appstate_iface->lookup_application_state (taskserver->appstate, app);
  connected_server_state *server_state = g_hash_table_lookup
    (taskserver->connected_servers, &node_id);
  
  g_debug ("Node %d requested to start application %s.", node_id, app);

  if (has_started_application (server_state, app))
    g_debug
      ("Received duplicate 'start' request from node %d for application %s.",
       node_id, app);
  else server_state->started_applications = g_list_prepend
	 (server_state->started_applications, strdup (app));

  /* Is this the first time the taskserver has heard about this application? *
     Do its tasks need to be resubmitted? */
  
  if (application_state == GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER)
    {
      GError *local_err = NULL;

      g_info ("Nominating node %d to resubmit tasks for application %s.",
	      node_id, app);
      
      appstate_iface->begin_resubmission
	(taskserver->appstate, app, node_id, &local_err);

      if (local_err == NULL)
	send_resubmit_tasks (server_state->socket, app);
      else propagate_appstate_error (err, local_err);
    }
  else if (application_state == GZOCHI_METAD_APPLICATION_STATE_STARTED)
    {
      gzochi_metad_taskmap_iface *taskmap_iface =
	GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
      GArray *unmapped_tasks = taskmap_iface->lookup_unmapped_tasks
	(taskserver->taskmap, app);
      int i = 0;
      
      g_debug ("Notifying node %d to join already-started application %s.",
	       node_id, app);

      send_app_started (server_state->socket, app);

      /* If there are any unassigned tasks available, assign them to the new
	 node. */
      
      for (; i < unmapped_tasks->len; i++)
	assign_and_notify (taskserver, server_state, app,
			   g_array_index (unmapped_tasks, guint64, i));

      g_array_unref (unmapped_tasks);
    }
  else
    {
      int resubmission_leader_id = appstate_iface->lookup_resubmission_leader
	(taskserver->appstate, app, NULL);

      /* If the server should be resubmitting but is instead asking to start the
	 application, something's wrong. */
      
      if (resubmission_leader_id == node_id)
	g_set_error
	  (err, GZOCHI_METAD_TASKSERVER_ERROR,
	   GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE,
	   "Resubmission leader %d sent duplicate 'start' request for "
	   "application %s.", node_id, app);
    }
}

/* A `GHFunc' implementation for sending a `GZOCHID_TASK_PROTOCOL_APP_STARTED'
   message to each connected server. */

void send_app_started_ghfunc (gpointer key, gpointer value, gpointer user_data)
{
  connected_server_state *server_state = value;
  
  send_app_started (server_state->socket, (const char *) user_data);
}

/* Translates the specified `GError' from the taskmap API (which must be
   non-`NULL' and have error domain `GZOCHI_METAD_TASKMAP_ERROR') to an
   equivalent `GError' in the `GZOCHI_METAD_TASKSERVER_ERROR' domain, setting
   the specified output error object if it is non-`NULL'. Like 
   `g_propagate_error', this function frees the input error. */

void
gzochi_metad_taskserver_application_resubmitted
(GzochiMetadTaskServer *taskserver, int node_id, const char *app, GError **err)
{
  gzochi_metad_appstate_iface *appstate_iface =
    GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);
  gzochi_metad_application_state application_state =
    appstate_iface->lookup_application_state (taskserver->appstate, app);
  connected_server_state *server_state = g_hash_table_lookup
    (taskserver->connected_servers, &node_id);
  
  /* Is the specified application currently in the resubmission state? */

  if (application_state == GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING)
    {
      GError *appstate_err = NULL;
      
      /* Is the specified node the resubmission leader? */

      int resubmission_leader_id = appstate_iface->lookup_resubmission_leader
	(taskserver->appstate, app, &appstate_err);

      assert (appstate_err == NULL);

      if (node_id == resubmission_leader_id)
	{
	  GArray *resubmitted_tasks = g_hash_table_lookup
	    (server_state->resubmitted_tasks, app);

	  if (resubmitted_tasks != NULL && resubmitted_tasks->len > 0)
	    {
	      int i = 0;
	      GError *taskmap_err = NULL;
	      gzochi_metad_taskmap_iface *taskmap_iface =
		GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
	      
	      /* Map all buffered tasks to resubmission leader. */
	      
	      for (; i < resubmitted_tasks->len; i++)
		{
		  guint64 task_id = g_array_index
		    (resubmitted_tasks, guint64, i);

		  taskmap_iface->map_task (taskserver->taskmap, app, task_id,
					   node_id, &taskmap_err);

		  if (taskmap_err != NULL)
		    {
		      propagate_taskmap_error (err, taskmap_err);
		      g_hash_table_remove
			(server_state->resubmitted_tasks, app);

		      /* TODO: Now what? Maybe choose a new leader and try 
			 again? */
		  
		      return;
		    }

		  /* Dispatch both the `submitted' and `assigned' events; we're
		     kind of retroactively acknowledging all of the tasks 
		     submitted by the resubmission leader once it completes
		     resubmission. */
		  
		  gzochid_event_dispatch
		    (taskserver->event_source,
		     g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
				   "application", app,
				   "node-id", node_id,
				   "type", TASK_SUBMITTED,
				   NULL));
		  
		  gzochid_event_dispatch
		    (taskserver->event_source,
		     g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
				   "application", app,
				   "node-id", node_id,
				   "type", TASK_ASSIGNED,
				   NULL));
		}

	      g_info ("Application %s started by node %d after resubmitting "
		       "%d task(s).", app, node_id, resubmitted_tasks->len);
	      
	      g_hash_table_remove (server_state->resubmitted_tasks, app);
	    }
	  else g_info ("Application %s started by node %d with no resubmitted "
		       "tasks.", app, node_id);

	  appstate_iface->start_application
	    (taskserver->appstate, app, &appstate_err);

	  if (appstate_err != NULL)
	    {
	      propagate_appstate_error (err, appstate_err);

	      /* TODO: Now what? Maybe force the app into an error state? */
	      
	      return;
	    }
	  
	  /* Notify all nodes that they can start. */

	  g_hash_table_foreach (taskserver->connected_servers,
				send_app_started_ghfunc, (char *) app);
	}
      else g_set_error
	     (err, GZOCHI_METAD_TASKSERVER_ERROR,
	      GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE,
	      "Node %d attempted to complete resubmission for application %s, "
	      "but it is not the leader.", node_id, app);
    }
  else g_set_error
	 (err, GZOCHI_METAD_TASKSERVER_ERROR,
	  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE,
	  "Node %d attempted to complete resubmission for application %s, "
	  "but it is not accepting resubmissions.", node_id, app);
}

void
gzochi_metad_taskserver_server_connected (GzochiMetadTaskServer *taskserver,
					  int node_id,
					  gzochid_client_socket *sock,
					  GError **err)
{
  if (g_hash_table_contains (taskserver->connected_servers, &node_id))
    g_set_error
      (err, GZOCHI_METAD_TASKSERVER_ERROR,
       GZOCHI_METAD_TASKSERVER_ERROR_ALREADY_CONNECTED,
       "Node with id %d is already connected.", node_id);

  else g_hash_table_insert
	 (taskserver->connected_servers, g_memdup (&node_id, sizeof (int)),
	  connected_server_state_new (node_id, sock));
}

/* Hold an election for a new task resubmission leader for the specified
   application. */

static void
reelect_leader (GzochiMetadTaskServer *taskserver, const char *app)
{
  GList *connected_server_states = g_hash_table_get_values
    (taskserver->connected_servers);
  GList *connected_server_states_ptr = connected_server_states;
  gboolean new_leader = FALSE;
  
  while (connected_server_states_ptr != NULL)
    {
      connected_server_state *server_state = connected_server_states_ptr->data;

      /* Take the first connected application server that has requested to
	 start the target application and make it the new leader. */
      
      if (has_started_application (server_state, app))
	{
	  g_debug ("Electing node %d as new resubmisison leader for %s.",
		   server_state->node_id, app);
	  
	  send_resubmit_tasks (server_state->socket, app);
	  new_leader = TRUE;
	  break;
	}
      else connected_server_states_ptr = connected_server_states_ptr->next;
    }

  /* If there are no such application servers, transition the application out
     of the `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state and wait for a
     suitable application server to appear. */
  
  if (!new_leader)
    {
      GError *appstate_err = NULL;
      gzochi_metad_appstate_iface *appstate_iface =
	GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);

      g_debug ("No eligible leaders for %s; resubmission canceled.", app);
      
      appstate_iface->cancel_resubmission
	(taskserver->appstate, app, &appstate_err);

      assert (appstate_err == NULL);
    }

  g_list_free (connected_server_states);
}

/* A `GFunc' implementation to hold a task resubmission leader election for each
   application (from a disconnecting server's list of started applications) in 
   the `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state. */

static void
reelect_leader_gfunc (gpointer data, gpointer user_data)
{
  const char *app = data;
  GzochiMetadTaskServer *taskserver = user_data;
  
  gzochi_metad_appstate_iface *appstate_iface =
    GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);
  
  if (appstate_iface->lookup_application_state (taskserver->appstate, app) ==
      GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING)
    reelect_leader (taskserver, app);
}

/* Attempts to assign all tasks from an application's "unmapped" task set across
   the set of servers that have requested to start that application. */

static void
assign_unmapped_tasks (GzochiMetadTaskServer *taskserver, const char *app)
{
  GList *connected_server_states = g_hash_table_get_values
    (taskserver->connected_servers);
  GList *connected_server_states_ptr = connected_server_states;
  GList *application_servers = NULL;

  assert (is_application_started (taskserver, app));

  /* First, identify all the servers that have requested to start the specified
     application. */
  
  while (connected_server_states_ptr != NULL)
    {
      connected_server_state *server_state = connected_server_states_ptr->data;
      
      if (has_started_application (server_state, app))
	application_servers = g_list_prepend
	  (application_servers, server_state);
      
      connected_server_states_ptr = connected_server_states_ptr->next;
    }

  g_debug ("Redistributing unmapped task for %s across %d server(s).",
	   app, g_list_length (application_servers));
  
  /* Then round-robin the task assignment over those servers until all tasks
     have been assigned. */
  
  if (application_servers != NULL)
    {
      GList *application_server_ptr = application_servers;
      gzochi_metad_taskmap_iface *taskmap_iface =
        GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);      
      GArray *unmapped_tasks = taskmap_iface->lookup_unmapped_tasks
	(taskserver->taskmap, app);
      int i = 0;

      for (; i < unmapped_tasks->len; i++)
	{
	  guint64 task_id = g_array_index (unmapped_tasks, guint64, i);
	  connected_server_state *server_state = application_server_ptr->data;

	  assign_and_notify (taskserver, server_state, app, task_id);

	  /* Start at the beginning of the matching server list once we hit the
	     end. */
	  
	  application_server_ptr = application_server_ptr->next;
	  if (application_server_ptr == NULL)
	    application_server_ptr = application_servers;
	}

      g_array_unref (unmapped_tasks);
    }

  g_list_free (connected_server_states);
  g_list_free (application_servers);
}

/* A `GFunc' implementation to use for invoking `assign_unmap_tasks' over each
   application requested to start by a disconnecting server. */

static void
assign_unmapped_tasks_gfunc (gpointer data, gpointer user_data)
{
  const char *app = data;
  GzochiMetadTaskServer *taskserver = user_data;
  
  if (is_application_started (taskserver, app))
    assign_unmapped_tasks (taskserver, app);
}

void
gzochi_metad_taskserver_server_disconnected (GzochiMetadTaskServer *taskserver,
					     int node_id, GError **err)
{
  if (g_hash_table_contains (taskserver->connected_servers, &node_id))
    {
      gzochi_metad_taskmap_iface *taskmap_iface =
        GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
      connected_server_state *server_state = g_hash_table_lookup
	(taskserver->connected_servers, &node_id);
      
      /* Unmap all the tasks mapped to the disconnected server. */

      gzochid_trace
	("Unmapping all tasks assigned to disconnected node %d.", node_id);
      
      taskmap_iface->unmap_all (taskserver->taskmap, node_id);
      g_hash_table_remove (taskserver->connected_servers, &node_id);      
      
      /* Was this server leading task resubmission for any applications? If so,
	 attempt to find a new leader. */

      g_list_foreach (server_state->started_applications, reelect_leader_gfunc,
		      taskserver);

      /* Remap all existing tasks if possible. */

      if (g_hash_table_size (taskserver->connected_servers) > 0)

	/* No point in attempting to assign tasks if there are no other
	   connected servers. (Even if there are connected servers, they might
	   not be hosting a given application.) */
	
	g_list_foreach (server_state->started_applications,
			assign_unmapped_tasks_gfunc, taskserver);
      
      connected_server_state_free (server_state);      
    }
  else g_set_error (err, GZOCHI_METAD_TASKSERVER_ERROR,
                    GZOCHI_METAD_TASKSERVER_ERROR_NOT_CONNECTED,
                    "No node with id %d connected.", node_id);
}

/* Maps the specified taks to the specified node, signaling an error if the
   operation against the taskmap fails. */

static void
submit_task (GzochiMetadTaskServer *taskserver, int node_id, const char *app,
	     guint64 task_id, GError **err)
{
  GError *taskmap_err = NULL;
  gzochi_metad_taskmap_iface *taskmap_iface =
    GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
  
  gzochid_trace ("Mapping task %s/%" G_GUINT64_FORMAT " to node %d.", app,
		 task_id, node_id);
  taskmap_iface->map_task (taskserver->taskmap, app, task_id, node_id,
			   &taskmap_err);

  if (taskmap_err != NULL)
    {
      g_debug ("Failed to map task %s/%" G_GUINT64_FORMAT " to node %d.", app,
	       task_id, node_id);
      propagate_taskmap_error (err, taskmap_err);
    }
  else gzochid_event_dispatch
	 (taskserver->event_source,
	  g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
			"application", app,
			"node-id", node_id,
			"type", TASK_SUBMITTED,
			NULL));
}

/*
  Buffers the specified task with respect to the specified node's resubmission
  queue, signaling an error if the operation against the taskmap fails. 

  All resubmitted tasks in the buffer will be mapped to the resubmitting node
  in bulk once the application is started.
*/

static void
resubmit_task (GzochiMetadTaskServer *taskserver, int node_id, const char *app,
	       guint64 task_id, GError **err)
{
  GError *appstate_err = NULL;
  gzochi_metad_appstate_iface *appstate_iface =
    GZOCHI_METAD_APPSTATE_IFACE (taskserver->appstate);

  int resubmission_leader_id = appstate_iface->lookup_resubmission_leader
    (taskserver->appstate, app, &appstate_err);

  assert (appstate_err == NULL);
  
  if (resubmission_leader_id == node_id)
    {
      connected_server_state *server_state = g_hash_table_lookup
	(taskserver->connected_servers, &node_id);
      GArray *resubmitted_tasks = g_hash_table_lookup
	(server_state->resubmitted_tasks, app);

      /* Lazily create the buffer. */
      
      if (resubmitted_tasks == NULL)
	{
	  resubmitted_tasks = g_array_new (FALSE, FALSE, sizeof (guint64));
	  g_hash_table_insert (server_state->resubmitted_tasks, strdup (app),
			       resubmitted_tasks);
	}
      
      gzochid_trace ("Buffering task %s/%" G_GUINT64_FORMAT
		     " resubmitted from node %d.", app, task_id, node_id);

      g_array_append_val (resubmitted_tasks, task_id);
    }
  else g_debug
	 ("Node %d attempted to resubmit task %s/%" G_GUINT64_FORMAT
	  " but it is not the resubmission leader.", node_id, app, task_id);
}

void
gzochi_metad_taskserver_task_submitted (GzochiMetadTaskServer *taskserver,
					int node_id, const char *app,
					guint64 task_id, GError **err)
{
  /* Task submission behaves differently if the application is in the`
     `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' vs.
     `GZOCHI_METAD_APPLICATION_STATE_STARTED'. */
  
  if (is_application_started (taskserver, app))
    submit_task (taskserver, node_id, app, task_id, err);
  else resubmit_task (taskserver, node_id, app, task_id, err);  
}
					
void
gzochi_metad_taskserver_task_completed (GzochiMetadTaskServer *taskserver,
					int node_id, const char *app,
					guint64 task_id, GError **err)
{
  if (is_application_started (taskserver, app))
    {  
      GError *local_err = NULL;
      gzochi_metad_taskmap_iface *iface =
	GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
      
      int mapped_node_id = iface->lookup_node
	(taskserver->taskmap, app, task_id, &local_err);

      /* Only nodes that were assigned a task are allowed to mark the task
	 complete. */
      
      if (mapped_node_id == node_id)
	{
	  gzochid_trace ("Unmapping task %s/%" G_GUINT64_FORMAT
			 " completed on node %d.", app, task_id, node_id);
	  iface->remove_task (taskserver->taskmap, app, task_id, &local_err);

	  if (local_err == NULL)
	    gzochid_event_dispatch
	      (taskserver->event_source,
	       g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
			     "application", app,
			     "node-id", node_id,
			     "type", TASK_COMPLETED,
			     NULL));
	}
      else g_debug ("Ignoring completion of task %s/%" G_GUINT64_FORMAT
		    " on incorrect node %d.", app, task_id, node_id);
      
      if (local_err != NULL)
	{
	  g_debug ("Failed to unmap task %s/%" G_GUINT64_FORMAT
		   " completed on node %d.", app, task_id, node_id);
	  propagate_taskmap_error (err, local_err);
	}
    }
  else g_debug ("Node %d sent a completion notification for %s/%"
		G_GUINT64_FORMAT " but the application has not started yet.",
		node_id, app, task_id);
}

void
gzochi_metad_taskserver_task_canceled (GzochiMetadTaskServer *taskserver,
				       int node_id, const char *app,
				       guint64 task_id, GError **err)
{
  if (is_application_started (taskserver, app))
    {
      GError *taskmap_err = NULL;
      gzochi_metad_taskmap_iface *taskmap_iface =
	GZOCHI_METAD_TASKMAP_IFACE (taskserver->taskmap);
      
      int mapped_node_id = taskmap_iface->lookup_node
	(taskserver->taskmap, app, task_id, &taskmap_err);
      
      if (taskmap_err == NULL)
	{
	  gzochid_trace
	    ("Removing task %" G_GUINT64_FORMAT " canceled by %d/%s.", task_id,
	     node_id, app);
	  
	  taskmap_iface->remove_task
	    (taskserver->taskmap, app, task_id, &taskmap_err);

	  if (taskmap_err == NULL)
	    {
	      connected_server_state *server_state = g_hash_table_lookup
		(taskserver->connected_servers, &mapped_node_id);
	      send_unassign_task (server_state->socket, app, task_id);

	      gzochid_event_dispatch
		(taskserver->event_source,
		 g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
			       "application", app,
			       "node-id", mapped_node_id,
			       "type", TASK_UNASSIGNED,
			       NULL));

	      gzochid_event_dispatch
		(taskserver->event_source,
		 g_object_new (GZOCHI_METAD_TYPE_TASK_EVENT,
			       "application", app,
			       "node-id", server_state->node_id,
			       "type", TASK_CANCELED,
			       NULL));
	    }
	  else propagate_taskmap_error (err, taskmap_err);
	}
      else propagate_taskmap_error (err, taskmap_err);
    }
  else g_set_error
	 (err, GZOCHI_METAD_TASKSERVER_ERROR,
	  GZOCHI_METAD_TASKSERVER_ERROR_WRONG_STATE,
	  "Node %d sent a cancellation for %s/%" G_GUINT64_FORMAT
	  " but the application has not started yet.", node_id, app, task_id);
}

GQuark
gzochi_metad_taskserver_error_quark ()
{
  return g_quark_from_static_string ("gzochi-metad-taskserver-error-quark");
}
