/* app-store.c: Application store-management routines
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>
#include <stdlib.h>

#include "app-store.h"
#include "gzochid-storage.h"
#include "oids.h"

gzochid_application_store *
gzochid_application_store_open (gzochid_storage_engine_interface *iface,
				const char *path,
				gzochid_oid_allocation_strategy *oid_strategy)
{
  gzochid_application_store *store = calloc
    (1, sizeof (gzochid_application_store));

  char *oids_db = g_strconcat (path, "/oids", NULL);
  char *names_db = g_strconcat (path, "/names", NULL);
  
  store->iface = iface;
  store->storage_context = iface->initialize ((char *) path);

  /* Failures to open either the storage environment or the store are serious
     enough that it's reasonable to exit hard, no matter who the caller is. */
  
  if (store->storage_context == NULL)
    {
      g_critical ("Failed to open storage environment in %s", path);
      exit (EXIT_FAILURE);
    }
  
  store->oids = iface->open
    (store->storage_context, oids_db, GZOCHID_STORAGE_CREATE);

  if (store->oids == NULL)
    {
      g_critical ("Failed to open store in %s", oids_db);
      exit (EXIT_FAILURE);
    }
  
  store->names = iface->open
    (store->storage_context, names_db, GZOCHID_STORAGE_CREATE);

  if (store->names == NULL)
    {
      g_critical ("Failed to open store in %s", names_db);
      exit (EXIT_FAILURE);
    }

  store->oid_strategy = oid_strategy;

  g_free (oids_db);
  g_free (names_db);
  
  return store;
}

void
gzochid_application_store_close (gzochid_application_store *store)
{
  store->iface->close_store (store->oids);
  store->iface->close_store (store->names);

  store->iface->close_context (store->storage_context);

  free (store);
}
