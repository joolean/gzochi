/* threads.h: Prototypes and declarations for threads.c
 * Copyright (C) 2019 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_THREADS_H
#define GZOCHID_THREADS_H

#include <glib.h>

/* Function typedef for a function callback with a "closure" pointer. */

typedef void (*gzochid_thread_worker) (gpointer, gpointer);

/*
  Construct and return a new `GThreadPool' instance with the specified closure
  pointer, which will be passed to each `gzochid_thread_worker' function called
  by the thread pool. The remaining arguments have the same meaning as the ones
  accepted by `g_thread_pool_new'.
*/

GThreadPool *gzochid_thread_pool_new (gpointer, gint, gboolean, GError **);

/*
  Free the specified `GThreadPool' instance, which must have been created by a
  call to `gzochid_thread_pool_new'. The arguments to this function have the
  same meaning as the ones accepted by `g_thread_pool_new'.
*/

void gzochid_thread_pool_free (GThreadPool *, gboolean, gboolean);

/*
  Submit the specified `gzochid_thread_worker' function and work data pointer
  to the specified `GThreadPool', which must have been created by a call to
  `gzochid_thread_pool_new'.
*/

void gzochid_thread_pool_push (GThreadPool *, gzochid_thread_worker, gpointer,
			       GError **);

#endif /* GZOCHID_THREADS_H */
