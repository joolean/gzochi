/* seqhash.c: Sequential hash table implementation for gzochid
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <stddef.h>
#include <stdlib.h>

#include "seqhash.h"

/* The sequential hash table struct, as a composition of a `GHashTable' and a
   `GSequence'. This implementation uses a mutex to protect its internal 
   consistency; callers may require additional synchronization to support 
   consistent access across more than one function call. */

struct _gzochid_seq_hash_table
{
  GCompareFunc key_compare_func; /* The key comparison function. */
  
  /* Mutex to ensure consistent access between the table and the sequence. */
  
  GMutex mutex; 

  /* Sequence of keys ordered by the comparison function. */

  GSequence *key_sequence; 

  GHashTable *table; /* The key-value mapping. */
};

gzochid_seq_hash_table *
gzochid_seq_hash_table_new (GCompareFunc key_compare_func, GHashFunc hash_func,
			    GEqualFunc equal_func,
			    GDestroyNotify key_destroy_notify,
			    GDestroyNotify value_destroy_notify)
{
  gzochid_seq_hash_table *seq_hash_table =
    malloc (sizeof (gzochid_seq_hash_table));

  assert (key_compare_func != NULL);

  seq_hash_table->key_compare_func = key_compare_func;
  g_mutex_init (&seq_hash_table->mutex);
  seq_hash_table->key_sequence = g_sequence_new (NULL);
  seq_hash_table->table = g_hash_table_new_full
    (hash_func, equal_func, key_destroy_notify, value_destroy_notify);
  
  return seq_hash_table;
}

void
gzochid_seq_hash_table_free (gzochid_seq_hash_table *seq_hash_table)
{
  g_mutex_clear (&seq_hash_table->mutex);
  g_sequence_free (seq_hash_table->key_sequence);
  g_hash_table_destroy (seq_hash_table->table);
  
  free (seq_hash_table);
}

gboolean
gzochid_seq_hash_table_is_empty (gzochid_seq_hash_table *seq_hash_table)
{
  return g_sequence_is_empty (seq_hash_table->key_sequence);
}

gpointer
gzochid_seq_hash_table_first_key (gzochid_seq_hash_table *seq_hash_table)
{
  gpointer key = NULL;
  
  g_mutex_lock (&seq_hash_table->mutex);

  if (!g_sequence_is_empty (seq_hash_table->key_sequence))
    {
      GSequenceIter *iter = g_sequence_get_begin_iter
	(seq_hash_table->key_sequence);
      key = g_sequence_get (iter);
    }
      
  g_mutex_unlock (&seq_hash_table->mutex);

  return key;
}

gpointer
gzochid_seq_hash_table_lookup (gzochid_seq_hash_table *seq_hash_table,
			       gpointer key)
{
  return g_hash_table_lookup (seq_hash_table->table, key);
}

/* Adapts the `GSequence' API (which expects a `GCompareDataFunc') to the
   `gzochid_seq_hash_table' constructor function. */

static gint
key_compare_data_func (gconstpointer a, gconstpointer b, gpointer user_data)
{
  gzochid_seq_hash_table *seq_hash_table = user_data;
  return seq_hash_table->key_compare_func (a, b);
}

gboolean
gzochid_seq_hash_table_insert (gzochid_seq_hash_table *seq_hash_table,
			       gpointer key, gpointer value)
{
  gboolean ret = TRUE;
  
  g_mutex_lock (&seq_hash_table->mutex);

  if (g_hash_table_contains (seq_hash_table->table, key))
    {
      /* If we're about to replace the key in the hash table, we need to replace
	 it in the sequence as well, since the key destroy function (if 
	 configured) it will be called on the existing key. */
      
      GSequenceIter *iter = g_sequence_lookup
	(seq_hash_table->key_sequence, key, key_compare_data_func,
	 seq_hash_table);

      g_sequence_set (iter, key);
    }
  else g_sequence_insert_sorted (seq_hash_table->key_sequence, key,
				 key_compare_data_func, seq_hash_table);

  ret = g_hash_table_insert (seq_hash_table->table, key, value);
  
  g_mutex_unlock (&seq_hash_table->mutex);
  return ret;
}

gboolean
gzochid_seq_hash_table_remove (gzochid_seq_hash_table *seq_hash_table,
			       gpointer key)
{
  gboolean ret = TRUE;
  
  g_mutex_lock (&seq_hash_table->mutex);

  if (g_hash_table_contains (seq_hash_table->table, key))
    {
      /* Remove the key from the sequence before renoving it from the table,
	 since the table may have a key destroy callback configured. */
      
      GSequenceIter *iter = g_sequence_lookup
	(seq_hash_table->key_sequence, key, key_compare_data_func,
	 seq_hash_table);

      g_sequence_remove (iter);
      g_hash_table_remove (seq_hash_table->table, key);
    }
  else ret = FALSE;
  
  g_mutex_unlock (&seq_hash_table->mutex);
  return ret;
}
