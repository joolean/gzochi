/* taskclient.h: Prototypes and declarations for taskclient.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_TASK_CLIENT_H
#define GZOCHID_TASK_CLIENT_H

#include <glib.h>
#include <glib-object.h>

/* The core data client type definitions. */

#define GZOCHID_TYPE_TASK_CLIENT gzochid_task_client_get_type ()

G_DECLARE_FINAL_TYPE (GzochidTaskClient, gzochid_task_client, GZOCHID,
		      TASK_CLIENT, GObject);

GType gzochid_task_client_get_type (void);

enum
  {
    /* Indicates a failure to resubmit durable tasks because the local server
       is not currently the leader node for task resubmission. */
    
    GZOCHID_TASKCLIENT_ERROR_NOT_RESUBMISSION_LEADER,

    /* Indicates a failure to process a message from the task server because it
       referred to application that is not running on the local server. */
    
    GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,

    /* Indicates a failure to submit a task because it is already mapped to the
       local server. */
    
    GZOCHID_TASKCLIENT_ERROR_TASK_ALREADY_MAPPED,

    /* Indicates a failure to send a task completion or to process an
       unassignment notification from the task server because the target task is
       not mapped to the local server. */
    
    GZOCHID_TASKCLIENT_ERROR_TASK_NOT_MAPPED,

    GZOCHID_TASKCLIENT_ERROR_FAILED /* Generic task client failure. */
  };

#define GZOCHID_TASKCLIENT_ERROR gzochid_taskclient_error_quark ()

GQuark gzochid_taskclient_error_quark (void);

/* The following functions direct information outwards to the meta server. */

/* Notifies the meta server that the local server is ready to participate in
   serving the specified application. */

void gzochid_taskclient_start_application (GzochidTaskClient *, const char *);

/*
  Notifies the meta server that the local server - which must have been 
  designated as the durable task resubmission leader via a previous call to 
  `gzochid_taskclient_resubmit_tasks' - has completed resubmission. 

  Signals an error if the local server is not the resubmission leader.
*/

void gzochid_taskclient_complete_resubmission (GzochidTaskClient *,
					       const char *, GError **);

/*
  Locally maps the specified application-qualified durable task oid to the 
  local task id and notifies the meta server of the durable task's 
  submission. 

  Signals an error if the specified task is already mapped to the local server.
*/

void gzochid_taskclient_submit_task (GzochidTaskClient *, const char *,
				     guint64, guint64, GError **);

/* Notifies the meta server to cancel the periodic task represented by the 
   specified application-qualified durable task oid, on whichever application 
   server node it is mapped to. */

void gzochid_taskclient_cancel_task (GzochidTaskClient *, const char *,
				     guint64);

/*
  Notifies the meta server that the specified application-qualified durable
  task oid has completed and can be unmapped from the local server. 

  Signals an error if the specified task is not mapped to the local server.
*/

void gzochid_taskclient_complete_task (GzochidTaskClient *, const char *,
				       guint64, GError **);

/* The following functions are callbacks for messages delivered from the meta 
   server. */

/*
  Notifies the local server that it has been elected as the durable task
  resubmission leader for the specified application, and that it should
  resubmit all pending durable tasks (via `gzochid_taskclient_submit_task') and
  let the meta server know when all tasks have been resubmitted (via
  `gzochid_taskclient_complete_resubmission'). 

  An error is signaled if the target application is not present on the local
  server.
*/

void gzochid_taskclient_resubmit_tasks (GzochidTaskClient *, const char *,
					GError **);

/*
  Notifies the local server that normal task processing may begin for the 
  specified application on the local server.
  
  An error is signaled if the target application is not present on the local
  server.
*/

void gzochid_taskclient_application_started (GzochidTaskClient *, const char *,
					     GError **);

/*
  Notifies the local server that the specified application-qualified durable
  task oid has been assigned to it and that it should submit a corresponding
  task to the local task queue.

  An error is signaled if the target application is not present on the local
  server.
*/  

void gzochid_taskclient_task_assigned (GzochidTaskClient *, const char *,
				       guint64, GError **);

/*
  Notifies the local server that the specified application-qualified durable
  task oid has been unassigned from it and that the task should be removed from
  the local task queue if it has not already executed.

  An error is signaled if the target application is not present on the local
  server or if the task is not currently mapped to the local server.
*/

void gzochid_taskclient_task_unassigned (GzochidTaskClient *, const char *,
					 guint64, GError **);

#endif /* GZOCHID_TASK_CLIENT_H */
