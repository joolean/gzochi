/* descriptor.h: Prototypes and declarations for descriptor.c
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_DESCRIPTOR_H
#define GZOCHID_DESCRIPTOR_H

#include <glib.h>
#include <glib-object.h>
#include <stdio.h>

#include "callback.h"

/* The core application descriptor type definitions. */

#define GZOCHID_TYPE_APPLICATION_DESCRIPTOR	\
  gzochid_application_descriptor_get_type ()

G_DECLARE_FINAL_TYPE (GzochidApplicationDescriptor,
		      gzochid_application_descriptor, GZOCHID,
		      APPLICATION_DESCRIPTOR, GObject);

/* The gzochid application descriptor struct. */

struct _GzochidApplicationDescriptor
{
  GObject parent_object;
  
  char *name;
  char *description;

  /* If this application descriptor was parsed from a descriptor file, this 
     gives that file's parent directory, and can be used to resolve relative 
     load paths. Otherwise, it is `NULL'. */
  
  char *deployment_root;
  
  GList *load_paths; /* Descriptor-specified module load paths. */

  /* The log message priority threshold for the application. Defaults to 
     `G_LOG_LEVEL_MESSAGE'. */

  GLogLevelFlags log_level;
  
  /* The relative or absolute full path to the log file for the application. */

  char *log_filename; 
  
  gzochid_application_callback *initialized;
  gzochid_application_callback *logged_in;
  gzochid_application_callback *ready;

  char *auth_type;
  GHashTable *auth_properties;

  GHashTable *properties;
};

GzochidApplicationDescriptor *gzochid_config_parse_application_descriptor 
(FILE *);

/* Parses the application descriptor from the specified path, which must resolve
   to the location of a regular file. Returns a reference to a new 
   `GzochidApplicationDescriptor' on success; returns `NULL' if the file cannot
   be opened or if the file could not be successfully parsed. 
   
   The returned reference should be released via `g_object_unref' when no longer
   needed. */

GzochidApplicationDescriptor *
gzochid_config_parse_application_descriptor_from_file (const char *);

#endif /* GZOCHID_DESCRIPTOR_H */
