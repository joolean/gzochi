/* schedule.c: Task execution and task queue management routines for gzochid
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/time.h>

#include "config.h"
#include "schedule.h"
#include "seqhash.h"
#include "task.h"
#include "util.h"

/* The enumeration of possible states for submitted tasks. */

enum pending_task_state
  {
    GZOCHID_PENDING_TASK_STATE_PENDING,
    GZOCHID_PENDING_TASK_STATE_COMPLETED
  };

/* The pending task structure. Represents the status of a task submitted to a
   task execution queue. 

   Note that The first member of this struct is a `gzochid_task', which allows 
   pointers to it to be "upcast" as necessary. */

struct _pending_task
{
  GzochidTask *task; /* The "root" configuration of this task. */
  
  GCond cond; /* A condition variable to signal when the task state changes. */
  GMutex mutex; /* The accompanying mutex. */

  guint64 ephemeral_task_id; /* The local task id. */
  enum pending_task_state state; /* The task state. */

  /* Whether to free this structure on task completion or to allow some other 
     party - e.g., `gzochid_schedule_run_task' - to handle destruction. */

  gboolean destroy_on_execute; 
};

typedef struct _pending_task pending_task;

/*
  Construct and return a new `pending_task' object based on the specified
  `gzochid_task' structure. The memory used by this object should be released 
  via `pending_task_free' when no longer needed. 

  If the destroy-on-execute flag is specified, this task will be freed
  immediately following execution.
*/

static pending_task *
pending_task_new (GzochidTask *task, gboolean destroy_on_execute)
{
  /* A kind of function-scoped task id counter for generating task ids. Can't
     use GLib's atomic operations for this, since it doesn't support 64-bit
     values; 32-bit values are probably not sufficient for the ephemeral task id
     space of a long-running server node. */
  
  static guint64 next_ephemeral_task_id = 0;
  G_LOCK_DEFINE_STATIC (next_ephemeral_task_id);

  pending_task *ret = malloc (sizeof (pending_task));

  ret->task = g_object_ref_sink (task);

  /* Calculate the next ephemeral task id. */
  
  G_LOCK (next_ephemeral_task_id);
  ret->ephemeral_task_id = next_ephemeral_task_id++;
  G_UNLOCK (next_ephemeral_task_id);
  
  ret->state = GZOCHID_PENDING_TASK_STATE_PENDING;
  ret->destroy_on_execute = destroy_on_execute;
  
  g_cond_init (&ret->cond);
  g_mutex_init (&ret->mutex);
 
  return ret;
}

/* Frees the memory associated with the specified `pending_task'. */

static void
pending_task_free (pending_task *pending_task)
{
  g_object_unref (pending_task->task);
  
  g_mutex_clear (&pending_task->mutex);
  g_cond_clear (&pending_task->cond);

  free (pending_task);
}

/* The pending task key, used to encapsulate chronological ordering and an
   ephemeral id for lookup with the task queue. */

struct _pending_task_key
{  
  guint64 ephemeral_task_id; /* The task id. */
  struct timeval target_execution_time; /* The target task execution time. */
};

typedef struct _pending_task_key pending_task_key;

/* Construct and return a new `pending_task_key' with the specified target
   execution time. The memory used by this object should be freed via 
   `pending_task_key_free' when no longer needed. */

static pending_task_key *
pending_task_key_new (pending_task *task)
{
  pending_task_key *task_key = malloc (sizeof (pending_task_key));

  task_key->ephemeral_task_id = task->ephemeral_task_id;
  task_key->target_execution_time = task->task->target_execution_time;
  
  return task_key;
}

/* Frees the memory used by the specified `pending_task_key'. */

static void
pending_task_key_free (pending_task_key *task_key)
{
  free (task_key);
}

/* A `GCompareFunc' implementation that compares two `pending_task_key' objects
   by target execution time. */

static gint
pending_task_key_compare (gconstpointer a, gconstpointer b)
{
  const pending_task_key *task_key_a = a;
  const pending_task_key *task_key_b = b;

  if (task_key_a->target_execution_time.tv_sec <
      task_key_b->target_execution_time.tv_sec)
    return -1;
  else if (task_key_a->target_execution_time.tv_sec >
	   task_key_b->target_execution_time.tv_sec)
    return 1;
  else if (task_key_a->target_execution_time.tv_usec <
	   task_key_b->target_execution_time.tv_usec)
    return -1;
  else if (task_key_a->target_execution_time.tv_usec >
	   task_key_b->target_execution_time.tv_usec)
    return 1;
  else return gzochid_util_guint64_compare
	 (&task_key_a->ephemeral_task_id, &task_key_b->ephemeral_task_id);
}

/* A `GHashFunc' implementation that hashes a `pending_task_key' by its 
   ephemeral id. */

static guint
pending_task_key_hash (gconstpointer key)
{
  const pending_task_key *task_key = key;
  return g_int64_hash (&task_key->ephemeral_task_id);
}

/* A `GEqualFunc' implementation that tests two `pending_task_key' for equality
   in terms of their ephemeral ids. */

static gboolean
pending_task_key_equal (gconstpointer a, gconstpointer b)
{
  const pending_task_key *task_key_a = a;
  const pending_task_key *task_key_b = b;

  return task_key_a->ephemeral_task_id == task_key_b->ephemeral_task_id;
}

/* The task queue structure. */

struct _GzochidTaskQueue
{
  GObject parent_instance;

  GzochidConfiguration *configuration;
  
  /* A condition variable to signal when the contents of the queue have been 
     modified. */
  
  GCond cond; 
  GMutex mutex; /* The accompanying mutex. */

  GThread *consumer_thread; /* The pool feeder thread. */

  /* Indicates whether or not the consumer thread should continue. */
  
  gboolean running; 

  /* The task queue, as a sequential map of `pending_task_key *' to 
     `pending_task *'. */

  gzochid_seq_hash_table *queue; 

  GThreadPool *pool; /* The pool to which ready tasks are fed. */
};

/* Boilerplate setup for the task queue object. */

G_DEFINE_TYPE (GzochidTaskQueue, gzochid_task_queue, G_TYPE_OBJECT);

enum gzochid_task_queue_properties
  {
    PROP_CONFIGURATION = 1,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
gzochid_task_queue_constructed (GObject *obj)
{
  GzochidTaskQueue *self = GZOCHID_TASK_QUEUE (obj);

  GHashTable *config = gzochid_configuration_extract_group
    (self->configuration, "game");
  int max_threads = gzochid_config_to_int 
    (g_hash_table_lookup (config, "thread_pool.max_threads"), 4);

  self->pool = gzochid_thread_pool_new (self, max_threads, FALSE, NULL);

  g_hash_table_destroy (config);
}

static void
gzochid_task_queue_set_property (GObject *object, guint property_id,
				 const GValue *value, GParamSpec *pspec)
{
  GzochidTaskQueue *self = GZOCHID_TASK_QUEUE (object);

  switch (property_id)
    {
    case PROP_CONFIGURATION:
      self->configuration = g_object_ref (g_value_get_object (value));
      break;
      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochid_task_queue_dispose (GObject *object)
{
  GzochidTaskQueue *queue = GZOCHID_TASK_QUEUE (object);

  g_object_unref (queue->configuration);
  
  G_OBJECT_CLASS (gzochid_task_queue_parent_class)->dispose (object);
}

static void
gzochid_task_queue_finalize (GObject *gobject)
{
  GzochidTaskQueue *queue = GZOCHID_TASK_QUEUE (gobject);

  g_cond_clear (&queue->cond);
  g_mutex_clear (&queue->mutex);

  /* Because, for good reason, the queue doesn't have a value destroy 
     notification callback, we have to explicitly free and remove any remaining
     pending tasks here. It's not the most efficient way to do this, but this
     function is not called that often. */
  
  while (!gzochid_seq_hash_table_is_empty (queue->queue))
    {
      pending_task_key *task_key =
	gzochid_seq_hash_table_first_key (queue->queue);

      pending_task_free
	(gzochid_seq_hash_table_lookup (queue->queue, task_key));
      gzochid_seq_hash_table_remove (queue->queue, task_key);
    }

  gzochid_seq_hash_table_free (queue->queue);
  gzochid_thread_pool_free (queue->pool, TRUE, TRUE);
  
  G_OBJECT_CLASS (gzochid_task_queue_parent_class)->finalize (gobject);  
}

static void
gzochid_task_queue_class_init (GzochidTaskQueueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = gzochid_task_queue_constructed;
  object_class->dispose = gzochid_task_queue_dispose;
  object_class->finalize = gzochid_task_queue_finalize;
  object_class->set_property = gzochid_task_queue_set_property;

  obj_properties[PROP_CONFIGURATION] = g_param_spec_object
    ("configuration", "config", "The global configuration object",
     GZOCHID_TYPE_CONFIGURATION, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

static void
gzochid_task_queue_init (GzochidTaskQueue *self)
{
  g_cond_init (&self->cond);
  g_mutex_init (&self->mutex);

  self->queue = gzochid_seq_hash_table_new
    (pending_task_key_compare, pending_task_key_hash, pending_task_key_equal,
     (GDestroyNotify) pending_task_key_free, NULL);
}

static void *
pending_task_executor_inner (void *data)
{
  void **args = data;
  pending_task *task = args[0];
  gpointer user_data = args[1];

  task->task->worker (task->task->data, user_data);

  return NULL;
}

static void 
pending_task_executor (gpointer data, gpointer user_data)
{
  pending_task *pending_task = data;
  void *args[2];

  g_mutex_lock (&pending_task->mutex);

  args[0] = pending_task;
  args[1] = user_data;

  scm_with_guile (pending_task_executor_inner, args);

  pending_task->state = GZOCHID_PENDING_TASK_STATE_COMPLETED;

  if (pending_task->destroy_on_execute)
    {
      g_mutex_unlock (&pending_task->mutex);
      pending_task_free (pending_task);
    }
  else
    {
      g_cond_broadcast (&pending_task->cond);
      g_mutex_unlock (&pending_task->mutex);
    }
}

gpointer 
gzochid_schedule_task_executor (gpointer data)
{
  GzochidTaskQueue *task_queue = data;

  g_mutex_lock (&task_queue->mutex);
  while (task_queue->running)
    {
      if (gzochid_seq_hash_table_is_empty (task_queue->queue))
	g_cond_wait (&task_queue->cond, &task_queue->mutex);
      else 
	{
	  struct timeval current_time;
	  pending_task_key *task_key = gzochid_seq_hash_table_first_key
	    (task_queue->queue);
	  pending_task *task = NULL;

	  assert (task_key != NULL);
	  
	  task = gzochid_seq_hash_table_lookup
	    (task_queue->queue, task_key);

	  gettimeofday (&current_time, NULL);

	  if (timercmp (&current_time, &task->task->target_execution_time, >))
	    {
	      gzochid_seq_hash_table_remove (task_queue->queue, task_key);
	      gzochid_thread_pool_push 
		(task_queue->pool, pending_task_executor, task, NULL);
	    }
	  else 
	    {
	      struct timeval interval;
	      gint64 until = g_get_monotonic_time ();
	      
	      timersub (&task->task->target_execution_time, &current_time,
			&interval);

	      until += interval.tv_sec * G_TIME_SPAN_SECOND + interval.tv_usec;
	      g_cond_wait_until (&task_queue->cond, &task_queue->mutex, until);
	    }
	}
    }
  g_mutex_unlock (&task_queue->mutex);

  return NULL;
}

void 
gzochid_schedule_task_queue_start (GzochidTaskQueue *task_queue)
{
  g_mutex_lock (&task_queue->mutex);
  if (task_queue->consumer_thread == NULL)
    {
      task_queue->consumer_thread = g_thread_new
	("task-consumer", gzochid_schedule_task_executor, task_queue);
      task_queue->running = TRUE;
    }
  g_mutex_unlock (&task_queue->mutex);
}

void
gzochid_schedule_task_queue_stop (GzochidTaskQueue *task_queue)
{
  g_mutex_lock (&task_queue->mutex);
  if (task_queue->consumer_thread != NULL)
    {
      GThread *consumer_thread_handle = task_queue->consumer_thread;
      
      task_queue->running = FALSE;
      task_queue->consumer_thread = NULL;

      g_cond_signal (&task_queue->cond);
      g_mutex_unlock (&task_queue->mutex);

      g_thread_join (consumer_thread_handle);
    }
  else g_mutex_unlock (&task_queue->mutex);
}

static void 
task_chain_worker (gpointer data, gpointer user_data)
{
  GList *tasks = (GList *) data;
  GList *task_ptr = tasks;

  while (task_ptr != NULL)
    {
      gzochid_schedule_execute_task (task_ptr->data);
      task_ptr = task_ptr->next;
    }

  g_list_free_full (tasks, g_object_unref);
}

void
gzochid_schedule_submit_task_chain (GzochidTaskQueue *task_queue, GList *tasks)
{
  GList *tasks_copy = gzochid_util_list_copy_deep
    (tasks, (GCopyFunc) g_object_ref, NULL);
  GzochidTask *task = NULL;

  assert (g_list_length (tasks) > 0);

  task = gzochid_task_new
    (task_chain_worker, NULL, tasks_copy,
     ((GzochidTask *) tasks_copy->data)->target_execution_time);

  gzochid_schedule_submit_task (task_queue, task);
}

/*
  Wraps the specified task in a `pending_task' wrapper and adds it to the task
  queue, waking up the scheduler. Returns the `pending_task' wrapper.
  
  This function must be called with the task queue mutex held.
*/

static pending_task *
submit_task (GzochidTaskQueue *task_queue, GzochidTask *task,
	     gboolean destroy_on_execute)
{
  pending_task *pending_task = pending_task_new (task, destroy_on_execute);
  pending_task_key *pending_task_key = pending_task_key_new (pending_task);
  
  gzochid_seq_hash_table_insert
    (task_queue->queue, pending_task_key, pending_task);
  g_cond_signal (&task_queue->cond);

  return pending_task;
}

guint64
gzochid_schedule_submit_task (GzochidTaskQueue *task_queue, GzochidTask *task)
{
  guint64 ephemeral_task_id = 0;
  pending_task *ptask = NULL;

  g_mutex_lock (&task_queue->mutex);

  ptask = submit_task (task_queue, task, TRUE);
  ephemeral_task_id = ptask->ephemeral_task_id;

  g_mutex_unlock (&task_queue->mutex);
  return ephemeral_task_id;
}

gboolean
gzochid_schedule_remove_task (GzochidTaskQueue *task_queue,
			      guint64 local_task_id)
{
  pending_task_key key = { local_task_id, { 0, 0 } };
  gboolean ret = FALSE;
  
  g_mutex_lock (&task_queue->mutex);

  if (gzochid_seq_hash_table_remove (task_queue->queue, &key))
    {
      g_cond_signal (&task_queue->cond);
      ret = TRUE;
    }
  
  g_mutex_unlock (&task_queue->mutex);
  return ret;
}

void 
gzochid_schedule_run_task (GzochidTaskQueue *task_queue, GzochidTask *task)
{
  pending_task *pending_task = NULL;

  g_mutex_lock (&task_queue->mutex);
  pending_task = submit_task (task_queue, task, FALSE);
  g_mutex_unlock (&task_queue->mutex);

  g_mutex_lock (&pending_task->mutex);
  while (pending_task->state == GZOCHID_PENDING_TASK_STATE_PENDING)
    g_cond_wait (&pending_task->cond, &pending_task->mutex);
  g_mutex_unlock (&pending_task->mutex);
  
  pending_task_free (pending_task);
}

void 
gzochid_schedule_execute_task (GzochidTask *task)
{
  task->worker (task->data, NULL);
}
