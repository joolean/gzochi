/* appstate-mem.c: In-memory appstate implementation for gzochi-metad
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "appstate.h"
#include "appstate-mem.h"

/*
  The functions and data structures defined below form an implementation of
  `gzochi_metad_appstate' in which application state information is held in
  memory. As such, it may be used in gzochi server cluster configurations in
  which there is only a single meta server.

  This implementation is not thread-safe; only a single thread should interact
  with a state structure at a time.
*/

struct _application_state
{
  gzochi_metad_application_state state; /* The application state. */

  /* The id of the node leading task resubmission if the application is in
     `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING'. Otherwise undefined. */

  int resubmitting_node_id;

  /* Node ids (ints) participating in application. */
  
  GArray *participating_nodes;
};

typedef struct _application_state application_state;

/*
  Construct and return a new `applicaiton_state' object, whose initial state is
  `GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER'.

  Should be freed via `application_state_Free' when no longer needed.
*/

static application_state *
application_state_new ()
{
  application_state *state = malloc (sizeof (application_state));
  
  state->state = GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER;
  state->resubmitting_node_id = 0;
  state->participating_nodes = g_array_new (FALSE, FALSE, sizeof (int));

  return state;
}

/* Frees the memory used by the specified `application_state' object. */

static void
application_state_free (application_state *state)
{
  g_array_unref (state->participating_nodes);
  free (state);
}

/* Memory-backed appstate struct definition. */

struct _gzochi_metad_appstate_mem
{
  gzochi_metad_appstate base; /* The base appstate type. */

  GHashTable *applications; /* Map of `char *' to `application_state *'. */
};

typedef struct _gzochi_metad_appstate_mem gzochi_metad_appstate_mem;

/* The `lookup_application_state' implementation for the 
   `gzochi_metad_appstate_mem' function interface. */

static gzochi_metad_application_state
lookup_application_state (gzochi_metad_appstate *appstate, const char *app)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;
  
  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);

  return state != NULL
    ? state->state
    : GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER;
}

/* Returns a pointer to the application state object for the specified
   application name, initializing an empty one and adding it to the specified
   appstate if it does not exist. */

static application_state *
ensure_application_state (gzochi_metad_appstate_mem *appstate_mem,
			  const char *app)
{
  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);
  
  if (state == NULL)
    {
      state = application_state_new ();
      g_hash_table_insert (appstate_mem->applications, strdup (app), state);
    }

  return state;
}

/* The `begin_resubmission' implementation for the `gzochi_metad_appstate_mem' 
   function interface. */

static void
begin_resubmission (gzochi_metad_appstate *appstate, const char *app,
		    int node_id, GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  /* Create the application state if it doesn't exist, because we're going to be
     transitioning to a new state. */
  
  application_state *state = ensure_application_state (appstate_mem, app);
  
  switch (state->state)
    {
    case GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER:
      state->state = GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING;
      state->resubmitting_node_id = node_id;
      
      break;
      
    case GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING:
      g_set_error
	(err, GZOCHI_METAD_APPSTATE_ERROR,
	 GZOCHI_METAD_APPSTATE_ERROR_ALREADY_RESUBMITTING,
	 "Task resubmission already in progress for application %s.", app);

      break;
      
    case GZOCHI_METAD_APPLICATION_STATE_STARTED:
      g_set_error
	(err, GZOCHI_METAD_APPSTATE_ERROR,
	 GZOCHI_METAD_APPSTATE_ERROR_ALREADY_STARTED,
	 "Application %s has already started.", app);

      break;
    }
}

/* The `cancel_resubmission' implementation for the `gzochi_metad_appstate_mem'
   function interface. */

static void
cancel_resubmission (gzochi_metad_appstate *appstate, const char *app,
		     GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);

  if (state != NULL
      && state->state == GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING)
    {
      state->state = GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER;
      state->resubmitting_node_id = 0;
    }
  else g_set_error
	 (err, GZOCHI_METAD_APPSTATE_ERROR,
	  GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING,
	  "No task resubmission in progress for application %s.", app);
}

/* The `lookup_resubmission_leader' implementation for the 
   `gzochi_metad_appstate_mem' function interface. */

static int
lookup_resubmission_leader (gzochi_metad_appstate *appstate, const char *app,
			    GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);

  if (state != NULL
      && state->state == GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING)
    return state->resubmitting_node_id;
  else
    {
      g_set_error
	(err, GZOCHI_METAD_APPSTATE_ERROR,
	 GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING,
	 "No task resubmission in progress for application %s.", app);
      return 0;
    }  
}

/* The `start_application' implementation for the `gzochi_metad_appstate_mem' 
   function interface. */

static void
start_application (gzochi_metad_appstate *appstate, const char *app,
		   GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);

  if (state != NULL)
    switch (state->state)
      {
      case GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER:
	g_set_error
	  (err, GZOCHI_METAD_APPSTATE_ERROR,
	   GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING,
	   "No task resubmission in progress for application %s.", app);
	
	break;
	
      case GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING:
	state->state = GZOCHI_METAD_APPLICATION_STATE_STARTED;
	state->resubmitting_node_id = 0;

	break;
	
      case GZOCHI_METAD_APPLICATION_STATE_STARTED:
	g_set_error
	  (err, GZOCHI_METAD_APPSTATE_ERROR,
	   GZOCHI_METAD_APPSTATE_ERROR_ALREADY_STARTED,
	   "Application %s has already started.", app);

	break;
      }
  else g_set_error
	 (err, GZOCHI_METAD_APPSTATE_ERROR,
	  GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING,
	  "No task resubmission in progress for application %s.", app);
}

/* Returns the index of the specified node id in the specified `GArray' or -1 if
   the node id is not present in the array. */

static int
index_of_node_id (const GArray *arr, int node_id)
{
  int i = 0;

  for (; i < arr->len; i++)
    if (g_array_index (arr, int, i) == node_id)
      return i;

  return -1;
}

/* The `add_participating_node' implementation for the 
   `gzochi_metad_appstate_mem' function interface. */

static void
add_participating_node (gzochi_metad_appstate *appstate, const char *app,
			int node_id, GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  /* Ensure that we create an application state, since we're going to want to
     add the node id to it. */
  
  application_state *state = ensure_application_state (appstate_mem, app);

  if (index_of_node_id (state->participating_nodes, node_id) < 0)
    g_array_append_val (state->participating_nodes, node_id);
  else g_set_error
	 (err, GZOCHI_METAD_APPSTATE_ERROR,
	  GZOCHI_METAD_APPSTATE_ERROR_ALREADY_PARTICIPATING,
	  "Node %d already participating in application %s.", node_id, app);
}

/* The `remove_participating_node' implementation for the 
   `gzochi_metad_appstate_mem' function interface. */

static void
remove_participating_node (gzochi_metad_appstate *appstate, const char *app,
			   int node_id, GError **err)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);
  int node_id_index = state != NULL
    ? index_of_node_id (state->participating_nodes, node_id)
    : -1;
  
  if (node_id_index < 0)
    g_set_error
      (err, GZOCHI_METAD_APPSTATE_ERROR,
       GZOCHI_METAD_APPSTATE_ERROR_NOT_PARTICIPATING,
       "Node %d not participating in application %s.", node_id, app);
  else g_array_remove_index (state->participating_nodes, node_id_index);
}

/* The `lookup_participating nodes' implementation for the 
   `gzochi_metad_appstate_mem' function interface. */

static GArray *
lookup_participating_nodes (gzochi_metad_appstate *appstate, const char *app)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  application_state *state = g_hash_table_lookup
    (appstate_mem->applications, app);

  return state == NULL
    ? g_array_new (FALSE, FALSE, sizeof (int))
    : g_array_ref (state->participating_nodes);
}

static gzochi_metad_appstate_iface mem_iface =
  {
    lookup_application_state,
    begin_resubmission,
    cancel_resubmission,
    lookup_resubmission_leader,
    start_application,

    add_participating_node,
    remove_participating_node,
    lookup_participating_nodes
  };

gzochi_metad_appstate *
gzochi_metad_appstate_mem_new ()
{
  gzochi_metad_appstate_mem *appstate =
    malloc (sizeof (gzochi_metad_appstate_mem));

  appstate->base.iface = &mem_iface;
  
  appstate->applications = g_hash_table_new_full
    (g_str_hash, g_str_equal, free, (GDestroyNotify) application_state_free);

  return (gzochi_metad_appstate *) appstate;
}

void
gzochi_metad_appstate_mem_free (gzochi_metad_appstate *appstate)
{
  gzochi_metad_appstate_mem *appstate_mem =
    (gzochi_metad_appstate_mem *) appstate;

  g_hash_table_destroy (appstate_mem->applications);

  free (appstate);
}

GQuark
gzochi_metad_appstate_error_quark ()
{
  return g_quark_from_static_string ("gzochi-metad-appstate-error-quark");
}
