/* debug.c: Remote debugging module for gzochid
 * Copyright (C) 2019 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>

#include "debug.h"
#include "guile.h"

static SCM scm_run_repl_server;
static SCM scm_stop_repl_server;

struct _GzochidDebugServer
{
  GObject parent_instance;

  GThread *thread;
};

G_DEFINE_TYPE (GzochidDebugServer, gzochid_debug_server, G_TYPE_OBJECT);

static void
gzochid_debug_server_class_init (GzochidDebugServerClass *klass)
{
}

static void
gzochid_debug_server_init (GzochidDebugServer *obj)
{
}

static void *
initialize_guile (void *data)
{
  SCM gzochi_private_debug = scm_c_resolve_module ("gzochi private debug");
  SCM run_repl_server_var = scm_c_module_lookup 
    (gzochi_private_debug, "gzochi:run-repl-server");
  SCM stop_repl_server_var = scm_c_module_lookup 
    (gzochi_private_debug, "gzochi:stop-repl-server");
  
  scm_run_repl_server = scm_variable_ref (run_repl_server_var);
  scm_gc_protect_object (scm_run_repl_server);

  scm_stop_repl_server = scm_variable_ref (stop_repl_server_var);
  scm_gc_protect_object (scm_stop_repl_server);

  return NULL;
}

static void *
run_repl_server (void *data)
{
  guint *port = data;
  SCM scm_port = scm_from_int (*port);

  free (port);
  
  gzochid_guile_invoke 
    (scm_run_repl_server, 
     scm_list_2 (scm_from_locale_keyword ("port"), scm_port), 
     SCM_BOOL_F);

  return NULL;
}

static gpointer
run (gpointer data)
{
  return scm_with_guile (run_repl_server, data);
}

void
gzochid_debug_server_start (GzochidDebugServer *debug_server, guint port,
			    GError **err)
{
  guint *port_ptr = malloc (sizeof (guint));

  *port_ptr = port;
  
  scm_with_guile (initialize_guile, NULL);
  debug_server->thread = g_thread_new ("repl-server", run, port_ptr);
  g_message ("Debug server listening on port %d", port);
}

static void *
stop_repl_server (void *data)
{
  g_message ("Shutting down debug server");
  gzochid_guile_invoke (scm_stop_repl_server, SCM_EOL, SCM_BOOL_F);

  return NULL;
}

void
gzochid_debug_server_stop (GzochidDebugServer *debug_server)
{
  if (debug_server->thread != NULL)
    {
      scm_with_guile (stop_repl_server, NULL);
      g_thread_join (debug_server->thread);
      debug_server->thread = NULL;
    }
}
