/* data.c: Application data management routines for gzochid
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "app-store.h"
#include "data.h"
#include "game.h"
#include "gzochid-storage.h"
#include "io.h"
#include "oids.h"
#include "tx.h"
#include "util.h"

#ifdef G_LOG_DOMAIN
#undef G_LOG_DOMAIN
#endif /* G_LOG_DOMAIN */
#define G_LOG_DOMAIN "gzochid.data"

struct _gzochid_data_transaction_context
{
  GzochidApplicationContext *app_context;

  /* The next available oid for objects created by this transaction. */

  guint64 next_oid; 

  /* The last oid that can be assigned by this transaction before allocating a
     fresh block of oids. */

  guint64 last_oid; 

  gzochid_storage_transaction *transaction;

  GHashTable *oids_to_references;
  GHashTable *ptrs_to_references;

  gboolean needs_unlock;
};

typedef struct _gzochid_data_transaction_context
gzochid_data_transaction_context;

GQuark 
gzochid_data_error_quark (void)
{
  return g_quark_from_static_string ("gzochid-data-error-quark");
}

gzochid_oid_holder *
gzochid_oid_holder_new (void)
{
  return calloc (1, sizeof (gzochid_oid_holder));
}

void 
gzochid_oid_holder_free (gzochid_oid_holder *holder)
{
  if (holder->err != NULL)
    g_error_free (holder->err);
  
  free (holder);
}

/* Convenience function for fetching the application store pointer from the
   application context. */

static gzochid_application_store *
get_store (GzochidApplicationContext *app_context)
{
  gzochid_application_store *store = NULL;

  g_object_get (app_context, "app-store", &store, NULL);
  return store;
}

static gzochid_data_transaction_context *
create_transaction_context (GzochidApplicationContext *app_context,
			    struct timeval *timeout)
{
  gzochid_data_transaction_context *tx_context = 
    calloc (1, sizeof (gzochid_data_transaction_context));
  gzochid_application_store *store = get_store (app_context);

  tx_context->app_context = g_object_ref (app_context);

  if (timeout != NULL)
    tx_context->transaction = store->iface->transaction_begin_timed
      (store->storage_context, *timeout);
  else tx_context->transaction = store->iface->transaction_begin
	 (store->storage_context);

  tx_context->oids_to_references = g_hash_table_new_full 
    (g_int64_hash, g_int64_equal, g_free, NULL);
  tx_context->ptrs_to_references = g_hash_table_new 
    (g_direct_hash, g_direct_equal);

  return tx_context;
}

static void 
transaction_context_free (gzochid_data_transaction_context *context)
{
  g_object_unref (context->app_context);
  
  g_hash_table_destroy (context->oids_to_references);
  g_hash_table_destroy (context->ptrs_to_references);

  free (context);
}

static gboolean 
flush_reference (gzochid_data_managed_reference *reference,
		 gzochid_data_transaction_context *context)
{
  GByteArray *out = NULL;
  GError *err = NULL;

  gzochid_application_store *store = get_store (context->app_context);
  gzochid_event_source *event_source = NULL;
  guint64 encoded_oid = gzochid_util_encode_oid (reference->oid);

  switch (reference->state)
    {
    case GZOCHID_MANAGED_REFERENCE_STATE_EMPTY:
    case GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_EMPTY:
      break;

    case GZOCHID_MANAGED_REFERENCE_STATE_NEW:
    case GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED:
      
      out = g_byte_array_new ();
      assert (reference->obj != NULL);

      g_debug ("Flushing new/modified reference '%" G_GUINT64_FORMAT "'.",
	       reference->oid);

      reference->serialization->serializer
	(context->app_context, reference->obj, out, &err);

      if (err != NULL || gzochid_transaction_rollback_only ())
	{
	  g_clear_error (&err);
	  g_byte_array_unref (out);
	  return FALSE;
	}

      store->iface->transaction_put
	(context->transaction, store->oids, (char *) &encoded_oid,
	 sizeof (guint64), out->data, out->len);

      g_object_get (context->app_context, "event-source", &event_source, NULL);
      
      /* Need to cast `out->len' to `guint64' to support environments in which 
	 `guint' is less than 64 bits. */
      
      gzochid_event_dispatch
	(event_source, g_object_new (GZOCHID_TYPE_DATA_EVENT,
				     "type", BYTES_WRITTEN,
				     "bytes", (guint64) out->len,
				     NULL));
      
      g_byte_array_unref (out);
      g_source_unref ((GSource *) event_source);
      
      if (context->transaction->rollback)
	return FALSE;
      else break;
      
    case GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_FETCHED:
    case GZOCHID_MANAGED_REFERENCE_STATE_NOT_MODIFIED: break;
    default:
      assert (1 == 0);
    }

  return TRUE;
}

static int 
data_prepare (gpointer data)
{
  gzochid_data_transaction_context *context = data;
  GList *references = g_hash_table_get_values (context->oids_to_references);
  GList *reference_ptr = references;
  gboolean flush_failed = FALSE;
  gzochid_application_store *store = get_store (context->app_context);

  while (reference_ptr != NULL)
    {
      if (!flush_reference (reference_ptr->data, context))
	{
	  flush_failed = TRUE;
	  break;
	}

      reference_ptr = reference_ptr->next;
    }
  g_list_free (references);

  if (flush_failed)
    return FALSE;

  store->iface->transaction_prepare (context->transaction);

  if (context->transaction->rollback)
    return FALSE;

  return TRUE;
}

/* Cleanup function for managed references. */

static void
free_reference (gzochid_data_managed_reference *reference)
{
  g_object_unref (reference->context);
  free (reference);
}

static void 
finalize_references (gzochid_data_transaction_context *context)
{
  GList *references = g_hash_table_get_values (context->oids_to_references);
  GList *reference_ptr = references;

  while (reference_ptr != NULL)
    {
      gzochid_data_managed_reference *reference = reference_ptr->data;

      if (reference->obj != NULL)
	{
	  reference->serialization->finalizer 
	    (context->app_context, reference->obj);
	  reference->obj = NULL;
	}
      reference_ptr = reference_ptr->next;
    }
  
  g_list_free_full (references, (GDestroyNotify) free_reference);
}

static void 
data_commit (gpointer data)
{
  gzochid_data_transaction_context *context = data;
  gzochid_application_store *store = get_store (context->app_context);

  store->iface->transaction_commit (context->transaction);

  finalize_references (context);
  transaction_context_free (context);
}

static void 
data_rollback (gpointer data)
{
  gzochid_data_transaction_context *context = data;

  /* The rollback may have been triggered by the data manager's failure to join
     the transaction - because of a timeout, for example - in which case the
     local transaction context will be NULL. */

  if (context != NULL)
    {
      gzochid_application_store *store = get_store (context->app_context);      
      store->iface->transaction_rollback (context->transaction);

      finalize_references (context);
      transaction_context_free (context);
    }
}

static gzochid_transaction_participant data_participant = 
  { "data", data_prepare, data_commit, data_rollback };

static guint64
get_binding (gzochid_data_transaction_context *context, char *name,
	     GError **err)
{
  size_t oid_bytes_len = 0;
  gzochid_application_store *store = get_store (context->app_context);
  unsigned char *oid_bytes = store->iface->transaction_get 
    (context->transaction, store->names, name, strlen (name) + 1,
     &oid_bytes_len);

  assert (oid_bytes == NULL || oid_bytes_len == sizeof (guint64));
  
  if (context->transaction->rollback)
    {
      gzochid_transaction_mark_for_rollback 
	(&data_participant, context->transaction->should_retry);

      g_set_error
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Transaction rolled back while retrieving binding.");

      return 0;
    }
  else if (oid_bytes != NULL)
    {
      guint64 oid;
      
      memcpy (&oid, oid_bytes, sizeof (guint64));
      free (oid_bytes);

      return gzochid_util_decode_oid (oid);
    }
 else
   {
     g_set_error
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND, 
	 "No data found for binding '%s'.", name);
     
     return 0;
   }
}

static void 
set_binding (gzochid_data_transaction_context *context, char *name, guint64 oid,
	     GError **err)
{
  guint64 encoded_oid = gzochid_util_encode_oid (oid);
  gzochid_application_store *store = get_store (context->app_context);

  store->iface->transaction_put 
    (context->transaction, store->names, name, strlen (name) + 1,
     (unsigned char *) &encoded_oid, sizeof (guint64));

  if (context->transaction->rollback)
    {
      gzochid_transaction_mark_for_rollback 
	(&data_participant, context->transaction->should_retry);
      g_set_error 
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Transaction rolled back while setting binding.");
    }
}

static gzochid_data_managed_reference *
create_empty_reference (GzochidApplicationContext *context, guint64 oid, 
			gzochid_io_serialization *serialization)
{
  gzochid_data_managed_reference *reference = 
    calloc (1, sizeof (gzochid_data_managed_reference));

  reference->context = g_object_ref (context);
  reference->oid = oid;
  reference->state = GZOCHID_MANAGED_REFERENCE_STATE_EMPTY;
  reference->serialization = serialization;
  
  return reference;
}

static gboolean
next_object_id (gzochid_data_transaction_context *context, guint64 *oid,
		GError **err)
{  
  gboolean first_allocation = context->next_oid == 0 && context->last_oid == 0;
  gboolean needs_more_oids = context->next_oid > context->last_oid;
  
  if (first_allocation || needs_more_oids)
    {
      gzochid_data_oids_block block;
      gzochid_application_store *store = NULL;

      g_object_get (context->app_context, "app-store", &store, NULL);

      if (gzochid_oids_reserve_block (store->oid_strategy, &block, NULL))
	{
	  context->next_oid = block.block_start;
	  context->last_oid = block.block_start + block.block_size - 1;
	}
      else
	{
	  g_set_error
	    (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION,
	     "Failed to compute next object id.");
	  return FALSE;
	}
    }
  
  *oid = context->next_oid++;

  return TRUE;
}

gzochid_data_managed_reference *
create_new_reference (GzochidApplicationContext *context, void *ptr, 
		      gzochid_io_serialization *serialization, GError **err)
{
  guint64 oid;
  GError *local_err = NULL;
  gzochid_application_store *store = get_store (context);
  gzochid_data_transaction_context *tx_context = 
    gzochid_transaction_context (&data_participant);
  
  if (!next_object_id (tx_context, &oid, &local_err))
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  /*
    Once this function returns, callers will have an oid of the object that will
    be the permanent / effective oid if the current transaction commits. To
    prevent a flurry of contention around the end of the transaction, take a
    write lock on the oid now, first checking to make sure the underlying 
    storage transaction is in a good state.
  */
  
  if (!tx_context->transaction->rollback)
    {
      gzochid_data_managed_reference *reference = 
	calloc (1, sizeof (gzochid_data_managed_reference));
      guint64 encoded_oid;

      reference->context = g_object_ref (tx_context->app_context);
      reference->oid = oid;
      reference->state = GZOCHID_MANAGED_REFERENCE_STATE_NEW;
      reference->serialization = serialization;
      reference->obj = ptr;

      encoded_oid = gzochid_util_encode_oid (reference->oid);
      
      store->iface->transaction_get_for_update
	(tx_context->transaction, store->oids, (char *) &encoded_oid,
	 sizeof (guint64), NULL);

      if (tx_context->transaction->rollback)	
	{
	  gzochid_transaction_mark_for_rollback 
	    (&data_participant, tx_context->transaction->should_retry);
	  g_set_error 
	    (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	     "Transaction rolled back while creating new reference.");

	  free_reference (reference);
	  return NULL;
	}
      else return reference;
    }
  else
    {
      g_set_error 
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Attempted reference creation after transaction marked for rollback.");
      return NULL;
    }
}

static gzochid_data_managed_reference *
get_reference_by_oid (GzochidApplicationContext *context, guint64 oid, 
		      gzochid_io_serialization *serialization)
{
  gzochid_data_transaction_context *tx_context = 
    gzochid_transaction_context (&data_participant);
  gzochid_data_managed_reference *reference = g_hash_table_lookup 
    (tx_context->oids_to_references, &oid);

  if (reference == NULL)
    {
      reference = create_empty_reference (context, oid, serialization);
      g_hash_table_insert
	(tx_context->oids_to_references, g_memdup (&oid, sizeof (guint64)),
	 reference);
    }

  return reference;
}

static gzochid_data_managed_reference *
get_reference_by_ptr (GzochidApplicationContext *context, void *ptr, 
		      gzochid_io_serialization *serialization, GError **err)
{
  gzochid_data_transaction_context *tx_context = 
    gzochid_transaction_context (&data_participant);
  gzochid_data_managed_reference *reference = g_hash_table_lookup 
    (tx_context->ptrs_to_references, ptr);

  if (reference == NULL)
    {
      GError *local_err = NULL;

      reference = create_new_reference
	(context, ptr, serialization, &local_err);

      if (reference != NULL)
	{
	  g_hash_table_insert
	    (tx_context->oids_to_references,
	     g_memdup (&reference->oid, sizeof (guint64)), reference);
	  g_hash_table_insert (tx_context->ptrs_to_references, ptr, reference);
	}
      else g_propagate_error (err, local_err);
    }
  
  return reference;
}

static void 
dereference (gzochid_data_transaction_context *context, 
	     gzochid_data_managed_reference *reference, gboolean for_update,
	     GError **err)
{
  size_t data_len = 0;
  unsigned char *data = NULL; 
  GByteArray *in = NULL;
  guint64 encoded_oid;
  GzochidApplicationContext *app_context = context->app_context;
  gzochid_application_store *store = get_store (app_context);
  
  if (reference->obj != NULL
      || reference->state == GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_EMPTY)
    {
      /* Does the lock need to be upgraded? */
      
      if (for_update &&
	  reference->state == GZOCHID_MANAGED_REFERENCE_STATE_NOT_MODIFIED)
	gzochid_data_mark
	  (app_context, reference->serialization, reference->obj, err);
      
      return;
    }

  g_debug ("Retrieving data for reference '%" G_GUINT64_FORMAT "'.",
	   reference->oid);

  encoded_oid = gzochid_util_encode_oid (reference->oid);
  
  if (for_update)
    data = store->iface->transaction_get_for_update
      (context->transaction, store->oids, (char *) &encoded_oid,
       sizeof (guint64), &data_len);
  else data = store->iface->transaction_get
	 (context->transaction, store->oids, (char *) &encoded_oid, 
	  sizeof (guint64), &data_len);

  if (context->transaction->rollback)
    {
      gzochid_transaction_mark_for_rollback 
	(&data_participant, context->transaction->should_retry);
      g_set_error 
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Transaction rolled back while retrieving reference '%"
	 G_GUINT64_FORMAT "'.", reference->oid);
    }
  else if (data == NULL)
    g_set_error 
      (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND, 
       "No data found for reference '%" G_GUINT64_FORMAT "'.", reference->oid);
  else 
    {
      GError *local_err = NULL;
      gzochid_event_source *event_source = NULL;

      g_object_get (app_context, "event-source", &event_source, NULL);
      
      /* Need to cast `data_len' to `guint64' since `size_t' may be a different
	 size on 32-bit platforms. */
      
      gzochid_event_dispatch
	(event_source,
	 g_object_new (GZOCHID_TYPE_DATA_EVENT,
		       "type", BYTES_READ, "bytes", (guint64) data_len, NULL));

      g_source_unref ((GSource *) event_source);
      
      in = g_byte_array_sized_new (data_len);
      g_byte_array_append (in, (unsigned char *) data, data_len);
      free (data);

      reference->obj = reference->serialization->deserializer 
	(app_context, in, &local_err);

      if (local_err != NULL)
	g_propagate_error (err, local_err);	  
      else if (for_update)
	reference->state = GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED;
      else reference->state = GZOCHID_MANAGED_REFERENCE_STATE_NOT_MODIFIED;

      g_hash_table_insert
	(context->oids_to_references,
	 g_memdup (&reference->oid, sizeof (guint64)), reference);
      g_hash_table_insert 
	(context->ptrs_to_references, reference->obj, reference);

      g_byte_array_unref (in);
    }

  return;
}

static void 
remove_object (gzochid_data_transaction_context *context, guint64 oid, 
	       GError **err)
{
  int ret = 0;
  gzochid_application_store *store = get_store (context->app_context);
  
  /* The transaction is presumed to have already been joined at this point. */
  
  gzochid_data_transaction_context *tx_context =
    gzochid_transaction_context (&data_participant);

  guint64 encoded_oid = gzochid_util_encode_oid (oid);
  
  g_debug ("Removing reference '%" G_GUINT64_FORMAT "'.", oid);
  ret = store->iface->transaction_delete 
    (context->transaction, store->oids, (char *) &encoded_oid,
     sizeof (guint64));

  if (ret == GZOCHID_STORAGE_ENOTFOUND)
    g_set_error (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND, 
		 "No data found for reference '%" G_GUINT64_FORMAT "'.", oid);
  else if (ret == GZOCHID_STORAGE_ETXFAILURE)
    {
      gzochid_transaction_mark_for_rollback 
	(&data_participant, tx_context->transaction->should_retry);
      g_set_error 
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Transaction rolled back while removing reference '%"
	 G_GUINT64_FORMAT "'.", oid);
    }

  return;
}

static void 
remove_reference (gzochid_data_transaction_context *context, 
		  gzochid_data_managed_reference *reference, GError **err)
{
  switch (reference->state)
    {
    case GZOCHID_MANAGED_REFERENCE_STATE_EMPTY:
      
      remove_object (context, reference->oid, err);
      reference->state = GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_EMPTY;

      break;

    case GZOCHID_MANAGED_REFERENCE_STATE_NOT_MODIFIED:
    case GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED:

      remove_object (context, reference->oid, err);

    case GZOCHID_MANAGED_REFERENCE_STATE_NEW:
      reference->state = GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_FETCHED;
    default: break;
    }

  return;
}

static gzochid_data_transaction_context *
join_transaction (GzochidApplicationContext *context, GError **err)
{
  gzochid_data_transaction_context *tx_context = NULL;

  if (!gzochid_transaction_active ()
      || (tx_context = gzochid_transaction_context (&data_participant)) == NULL)
    {
      if (gzochid_transaction_timed ())
	{
	  struct timeval remaining = gzochid_transaction_time_remaining ();
	  if (remaining.tv_sec > 0 || remaining.tv_usec > 0)
	    tx_context = create_transaction_context (context, &remaining);
	}
      else tx_context = create_transaction_context (context, NULL);
      gzochid_transaction_join (&data_participant, tx_context);

      if (tx_context == NULL)
	{
	  g_set_error 
	    (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	     "Transaction timed out.");
	  gzochid_transaction_mark_for_rollback (&data_participant, TRUE);
	}
    }

  return tx_context;
}

void *
gzochid_data_get_binding (GzochidApplicationContext *context, char *name, 
			  gzochid_io_serialization *serialization, GError **err)
{
  guint64 oid;
  GError *local_err = NULL;
  gzochid_data_managed_reference *reference = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);
  
  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  oid = get_binding (tx_context, name, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  reference = get_reference_by_oid (context, oid, serialization);  

  dereference (tx_context, reference, FALSE, err);
  return reference->obj;
}

void 
gzochid_data_set_binding_to_oid (GzochidApplicationContext *context, 
				 char *name, guint64 oid, GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);

  if (local_err != NULL)
    g_propagate_error (err, local_err);
  else set_binding (tx_context, name, oid, err);
}

char *
gzochid_data_next_binding_oid (GzochidApplicationContext *context, char *key,
			       guint64 *oid, GError **err)
{
  char *next_key = NULL;
  GError *local_err = NULL;
  gzochid_application_store *store = get_store (context);
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  next_key = store->iface->transaction_next_key 
    (tx_context->transaction, store->names, key, strlen (key) + 1, NULL);
  if (tx_context->transaction->rollback)
    gzochid_transaction_mark_for_rollback 
      (&data_participant, tx_context->transaction->should_retry);

  if (next_key != NULL)
    {
      GError *local_err = NULL;
      guint64 local_oid = get_binding (tx_context, next_key, &local_err);

      if (local_err != NULL)
	{
	  g_propagate_error (err, local_err);
	  return NULL;
	}

      *oid = local_oid;
      return next_key;
    }
  else return NULL;
}

void 
gzochid_data_set_binding (GzochidApplicationContext *context, char *name, 
			  gzochid_io_serialization *serialization, void *data, 
			  GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);
  gzochid_data_managed_reference *reference = NULL;

  if (local_err != NULL)
    g_propagate_error (err, local_err);
  else 
    {
      reference = get_reference_by_ptr
	(context, data, serialization, &local_err);
      if (local_err == NULL)
	set_binding (tx_context, name, reference->oid, &local_err);
      if (local_err != NULL)
	g_propagate_error (err, local_err);
    }
}

void 
gzochid_data_remove_binding (GzochidApplicationContext *context, char *name, 
			     GError **err)
{
  int ret = 0;
  GError *local_err = NULL;
  gzochid_application_store *store = get_store (context);
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return;
    }

  ret = store->iface->transaction_delete 
    (tx_context->transaction, store->names, name, strlen (name) + 1);

  if (ret == GZOCHID_STORAGE_ENOTFOUND)
    g_set_error (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_NOT_FOUND, 
		 "No data found for binding '%s'.", name);
  else if (ret == GZOCHID_STORAGE_ETXFAILURE)
    {
      gzochid_transaction_mark_for_rollback 
	(&data_participant, tx_context->transaction->should_retry);
      g_set_error 
	(err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	 "Transaction rolled back while removing binding '%s'.", name);
    }
}

gboolean
gzochid_data_binding_exists (GzochidApplicationContext *context, char *name, 
			     GError **err)
{
  gboolean ret = TRUE;
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return FALSE;
    }

  get_binding (tx_context, name, &local_err);
  
  if (local_err != NULL)
    {
      if (local_err->code != GZOCHID_DATA_ERROR_NOT_FOUND)       
	g_propagate_error (err, local_err);
      else g_error_free (local_err);

      ret = FALSE;
    }

  return ret;
}

gzochid_data_managed_reference *gzochid_data_create_reference
(GzochidApplicationContext *context, gzochid_io_serialization *serialization,
 void *ptr, GError **err)
{
  GError *local_err = NULL;
  join_transaction (context, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }
  return get_reference_by_ptr (context, ptr, serialization, err);
}

gzochid_data_managed_reference *
gzochid_data_create_reference_to_oid (GzochidApplicationContext *context, 
				      gzochid_io_serialization *serialization, 
				      guint64 oid)
{
  GError *err = NULL;

  join_transaction (context, &err);

  if (err != NULL)
    {
      g_error_free (err);
      return create_empty_reference (context, oid, serialization);
    }
  else return get_reference_by_oid (context, oid, serialization);
}

void *
gzochid_data_dereference (gzochid_data_managed_reference *reference, 
			  GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (reference->context, &local_err);
  
  if (local_err != NULL)
    g_propagate_error (err, local_err);
  else dereference (tx_context, reference, FALSE, err);

  return reference->obj;
}

void *
gzochid_data_dereference_for_update (gzochid_data_managed_reference *reference, 
				     GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (reference->context, &local_err);
  
  if (local_err != NULL)
    g_propagate_error (err, local_err);
  else dereference (tx_context, reference, TRUE, err);

  return reference->obj;
}

void 
gzochid_data_remove_object (gzochid_data_managed_reference *reference, 
			    GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (reference->context, &local_err);

  if (local_err != NULL)
    g_propagate_error (err, local_err);
  else remove_reference (tx_context, reference, err);
}

void 
gzochid_data_mark (GzochidApplicationContext *context, 
		   gzochid_io_serialization *serialization, void *ptr, 
		   GError **err)
{
  GError *local_err = NULL;
  gzochid_data_transaction_context *tx_context = 
    join_transaction (context, &local_err);
  gzochid_data_managed_reference *reference = NULL;

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return;
    }

  reference = get_reference_by_ptr (context, ptr, serialization, err);

  if (reference != NULL
      && reference->state != GZOCHID_MANAGED_REFERENCE_STATE_NEW
      && reference->state != GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED)
    {
      unsigned char *data = NULL;
      guint64 encoded_oid = gzochid_util_encode_oid (reference->oid);
      gzochid_application_store *store = get_store (context);
      
      reference->state = GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED;
      g_debug ("Marking reference '%" G_GUINT64_FORMAT "' for update.",
	       reference->oid);

      data = store->iface->transaction_get_for_update
	(tx_context->transaction, store->oids, (char *) &encoded_oid,
	 sizeof (guint64), NULL);
   
      if (tx_context->transaction->rollback)
	{
	  gzochid_transaction_mark_for_rollback 
	    (&data_participant, tx_context->transaction->should_retry);
	  g_set_error 
	    (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION, 
	     "Transaction rolled back while removing reference '%"
	     G_GUINT64_FORMAT "'.", reference->oid);
	}
      else free (data);
    }
}

/*
  Print to standard output the list of managed references accessed by the
  specified transaction context, along with their state - `MODIFIED', `NEW', 
  etc.

  Use this function within GDB to inspect the behavior of transactions against
  the data service.
*/

void
gzochid_data_print_references (gzochid_data_transaction_context *context)
{
  GList *values = g_hash_table_get_values (context->ptrs_to_references);
  GList *values_ptr = values;

  while (values_ptr != NULL)
    {
      const char *state;
      gzochid_data_managed_reference *ref = values_ptr->data;
      
      switch (ref->state)
	{
	case GZOCHID_MANAGED_REFERENCE_STATE_NEW: state = "NEW"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_EMPTY: state = "EMPTY"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_NOT_MODIFIED:
	  state = "NOT MODIFIED"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_MAYBE_MODIFIED:
	  state = "MAYBE MODIFIED"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_MODIFIED: state =
	    "MODIFIED"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_FLUSHED: state = "FLUSHED"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_EMPTY:
	  state = "REMOVED EMPTY"; break;
	case GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_FETCHED:
	  state = "REMOVED FETCHED"; break;
	default: state = "UNKNOWN";
	}
      
      printf ("%" G_GUINT64_FORMAT " (serialization: %p) -> %s\n", ref->oid,
	      ref->serialization, state);

      values_ptr = values_ptr->next;
    }
  
  g_list_free (values);
}
