/* app-task.h: Prototypes and declarations for app-task.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_APP_TASK_H
#define GZOCHID_APP_TASK_H

#include <glib.h>
#include <sys/time.h>

#include "app.h"
#include "gzochid-auth.h"
#include "io.h"
#include "task.h"
#include "tx.h"

/* Enumeration of application states during the bootstrap process. */

enum _gzochid_application_bootstrap_stage
  {
    /* The application is still waiting to begin normal task processing. */

    NOT_STARTED,

    /* The bootstrap process has started (i.e., the transactional bootstrap 
       work has been scheduled) but has not yet completed. */
    
    STARTING,
    
    STARTED, /* The application is fully bootstrapped and processing tasks. */
    FAILED /* The transactional bootstrap work did not complete successfully. */
  };

typedef enum _gzochid_application_bootstrap_stage
gzochid_application_bootstrap_stage;

/* Encapsulates state related to whether an application is ready to submit tasks
   to the local server's task processing queue. */

struct _gzochid_application_state
{
  /* The bootstrap stage. */

  gzochid_application_bootstrap_stage bootstrap_stage; 
  
  /* List of `guint64' pointers representing durable task oids buffered for 
     queue submission. */

  GList *queued_task_oids; 

  GMutex mutex; /* Synchronizes access to the running flag and task buffers. */
  GCond cond; /* Condition for broadcasting changes in running status. */
};

typedef struct _gzochid_application_state gzochid_application_state;

/* Construct and return a pointer to a new application state object. The memory
   used by this object should be released via `gzochid_application_state_free'
   when no longer needed. */

gzochid_application_state *gzochid_application_state_new ();

/* Frees the memory associated with the specified `gzochid_application_state'
   object. */

void gzochid_application_state_free (gzochid_application_state *);

/* A function type to represent an application-specific task executing on behalf
   of a particular identity. */

typedef void (*gzochid_application_worker) 
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);

/* An application-specific task. Conceptually, this is a layer on top of 
   `GzochidTask'. */

struct _gzochid_application_task
{
  /* The application to which the task belongs. */

  GzochidApplicationContext *context; 

  gzochid_auth_identity *identity; /* The identity that "owns" the task. */
  gzochid_application_worker worker; /* The task worker function. */

  /* The task destroy function, called when the task's reference count reaches 
     zero. */

  GDestroyNotify destroy_notify; 

  gpointer data; /* The task data. */
  guint ref_count; /* The reference count. */
};

typedef struct _gzochid_application_task gzochid_application_task;

struct _gzochid_transactional_application_task_execution
{
  /* The main task to execute. This task is attempted up to the maximum number 
     of retries, in separate transactions. */
  
  gzochid_application_task *task;

  /* An optional "catch" task to be executed if and only if the main task's
     transaction is marked for rollback and cannot be retried. This task is
     attempted once, in a separate transaction. */
  
  gzochid_application_task *catch_task;

  /* An optional "cleanup" task that is always executed after the main task's
     final attempt - and after the catch task, if one is present. This task is
     executed once, outside of a transaction. */
  
  gzochid_application_task *cleanup_task;

  struct timeval *timeout;
  unsigned int attempts;
  unsigned int max_attempts;
  gzochid_transaction_result result;
};

typedef struct _gzochid_transactional_application_task_execution
gzochid_transactional_application_task_execution;

gzochid_transaction_result gzochid_application_transaction_execute 
(GzochidApplicationContext *, void (*) (gpointer), gpointer);

gzochid_transaction_result gzochid_application_transaction_execute_timed 
(GzochidApplicationContext *, void (*) (gpointer), gpointer, struct timeval);

gzochid_application_task *gzochid_application_task_new 
(GzochidApplicationContext *, gzochid_auth_identity *, 
 gzochid_application_worker, GDestroyNotify, gpointer);

/* Increases the reference count of the specified application task and returns 
   the application task. */

gzochid_application_task *gzochid_application_task_ref
(gzochid_application_task *);

/* Decreases the reference count of the specified application task. When the
   reference count reaches zero, the memory associated with the application task
   will be freed. */

void gzochid_application_task_unref (gzochid_application_task *);

/* Frees the memory associated with the specified application task. */

void gzochid_application_task_free (gzochid_application_task *);

/* Create a new transactional application task execution context with the
   specified main task and optional catch and cleanup tasks. */

gzochid_transactional_application_task_execution *
gzochid_transactional_application_task_execution_new 
(gzochid_application_task *, gzochid_application_task *,
 gzochid_application_task *);

/* Create a new transactional application task execution context with the
   specified main task, optional catch and cleanup tasks, a timeval giving an
   upper bound on the main task's allowed execution time. */

gzochid_transactional_application_task_execution *
gzochid_transactional_application_task_timed_execution_new 
(gzochid_application_task *, gzochid_application_task *,
 gzochid_application_task *, struct timeval);

void gzochid_transactional_application_task_execution_free
(gzochid_transactional_application_task_execution *);

void gzochid_application_transactional_task_worker 
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);

/* A `gzochid_application_worker' implementation that synchronously executes the
   transactional application task wrapped by the specified 
   `gzochid_transactional_application_task_execution', synchronously retrying on
   transaction failure up to the task retry maximum; and runs the wrapped
   catch and cleanup handler tasks as appropriate. */

void gzochid_application_reexecuting_transactional_task_worker 
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);

/* A `gzochid_application_worker' implementation that asynchronously executes
   the transactional application task wrapped by the specified
   `gzochid_transactional_application_task_execution', resubmitting the task on
   transaction failure up to the task retry maximum; and submits the wrapped
   catch and cleanup handler tasks as appropriate. */

void gzochid_application_resubmitting_transactional_task_worker 
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);

void gzochid_application_task_thread_worker (gpointer, gpointer);

gboolean gzochid_application_should_retry 
(gzochid_transactional_application_task_execution *);

#endif /* GZOCHID_APP_TASK_H */
