/* taskserver-protocol.c: Implementation of taskserver protocol.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <gzochi-common.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "meta-protocol.h"
#include "protocol-common.h"
#include "taskserver-protocol.h"
#include "taskserver.h"
#include "socket.h"

/* Taskserver client struct. */

struct _gzochi_metad_taskserver_client
{
  guint node_id; /* The client ephemeral node id. */  
  GzochiMetadTaskServer *taskserver; /* Reference to the task server. */
};

gzochi_metad_taskserver_client *
gzochi_metad_taskserver_client_new (GzochiMetadTaskServer *taskserver,
				    gzochid_client_socket *sock,
				    unsigned int node_id)
{
  gzochi_metad_taskserver_client *client =
    malloc (sizeof (gzochi_metad_taskserver_client));

  client->taskserver = g_object_ref (taskserver);
  client->node_id = node_id;
  
  return client;
}

void
gzochi_metad_taskserver_client_free (gzochi_metad_taskserver_client *client)
{
  g_object_unref (client->taskserver);  

  free (client);
}

/*
  Returns `TRUE' if the specified buffer has a complete message that it is 
  ready (with respect to its length) to be dispatched to a handler. The length
  encoding is similar to the one used for the game protocol: A two-byte, 
  big-endian prefix giving the length of the message payload minus the opcode,
  which is the byte directly following the length prefix. 
  
  (So the smallest possible message would be three bytes, in which the first 
  two bytes were `NULL'.) 
*/

static gboolean
can_dispatch (const GByteArray *buffer, gpointer user_data)
{
  if (buffer->len >= 3)
    {
      unsigned short payload_len =
        gzochi_common_io_read_short (buffer->data, 0);
      return buffer->len >= payload_len + 3;
    }
  else return FALSE;
}

/* Processes the message payload following the 
   `GZOZCHID_TASK_PROTOCOL_START_APP' opcode. */

static void
dispatch_start_app (gzochi_metad_taskserver_client *client,
		    unsigned char *message, unsigned short len)
{
  size_t str_len = 0;
  const char *app = gzochid_protocol_read_str (message, len, &str_len);

  if (app == NULL || str_len <= 1)
    g_warning ("Received malformed 'APP_START' message from node %d.",
	       client->node_id);
  else
    {
      GError *err = NULL;

      gzochi_metad_taskserver_application_started
	(client->taskserver, client->node_id, app, &err);

      if (err != NULL)
	{
	  g_warning ("Failed to process start notification: %s", err->message);
	  g_error_free (err);
	}
    }
}

/* Processes the message payload following the 
   `GZOZCHID_TASK_PROTOCOL_COMPLETE_RESUBMISSION' opcode. */

static void
dispatch_complete_resubmission (gzochi_metad_taskserver_client *client,
				unsigned char *message, unsigned short len)
{
  size_t str_len = 0;
  const char *app = gzochid_protocol_read_str (message, len, &str_len);

  if (app == NULL || str_len <= 1)
    g_warning
      ("Received malformed 'COMPLETE_RESUBMISSION' message from node %d.",
       client->node_id);
  else
    {
      GError *err = NULL;

      gzochi_metad_taskserver_application_resubmitted
	(client->taskserver, client->node_id, app, &err);

      if (err != NULL)
	{
	  g_warning ("Failed to process resubmission completion: %s",
		     err->message);
	  g_error_free (err);
	}
    }
}

/* Processes the message payload following the 
   `GZOZCHID_TASK_PROTOCOL_SUBMIT_TASK' opcode. */

static void
dispatch_submit_task (gzochi_metad_taskserver_client *client,
		      unsigned char *message, unsigned short len)
{
  size_t str_len = 0;
  const char *app = gzochid_protocol_read_str (message, len, &str_len);

  if (app == NULL || str_len <= 1 || len - str_len < 8)
    g_warning ("Received malformed 'SUBMIT_TASK' message from node %d.",
	       client->node_id);
  else
    {
      GError *err = NULL;
      guint64 task_id = gzochi_common_io_read_long (message, str_len);

      gzochi_metad_taskserver_task_submitted
	(client->taskserver, client->node_id, app, task_id, &err);

      if (err != NULL)
	{
	  g_warning ("Failed to process task submission: %s", err->message);
	  g_error_free (err);
	}
    }
}

/* Processes the message payload following the 
   `GZOZCHID_TASK_PROTOCOL_TASK_COMPLETED' opcode. */

static void
dispatch_task_completed (gzochi_metad_taskserver_client *client,
			 unsigned char *message, unsigned short len)
{
  size_t str_len = 0;
  const char *app = gzochid_protocol_read_str (message, len, &str_len);

  if (app == NULL || str_len <= 1 || len - str_len < 8)
    g_warning ("Received malformed 'TASK_COMPLETED' message from node %d.",
	       client->node_id);
  else
    {
      GError *err = NULL;
      guint64 task_id = gzochi_common_io_read_long (message, str_len);

      gzochi_metad_taskserver_task_completed
	(client->taskserver, client->node_id, app, task_id, &err);

      if (err != NULL)
	{
	  g_warning ("Failed to process task completion: %s", err->message);
	  g_error_free (err);
	}
    }
}

/* Processes the message payload following the 
   `GZOZCHID_TASK_PROTOCOL_CANCEL_TASK' opcode. */

static void
dispatch_cancel_task (gzochi_metad_taskserver_client *client,
		      unsigned char *message, unsigned short len)
{
  size_t str_len = 0;
  const char *app = gzochid_protocol_read_str (message, len, &str_len);

  if (app == NULL || str_len <= 1 || len - str_len < 8)
    g_warning ("Received malformed 'CANCEL_TASK' message from node %d.",
	       client->node_id);
  else
    {
      GError *err = NULL;
      guint64 task_id = gzochi_common_io_read_long (message, str_len);

      gzochi_metad_taskserver_task_canceled
	(client->taskserver, client->node_id, app, task_id, &err);

      if (err != NULL)
	{
	  g_warning ("Failed to process task cancellation: %s", err->message);
	  g_error_free (err);
	}
    }
}

/* Attempt to dispatch a fully-buffered message from the specified client based
   on its opcode. */

static void 
dispatch_message (gzochi_metad_taskserver_client *client,
                  unsigned char *message, unsigned short len)
{
  int opcode = message[0];
  unsigned char *payload = message + 1;
  
  len--;
  
  switch (opcode)
    {
    case GZOCHID_TASK_PROTOCOL_START_APP:
      dispatch_start_app (client, payload, len); break; 
    case GZOCHID_TASK_PROTOCOL_COMPLETE_RESUBMISSION:
      dispatch_complete_resubmission (client, payload, len); break;
    case GZOCHID_TASK_PROTOCOL_SUBMIT_TASK:
      dispatch_submit_task (client, payload, len); break;       
    case GZOCHID_TASK_PROTOCOL_TASK_COMPLETED:
      dispatch_task_completed (client, payload, len); break;       
    case GZOCHID_TASK_PROTOCOL_CANCEL_TASK:
      dispatch_cancel_task (client, payload, len); break;       
      
    default:
      g_warning ("Unexpected opcode %d received from client", opcode);
    }
  
  return;
}

/* Attempts to dispatch all messages in the specified buffer. Returns the 
   number of successfully dispatched messages. */

static unsigned int
client_dispatch (const GByteArray *buffer, gpointer user_data)
{
  gzochi_metad_taskserver_client *client = user_data;

  int offset = 0, total = 0;
  int remaining = buffer->len;

  while (remaining >= 3)
    {
      unsigned short len = gzochi_common_io_read_short
        ((unsigned char *) buffer->data, offset);
      
      if (++len > remaining - 2)
        break;
      
      offset += 2;

      dispatch_message (client, (unsigned char *) buffer->data + offset, len);
      
      offset += len;
      remaining -= len + 2;
      total += len + 2;
    }

  return total;
}

/* The client error handler. */

static void
client_error (gpointer user_data)
{
  GError *err = NULL;
  gzochi_metad_taskserver_client *client = user_data;

  gzochi_metad_taskserver_server_disconnected
    (client->taskserver, client->node_id, &err);
}

/* Client finalization callback. */

static void
client_free (gpointer user_data)
{
  gzochi_metad_taskserver_client_free (user_data);
}

gzochid_client_protocol gzochi_metad_taskserver_client_protocol =
  { can_dispatch, client_dispatch, client_error, client_free };
