/* storageengine.h: Prototypes and declarations for storageengine.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_STORAGE_ENGINE_H
#define GZOCHID_STORAGE_ENGINE_H

#include <glib-object.h>

/* The core storage engine container type definitions. */

#define GZOCHID_TYPE_STORAGE_ENGINE_CONTAINER \
  gzochid_storage_engine_container_get_type ()

G_DECLARE_FINAL_TYPE (GzochidStorageEngineContainer,
		      gzochid_storage_engine_container, GZOCHID,
                      STORAGE_ENGINE_CONTAINER, GObject);

#endif /* GZOCHID_STORAGE_ENGINE_H */
