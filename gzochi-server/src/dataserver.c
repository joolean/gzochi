/* dataserver.c: Data server for gzochi-metad
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "app-store.h"
#include "config.h"
#include "dataserver-protocol.h"
#include "dataserver.h"
#include "gzochid-storage.h"
#include "httpd.h"
#include "lock.h"
#include "log.h"
#include "oids-storage.h"
#include "oids.h"
#include "resolver.h"
#include "socket.h"
#include "storage-mem.h"
#include "storage.h"
#include "util.h"

#ifdef G_LOG_DOMAIN
#undef G_LOG_DOMAIN
#endif /* G_LOG_DOMAIN */
#define G_LOG_DOMAIN "gzochi-metad.dataserver"

#ifndef GZOCHID_STORAGE_ENGINE_DIR
#define GZOCHID_STORAGE_ENGINE_DIR "./storage"
#endif /* GZOCHID_STORAGE_ENGINE_DIR */

#define LOCK_ACCESS(for_write) (for_write ? "r/w" : "read")

/* A wrapper around `gzochid_application_store' that composes it with a pair
   of lock tables. */

struct _gzochid_lockable_application_store
{
  gzochid_application_store *store; /* The application store. */

  gzochid_lock_table *oids_locks; /* Lock table for the object store. */
  gzochid_lock_table *names_locks; /* Lock table for the binding store. */

  /* Map of node id (as `guint') to `GHashTable' mapping of changeset id (as 
     `guint64') to `GArray' of `gzochid_data_change'. */

  GHashTable *changeset_submissions; 
};

typedef struct _gzochid_lockable_application_store
gzochid_lockable_application_store;

/* Boilerplate setup for the data server object. */

/* The data server object. */

struct _GzochiMetadDataServer
{
  GObject parent_instance; /* The parent struct, for casting. */

  GzochidConfiguration *configuration; /* The global configuration object. */

  /* The data client configuration table; extracted from the global 
     configuration object and cached for convenience. */

  GHashTable *data_configuration;

  /* The global resolution context; used for getting a (temporary) handle to
     the meta server's embedded HTTP server (if available) to extract the base
     URL to send to the client as part of the login response. */
  
  GzochidResolutionContext *resolution_context;
  
  /* The socket server for the data server. */
  
  GzochidSocketServer *socket_server; 

  gzochid_storage_engine *storage_engine; /* The storage engine. */

  /* Mapping application name to `gzochid_lockable_application_store'. */

  GHashTable *application_stores; 
};

#define STORAGE_INTERFACE(server) server->storage_engine->interface

G_DEFINE_TYPE (GzochiMetadDataServer, gzochi_metad_data_server, G_TYPE_OBJECT);

enum gzochi_metad_data_server_properties
  {
    PROP_CONFIGURATION = 1,
    PROP_RESOLUTION_CONTEXT,
    PROP_SOCKET_SERVER,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
gzochi_metad_data_server_constructed (GObject *object)
{
  GzochiMetadDataServer *data_server = GZOCHI_METAD_DATA_SERVER (object);

  data_server->data_configuration = gzochid_configuration_extract_group
    (data_server->configuration, "data");  
}

static void
gzochi_metad_data_server_set_property (GObject *object, guint property_id,
				       const GValue *value, GParamSpec *pspec)
{
  GzochiMetadDataServer *self = GZOCHI_METAD_DATA_SERVER (object);

  switch (property_id)
    {
    case PROP_CONFIGURATION:
      self->configuration = g_object_ref (g_value_get_object (value));
      break;

    case PROP_RESOLUTION_CONTEXT:
      self->resolution_context = g_object_ref_sink (g_value_get_object (value));
      break;
      
    case PROP_SOCKET_SERVER:
      self->socket_server = g_object_ref (g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochi_metad_data_server_finalize (GObject *gobject)
{
  GzochiMetadDataServer *server = GZOCHI_METAD_DATA_SERVER (gobject);
  
  g_hash_table_destroy (server->data_configuration);
  g_hash_table_destroy (server->application_stores);

  if (server->storage_engine != NULL)
    free (server->storage_engine);

  G_OBJECT_CLASS (gzochi_metad_data_server_parent_class)->finalize (gobject);
}

static void
gzochi_metad_data_server_dispose (GObject *gobject)
{
  GzochiMetadDataServer *server = GZOCHI_METAD_DATA_SERVER (gobject);

  g_object_unref (server->configuration);
  g_object_unref (server->resolution_context);
  g_object_unref (server->socket_server);

  G_OBJECT_CLASS (gzochi_metad_data_server_parent_class)->dispose (gobject);
}

static void
gzochi_metad_data_server_class_init (GzochiMetadDataServerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = gzochi_metad_data_server_constructed;
  object_class->dispose = gzochi_metad_data_server_dispose;
  object_class->finalize = gzochi_metad_data_server_finalize;
  object_class->set_property = gzochi_metad_data_server_set_property;

  obj_properties[PROP_CONFIGURATION] = g_param_spec_object
    ("configuration", "config", "The global configuration object",
     GZOCHID_TYPE_CONFIGURATION, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  obj_properties[PROP_RESOLUTION_CONTEXT] = g_param_spec_object
    ("resolution-context", "resolution-context",
     "The global resolution context", GZOCHID_TYPE_RESOLUTION_CONTEXT,
     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);
  
  obj_properties[PROP_SOCKET_SERVER] = g_param_spec_object
    ("socket-server", "socket-server", "The global socket server",
     GZOCHID_TYPE_SOCKET_SERVER, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

static void
close_application_store (gpointer ptr)
{
  gzochid_lockable_application_store *lockable_store = ptr;

  gzochid_oid_allocation_strategy_free (lockable_store->store->oid_strategy);
  gzochid_application_store_close (lockable_store->store);
  
  gzochid_lock_table_free (lockable_store->oids_locks);
  gzochid_lock_table_free (lockable_store->names_locks);

  g_hash_table_destroy (lockable_store->changeset_submissions);
  
  free (lockable_store);
}

static void
gzochi_metad_data_server_init (GzochiMetadDataServer *self)
{
  self->application_stores = g_hash_table_new_full
    (g_str_hash, g_str_equal, free, close_application_store);
}

void
gzochi_metad_dataserver_start (GzochiMetadDataServer *self)
{
  if (g_hash_table_contains (self->data_configuration, "storage.engine"))
    {
      const char *dir = NULL;
      const char *env = getenv ("GZOCHID_STORAGE_ENGINE_DIR");
      
      if (env != NULL)
        dir = env;
      else 
        {
          const char *conf_dir = g_hash_table_lookup
	    (self->data_configuration, "storage.engine.dir");
          dir = conf_dir == NULL ? GZOCHID_STORAGE_ENGINE_DIR : conf_dir;
        }

      self->storage_engine = gzochid_storage_load_engine 
        (dir, g_hash_table_lookup (self->data_configuration, "storage.engine"));
    }
  else g_message 
         ("No durable storage engine configured; memory engine will be used.");
  
  if (self->storage_engine == NULL)
    {
      g_message ("\
Using in-memory storage for application data. THIS CONFIGURATION IS NOT SAFE \
FOR PRODUCTION USE.");

      self->storage_engine = calloc (1, sizeof (gzochid_storage_engine));
      self->storage_engine->interface = &gzochid_storage_engine_interface_mem;
    }
}

/* Switch on the specified store name to fill out the specified store and lock
   table pointers to pointer to the oids store and lock table, the names 
   bindings store and lock table, or set an error if the name was not "oids" or 
   "names." */

static void
get_lockable_store (gzochid_lockable_application_store *app_store,
		    const char *name, gzochid_storage_store **store,
		    gzochid_lock_table **locks, GError **err)
{
  if (strcmp (name, "oids") == 0)
    {
      *store = app_store->store->oids;
      *locks = app_store->oids_locks;
    }
  else if (strcmp (name, "names") == 0)
    {
      *store = app_store->store->names;
      *locks = app_store->names_locks;
    }
  else g_set_error
	 (err, GZOCHI_METAD_DATASERVER_ERROR,
	  GZOCHI_METAD_DATASERVER_ERROR_STORE_NAME, "Invalid store name '%s'.",
	  name);
}

/* 
  Ensures that an application store for the specified application name is open
  and managed by the specified data store, opening / creating one if 
  necessary.
  
  This function returns a pointer to the new or existing application store; 
  this pointer is owned by the data server and should not be freed. 
*/

static gzochid_lockable_application_store *
ensure_open_application_store (const GzochiMetadDataServer *server,
			       const char *app)
{
  if (g_hash_table_contains (server->application_stores, app))
    return g_hash_table_lookup (server->application_stores, app);
  else
    {
      gzochid_storage_engine_interface *iface = STORAGE_INTERFACE (server);
      gzochid_lockable_application_store *lockable_store =
	malloc (sizeof (gzochid_lockable_application_store));

      lockable_store->store = gzochid_application_store_open
	(iface, app, gzochid_storage_oid_strategy_new (iface, app));
      lockable_store->oids_locks = gzochid_lock_table_new ("oids");
      lockable_store->names_locks = gzochid_lock_table_new ("names");
      lockable_store->changeset_submissions = g_hash_table_new_full
	(g_int_hash, g_int_equal, free, (GDestroyNotify) g_hash_table_destroy);
      
      g_message ("Initializing application storage for '%s'.", app);
      
      g_hash_table_insert
	(server->application_stores, strdup (app), lockable_store);
      return lockable_store;
    }
}

gzochid_data_reserve_oids_response *
gzochi_metad_dataserver_reserve_oids (GzochiMetadDataServer *server,
				      guint node_id, const char *app)
{
  gzochid_data_oids_block oids_block;
  gzochid_data_reserve_oids_response *response = NULL;
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);

  gzochid_trace ("Node %d requested oid block for %s.", node_id, app);
  
  assert (gzochid_oids_reserve_block
	  (app_store->store->oid_strategy, &oids_block, NULL));

  gzochid_trace ("Reserved block { %" G_GUINT64_FORMAT ", %d } for node %d/%s.",
		 oids_block.block_start, oids_block.block_size, node_id, app);
  
  response = gzochid_data_reserve_oids_response_new (app, &oids_block);
  return response;
}

gzochid_data_response *
gzochi_metad_dataserver_request_value (GzochiMetadDataServer *server,
				       guint node_id, const char *app,
				       const char *store_name, GBytes *key,
				       gboolean for_write, GError **err)
{
  gzochid_data_response *response = NULL;
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);

  GError *local_err = NULL;
  gzochid_storage_store *store = NULL;
  gzochid_lock_table *locks = NULL;
  struct timeval most_recent_lock;

  get_lockable_store (app_store, store_name, &store, &locks, &local_err);
  
  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  if (gzochid_log_level_visible (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
    GZOCHID_WITH_FORMATTED_BYTES
      (key, buf, 33, gzochid_trace
       ("Node %d requested %s lock on key %s/%s/%s.", node_id,
	LOCK_ACCESS (for_write), app, store_name, buf));
  
  if (gzochid_lock_check_and_set
      (locks, node_id, key, for_write, &most_recent_lock))
    {
      size_t data_len = 0;
      gzochid_storage_transaction *transaction = app_store->store->iface
	->transaction_begin (app_store->store->storage_context);
      unsigned char *data = app_store->store->iface->transaction_get
	(transaction, store, (char *) g_bytes_get_data (key, NULL),
	 g_bytes_get_size (key), &data_len);

      if (data != NULL)
	{
	  GBytes *data_bytes = g_bytes_new_with_free_func
	    (data, data_len, (GDestroyNotify) free, data);
	  response = gzochid_data_response_new
	    (app, store_name, TRUE, data_bytes);

	  /* Turn ownership of the data over to the response object. */

	  g_bytes_unref (data_bytes);
	}
      else response = gzochid_data_response_new (app, store_name, TRUE, NULL);

      app_store->store->iface->transaction_rollback (transaction);

      if (gzochid_log_level_visible (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
	GZOCHID_WITH_FORMATTED_BYTES
	  (key, buf, 33, gzochid_trace
	   ("Granted %s lock on key %s/%s/%s to node %d.",
	    LOCK_ACCESS (for_write), app, store_name, buf, node_id));
      
      return response;
    }
  else
    {
      if (gzochid_log_level_visible (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG))
	GZOCHID_WITH_FORMATTED_BYTES
	  (key, buf, 33, g_debug ("Denied %s lock on key %s/%s/%s to node %d.",
				  LOCK_ACCESS (for_write), app, store_name, buf,
				  node_id));
      
      return gzochid_data_response_new (app, store_name, FALSE, NULL);
    }
}

gzochid_data_response *
gzochi_metad_dataserver_request_next_key (GzochiMetadDataServer *server,
					  guint node_id, const char *app,
					  const char *store_name, GBytes *key,
					  GError **err)
{
  gzochid_data_response *response = NULL;
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);
  GBytes *to_key = NULL;
  
  GError *local_err = NULL;
  gzochid_storage_store *store = NULL;
  gzochid_lock_table *locks = NULL;

  struct timeval most_recent_lock;

  size_t data_len = 0;
  gzochid_storage_transaction *transaction = NULL;
  char *data = NULL;

  get_lockable_store (app_store, store_name, &store, &locks, &local_err);

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }
  
  transaction = app_store->store->iface->transaction_begin
    (app_store->store->storage_context);
  
  if (key == NULL)
    {
      gzochid_trace ("Node %d requested range lock on %s/%s keyspace.", node_id,
		     app, store_name);
      data = app_store->store->iface->transaction_first_key
	(transaction, store, &data_len);
    }
  else
    {
      if (gzochid_log_level_visible (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
	GZOCHID_WITH_FORMATTED_BYTES
	  (key, buf, 33, gzochid_trace
	   ("Node %d requested range lock from key %s/%s/%s.", node_id, app,
	    store_name, buf));
	   
      data = app_store->store->iface->transaction_next_key
	(transaction, store, (char *) g_bytes_get_data (key, NULL),
	 g_bytes_get_size (key), &data_len);
    }
  
  assert (!transaction->rollback);
  
  if (data != NULL)
    to_key = g_bytes_new_with_free_func
      (data, data_len, (GDestroyNotify) free, data);

  app_store->store->iface->transaction_rollback (transaction);

  if (!gzochid_lock_range_check_and_set
      (locks, node_id, key, to_key, &most_recent_lock))
    {
      if (key == NULL)
	g_debug ("Denied range lock on %s/%s keyspace to node %d.", app,
		 store_name, node_id);
      else if (gzochid_log_level_visible (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG))
	GZOCHID_WITH_FORMATTED_BYTES
	  (key, buf, 33, g_debug
	   ("Denied range lock starting at %s/%s/%s to node %d.", app,
	    store_name, buf, node_id));
      
      response = gzochid_data_response_new (app, store_name, FALSE, NULL);
    }
  else
    {
      if (key == NULL)
	gzochid_trace ("Granted range lock on %s/%s keyspace to node %d.", app,
		       store_name, node_id);
      else if (gzochid_log_level_visible
	       (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
	GZOCHID_WITH_FORMATTED_BYTES
	  (key, buf, 33, gzochid_trace
	   ("Granted range lock starting at %s/%s/%s to node %d.", app,
	    store_name, buf, node_id));
      
      response = gzochid_data_response_new (app, store_name, TRUE, to_key);
    }

  if (to_key != NULL)
    g_bytes_unref (to_key);
  
  return response;
}

void
gzochi_metad_dataserver_begin_changeset (GzochiMetadDataServer *server,
					 guint node_id, const char *app,
					 guint64 changeset_id, GError **err)
{
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);
  GHashTable *node_submissions = NULL;
  
  if (g_hash_table_contains (app_store->changeset_submissions, &node_id))
    node_submissions = g_hash_table_lookup
      (app_store->changeset_submissions, &node_id);
  else
    {
      node_submissions = g_hash_table_new_full
	(g_int64_hash, g_int64_equal, free, (GDestroyNotify) g_array_unref);
      g_hash_table_insert (app_store->changeset_submissions,
			   g_memdup (&node_id, sizeof (guint)),
			   node_submissions);
    }

  if (g_hash_table_contains (node_submissions, &changeset_id))
    g_set_error
      (err, GZOCHI_METAD_DATASERVER_ERROR,
       GZOCHI_METAD_DATASERVER_ERROR_CHANGESET_ID,
       "Changeset submission for changeset %" G_GUINT64_FORMAT " for %d/%s "
       "already in progress.", changeset_id, node_id, app);
  else g_hash_table_insert
	 (node_submissions,
	  g_memdup (&changeset_id, sizeof (guint64)),
	  g_array_new (FALSE, FALSE, sizeof (gzochid_data_change)));
}

void
gzochi_metad_dataserver_process_changeset (GzochiMetadDataServer *server,
					   guint node_id,
					   gzochid_data_changeset *changeset,
					   GError **err)
{
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, changeset->app);
  GHashTable *node_submissions = g_hash_table_lookup
    (app_store->changeset_submissions, &node_id);
  GArray *pending_changes = NULL;  
  
  if (node_submissions == NULL
      || !g_hash_table_contains (node_submissions, &changeset->id))
    {
      g_set_error (err, GZOCHI_METAD_DATASERVER_ERROR,
		   GZOCHI_METAD_DATASERVER_ERROR_CHANGESET_ID,
		   "Unknown changeset %" G_GUINT64_FORMAT " for %d/%s.",
		   changeset->id, node_id, changeset->app);
      return;
    }

  gzochid_trace ("Processing %d changes from %d/%s.", changeset->changes->len,
		 node_id, changeset->app);

  pending_changes = g_hash_table_lookup (node_submissions, &changeset->id);
  g_array_append_vals (pending_changes, changeset->changes->data,
		       changeset->changes->len);
}

/* Frees the memory held by the internal pointers for a change record. The 
   record itself is part of the change array and will be cleaned up when the 
   array is destroyed. */

static void
change_clear (gzochid_data_change *change)
{
  free (change->store);
  g_bytes_unref (change->key);
  
  if (!change->delete)
    g_bytes_unref (change->data);
}

void
gzochi_metad_dataserver_end_changeset (GzochiMetadDataServer *server,
				       guint node_id, const char *app,
				       guint64 changeset_id, GError **err)
{
  gint i = 0;
  gboolean needs_rollback = FALSE;
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);
  GHashTable *node_submissions = g_hash_table_lookup
    (app_store->changeset_submissions, &node_id);
  GArray *pending_changes = NULL;
  gzochid_storage_transaction *transaction = NULL;
  
  if (node_submissions == NULL
      || !g_hash_table_contains (node_submissions, &changeset_id))
    {
      g_set_error (err, GZOCHI_METAD_DATASERVER_ERROR,
		   GZOCHI_METAD_DATASERVER_ERROR_CHANGESET_ID,
		   "Unknown changeset %" G_GUINT64_FORMAT " for %d/%s.",
		   changeset_id, node_id, app);
      return;
    }

  pending_changes = g_hash_table_lookup (node_submissions, &changeset_id);
  transaction = app_store->store->iface->transaction_begin
    (app_store->store->storage_context);
  
  for (; i < pending_changes->len; i++)
    {
      gzochid_data_change change = g_array_index
	(pending_changes, gzochid_data_change, i);
      GError *local_err = NULL;
      gzochid_storage_store *store = NULL;
      gzochid_lock_table *locks = NULL;

      get_lockable_store (app_store, change.store, &store, &locks, &local_err);

      if (local_err != NULL)
	{
	  g_propagate_error (err, local_err);
	  needs_rollback = TRUE;
	  break;
	}

      if (!gzochid_lock_check (locks, node_id, change.key, TRUE))
	{
	  g_set_error
	    (err, GZOCHI_METAD_DATASERVER_ERROR,
	     GZOCHI_METAD_DATASERVER_ERROR_LOCK_CONFLICT,
	     "Attempted to commit change to oid '%s' without write lock.",
	     (char *) g_bytes_get_data (change.key, NULL));

	  needs_rollback = TRUE;
	  break;
	}

	if (change.delete)
	  app_store->store->iface->transaction_delete
	    (transaction, store, (char *) g_bytes_get_data (change.key, NULL),
	     g_bytes_get_size (change.key));

	else app_store->store->iface->transaction_put
	       (transaction, store,
		(char *) g_bytes_get_data (change.key, NULL),
		g_bytes_get_size (change.key),
		(unsigned char *) g_bytes_get_data (change.data, NULL),
		g_bytes_get_size (change.data));

	if (transaction->rollback)
	  {
	    g_set_error
	      (err, GZOCHI_METAD_DATASERVER_ERROR,
	       GZOCHI_METAD_DATASERVER_ERROR_LOCK_CONFLICT,
	       "Transaction failure writing oid '%s'.",
	       (char *) g_bytes_get_data (change.key, NULL));

	    needs_rollback = TRUE;
	    break;
	  }
    }
    
  if (needs_rollback)
    {
      g_debug ("Transaction for changeset %" G_GUINT64_FORMAT " for %d/%s was "
	       "rolled back during processing.", changeset_id, node_id, app);
      app_store->store->iface->transaction_rollback (transaction);
    }
  else
    {
      app_store->store->iface->transaction_prepare (transaction);
  
      if (transaction->rollback)
	{
	  g_set_error
	    (err, GZOCHI_METAD_DATASERVER_ERROR,
	     GZOCHI_METAD_DATASERVER_ERROR_FAILED,
	     "Transaction for changeset %" G_GUINT64_FORMAT " for %d/%s failed "
	     "to prepare.", changeset_id, node_id, app);
	  g_debug ("Changeset %" G_GUINT64_FORMAT " for %d/%s rolled back "
		   "during prepare.", changeset_id, node_id, app);
	  app_store->store->iface->transaction_rollback (transaction);
	}
      else
	{
	  app_store->store->iface->transaction_commit (transaction);
	  gzochid_trace ("Committed changes from %" G_GUINT64_FORMAT
			 " for %d/%s.", changeset_id, node_id, app);
	}
    }

  for (i = 0; i < pending_changes->len; i++)
    change_clear (&g_array_index (pending_changes, gzochid_data_change, i));
  
  g_hash_table_remove (node_submissions, &changeset_id);
}

void
gzochi_metad_dataserver_release_key (GzochiMetadDataServer *server,
				     guint node_id, const char *app,
				     const char *store_name, GBytes *key)
{
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);

  GError *local_err = NULL;
  gzochid_storage_store *store = NULL;
  gzochid_lock_table *locks = NULL;

  get_lockable_store (app_store, store_name, &store, &locks, &local_err);

  if (local_err != NULL)
    {
      g_warning
	("Failed to release key for application '%s': %s", app,
	 local_err->message);
      g_error_free (local_err);
      return;
    }

  if (gzochid_log_level_visible (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
    GZOCHID_WITH_FORMATTED_BYTES
      (key, buf, 33, gzochid_trace ("Node id %d releasing key %s/%s/%s.",
				    node_id, app, store_name, buf));
  
  gzochid_lock_release (locks, node_id, key);
}

void
gzochi_metad_dataserver_release_range (GzochiMetadDataServer *server,
				       guint node_id, const char *app,
				       const char *store_name,
				       GBytes *first_key, GBytes *last_key)
{
  gzochid_lockable_application_store *app_store =
    ensure_open_application_store (server, app);
  
  GError *local_err = NULL;
  gzochid_storage_store *store = NULL;
  gzochid_lock_table *locks = NULL;
  
  get_lockable_store (app_store, store_name, &store, &locks, &local_err);

  if (local_err != NULL)
    {
      g_warning
	("Failed to release key range for application '%s': %s", app,
	 local_err->message);
      g_error_free (local_err);
      return;
    }

  if (first_key == NULL)
    gzochid_trace ("Node %d releasing range lock on %s/%s keyspace.", node_id,
		   app, store_name);
  else if (gzochid_log_level_visible (G_LOG_DOMAIN, GZOCHID_LOG_LEVEL_TRACE))
    GZOCHID_WITH_FORMATTED_BYTES
      (first_key, buf, 33, gzochid_trace 
       ("Node id %d releasing range lock from %s/%s/%s.", node_id, app,
	store_name, buf));
  
  gzochid_lock_release_range (locks, node_id, first_key, last_key);
}

void
gzochi_metad_dataserver_release_all (GzochiMetadDataServer *server,
				     guint node_id)
{
  GHashTableIter iter;
  gpointer value = NULL;

  g_hash_table_iter_init (&iter, server->application_stores);

  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      gzochid_lockable_application_store *store = value;

      gzochid_lock_release_all (store->oids_locks, node_id);
      gzochid_lock_release_all (store->names_locks, node_id);
    }
}

/* A `GHFunc' implementation for clearing out each pending changeset associated
   with a particular node's participation in a particular application. */

static void
abort_submission_ghfunc (gpointer key, gpointer value, gpointer user_data)
{
  gint i = 0;
  GArray *pending_changes = value;

  for (; i < pending_changes->len; i++)
    change_clear (&g_array_index (pending_changes, gzochid_data_change, i));
}

void
gzochi_metad_dataserver_abort_all (GzochiMetadDataServer *server, guint node_id)
{
  GHashTableIter iter;
  gpointer value = NULL;

  g_hash_table_iter_init (&iter, server->application_stores);

  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      gzochid_lockable_application_store *store = value;
      GHashTable *node_submissions = g_hash_table_lookup
	(store->changeset_submissions, &node_id);

      if (node_submissions != NULL)
	{      
	  g_hash_table_foreach
	    (node_submissions, abort_submission_ghfunc, NULL);
	  g_hash_table_remove (store->changeset_submissions, &node_id);
	}
    }
}

GQuark
gzochi_metad_dataserver_error_quark ()
{
  return g_quark_from_static_string ("gzochi-metad-dataserver-error-quark");
}
