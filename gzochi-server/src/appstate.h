/* appstate.h: Generic interface for appstate implementations
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHI_METAD_APPSTATE_H
#define GZOCHI_METAD_APPSTATE_H

#include <glib.h>

/* The following prototypes describe a mechanism for managing the state of an
   application with respect to its initialization and availability across a
   cluster of gzochi application server nodes. In the future, it should be
   possible for state information to span multiple gzochi-meta server nodes. */

/* The enumeration of application states. */

enum _gzochi_metad_application_state
  {
    /* The application has not been started by any nodes, or task resubmission
       has been canceled and a new resubmission leader has not been elected. */
    
    GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER = 1,

    /* One of the application server nodes is resubmitting tasks. */
    
    GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING,

    /* All tasks have been resubmitted and all connected application server
       nodes have been instructed to start normal task processing; 
       newly-connected nodes will be instrucfted to start normal task processing
       immediately. */
    
    GZOCHI_METAD_APPLICATION_STATE_STARTED
  };

typedef enum _gzochi_metad_application_state gzochi_metad_application_state;

/* The base struct type for appstate implementations. */

struct _gzochi_metad_appstate
{
  struct _gzochi_metad_appstate_iface *iface; /* The appstate vtable. */
};

typedef struct _gzochi_metad_appstate gzochi_metad_appstate;

/* The appstate functional interface, as a kind of poor man's vtable. */

struct _gzochi_metad_appstate_iface
{
  /* Returns the state of the specified application. */
  
  gzochi_metad_application_state (*lookup_application_state)
  (gzochi_metad_appstate *, const char *);

  /* Transitions the specified application to the 
     `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state, with the specified 
     node id elected to lead the resubmission of durable tasks. Signals an 
     error if the application is not in the 
     `GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER' state. */
  
  void (*begin_resubmission) (gzochi_metad_appstate *, const char *, int,
			      GError **);

  /* Transitions the specified application back to the
     `GZOCHI_METAD_APPLICATION_STATE_WAITING_FOR_LEADER' state. Signals an error
     if the application is not in the 
     `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state. */

  void (*cancel_resubmission) (gzochi_metad_appstate *, const char *,
			       GError **);

  /* Returns the id of the node leading the task resubmission process for the
     specified application. Signals an error if the application is not in the
     `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING_STATE'. */
  
  int (*lookup_resubmission_leader) (gzochi_metad_appstate *, const char *,
				     GError **);

  /* Transitions the specified application to the 
     `GZOCHI_METAD_APPLICATION_STATE_STARTED'. Signals an error if the 
     application is not in the `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING'. */
  
  void (*start_application) (gzochi_metad_appstate *, const char *, GError **);

  /* Adds the specified node id to the set of the nodes participating in serving
     the specified application. Signals an error if the node has already been
     added to the participating node set. */
  
  void (*add_participating_node) (gzochi_metad_appstate *, const char *, int,
				  GError **);

  /* Removes the specified node id from the set of the nodes participating in 
     serving the specified application. Signals an error if the node is not 
     present in the participating node set. */

  void (*remove_participating_node) (gzochi_metad_appstate *, const char *, int,
				     GError **);

  /*
    Returns the set of nodes participating in serving the specified application,
    in the form of a `GArray' of ints.
    
    The returned `GArray' should be released via `g_array_unref' when no longer
    needed.
  */

  GArray *(*lookup_participating_nodes) (gzochi_metad_appstate *, const char *);
};

typedef struct _gzochi_metad_appstate_iface gzochi_metad_appstate_iface;

/* Convenience macro for extracting the functional interface from a given
   appstate instance. */

#define GZOCHI_METAD_APPSTATE_IFACE(appstate) \
  ((gzochi_metad_appstate *) (appstate))->iface;

enum
  { 
    /* Indicates a failure to transition the specified application to the 
       `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state because it is already
       in that state. */

    GZOCHI_METAD_APPSTATE_ERROR_ALREADY_RESUBMITTING,

    /* Indicates a failure to transition the specified application to some other
       state because it is not currently in the
       `GZOCHI_METAD_APPLICATION_STATE_RESUBMITTING' state. */

    GZOCHI_METAD_APPSTATE_ERROR_NOT_RESUBMITTING,

    /* Indicates a failure to transition the specified application to the 
       `GZOCHI_METAD_APPLICATION_STATE_STARTED' state because it is already in
       that state. */
    
    GZOCHI_METAD_APPSTATE_ERROR_ALREADY_STARTED,

    /* Indicates a failure to add a node to the participating node set because
       it was already in the participating node set. */
    
    GZOCHI_METAD_APPSTATE_ERROR_ALREADY_PARTICIPATING,

    /* Indicates a failure to remove a node from the participating node set
       because it was not already in the participating node set. */
    
    GZOCHI_METAD_APPSTATE_ERROR_NOT_PARTICIPATING,

    /* Generic appstate failure. */
    
    GZOCHI_METAD_APPSTATE_ERROR_FAILED
  };

#define GZOCHI_METAD_APPSTATE_ERROR gzochi_metad_appstate_error_quark ()

GQuark gzochi_metad_appstate_error_quark ();

#endif /* GZOCHI_METAD_APPSTATE_H */
