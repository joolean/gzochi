/* taskclient.c: Taskserver client for gzochid
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <gzochi-common.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "auth_int.h"
#include "data.h"
#include "durable-task.h"
#include "event.h"
#include "event-app.h"
#include "game.h"
#include "gzochid-auth.h"
#include "log.h"
#include "meta-protocol.h"
#include "schedule.h"
#include "socket.h"
#include "taskclient.h"
#include "tx.h"
#include "txlog.h"
#include "util.h"

#ifdef G_LOG_DOMAIN
#undef G_LOG_DOMAIN
#endif /* G_LOG_DOMAIN */
#define G_LOG_DOMAIN "gzochid.taskclient"

/* Encapsulates the state of a locally-running application with respect to the
   tasks assigned to it. */

struct _application_task_state
{
  /* Synchronizes access to the task tables. This is a recursive mutext because
     certain operations, such as task assignment, sometimes need to safely 
     "re-enter" taskclient code. */

  GRecMutex mutex;

  /* `TRUE' if task resubmission is currently in progress for the application 
     and the local server has been elected as the resubmission leader; `FALSE'
     otherwise. */
  
  gboolean resubmission_leader;
  
  /* Tasks that have been assigned by the meta server but not yet submitted to
     the local task queue, in the form of a list of `guint64 *'. */
  
  GList *assigned_tasks;

  /* Tasks that have been unassigned by the meta server but not yet submitted to
     the local task queue, in the form of a list of `guint64 *'. */
  
  GList *unassigned_tasks;
  
  /* A map of `guint64 *' to `guint64 *', relating an application-specific 
     durable task handle oid to the id of the corresponding `GzochidTask' in the
     local server's task queue. */

  GHashTable *qualified_task_to_local_task;
};

typedef struct _application_task_state application_task_state;

/* Construct and return a new application task state object. This object should
   be freed via `application_task_state_free' when no longer needed. */

static application_task_state *
application_task_state_new ()
{
  application_task_state *task_state = malloc (sizeof (application_task_state));

  g_rec_mutex_init (&task_state->mutex);

  task_state->resubmission_leader = FALSE;
  task_state->assigned_tasks = NULL;
  task_state->unassigned_tasks = NULL;
  task_state->qualified_task_to_local_task =
    g_hash_table_new_full (g_int64_hash, g_int64_equal, free, free);
  
  return task_state;
}

/* Frees the memory associated with the specified `application_task_state'
   object. */

static void
application_task_state_free (application_task_state *task_state)
{
  g_rec_mutex_clear (&task_state->mutex);

  g_list_free_full (task_state->assigned_tasks, free);
  g_list_free_full (task_state->unassigned_tasks, free);
  g_hash_table_destroy (task_state->qualified_task_to_local_task);

  free (task_state);
}

/* Boilerplate setup for the task client object. */

/* The task client object. */

struct _GzochidTaskClient
{
  GObject parent_instance; /* The parent instance, for casting. */

  GzochidGameServer *game_server; /* The game server. */
  GzochidTaskQueue *task_queue; /* The shared local task queue. */
  GMutex mutex; /* Synchronizes access to applications table. */

  /* Map of application name to `application_task_state *'. */
  
  GHashTable *applications;
  
  /* The reconnectable socket wrapping the connection to the metaserver, to be
     used for writes. This field is "inherited" from the metaclient. */
  
  gzochid_reconnectable_socket *socket;

};

/* Boilerplate setup for the task client object. */

G_DEFINE_TYPE (GzochidTaskClient, gzochid_task_client, G_TYPE_OBJECT);

enum gzochid_task_client_properties
  {
    PROP_GAME_SERVER = 1,
    PROP_TASK_QUEUE,
    PROP_SOCKET,
    N_PROPERTIES
  };

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
gzochid_task_client_dispose (GObject *object)
{
  GzochidTaskClient *taskclient = GZOCHID_TASK_CLIENT (object);

  g_object_unref (taskclient->game_server);
  g_object_unref (taskclient->task_queue);

  G_OBJECT_CLASS (gzochid_task_client_parent_class)->dispose (object);
}

static void
gzochid_task_client_finalize (GObject *object)
{
  GzochidTaskClient *taskclient = GZOCHID_TASK_CLIENT (object);

  g_hash_table_destroy (taskclient->applications);

  G_OBJECT_CLASS (gzochid_task_client_parent_class)->finalize (object);
}

static void
gzochid_task_client_set_property (GObject *object, guint property_id,
				  const GValue *value, GParamSpec *pspec)
{
  GzochidTaskClient *self = GZOCHID_TASK_CLIENT (object);

  switch (property_id)
    {
    case PROP_GAME_SERVER:
      self->game_server = g_object_ref (g_value_get_object (value));
      break;

    case PROP_TASK_QUEUE:
      self->task_queue = g_object_ref (g_value_get_object (value));
      break;

    case PROP_SOCKET:
      self->socket = g_value_get_pointer (value);
      break;
      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gzochid_task_client_class_init (GzochidTaskClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gzochid_task_client_dispose;
  object_class->finalize = gzochid_task_client_finalize;
  object_class->set_property = gzochid_task_client_set_property;

  obj_properties[PROP_GAME_SERVER] = g_param_spec_object
    ("game-server", "game-server", "The game protocol server",
     GZOCHID_TYPE_GAME_SERVER, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);
  
  obj_properties[PROP_TASK_QUEUE] = g_param_spec_object
    ("task-queue", "task-queue", "The global task queue",
     GZOCHID_TYPE_TASK_QUEUE, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  obj_properties[PROP_SOCKET] = g_param_spec_pointer
    ("reconnectable-socket", "socket", "The meta client's reconnectable socket",
     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  g_object_class_install_properties
    (object_class, N_PROPERTIES, obj_properties);
}

static void
gzochid_task_client_init (GzochidTaskClient *self)
{
  self->applications = g_hash_table_new_full
    (g_str_hash, g_str_equal, free,
     (GDestroyNotify) application_task_state_free);
}

/* End boilerplate. */

/*
  Convenience function for grabbing a reference to the taskclient with only
  the application as context, as might be necessary from within the worker of a
  `gzochid_application_task'. 

  The returned object should be released via `g_object_unref' when it is no
  longer needed.
*/

static GzochidTaskClient *
get_taskclient (GzochidApplicationContext *app_context)
{
  GzochidMetaClient *metaclient =
    gzochid_application_context_get_metaclient (app_context);
  GzochidTaskClient *taskclient = NULL;

  /* This should never be `NULL'. */
  
  assert (metaclient != NULL);
  g_object_get (metaclient, "task-client", &taskclient, NULL);

  /* Release the metaclient ref, which was automatically incremented above. */

  g_object_unref (metaclient);

  return taskclient;
}

/* Synchronously gets or creates an `application_task_state' for the specified
   application name, depending on whether or not it already exists in the
   taskclient's application table. */

static application_task_state *
ensure_application_task_state (GzochidTaskClient *taskclient, const char *app)
{
  application_task_state *task_state = NULL;
  
  g_mutex_lock (&taskclient->mutex);
  task_state = g_hash_table_lookup (taskclient->applications, app);

  if (task_state == NULL)
    {
      task_state = application_task_state_new ();
      g_hash_table_insert (taskclient->applications, strdup (app), task_state);
    }

  g_mutex_unlock (&taskclient->mutex);
  return task_state;
}

void
gzochid_taskclient_start_application (GzochidTaskClient *taskclient,
				      const char *app)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length prefix, plus 
     opcode. */
  
  size_t total_len = app_len + 4;
  unsigned char *buf = malloc (sizeof (unsigned char) * total_len);

  g_debug ("Asking meta server for permission to start application %s.", app);
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_START_APP;
  memcpy (buf + 3, app, app_len + 1);
  
  gzochid_reconnectable_socket_write (taskclient->socket, buf, total_len);

  free (buf);
}

/* The main transaction worker for the task resubmission transaction. */

static void
resubmit_tasks_transactional_worker (GzochidApplicationContext *app_context,
				     gzochid_auth_identity *identity,
				     gpointer data)
{
  /* What'll happen as part of this process is that the persisted durable tasks
     will be dredged up out of storage and hydrated back into tasks which'll be
     queued for submission via `gzochid_taskclient_submit_task' on commit. */
  
  gzochid_restart_tasks (app_context);
}

/* A "catch" worker for the task resubmission transaction. */

static void
resubmit_tasks_catch_worker (GzochidApplicationContext *app_context,
			     gzochid_auth_identity *identity, gpointer data)
{
  g_warning ("Failed to restart tasks for application %s.",
	     gzochid_application_context_get_name (app_context));
}

/* Construct and return a `GzochidTask' that wraps a `gzochid_application_task'
   that executes the task resubmission process in a retryable transaction. */

static GzochidTask *
create_resubmit_tasks_bootstrap_task (GzochidApplicationContext *app_context)
{
  gzochid_application_task *transactional_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     resubmit_tasks_transactional_worker, NULL, NULL);
  gzochid_application_task *catch_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (), resubmit_tasks_catch_worker,
     NULL, NULL);

  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (transactional_task, catch_task, NULL);

  gzochid_application_task *application_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     gzochid_application_resubmitting_transactional_task_worker,
     (GDestroyNotify) gzochid_transactional_application_task_execution_free,
     execution);

  gzochid_application_task_unref (transactional_task);
  gzochid_application_task_unref (catch_task);
  
  return gzochid_task_immediate_new
    (gzochid_application_task_thread_worker,
     (GDestroyNotify) gzochid_application_task_unref, application_task);
}

void
gzochid_taskclient_resubmit_tasks (GzochidTaskClient *taskclient,
				   const char *app, GError **err)
{
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);

  if (app_context != NULL)
    {
      application_task_state *task_state =
	ensure_application_task_state (taskclient, app);

      g_debug ("Bootstrapping task resubmission for %s.", app);
      
      g_rec_mutex_lock (&task_state->mutex);
      task_state->resubmission_leader = TRUE;

      /* Restarting tasks needs to happen in a transaction, and should run
	 asynchronously with respect to this function, which is typically called
	 in response to a notification from the task server. */ 
      
      gzochid_schedule_submit_task
	(taskclient->task_queue,
	 create_resubmit_tasks_bootstrap_task (app_context));
      g_rec_mutex_unlock (&task_state->mutex);
    }
  else g_set_error (err, GZOCHID_TASKCLIENT_ERROR_FAILED,
		    GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		    "No such application %s.", app);
}

/* Handles formatting and sending a 
   `GZOCHID_TASK_PROTOCOL_COMPLETE_RESUBMISSION' message on the taskclient's 
   socket. */

static void
send_complete_resubmission (gzochid_reconnectable_socket *sock, const char *app)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte. */
  
  size_t total_len = app_len + 4;
  unsigned char *buf = malloc (sizeof (unsigned char) * total_len);
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_COMPLETE_RESUBMISSION;
  memcpy (buf + 3, app, app_len + 1);
  
  gzochid_reconnectable_socket_write (sock, buf, total_len);
  
  free (buf);
}

void
gzochid_taskclient_complete_resubmission (GzochidTaskClient *taskclient,
					  const char *app, GError **err)
{
  application_task_state *task_state =
    ensure_application_task_state (taskclient, app);
  
  g_rec_mutex_lock (&task_state->mutex);
  
  if (task_state->resubmission_leader)
    {
      task_state->resubmission_leader = FALSE;
      send_complete_resubmission (taskclient->socket, app);

      g_debug ("Completed task resubmission for %s.", app);
    }
  else g_set_error
	 (err, GZOCHID_TASKCLIENT_ERROR,
	  GZOCHID_TASKCLIENT_ERROR_NOT_RESUBMISSION_LEADER,
	  "This node is not the resubmission leader for application %s.", app);
  
  g_rec_mutex_unlock (&task_state->mutex);
}

void
gzochid_taskclient_application_started (GzochidTaskClient *taskclient,
					const char *app, GError **err)
{
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);

  if (app_context != NULL)
    {
      g_debug ("Starting normal task processing for %s.", app);
      gzochid_start_task_processing (app_context);
    }
  else g_set_error (err, GZOCHID_TASKCLIENT_ERROR,
		    GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		    "No such application %s.", app);
}

/* Handles formatting and sending a
   `GZOCHID_TASK_PROTOCOL_SUBMIT_TASK' message on the taskclient's socket. */

void
send_submit_task (gzochid_reconnectable_socket *sock, const char *app,
		  guint64 task_id)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length
     prefix, plus opcode, plus 8-byte task oid. */
  
  size_t total_len = app_len + 12;
  unsigned char *buf = malloc (sizeof (unsigned char) * (app_len + 12));
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_SUBMIT_TASK;
  memcpy (buf + 3, app, app_len + 1);
  gzochi_common_io_write_long (task_id, buf, app_len + 4);
  
  gzochid_reconnectable_socket_write (sock, buf, total_len);
  
  free (buf);
}

/*
  Checks the task unassignment list for the specified application task state.
  If the specified task is found in the list, it is removed, and this function
  returns `TRUE'; otherwise this function returns `FALSE'. 

  This function must be called with the task state mutex locked.
*/

static gboolean
acknowledge_possible_unassignment (const char *app,
				   application_task_state *task_state,
				   guint64 task_id)
{
  GList *unassigned_task_ptr = g_list_find_custom
    (task_state->unassigned_tasks, &task_id, gzochid_util_guint64_compare);
  
  if (unassigned_task_ptr != NULL)
    {
      free (unassigned_task_ptr->data);      
      task_state->unassigned_tasks = g_list_delete_link
	(task_state->unassigned_tasks, unassigned_task_ptr);

      gzochid_trace ("Unassigned task %s/%" G_GUINT64_FORMAT
		     ", which was still bootstrapping.", app, task_id);
      
      return TRUE;
    }
  else return FALSE;
}

void
gzochid_taskclient_submit_task (GzochidTaskClient *taskclient, const char *app,
				guint64 task_id, guint64 local_task_id,
				GError **err)
{
  GList *assigned_task_ptr = NULL;
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);
  application_task_state *task_state = NULL;
  gboolean notify_server = TRUE, assigned = TRUE;

  if (app_context == NULL)
    {
      g_set_error (err, GZOCHID_TASKCLIENT_ERROR,
		   GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		   "No such application %s.", app);
      return;
    }

  task_state = ensure_application_task_state (taskclient, app);
  
  g_rec_mutex_lock (&task_state->mutex);

  /*
    The durable task handling code will call this function during the commit
    phase of the transaction initiated by the transactional task assignment
    bootstrap process below. When that happens, we don't want to notify the
    task server - it already knows the task is assigned here. 

    So, first we need to detect that condition...
  */
  
  assigned_task_ptr = g_list_find_custom
    (task_state->assigned_tasks, &task_id, gzochid_util_guint64_compare);
  
  if (assigned_task_ptr != NULL)
    {
      gzochid_event_source *event_source = NULL;
  
      free (assigned_task_ptr->data);

      /* ...and use it to kind of complete the task assignment handshake by
	 removing the task from the client's list of assigned but unmapped 
	 tasks. */
      
      task_state->assigned_tasks = g_list_delete_link
	(task_state->assigned_tasks, assigned_task_ptr);
      notify_server = FALSE;

      gzochid_trace
	("Completed assignment bootstrap for %s/%" G_GUINT64_FORMAT ".", app,
	 task_id);
      
      g_object_get (app_context, "event-source", &event_source, NULL);

      gzochid_event_dispatch
	(event_source, g_object_new (GZOCHID_TYPE_DURABLE_TASK_EVENT,
				     "application", app,
				     "type", DURABLE_TASK_ASSIGNED,
				     NULL));

      g_source_unref ((GSource *) event_source);
    }

  /* It's also possible that we could have received an unassignment in the 
     window between when the assignment transaction began to commit and when 
     this function was called. */	 
  
  else if (acknowledge_possible_unassignment (app, task_state, task_id))
    assigned = FALSE;
  
  /* Then, whether it was previously in the pending assignments list or not, we
     want to map the task handle oid to the id of the `GzochidTask' in the local
     task queue so that we can remove it later if necessary. */

  if (assigned)
    {
      if (!g_hash_table_contains (task_state->qualified_task_to_local_task,
				  &task_id))
	{
	  if (notify_server)
	    {
	      send_submit_task (taskclient->socket, app, task_id);
	      gzochid_trace ("Submitted task %s/%" G_GUINT64_FORMAT ".", app,
			     task_id);
	    }
	}
      else g_debug ("Not notifying server of already-mapped task %s/%"
		    G_GUINT64_FORMAT "; updated its local task id.", app,
		    task_id);
      
      g_hash_table_insert (task_state->qualified_task_to_local_task,
			   g_memdup (&task_id, sizeof (guint64)),
			   g_memdup (&local_task_id, sizeof (guint64)));
    }
  else
    {
      g_debug
	("Task %s/%" G_GUINT64_FORMAT " was asynchronously unassigned; "
	 "unscheduling...", app, task_id);

      gzochid_schedule_remove_task (taskclient->task_queue, local_task_id);
    }
    
  g_rec_mutex_unlock (&task_state->mutex);
}

void
gzochid_taskclient_cancel_task (GzochidTaskClient *taskclient, const char *app,
				guint64 task_id)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length
     prefix, plus opcode, plus 8-byte task oid. */
  
  size_t total_len = app_len + 12;
  unsigned char *buf = malloc (sizeof (unsigned char) * (app_len + 12));

  g_debug ("Cancelling task %s/%" G_GUINT64_FORMAT
	   ", which is not locally assigned.", app, task_id);
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_CANCEL_TASK;
  memcpy (buf + 3, app, app_len + 1);
  gzochi_common_io_write_long (task_id, buf, app_len + 4);
  
  gzochid_reconnectable_socket_write (taskclient->socket, buf, total_len);
  
  free (buf);
}

/* Handles formatting and sending a 
   `GZOCHID_TASK_PROTOCOL_TASK_COMPLETED' message on the taskclient's socket. */

static void
send_task_completed (gzochid_reconnectable_socket *sock, const char *app,
		     guint64 task_id)
{
  size_t app_len = strlen (app);
  
  /* Length of app name plus terminating `NULL' byte, plus length
     prefix, plus opcode, plus 8-byte task oid. */
  
  size_t total_len = app_len + 12;
  unsigned char *buf = malloc (sizeof (unsigned char) * (app_len + 12));
  
  gzochi_common_io_write_short (total_len - 3, buf, 0);
  buf[2] = GZOCHID_TASK_PROTOCOL_TASK_COMPLETED;
  memcpy (buf + 3, app, app_len + 1);
  gzochi_common_io_write_long (task_id, buf, app_len + 4);
  
  gzochid_reconnectable_socket_write (sock, buf, total_len);
  
  free (buf);
}

void
gzochid_taskclient_complete_task (GzochidTaskClient *taskclient,
				  const char *app, guint64 task_id,
				  GError **err)
{
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);
  application_task_state *task_state = NULL;

  if (app_context == NULL)
    {
      g_set_error (err, GZOCHID_TASKCLIENT_ERROR,
		   GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		   "No such application %s.", app);
      return;
    }

  task_state = ensure_application_task_state (taskclient, app);
  
  g_rec_mutex_lock (&task_state->mutex);

  /* Was this task previously mapped? If so, unmap it. */
  
  if (g_hash_table_remove (task_state->qualified_task_to_local_task, &task_id))
    {
      send_task_completed (taskclient->socket, app, task_id);
      gzochid_trace ("Completed task %s/%" G_GUINT64_FORMAT ".", app, task_id);
    }

  /* If not, that merits an error, since it shouldn't be possible for a task to
     complete before getting an entry in the mapping table. */
  
  else g_set_error
	 (err, GZOCHID_TASKCLIENT_ERROR,
	  GZOCHID_TASKCLIENT_ERROR_TASK_NOT_MAPPED,
	  "Task %s/%" G_GUINT64_FORMAT " is not mapped.", app, task_id);

  g_rec_mutex_unlock (&task_state->mutex);
}

/* The main transaction worker for the task assignment transaction. */

static void
assigned_task_transactional_worker (GzochidApplicationContext *app_context,
				    gzochid_auth_identity *identity,
				    gpointer data)
{
  GError *err = NULL;
  guint64 *task_id_ptr = data;
  gzochid_data_managed_reference *ref = gzochid_data_create_reference_to_oid
    (app_context, &gzochid_durable_application_task_handle_serialization,
     *task_id_ptr);

  gzochid_data_dereference (ref, &err);
  
  if (err != NULL)
    g_error_free (err);
  else
    {
      const char *app_name = gzochid_application_context_get_name (app_context);
      GzochidTaskClient *taskclient = get_taskclient (app_context);
      application_task_state *task_state = NULL;
      
      task_state = ensure_application_task_state (taskclient, app_name);
      
      g_rec_mutex_lock (&task_state->mutex);

      /* Make sure the task is still assigned - it could have been unassigned
	 between when it was originally assigned and when this task executed. */
      
      if (g_list_find_custom (task_state->assigned_tasks, task_id_ptr,
			      gzochid_util_guint64_compare))
	{
	  gzochid_schedule_durable_task_handle (app_context, ref->obj, &err);

	  if (err != NULL)
	    g_error_free (err);
	}

      /* See if it was unassigned... */
      
      else if (acknowledge_possible_unassignment
	       (app_name, task_state, *task_id_ptr))

	gzochid_tx_debug
	  (app_context, "Assigned task %s/%" G_GUINT64_FORMAT " was "
	   "asynchronously unassigned.", app_name, *task_id_ptr);

      /* ...or if we have no idea what's up with it. */
      
      else gzochid_tx_debug
	     (app_context, "Assigned task %s/%" G_GUINT64_FORMAT " was not "
	      "found.", app_name, *task_id_ptr);
      
      g_rec_mutex_unlock (&task_state->mutex);
      g_object_unref (taskclient);
    }
}

/* A "catch" worker for the task assignment transaction. */

static void
assigned_task_catch_worker (GzochidApplicationContext *app_context,
			    gzochid_auth_identity *identity, gpointer data)
{
  guint64 *task_id_ptr = data;
  const char *app_name = gzochid_application_context_get_name (app_context);
  GzochidTaskClient *taskclient = get_taskclient (app_context);
  application_task_state *task_state = NULL;
  GList *assigned_task_ptr = NULL;
  
  task_state = ensure_application_task_state (taskclient, app_name);
  
  g_rec_mutex_lock (&task_state->mutex);

  g_warning ("Bootstrapping for assigned task %s/%" G_GUINT64_FORMAT " failed.",
	     app_name, *task_id_ptr);
  
  assigned_task_ptr = g_list_find_custom
    (task_state->assigned_tasks, task_id_ptr, gzochid_util_guint64_compare);
  
  if (assigned_task_ptr != NULL)
    {
      free (assigned_task_ptr->data);
      task_state->assigned_tasks = g_list_delete_link
	(task_state->assigned_tasks, assigned_task_ptr);
    }

  /* It might've been unassigned... */
  
  else if (!acknowledge_possible_unassignment
	   (app_name, task_state, *task_id_ptr))

    g_debug ("Failed to clean up assigned task %s/%" G_GUINT64_FORMAT
	     " that could not be scheduled.", app_name, *task_id_ptr);
  
  g_rec_mutex_unlock (&task_state->mutex);
  g_object_unref (taskclient);
}

/* Construct and return a `GzochidTask' that wraps a `gzochid_application_task'
   that ensures that an assigned durable task is correctly scheduled to the 
   local task queue with respect to subsequent, asynchronous unassignments and
   re-assignments. */

static GzochidTask *
create_assigned_task_bootstrap_task (GzochidApplicationContext *app_context,
				     guint64 task_id)
{
  gzochid_application_task *transactional_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     assigned_task_transactional_worker, free,
     g_memdup (&task_id, sizeof (guint64)));
  gzochid_application_task *catch_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (), assigned_task_catch_worker,
     free, g_memdup (&task_id, sizeof (guint64)));

  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (transactional_task, catch_task, NULL);

  gzochid_application_task *application_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     gzochid_application_resubmitting_transactional_task_worker,
     (GDestroyNotify) gzochid_transactional_application_task_execution_free,
     execution);

  gzochid_application_task_unref (transactional_task);
  gzochid_application_task_unref (catch_task);
  
  return gzochid_task_immediate_new
    (gzochid_application_task_thread_worker,
     (GDestroyNotify) gzochid_application_task_unref, application_task);
}

void
gzochid_taskclient_task_assigned (GzochidTaskClient *taskclient,
				  const char *app, guint64 task_id,
				  GError **err)
{
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);
  application_task_state *task_state = NULL;

  if (app_context == NULL)
    {
      g_set_error (err, GZOCHID_TASKCLIENT_ERROR,
		   GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		   "No such application %s.", app);
      return;
    }

  task_state = ensure_application_task_state (taskclient, app);

  g_rec_mutex_lock (&task_state->mutex);

  /* It's possible that we're re-assigning a task that was assigned and then
     unassigned before it could be bootstrapped into the task queue. If that
     happened, we need to clear it from the unassigned task list. */
  
  if (acknowledge_possible_unassignment (app, task_state, task_id))
    g_debug ("Re-assigning previously unassigned task %s/%" G_GUINT64_FORMAT,
	     app, task_id);

  /* Ignore re-assignments of tasks that are already mapped... */
  
  if (!g_hash_table_contains
      (task_state->qualified_task_to_local_task, &task_id))
    {
      /* ...or that are already assigned pending mapping. */
      
      if (g_list_find_custom (task_state->assigned_tasks, &task_id,
			      gzochid_util_guint64_compare) == NULL)
	{
	  task_state->assigned_tasks = g_list_prepend
	    (task_state->assigned_tasks, g_memdup (&task_id, sizeof (guint64)));

	  gzochid_trace ("Bootstrapping task assignment for %s/%"
			 G_GUINT64_FORMAT ".", app, task_id);
	  
	  /* Submit the assignment bootstrap task, which will ensure that the
	     task will get scheduled through the durable task scheduling module,
	     leading in turn to it flowing back into the taskclient via
	     `gzochid_taskclient_submit_task'. */
	  
	  gzochid_schedule_submit_task
	    (taskclient->task_queue, create_assigned_task_bootstrap_task
	     (app_context, task_id));
	}
      else g_debug ("Ignoring already-assigned task %s/%" G_GUINT64_FORMAT ".",
		    app, task_id);
    }
  else g_debug ("Ignoring already-mapped task %s/%" G_GUINT64_FORMAT ".", app,
		task_id);
  
  g_rec_mutex_unlock (&task_state->mutex);
}

void
gzochid_taskclient_task_unassigned (GzochidTaskClient *taskclient,
				    const char *app, guint64 task_id,
				    GError **err)
{
  GzochidApplicationContext *app_context =
    gzochid_game_server_lookup_application (taskclient->game_server, app);
  application_task_state *task_state = NULL;
  guint64 *local_task_id = NULL;

  if (app_context == NULL)
    {
      g_set_error (err, GZOCHID_TASKCLIENT_ERROR,
		   GZOCHID_TASKCLIENT_ERROR_UNKNOWN_APPLICATION,
		   "No such application %s.", app);
      return;
    }

  task_state = ensure_application_task_state (taskclient, app);
  
  g_rec_mutex_lock (&task_state->mutex);

  local_task_id = g_hash_table_lookup
    (task_state->qualified_task_to_local_task, &task_id);

  /* Was the task already submitted / mapped? */
  
  if (local_task_id != NULL)
    {
      gzochid_event_source *event_source = NULL;
      
      /* ...and remove it from the mapping table. */
      
      gzochid_schedule_remove_task (taskclient->task_queue, *local_task_id);
      g_hash_table_remove (task_state->qualified_task_to_local_task, &task_id);

      gzochid_trace ("Synchronously unassigned scheduled task %s/%"
		     G_GUINT64_FORMAT ".", app, task_id);
      
      g_object_get (app_context, "event-source", &event_source, NULL);

      gzochid_event_dispatch
	(event_source, g_object_new (GZOCHID_TYPE_DURABLE_TASK_EVENT,
				     "application", app,
				     "type", DURABLE_TASK_UNASSIGNED,
				     NULL));

      g_source_unref ((GSource *) event_source);
    }
  else
    {
      /* Was the task previously assigned and not yet mapped? If so, remove it 
	 from the task assignment list... */
      
      GList *assigned_task = g_list_find_custom
	(task_state->assigned_tasks, &task_id, gzochid_util_guint64_compare);

      if (assigned_task != NULL)
	{
	  free (assigned_task->data);
	  task_state->assigned_tasks = g_list_delete_link
	    (task_state->assigned_tasks, assigned_task);

	  /* ...and add it to the unassignment list. */
	  
	  task_state->unassigned_tasks = g_list_append
	    (task_state->unassigned_tasks,
	     g_memdup (&task_id, sizeof (guint64)));

	  gzochid_trace ("Asynchronously unassigning task %s/%" G_GUINT64_FORMAT
			 ", which was not yet scheduled.", app, task_id);
	}
      else g_set_error
	     (err, GZOCHID_TASKCLIENT_ERROR,
	      GZOCHID_TASKCLIENT_ERROR_TASK_NOT_MAPPED,
	      "Task %s/%" G_GUINT64_FORMAT " not previously assigned.", app,
	      task_id);
    }

  g_rec_mutex_unlock (&task_state->mutex);   
}

GQuark
gzochid_taskclient_error_quark ()
{
  return g_quark_from_static_string ("gzochid-task-client-error-quark");
}
