/* task.h: Prototypes and declarations for task.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_TASK_H
#define GZOCHID_TASK_H

#include <glib.h>
#include <glib-object.h>
#include <sys/time.h>

#include "threads.h"

/* GObject type definition for the task object. */

#define GZOCHID_TYPE_TASK gzochid_task_get_type ()

/* The task type. Encapsulates a closure of a `gzochid_thread_worker' and a
   piece of data to execute it on at a specified time. This is the most
   fundamental unit of work that can be executed on a gzochi application server
   node. */

G_DECLARE_FINAL_TYPE (GzochidTask, gzochid_task, GZOCHID, TASK,
		      GInitiallyUnowned);

/* The GzochidTask struct definition. */

struct _GzochidTask
{
  /* Tasks are created with a floating reference since they are almost always 
     immediately submitted to a task queue which takes ownership of them. */

  GInitiallyUnowned parent_instance; 
  
  gzochid_thread_worker worker; /* The task worker function. */

  /* An optional destroy function to be called on the task's data when the task
     is destroy. */
  
  GDestroyNotify destroy_notify;

  gpointer data; /* The task data. */
  struct timeval target_execution_time; /* The task target execution time. */
};

/* Construct a new task with specified worker, data, and optional destroy 
   function to be executed at some time after the specified timeout has 
   elapsed. */

GzochidTask *gzochid_task_new (gzochid_thread_worker, GDestroyNotify, gpointer,
			       struct timeval);

/* Construct a new task with specified worker, data, and optional destroy
   function to be executed as soon as possible. The `GDestroyNotify' argument 
   gives a finalization function to be called on the data argument when 
   `gzochid_task_free' is called on the task. */

GzochidTask *gzochid_task_immediate_new (gzochid_thread_worker, GDestroyNotify,
					 gpointer);

#endif /* GZOCHID_TASK_H */
