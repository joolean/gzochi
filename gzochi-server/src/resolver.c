/* resolver.c: Minimal dependency injection framework for gzochid components
 * Copyright (C) 2019 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>

#include "resolver.h"

GQuark
gzochid_resolution_error_quark (void)
{
  return g_quark_from_static_string ("gzochid-resolution-error-quark");
}

struct _GzochidResolutionContext
{
  GObject parent_instance;

  /* The instance cache, as a map of `GType *' to `GWeakRef *'.  */

  GHashTable *instances; 

  /*
    List of strong references to instances provided to the resolution context
    via `gzochid_resolver_provide'.
  */

  GList *provided_instances;

  /* 
     Temporary storage of instances created or referenced during a resolution
     cycle, for unref at the end.
  */

  GList *referenced_instances;
};

G_DEFINE_TYPE (GzochidResolutionContext, gzochid_resolution_context,
	       G_TYPE_OBJECT);

static void
gzochid_resolution_context_dispose (GObject *object)
{
  GzochidResolutionContext *self = GZOCHID_RESOLUTION_CONTEXT (object);

  g_list_foreach (self->provided_instances, (GFunc) g_object_unref, NULL);
  g_list_foreach (self->referenced_instances, (GFunc) g_object_unref, NULL);
  
  G_OBJECT_CLASS (gzochid_resolution_context_parent_class)->dispose (object);
}

static void
gzochid_resolution_context_finalize (GObject *object)
{
  GzochidResolutionContext *self = GZOCHID_RESOLUTION_CONTEXT (object);
  
  g_hash_table_destroy (self->instances);
  g_list_free (self->provided_instances);
  g_list_free (self->referenced_instances);

  G_OBJECT_CLASS (gzochid_resolution_context_parent_class)->finalize (object);
}

static void
gzochid_resolution_context_class_init (GzochidResolutionContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gzochid_resolution_context_dispose;
  object_class->finalize = gzochid_resolution_context_finalize;
}

/* `GDestroyNotify' callback for the `GWeakRef' values in the instance table. */

static void
free_weak_ref (gpointer data)
{
  g_weak_ref_clear (data);
  free (data);
}

static void
gzochid_resolution_context_init (GzochidResolutionContext *self)
{
  self->instances = g_hash_table_new_full
    (g_int_hash, g_int_equal, free, free_weak_ref);
}

/* Represents a maximally-injectable constructor for a GObject-derived type: The
   set of writable, construct-time GObject-derived parameters with everything
   other parameter stripped out. */

struct _constructor
{
  GObjectClass *klass;

  guint n_parameters;
  GParamSpec *parameters;
};

typedef struct _constructor constructor;

/* Returns `TRUE' if the specified parameter is a candidate for dependency
   injection by the resolver framework. */

static gboolean
is_injectable (const GParamSpec *pspec)
{
  return g_type_is_a (pspec->value_type, G_TYPE_OBJECT)
    && pspec->flags & G_PARAM_WRITABLE
    && (pspec->flags & G_PARAM_CONSTRUCT
	|| pspec->flags & G_PARAM_CONSTRUCT_ONLY);
}

static constructor *
injectable_constructor_for_type (GType type, GError **err)
{
  guint i = 0;
  guint n_properties = 0;
  guint n_injectable_properties = 0;
  GObjectClass *klass = g_type_class_ref (type);
  GParamSpec **properties = g_object_class_list_properties
    (klass, &n_properties);

  constructor *ctor = NULL;

  /* Make one pass through the list to identify the eligible parameters and make
     sure that there are no uninjectable parameters... */
  
  for (; i < n_properties; i++)
    {
      GParamSpec *pspec = properties[i];

      if (is_injectable (pspec))
	n_injectable_properties++;
      else if (pspec->flags & G_PARAM_CONSTRUCT_ONLY)
	{
	  g_set_error
	    (err, GZOCHID_RESOLUTION_ERROR,
	     GZOCHID_RESOLUTION_ERROR_INCOMPATIBLE_CONSTRUCTOR,
	     "Type '%s' requires non-injectable constructor argument '%s'.",
	     g_type_name (type), pspec->name);
	     
	  free (properties);
	  g_type_class_unref (klass);
	  
	  return NULL;
	}
    }

  ctor = calloc (1, sizeof (constructor));
  ctor->klass = klass;

  /* ...then make another pass (if necessary) to collate them into a constructor
     invocation that'll get pushed onto the stack. */
  
  if (n_injectable_properties > 0)
    {
      ctor->n_parameters = n_injectable_properties;
      ctor->parameters = malloc (sizeof (GParamSpec) * ctor->n_parameters);

      guint j = 0;

      for (i = 0; i < n_properties; i++)
	{
	  GParamSpec *pspec = properties[i];
	  
	  if (is_injectable (pspec))
	    ctor->parameters[j++] = *properties[i];
	}
    }
  
  free (properties);
  return ctor;
}

static void
free_constructor (constructor *ctor)
{
  g_type_class_unref (ctor->klass);

  if (ctor->n_parameters > 0)
    free (ctor->parameters);
  
  free (ctor);
}

static gboolean
free_node_constructor (GNode *node, gpointer data)
{
  free_constructor (node->data);
  return FALSE;
}

/* A representation of the state required during the construction of the
   dependency tree. */

struct _tree_state
{
  GNode *node; /* The "current" node in the tree. */

  /* The remaining types that need constructors to be added as children of the 
     current node. */

  GList *types; 
};

typedef struct _tree_state tree_state;

static tree_state *
create_tree_state (GNode *node, GList *types)
{
  tree_state *state = malloc (sizeof (tree_state));

  state->node = node;
  state->types = types;

  return state;
}

static void
free_tree_state (gpointer data)
{
  tree_state *state = data;
  
  g_list_free_full (state->types, free);
  free (state);
}

static gboolean
has_ancestor_with_type (GNode *node, GType type)
{
  while (node != NULL)
    {
      constructor *ctor = node->data;
	
      if (G_OBJECT_CLASS_TYPE (ctor->klass) == type)
	return TRUE;

      node = node->parent;
    }

  return FALSE;
}

static GList *
extract_dependencies (constructor *ctor)
{
  guint i = 0;
  GList *dependencies = NULL;

  for (; i < ctor->n_parameters; i++)
    {
      GType *type = malloc (sizeof (GType));
      *type = ctor->parameters[i].value_type;
      dependencies = g_list_prepend (dependencies, type);
    }
  
  return g_list_reverse (dependencies);
}

static tree_state *
build_dependency_tree_inner (GzochidResolutionContext *context, GList *stack,
			     GError **err)
{
  GError *local_err = NULL;
  tree_state *state = NULL;
  GType *type = NULL;
  GWeakRef *weak_ref = NULL;
  GObject *inst = NULL;
  
  assert (stack != NULL);

  state = stack->data;

  if (state->types == NULL)
    return NULL;

  /* Pop the next sibling type off the list of constructor dependencies. */
  
  type = state->types->data;
  state->types = g_list_delete_link (state->types, state->types);  
  weak_ref = g_hash_table_lookup (context->instances, type);
  inst = weak_ref != NULL ? g_weak_ref_get (weak_ref) : NULL;
  
  if (*type == GZOCHID_TYPE_RESOLUTION_CONTEXT || inst != NULL)
    {
      if (inst != NULL)
	g_object_unref (inst);
	
      free (type);
      return NULL;
    }
  
  /* Circular dependencies are detected by walking up the tree to make sure that
     a type never appears as its own ancestor reachable on the same "branch" of
     the dependency tree. */
  
  if (has_ancestor_with_type (state->node, *type))
    {
      g_set_error
	(err, GZOCHID_RESOLUTION_ERROR,
	 GZOCHID_RESOLUTION_ERROR_CIRCULAR_DEPENDENCY,
	 "Circular dependency on type '%s'.", g_type_name (*type));

      free (type);
      
      return NULL;
    }
  else
    {
      constructor *ctor = injectable_constructor_for_type (*type, &local_err);

      free (type);
      
      if (local_err != NULL)
	{
	  g_propagate_error (err, local_err);
	  return NULL;
	}
      else
	{
	  GNode *new_node = g_node_new (ctor);
	  g_node_insert (state->node, -1, new_node);

	  if (ctor->n_parameters > 0)
	    return create_tree_state
	      (new_node, extract_dependencies (ctor));
	  else return create_tree_state (new_node, NULL);
	}
    }
}

/* Build a dependency tree in a roughly tail-recursive way. The process goes 
   like this: For each type to be instantiated, create a representation of the 
   constructor to be used to instantiate it and attach it to a new node in the 
   dependency tree; if it has any eligible parameters, push that node and a list
   of those parameters onto the stack. Peek at the top of the stack: If the
   element has a non-empty list of types to be constructed, remove the first
   type from that list and start from the beginning; otherwise, pop the
   stack. */

static GNode *
build_dependency_tree (GzochidResolutionContext *context, GType root_type,
		       GError **err)
{
  GError *local_err = NULL;
  constructor *ctor = injectable_constructor_for_type (root_type, &local_err);

  GNode *root = NULL;
  tree_state *state = NULL;
  GList *stack = NULL;

  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }

  root = g_node_new (ctor);

  state = create_tree_state (root, extract_dependencies (ctor));
  stack = g_list_append (NULL, state);
  
  while (stack != NULL)
    {
      GError *local_err = NULL;
      tree_state *old_state = stack->data, *new_state = NULL;

      if (old_state->types == NULL)
	{
	  free_tree_state (old_state);
	  stack = g_list_delete_link (stack, stack);
	  continue;
	}
      
      new_state = build_dependency_tree_inner (context, stack, &local_err);

      if (local_err != NULL)
	{
	  g_propagate_error (err, local_err);

	  /* If there was an error building the tree, cleanup the intermediate
	     storage. */
	  
	  g_node_traverse
	    (root, G_POST_ORDER, G_TRAVERSE_ALL, -1, free_node_constructor,
	     NULL);
	  g_node_destroy (root);
	  
	  g_list_free_full (stack, free_tree_state);

	  return NULL;
	}

      if (new_state != NULL)
	stack = g_list_prepend (stack, new_state);
    }

  return root;
}

static gboolean
construct_instance (GNode *node, gpointer data)
{
  constructor *ctor = node->data;
  GzochidResolutionContext *context = data;
  GType type = G_TYPE_FROM_CLASS (ctor->klass);
  GWeakRef *weak_ref = g_hash_table_lookup (context->instances, &type);
  GObject *inst = weak_ref != NULL ? g_weak_ref_get (weak_ref) : NULL;
  
  if (inst == NULL)
    {
      if (ctor->n_parameters > 0)
	{
	  guint i = 0;
	  const char *names[ctor->n_parameters];
	  GValue values[ctor->n_parameters];

	  for (; i < ctor->n_parameters; i++)
	    {
	      GParamSpec pspec = ctor->parameters[i];
	      GType ptype = pspec.value_type;
	      GValue value = G_VALUE_INIT;
	      GObject *obj = NULL;

	      names[i] = pspec.name;
	      values[i] = value;

	      g_value_init (&values[i], ptype);

	      if (ptype == GZOCHID_TYPE_RESOLUTION_CONTEXT)
		obj = G_OBJECT (context);
	      else
		{		  
		  assert (g_hash_table_contains (context->instances, &ptype));
		  obj = g_weak_ref_get
		    (g_hash_table_lookup (context->instances, &ptype));
		  assert (obj != NULL);

		  /* 
		     Calling `g_weak_ref_get' increments the reference count,
		     so we need to make sure we unref it later.
		  */
		  
		  context->referenced_instances = g_list_append
		    (context->referenced_instances, obj);
		}
	      
	      g_value_take_object (&values[i], obj);
	    }
	 
	  inst = g_object_new_with_properties
	    (type, ctor->n_parameters, names, values);	  
	}
      else inst = g_object_new (type, NULL);

      if (weak_ref == NULL)
	{
	  GType *type_key = malloc (sizeof (GType));

	  *type_key = type;

	  weak_ref = malloc (sizeof (GWeakRef));
	  g_weak_ref_init (weak_ref, inst);
	  g_hash_table_insert (context->instances, type_key, weak_ref);
	}
      else g_weak_ref_set (weak_ref, inst);
    }

  if (inst != NULL)
    context->referenced_instances =
      g_list_append (context->referenced_instances, inst);

  free_constructor (ctor);

  return FALSE;
}

gpointer
gzochid_resolver_require_full (GzochidResolutionContext *context,
			       const GType type, GError **err)
{
  GError *local_err = NULL;
  GWeakRef *weak_ref = g_hash_table_lookup (context->instances, &type);
  GObject *inst = weak_ref != NULL ? g_weak_ref_get (weak_ref) : NULL;
  
  assert (context != NULL);

  if (type == GZOCHID_TYPE_RESOLUTION_CONTEXT)
    return context;
  else if (inst == NULL)
    {
      GNode *dependency_tree = build_dependency_tree
	(context, type, &local_err);

      if (local_err != NULL)
	{
	  g_propagate_error (err, local_err);
	  return NULL;
	}
      
      g_node_traverse
	(dependency_tree, G_POST_ORDER, G_TRAVERSE_ALL, -1, construct_instance,
	 context);
      g_node_destroy (dependency_tree);

      weak_ref = g_hash_table_lookup (context->instances, &type);
      inst = g_weak_ref_get (weak_ref);
    }

  /* Unref all the instances referenced during this type resolution cycle. */
  
  g_list_free_full (context->referenced_instances, g_object_unref);
  
  context->referenced_instances = NULL;

  return inst;
}

gpointer
gzochid_resolver_require (GType type, GError **err)
{
  GzochidResolutionContext *context = g_object_new
    (GZOCHID_TYPE_RESOLUTION_CONTEXT, NULL);
  GObject *obj = gzochid_resolver_require_full (context, type, err);

  g_object_unref (context);
  return obj;
}

void
gzochid_resolver_provide (GzochidResolutionContext *context, GObject *obj,
			  GError **err)
{
  GType type = G_OBJECT_TYPE (obj);
  GWeakRef *weak_ref = g_hash_table_lookup (context->instances, &type);
  
  if (type != GZOCHID_TYPE_RESOLUTION_CONTEXT && weak_ref == NULL)
    {
      GType *type_key = malloc (sizeof (GType));

      *type_key = type;

      weak_ref = malloc (sizeof (GWeakRef));
      g_weak_ref_init (weak_ref, g_object_ref_sink (obj));
      context->provided_instances = g_list_append
	(context->provided_instances, obj);

      g_hash_table_insert (context->instances, type_key, weak_ref);
    }
  else g_set_error
	 (err, GZOCHID_RESOLUTION_ERROR,
	  GZOCHID_RESOLUTION_ERROR_DUPLICATE_TYPE,
	  "Type '%s' already present in resolution context.",
	  g_type_name (type));
}

/* A `GHFunc' implementation to insert all objects in the source instance cache
   (with the exceptioon of the source resolution context itself) into the
   destination resolution context instant cache. As part of this process the 
   "copied" objects have their reference counts increased. */

static void
copy_ref (gpointer key, gpointer value, gpointer user_data)
{
  GType *src_type = key;
  GObject *value_obj = g_weak_ref_get (value);
  
  if (*src_type != GZOCHID_TYPE_RESOLUTION_CONTEXT && value_obj != NULL)
    {
      GType *dst_type = malloc (sizeof (GType));
      GWeakRef *weak_ref = malloc (sizeof (GWeakRef));

      GzochidResolutionContext *dst = user_data;      

      *dst_type = *src_type;
      g_weak_ref_init (weak_ref, value_obj);
      g_object_unref (value_obj);
      
      g_hash_table_insert (dst->instances, dst_type, weak_ref);
    }
}

GzochidResolutionContext *
gzochid_resolver_clone (GzochidResolutionContext *src)
{
  GzochidResolutionContext *dst = g_object_new
    (GZOCHID_TYPE_RESOLUTION_CONTEXT, NULL);

  g_hash_table_foreach (src->instances, copy_ref, dst);
  
  return dst;
}
