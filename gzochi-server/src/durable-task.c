/* durable-task.c: Persistent task management routines for gzochid
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <glib-object.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "app.h"
#include "auth_int.h"
#include "data.h"
#include "descriptor.h"
#include "durable-task.h"
#include "event.h"
#include "event-app.h"
#include "gzochid-auth.h"
#include "schedule.h"
#include "scheme-task.h"
#include "taskclient.h"
#include "txlog.h"
#include "util.h"

#define PENDING_TASK_PREFIX "s.pendingTask."

GHashTable *serialization_registry = NULL;

/* Stores contextual information about the durable task management subsystem's
   participation in an application transaction. */

struct _durable_task_transaction_context
{
  /* The application context to which the transaction is bound. */
  
  GzochidApplicationContext *app_context; 

  /* The identity that initiated the transaction. */

  gzochid_auth_identity *identity;

  /* List of durable tasks to be scheduled to the local task queue upon 
     commit. */
  
  GList *scheduled_tasks;

  /* Whether there's a connected meta server that shoudl be notified about task
     events related to this transaction. */

  gboolean should_notify; 

  /* Is this transaction related to task resubmission? */

  gboolean is_resubmitting; 
  
  /* Tasks scheduled in this transaction, as a list of `guint64 *'. */

  GList *scheduled_task_oids;

  /* Tasks canceled in this transaction, as a list of `guint64 *'. */
  
  GList *canceled_task_oids;

  /* Pointer to the oid of the task completing in this transaction, if any. */

  guint64 *completed_task_oid;
};

typedef struct _durable_task_transaction_context
durable_task_transaction_context;

struct _gzochid_durable_application_task
{
  guint64 handle_oid;
};

typedef struct _gzochid_durable_application_task
gzochid_durable_application_task;

struct _gzochid_durable_application_task_handle
{
  gzochid_application_task_serialization *serialization;

  /* The named binding for this durable task, or `NULL' if not durably 
     persisted for resubmit on bootstrap. */

  char *binding;

  gzochid_application_worker task_worker;
  gzochid_data_managed_reference *task_data_reference;

  gzochid_auth_identity *identity;
  
  gboolean repeats;
  struct timeval period;
  struct timeval target_execution_time;
};

static int 
task_prepare (gpointer data)
{
  return TRUE;
}

static durable_task_transaction_context *
create_transaction_context (GzochidApplicationContext *app_context)
{
  GzochidMetaClient *metaclient =
    gzochid_application_context_get_metaclient (app_context);
  durable_task_transaction_context *tx_context = 
    calloc (1, sizeof (durable_task_transaction_context));

  tx_context->app_context = g_object_ref (app_context);

  if (metaclient != NULL)
    {
      tx_context->should_notify = TRUE;
      g_object_unref (metaclient);
    }
  
  return tx_context;
}

/* Releases the resources used by the specified task transaction. */ 

static void 
cleanup_transaction (durable_task_transaction_context *tx_context)
{
  g_list_free_full (tx_context->scheduled_tasks, g_object_unref);

  g_list_free_full (tx_context->scheduled_task_oids, free);

  if (tx_context->completed_task_oid != NULL)
    free (tx_context->completed_task_oid);

  g_list_free_full (tx_context->canceled_task_oids, free);
  
  g_object_unref (tx_context->app_context);
  
  free (tx_context);
}

/*
  Convenience function for handling the task scheduling and, when running in 
  distributed mode, tascklient notifications related to the side effects of the
  committing transaction.
  
  This function must be called with the application state mutex locked.
*/

static void
schedule_and_submit (gzochid_application_state *state,
		     GzochidMetaClient *metaclient,
		     durable_task_transaction_context *tx_context)
{
  GError *err = NULL;
  gzochid_event_source *event_source = NULL;
  GzochidTaskQueue *task_queue = NULL; 
  GzochidTaskClient *taskclient = NULL;

  G_LOCK_DEFINE_STATIC (submit_complete);
  
  GList *scheduled_tasks = tx_context->scheduled_tasks;
  GList *scheduled_task_oids = tx_context->scheduled_task_oids;
  GList *canceled_task_oids = tx_context->canceled_task_oids;

  const char *app_name =
    gzochid_application_context_get_name (tx_context->app_context);

  g_object_get (tx_context->app_context,
		"event-source", &event_source,
		"task-queue", &task_queue,
		NULL);

  if (metaclient != NULL)
    g_object_get (metaclient, "task-client", &taskclient, NULL);

  G_LOCK (submit_complete);
  
  while (scheduled_task_oids != NULL)
    {
      guint64 *task_oid = scheduled_task_oids->data;

      /* For tasks to actually be scheduled and potentially submitted to the 
	 taskclient, the application must be running _or_ this transaction must
	 have been initiated to support durable task resubmission. */	 
      
      if (state->bootstrap_stage == STARTING
	  || state->bootstrap_stage == STARTED
	  || tx_context->is_resubmitting)
	{
	  GzochidTask *task = scheduled_tasks->data;

	  /* Submit the task to the local task queue to get a local id... */
	  
	  guint64 local_task_id = gzochid_schedule_submit_task
	    (task_queue, task);	  
	  
	  if (tx_context->should_notify)
	    {
	      /* ...that can be associated with the task if we're submitting it
		 to the meta server via the taskclient. */
	      
	      gzochid_taskclient_submit_task
		(taskclient, app_name, *task_oid, local_task_id, &err);

	      if (err != NULL)
		{
		  g_warning
		    ("Failed to submit task %s/%" G_GUINT64_FORMAT
		     " in distributed mode; aborting task submission: %s",
		     app_name, *task_oid, err->message);

		  g_error_free (err);
		  break;
		}
	    }

	  gzochid_event_dispatch
	    (event_source, g_object_new (GZOCHID_TYPE_DURABLE_TASK_EVENT,
					 "application", app_name,
					 "type", DURABLE_TASK_SUBMITTED,
					 NULL));

	  scheduled_tasks = scheduled_tasks->next;
	}

      /* If neither of the conditions above are true, save the oids of the
	 scheduled tasks for submission when the application fully starts up. */
      
      else state->queued_task_oids = g_list_prepend
	     (state->queued_task_oids, g_memdup (task_oid, sizeof (guint64)));

      scheduled_task_oids = scheduled_task_oids->next;	  
    }

  /* Also submit any task completions... */
  
  if (tx_context->completed_task_oid != NULL)
    {
      if (tx_context->should_notify)
	{
	  GError *err = NULL;
	  
	  gzochid_taskclient_complete_task
	    (taskclient, app_name, *tx_context->completed_task_oid, &err);

	  assert (err == NULL);
	}

      gzochid_event_dispatch (event_source,
			      g_object_new (GZOCHID_TYPE_DURABLE_TASK_EVENT,
					    "application", app_name,
					    "type", DURABLE_TASK_COMPLETED,
					    NULL));      
    }

  G_UNLOCK (submit_complete);

  /* ...and task cancellations. */
      
  while (canceled_task_oids != NULL)
    {
      guint64 *task_oid = canceled_task_oids->data;

      if (tx_context->should_notify)
	gzochid_taskclient_cancel_task (taskclient, app_name, *task_oid);

      gzochid_event_dispatch (event_source,
			      g_object_new (GZOCHID_TYPE_DURABLE_TASK_EVENT,
					    "application", app_name,
					    "type", DURABLE_TASK_CANCELED,
					    NULL));

      canceled_task_oids = canceled_task_oids->next;
    }

  if (tx_context->is_resubmitting)
    {
      /* If we're resubmitting tasks because the meta server asked us to, we
	 need to tell the meta server that we're done. We'll still need to wait
	 for the meta server to tell us to start normal task processing. */
      
      if (tx_context->should_notify)
	{
 	  gzochid_taskclient_complete_resubmission (taskclient, app_name, &err);

	  if (err != NULL)
	    {
	      g_warning
		("Failed to notify metaserver of task resubmission for "
		 "application %s: %s", app_name, err->message);
	      g_error_free (err);
	    }
	}
    }
  
  if (taskclient != NULL)
    g_object_unref (taskclient);

  g_object_unref (task_queue);
  g_source_unref ((GSource *) event_source);
}

static void 
task_commit (gpointer data)
{
  durable_task_transaction_context *tx_context = data;

  const char *app_name =
    gzochid_application_context_get_name (tx_context->app_context);
  GzochidMetaClient *metaclient = gzochid_application_context_get_metaclient
    (tx_context->app_context);
  gzochid_application_state *state = NULL;
  
  g_object_get (tx_context->app_context, "app-state", &state, NULL);

  g_mutex_lock (&state->mutex);
  if (metaclient == NULL
      && state->bootstrap_stage != STARTING
      && state->bootstrap_stage != STARTED
      && !tx_context->is_resubmitting)

    g_warning ("Application %s not marked as running or resubmitting; aborting "
	       "task submission.", app_name);

  else schedule_and_submit (state, metaclient, tx_context);
  g_mutex_unlock (&state->mutex);

  cleanup_transaction (tx_context);

  if (metaclient != NULL)
    g_object_unref (metaclient);
}

static void 
task_rollback (gpointer data)
{ 
  cleanup_transaction (data);
}

static gzochid_transaction_participant task_participant = 
  { "task", task_prepare, task_commit, task_rollback };

static durable_task_transaction_context *
join_transaction (GzochidApplicationContext *context)
{
  durable_task_transaction_context *tx_context = NULL;

  if (!gzochid_transaction_active ()
      || (tx_context = gzochid_transaction_context (&task_participant)) == NULL)
    {
      tx_context = create_transaction_context (context);
      gzochid_transaction_join (&task_participant, tx_context);
    }

  return tx_context;
}

gzochid_application_task_serialization *
gzochid_lookup_task_serialization (char *name)
{
  return g_hash_table_lookup (serialization_registry, name);
}

void 
gzochid_task_register_serialization 
(gzochid_application_task_serialization *serialization)
{
  g_hash_table_insert 
    (serialization_registry, serialization->name, serialization);
}

void 
gzochid_task_initialize_serialization_registry (void)
{
  serialization_registry = g_hash_table_new (g_str_hash, g_str_equal);
}

gzochid_application_worker_serialization 
received_message_worker_serialization = { NULL, NULL };

gzochid_io_serialization 
received_message_data_serialization = { NULL, NULL, NULL };

gzochid_application_task_serialization 
gzochid_client_received_message_task_serialization = 
  { 
    "received-message",
    &received_message_worker_serialization, 
    &received_message_data_serialization 
  };

gzochid_durable_application_task *
gzochid_durable_application_task_new (guint64 handle_oid)
{
  gzochid_durable_application_task *task = 
    malloc (sizeof (gzochid_durable_application_task));

  task->handle_oid = handle_oid;

  return task;
}

gzochid_durable_application_task_handle *
create_durable_task_handle 
(gzochid_data_managed_reference *task_data_reference,
 gzochid_application_task_serialization *serialization,
 gzochid_auth_identity *identity, struct timeval target_execution_time)
{
  gzochid_durable_application_task_handle *durable_task_handle = 
    malloc (sizeof (gzochid_durable_application_task_handle));
  struct timeval immediate = { 0, 0 };

  durable_task_handle->task_data_reference = task_data_reference;
  durable_task_handle->serialization = serialization;
  durable_task_handle->binding = NULL;
  durable_task_handle->identity = gzochid_auth_identity_ref (identity);
  durable_task_handle->repeats = FALSE;
  durable_task_handle->period = immediate;
  durable_task_handle->target_execution_time = target_execution_time;

  return durable_task_handle;
}

gzochid_durable_application_task_handle *
create_durable_periodic_task_handle
(gzochid_data_managed_reference *task_data_reference, 
 gzochid_application_task_serialization *serialization, 
 gzochid_auth_identity *identity, struct timeval target_execution_time, 
 struct timeval period)
{
  gzochid_durable_application_task_handle *durable_task_handle = 
    create_durable_task_handle 
    (task_data_reference, serialization, identity, target_execution_time);

  durable_task_handle->repeats = TRUE;
  durable_task_handle->period = period;
  
  return durable_task_handle;
}

void 
gzochid_durable_application_task_free (gzochid_durable_application_task *task)
{
  free (task);
}

static gpointer 
deserialize_durable_task_handle (GzochidApplicationContext *context,
				 GByteArray *in, GError **err)
{
  gzochid_durable_application_task_handle *handle = 
    malloc (sizeof (gzochid_durable_application_task_handle));
  char *serialization_name = NULL;

  guint64 oid = gzochid_util_deserialize_oid (in);
  
  serialization_name = gzochid_util_deserialize_string (in);
  handle->serialization = gzochid_lookup_task_serialization 
    (serialization_name);

  assert (handle->serialization != NULL);

  if (gzochid_util_deserialize_boolean (in))
    handle->binding = gzochid_util_deserialize_string (in);
  else handle->binding = NULL;
  
  handle->task_worker = 
    handle->serialization->worker_serialization->deserializer (context, in);
  handle->task_data_reference = gzochid_data_create_reference_to_oid 
    (context, handle->serialization->data_serialization, oid);
 
  handle->identity = gzochid_auth_identity_deserializer (context, in, NULL);
  handle->repeats = gzochid_util_deserialize_boolean (in);

  if (handle->repeats)
    handle->period = gzochid_util_deserialize_timeval (in);

  handle->target_execution_time = gzochid_util_deserialize_timeval (in);
  
  free (serialization_name);
  return handle;
}

static void 
serialize_durable_task_handle (GzochidApplicationContext *context,
			       gpointer data, GByteArray *out, GError **err)
{
  gzochid_durable_application_task_handle *handle = data;

  gzochid_util_serialize_oid (handle->task_data_reference->oid, out);
  gzochid_util_serialize_string (handle->serialization->name, out);

  gzochid_util_serialize_boolean (handle->binding != NULL, out);
  if (handle->binding != NULL)
    gzochid_util_serialize_string (handle->binding, out);

  handle->serialization->worker_serialization->serializer 
    (context, handle->task_worker, out);

  gzochid_auth_identity_serializer (context, handle->identity, out, NULL);

  gzochid_util_serialize_boolean (handle->repeats, out);
  if (handle->repeats)
    gzochid_util_serialize_timeval (handle->period, out);

  gzochid_util_serialize_timeval (handle->target_execution_time, out);
}

static void
finalize_durable_task_handle (GzochidApplicationContext *context, gpointer data)
{
  gzochid_durable_application_task_handle *handle = data;

  if (handle->binding != NULL)
    free (handle->binding);
  
  gzochid_auth_identity_finalizer (context, handle->identity);
  free (handle);
}

gzochid_io_serialization
gzochid_durable_application_task_handle_serialization = 
  { 
    serialize_durable_task_handle, 
    deserialize_durable_task_handle, 
    finalize_durable_task_handle
  };

static void durable_task_application_worker
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);
static void durable_task_catch_worker
(GzochidApplicationContext *, gzochid_auth_identity *, gpointer);

static void 
remove_durable_task (GzochidApplicationContext *context, guint64 oid, 
		     GError **err)
{
  GError *local_err = NULL;
  gzochid_data_managed_reference *reference =
    gzochid_data_create_reference_to_oid
    (context, &gzochid_durable_application_task_handle_serialization, oid);   
  gzochid_durable_application_task_handle *task_handle =
    gzochid_data_dereference (reference, &local_err);

  if (task_handle == NULL)
    {
      g_propagate_error (err, local_err);
      return;
    }

  if (task_handle->binding != NULL)
    gzochid_data_remove_binding
      (reference->context, task_handle->binding, &local_err);
  
  if (local_err == NULL)
    {
      gzochid_data_remove_object (reference, &local_err);
      if (local_err != NULL)
	g_propagate_error (err, local_err);
    }
  else g_propagate_error (err, local_err);
}

/*
  Returns a newly-allocated string by concatenating "s.pendingTask." with the
  base-16 representation of the specified object id. When written to the `names'
  store, this binding will be used to resubmit the durable task that has the
  specified oid.
  
  This string should be freed via `free' when no longer in use.
*/

static char *
create_pending_task_binding (guint64 oid)
{
  GString *str = g_string_new (PENDING_TASK_PREFIX);
  g_string_append_printf (str, "%" G_GUINT64_FORMAT, oid);
  return g_string_free (str, FALSE);
}


gzochid_durable_application_task_handle *
gzochid_create_durable_application_task_handle
(gzochid_application_task *task, 
 gzochid_application_task_serialization *serialization, 
 struct timeval delay, struct timeval *period, GError **err)
{
  GError *local_err = NULL;
  struct timeval target;

  gzochid_data_managed_reference *task_data_reference = NULL;
  gzochid_durable_application_task_handle *task_handle = NULL;

  gettimeofday (&target, NULL);

  target.tv_sec += delay.tv_sec;
  target.tv_usec += delay.tv_usec;

  task_data_reference = gzochid_data_create_reference 
    (task->context, serialization->data_serialization, task->data, &local_err);

  /* Creating a new reference may fail if the transaction's in a bad state. */
  
  if (local_err != NULL)
    {
      g_propagate_error (err, local_err);
      return NULL;
    }
  
  if (period != NULL)
    task_handle = create_durable_periodic_task_handle
      (task_data_reference, serialization, task->identity, target, *period);
  else task_handle = create_durable_task_handle
	 (task_data_reference, serialization, task->identity, target);

  /* Make the task handle consistent with respect to the serialization /
     deserialization cycle. This is a convenience primarily for test code that 
     wants to execute a durable task without first flushing it to disk and
     reading it back. */
  
  task_handle->task_worker = task->worker;
  
  return task_handle;
}
 
static void 
durable_task_application_worker (GzochidApplicationContext *context, 
				 gzochid_auth_identity *identity, gpointer data)
{
  GError *err = NULL;
  durable_task_transaction_context *tx_context = join_transaction (context);

  gzochid_durable_application_task *task = data;
  gzochid_data_managed_reference *handle_reference =
    gzochid_data_create_reference_to_oid
    (context, &gzochid_durable_application_task_handle_serialization, 
     task->handle_oid);
  gzochid_durable_application_task_handle *handle = NULL;
  gzochid_application_task *inner_task = NULL;

  gzochid_data_dereference (handle_reference, &err);

  if (err != NULL)
    {
      g_error_free (err);
      return;
    }
      
  handle = handle_reference->obj;
  gzochid_data_dereference (handle->task_data_reference, &err);

  if (err != NULL)
    {
      g_error_free (err);
      return;
    }

  inner_task = gzochid_application_task_new 
    (context, identity, handle->task_worker, NULL,
     handle->task_data_reference->obj);
  inner_task->worker (context, identity, inner_task->data);
  gzochid_application_task_free (inner_task);

  if (gzochid_transaction_rollback_only ())

    /* The body of the task worker may have done something to cause the
       transaction to be rolled back. If that's the case, we should not bother
       to attempt any logic that involves the data manager. */

    return;

  /* The repeats status of the task may have changed while executing the 
     worker, so check again to see if it needs to be removed. */

  if (! handle->repeats)
    {
      remove_durable_task (context, task->handle_oid, &err);

      if (err != NULL)
	{
	  g_error_free (err);
	  return;
	}

      if (tx_context->should_notify)

	/* Only need to track completions if we're going to be sending them to
	   the meta server. */

	tx_context->completed_task_oid =
	  g_memdup (&task->handle_oid, sizeof (guint64));
    }
  else
    {
      struct timeval now;

      gettimeofday (&now, NULL);
      timeradd (&handle->period, 
		&handle->target_execution_time, 
		&handle->target_execution_time);

      gzochid_data_mark 
	(context, &gzochid_durable_application_task_handle_serialization, 
	 handle, &err);

      if (err == NULL)
	gzochid_schedule_durable_task_handle (context, handle, NULL);
      else g_error_free (err);
    }
}

static void 
durable_task_catch_worker (GzochidApplicationContext *context, 
			   gzochid_auth_identity *identity, gpointer data)
{
  guint64 *oid = data;
  remove_durable_task (context, *oid, NULL);
}

void 
gzochid_schedule_durable_task 
(GzochidApplicationContext *context, gzochid_auth_identity *identity,
 gzochid_application_task *task,
 gzochid_application_task_serialization *serialization, GError **err)
{
  gzochid_schedule_delayed_durable_task 
    (context, identity, task, serialization, (struct timeval) { 0, 0 }, err);
}

void 
gzochid_schedule_delayed_durable_task
(GzochidApplicationContext *context, gzochid_auth_identity *identity,
 gzochid_application_task *task, 
 gzochid_application_task_serialization *serialization, 
 struct timeval delay, GError **err)
{
  GError *local_err = NULL;
  
  gzochid_durable_application_task_handle *handle = 
    gzochid_create_durable_application_task_handle
    (task, serialization, delay, NULL, &local_err);

  if (handle != NULL)
    gzochid_schedule_durable_task_handle (context, handle, err);
  else g_propagate_error (err, local_err);
}

gzochid_periodic_task_handle *
gzochid_schedule_periodic_durable_task 
(GzochidApplicationContext *context, gzochid_auth_identity *identity, 
 gzochid_application_task *task,
 gzochid_application_task_serialization *serialization, 
 struct timeval delay, struct timeval period, GError **err)
{
  GError *local_err = NULL;
  gzochid_durable_application_task_handle *handle = 
    gzochid_create_durable_application_task_handle
    (task, serialization, delay, &period, &local_err);

  if (handle != NULL)
    {
      gzochid_schedule_durable_task_handle (context, handle, &local_err);

      if (local_err == NULL)
	return handle;
      else
	{
	  g_debug
	    ("Failed to schedule durable task in transaction: %s",
	     local_err->message);

	  g_propagate_error (err, local_err);
	  return NULL;
	}
    }
  else
    {
      g_propagate_error (err, local_err);
      return NULL;
    }
}

void gzochid_schedule_durable_task_handle
(GzochidApplicationContext *app_context,
 gzochid_durable_application_task_handle *task_handle, GError **err)
{
  guint64 *oid = NULL;
  GError *local_err = NULL;

  gzochid_application_task *transactional_task = NULL;
  gzochid_application_task *catch_task = NULL;
  
  gzochid_transactional_application_task_execution *execution = NULL;

  gzochid_application_task *application_task = NULL;

  gzochid_data_managed_reference *durable_task_handle_reference = 
    gzochid_data_create_reference
    (app_context, &gzochid_durable_application_task_handle_serialization,
     task_handle, NULL);
  
  gzochid_durable_application_task *durable_task = NULL;  
  durable_task_transaction_context *tx_context = NULL;

  guint64 tx_timeout_ms = 0;
  struct timeval tx_timeout;

  GzochidTask *task = NULL;
  
  /* The task handle pointer should already be cached in the current transaction
     since it must have been created and persisted via 
     `gzochid_create_durable_application_task_handle' or we are rescheduling an
     existing task handle. */
  
  assert (durable_task_handle_reference != NULL);

  durable_task = gzochid_durable_application_task_new
    (durable_task_handle_reference->oid);

  /* The memory allocated for this oid is freed by the cleanup task. */
  
  oid = g_memdup (&durable_task_handle_reference->oid, sizeof (guint64));

  if (task_handle->binding == NULL)

    /* Only create a fresh task binding if the task handle doesn't have one set.
       Repeating tasks that are being rescheduled will already have a 
       binding. */
    
    task_handle->binding = create_pending_task_binding
      (durable_task_handle_reference->oid);
  
  gzochid_data_set_binding_to_oid 
    (app_context, task_handle->binding, durable_task_handle_reference->oid,
     &local_err);

  if (local_err != NULL)
    {
      g_free (oid);
      gzochid_durable_application_task_free (durable_task);      

      g_propagate_error (err, local_err);
      
      return;
    }
  
  tx_context = join_transaction (app_context);
  
  transactional_task = gzochid_application_task_new
    (app_context, task_handle->identity, durable_task_application_worker,
     (GDestroyNotify) gzochid_durable_application_task_free, durable_task);
  catch_task = gzochid_application_task_new
    (app_context, task_handle->identity, durable_task_catch_worker, free, oid);

  g_object_get (app_context, "tx-timeout", &tx_timeout_ms, NULL);

  tx_timeout.tv_sec = tx_timeout_ms / 1000;
  tx_timeout.tv_usec = (tx_timeout_ms % 1000) * 1000;

  execution = gzochid_transactional_application_task_timed_execution_new 
    (transactional_task, catch_task, NULL, tx_timeout);

  /* Not necessary to hold a ref to these, as we've transferred them to the
     execution. */

  gzochid_application_task_unref (transactional_task);
  gzochid_application_task_unref (catch_task);
  
  application_task = gzochid_application_task_new
    (app_context, task_handle->identity,
     gzochid_application_resubmitting_transactional_task_worker,
     (GDestroyNotify) gzochid_transactional_application_task_execution_free,
     execution);

  task = gzochid_task_new
    (gzochid_application_task_thread_worker,
     (GDestroyNotify) gzochid_application_task_unref,
     application_task, task_handle->target_execution_time);
  
  tx_context->scheduled_tasks = g_list_append 
    (tx_context->scheduled_tasks, g_object_ref_sink (task));

  /*
    Save the oid of the durable task that's going to be scheduled, so that we
    can send it to the meta server if we're running in distributed mode, or
    buffer it locally if the application isn't fully started.
  
    It's critical that these lists have the same ordering relative to each 
    other.
  */

  tx_context->scheduled_task_oids = g_list_append
    (tx_context->scheduled_task_oids, g_memdup (oid, sizeof (guint64)));
}

/*
  The following data structures and functions can be used to manage the 
  execution in serial of a sequence of tasks, as submitted via 
  `gzochid_schedule_durable_task_chain'. The basic mechanism is this:

  Each task in the sequence is "hosted" by a "coordinator," which deserializes
  the task queue, pops the first task off of it, runs that task retryably, and,
  if there are more tasks in the queue, reschedules itself. The coordinator task
  is _not_ itself durable. Upon scheduling, a pending task binding for a task 
  that schedules the coordinator is written to the names store to allow 
  servicing of the queue to resume on container bootstrap. This binding is 
  removed - along with the queue itself - when the queue is found to be empty.
*/

/* Contextual / state information for the sequence of tasks. */

struct _task_chain_context
{
  /* A reference to the durable task that re-starts the processing of the task
     chain on server start-up. */
  
  gzochid_data_managed_reference *bootstrap_ref;

  /* A reference to the durable queue that holds the sequence of tasks. */
  
  gzochid_data_managed_reference *chain_ref;
};

typedef struct _task_chain_context task_chain_context;

/* Serialization routines for the durable task chain context. */

static void
serialize_task_chain_context (GzochidApplicationContext *app_context,
			      void *data, GByteArray *out, GError **err)
{
  task_chain_context *chain_context = data;
  
  gzochid_util_serialize_oid (chain_context->bootstrap_ref->oid, out);
  gzochid_util_serialize_oid (chain_context->chain_ref->oid, out);
}

static void *
deserialize_task_chain_context (GzochidApplicationContext *app_context,
				GByteArray *in, GError **err)
{
  task_chain_context *chain_context = malloc (sizeof (task_chain_context));
  guint64 oid = gzochid_util_deserialize_oid (in);
  
  chain_context->bootstrap_ref = gzochid_data_create_reference_to_oid
    (app_context, &gzochid_durable_application_task_handle_serialization, oid);

  oid = gzochid_util_deserialize_oid (in);
  
  chain_context->chain_ref = gzochid_data_create_reference_to_oid
    (app_context, &gzochid_durable_queue_serialization, oid);
  
  return chain_context;
}

static void
finalize_task_chain_context (GzochidApplicationContext *app_context, void *data)
{
  free (data);
}

static gzochid_io_serialization
task_chain_context_serialization =
  {
    serialize_task_chain_context,
    deserialize_task_chain_context,
    finalize_task_chain_context
  };

/*
  Clean up the artifacts associated with a durable task chain execution 
  context. Specifically: 
  
  - Stop the coordinator from running again.
  - Remove any remaining durable task handles from the durable task queue.
  - Remove the durable task queue object.
  - Remove the task context object.
*/

static void
cleanup_coordinator_task (GzochidApplicationContext *app_context,
			  task_chain_context *task_chain_context)
{
  GError *err = NULL;
  gzochid_durable_application_task_handle *task_handle = NULL;
  gzochid_durable_queue *queue = NULL;

  task_handle = gzochid_data_dereference
    (task_chain_context->bootstrap_ref, &err);

  if (err == NULL)
    {
      /* Tell the coordinator to stop repeating. */
      
      task_handle->repeats = FALSE;
      gzochid_data_mark	(app_context, &task_chain_context_serialization,
			 task_chain_context, &err);
    }
  if (err == NULL)
    queue = gzochid_data_dereference_for_update
      (task_chain_context->chain_ref, &err);
  
  if (err == NULL)
    while ((task_handle = gzochid_durable_queue_pop
	    (queue, &gzochid_durable_application_task_handle_serialization,
	     &err)) != NULL && err == NULL)
      {
	gzochid_data_managed_reference *handle_ref =
	  gzochid_data_create_reference
	  (app_context, &gzochid_durable_application_task_handle_serialization,
	   task_handle, &err);
	
	/* Creating a new reference may fail if the transaction's in a bad 
	   state. */
	
	if (err == NULL)
	  gzochid_data_remove_object (handle_ref, &err);
      }
  
  if (err == NULL)
    gzochid_data_remove_object (task_chain_context->chain_ref, &err);
  if (err == NULL)
    gzochid_data_remove_object
      (gzochid_data_create_reference
       (app_context, &task_chain_context_serialization, task_chain_context,
	NULL), &err);

  if (err != NULL)
    {
      g_debug ("Failed to clean up coordinator task context: %s", err->message);
      g_error_free (err);
    }
}

/* The coordinator worker task implementation. Deserializes the task chain
   context, pops the next task, runs it, and then either reschedules itself (if
   there's more work to do) or cleans up the queue (if there isn't). */

static void
task_chain_coordinator_worker (GzochidApplicationContext *app_context,
			       gzochid_auth_identity *identity, gpointer data)
{
  GError *err = NULL;
  task_chain_context *task_chain_context = data;
  gzochid_durable_queue *queue = NULL;

  gzochid_durable_application_task_handle *next_task_handle = NULL;

  if (err != NULL)
    {
      g_debug ("Failed to load task chain context: %s", err->message);
      g_error_free (err);
      return;
    }

  /* Deference the queue for update - we're going to pop it immediately. */
  
  queue = gzochid_data_dereference_for_update
    (task_chain_context->chain_ref, &err);

  if (err != NULL)
    {
      g_debug ("Failed to load task chain: %s", err->message);
      g_error_free (err);
      return;
    }

  next_task_handle = gzochid_durable_queue_pop
    (queue, &gzochid_durable_application_task_handle_serialization, &err);
  
  if (err != NULL)
    {
      g_debug ("Failed to pop the next chained task: %s", err->message);
      g_error_free (err);
      return;
    }

  if (next_task_handle != NULL)
    {
      gboolean has_next = FALSE;
      gzochid_data_managed_reference *next_task_handle_ref =
	gzochid_data_create_reference
	(app_context, &gzochid_durable_application_task_handle_serialization,
	 next_task_handle, NULL);
      gzochid_durable_application_task *task = NULL;

      durable_task_transaction_context *tx_context =
	join_transaction (app_context);

      /* Repeating tasks are not supported as members of a task chain. */
  
      assert (!next_task_handle->repeats);

      /* The reference to the next task handle should already be cached in the 
	 current transaction (as a result of popping it off the queue above) and
	 so there's no reason that getting a reference to it should fail. */
      
      assert (next_task_handle_ref != NULL);
      
      gzochid_data_mark
	(app_context, &gzochid_durable_application_task_handle_serialization,
	 next_task_handle_ref->obj, &err);

      if (err != NULL)
	{
	  g_debug ("Failed to mark chain task for update: %s", err->message);
	  g_error_free (err);
	  return;
	}

      /* Re-hydrate the inner task using the oid from the task handle. */
      
      task = gzochid_durable_application_task_new (next_task_handle_ref->oid);

      /* Execute the inner task. This may cause the transaction to be rolled 
	 back, but the worker interafce doesn't offer a way to signal that
	 directly... */
      
      durable_task_application_worker
	(app_context, next_task_handle->identity, task);

      gzochid_durable_application_task_free (task);      

      if (tx_context->completed_task_oid != NULL)
	{
	  free (tx_context->completed_task_oid);
	  tx_context->completed_task_oid = NULL;
	}

      /* Remove the task. */
      
      gzochid_data_remove_object (next_task_handle_ref, &err);

      /* Before rescheduling the coordinator, check to see whether there are
	 more tasks to process. There's no point in adding empty work to the
	 scheduling queue. */
	  
      if (err == NULL)
	has_next = gzochid_durable_queue_peek
	  (queue, &gzochid_durable_application_task_handle_serialization,
	   &err) != NULL;      
      if (err == NULL)
	{
	  if (!has_next)

	    /* If the queue has no next element, then run the clean up 
	       process in the current transaction. */
	    
	    cleanup_coordinator_task (app_context, task_chain_context);
	}
      else
	{
	  g_debug ("Failed to retrieve next chain task: %s", err->message);
	  g_error_free (err);
	}
    }
}

/* Task serialization routines for the durable task chain bootstrap task. */

static void
serialize_task_chain_coordinator_worker (GzochidApplicationContext *app_context,
					 gzochid_application_worker worker,
					 GByteArray *out)
{
}

static gzochid_application_worker
deserialize_task_chain_coordinator_worker
(GzochidApplicationContext *app_context, GByteArray *in)
{
  return task_chain_coordinator_worker;
}

static gzochid_application_worker_serialization
task_chain_coordinator_worker_serialization =
  {
    serialize_task_chain_coordinator_worker,
    deserialize_task_chain_coordinator_worker
  };

gzochid_application_task_serialization
gzochid_task_chain_coordinator_task_serialization =
  {
    "task-chain",
    &task_chain_coordinator_worker_serialization,
    &task_chain_context_serialization
  };

void
gzochid_schedule_durable_task_chain (GzochidApplicationContext *app_context,
				     gzochid_auth_identity *identity,
				     gzochid_durable_queue *task_chain,
				     GError **err)
{
  GError *local_err = NULL;
  task_chain_context *chain_context = calloc (1, sizeof (task_chain_context));

  gzochid_application_task *coordinator_task = NULL;
  gzochid_periodic_task_handle *task_handle = NULL;
  
  if (local_err != NULL)
    {
      g_debug
	("Failed to create reference to task chain context: %s",
	 local_err->message);
      g_propagate_error (err, local_err);
      return;
    }

  coordinator_task = gzochid_application_task_new
    (app_context, identity, task_chain_coordinator_worker, NULL, chain_context);
  
  task_handle = gzochid_schedule_periodic_durable_task
    (app_context, identity, coordinator_task,
     &gzochid_task_chain_coordinator_task_serialization,
     (struct timeval) { 0, 0 }, (struct timeval) { 0, 0 }, &local_err);

  gzochid_application_task_unref (coordinator_task);
  
  if (local_err == NULL)
    chain_context->chain_ref = gzochid_data_create_reference
      (app_context, &gzochid_durable_queue_serialization, task_chain,
       &local_err);

  /* Add a ref to the task handle to the chain context itself, to make it easy
     to manipulate its state (e.g., change the `repeats' flag) in the worker. */
  
  if (local_err == NULL)
    chain_context->bootstrap_ref = gzochid_data_create_reference
      (app_context, &gzochid_durable_application_task_handle_serialization,
       task_handle, &local_err);

  if (local_err != NULL)
    {
      g_debug
	("Failed to create reference to task chain task: %s",
	 local_err->message);
      g_propagate_error (err, local_err);
    }
}

void 
gzochid_cancel_periodic_task (GzochidApplicationContext *context, 
			      gzochid_periodic_task_handle *handle)
{
  GError *local_err = NULL;
  durable_task_transaction_context *tx_context = join_transaction (context);
  
  handle->repeats = FALSE;

  gzochid_data_mark 
    (context, &gzochid_durable_application_task_handle_serialization, 
     handle, NULL);

  if (tx_context->should_notify)
    {
      /* Only need to save canceled task oids if we're going to be submitting
	 them to the meta server upon commit... */
      
      gzochid_data_managed_reference *ref = gzochid_data_create_reference
	(context, &gzochid_durable_application_task_handle_serialization,
	 handle, &local_err);

      tx_context->canceled_task_oids = g_list_append
	(tx_context->canceled_task_oids,
	 g_memdup (&ref->oid, sizeof (guint64)));
    }
}

void 
gzochid_restart_tasks (GzochidApplicationContext *context)
{
  guint64 oid;
  GError *err = NULL;
  char *next_binding = NULL;
  int num_tasks = 0;

  durable_task_transaction_context *tx_context = join_transaction (context);

  /* Need to mark this transaction as being related to task resubmission, since
     that information will change the behavior somewhat during commit. */
  
  tx_context->is_resubmitting = TRUE;
  
  gzochid_tx_info (context, "Resubmitting durable tasks.");
  next_binding = gzochid_data_next_binding_oid 
    (context, PENDING_TASK_PREFIX, &oid, &err);

  assert (err == NULL);

  while (next_binding != NULL 
	 && strncmp (PENDING_TASK_PREFIX, next_binding, 14) == 0)
    {
      char *next_next_binding = NULL;
      gzochid_data_managed_reference *handle_reference = 
	gzochid_data_create_reference_to_oid 
	(context, &gzochid_durable_application_task_handle_serialization, oid);

      gzochid_data_dereference (handle_reference, &err);

      if (err != NULL)
	{
	  assert (err->code == GZOCHID_DATA_ERROR_NOT_FOUND);

	  gzochid_tx_warning 
	    (context, "Task handle not found for resubmitted task.");
	  g_error_free (err);

	  err = NULL;
	  
	  gzochid_data_remove_binding (context, next_binding, &err);
	  assert (err == NULL);
	}
      else 
	{
	  gzochid_schedule_durable_task_handle
	    (context, handle_reference->obj, &err);

	  /* There's no reason this should fail: In this context it's being 
	     scheduled with a single active thread with an infinite transaction
	     timeout. */
	  
	  assert (err == NULL);
	  
	  num_tasks++;
	}

      next_next_binding = 
	gzochid_data_next_binding_oid (context, next_binding, &oid, &err);

      assert (err == NULL);

      free (next_binding);
      next_binding = next_next_binding;
    }

  if (num_tasks == 1)
    gzochid_tx_info (context, "Resubmitted 1 task.", num_tasks);
  else if (num_tasks > 1)
    gzochid_tx_info (context, "Resubmitted %d tasks.", num_tasks);
  else gzochid_tx_info (context, "No tasks found to resubmit.");
}

static void
initialize (GzochidApplicationContext *app_context)
{
  GError *err = NULL;
  gboolean initialized = gzochid_data_binding_exists 
    (app_context, "s.initializer", &err);

  assert (err == NULL);

  /* Execute the Scheme `initialized' callback if this is the first time the
     application has ever been run. */
    
  if (!initialized)
    {
      GHashTable *properties = NULL;
      GzochidApplicationDescriptor *descriptor = NULL;

      g_object_get (app_context, "descriptor", &descriptor, NULL);
      properties = descriptor->properties;

      /* Unref the descriptor here to avoid leaking a reference if the worker
	 exits non-locally. */

      g_object_unref (descriptor);

      gzochid_scheme_application_initialized_worker
	(app_context, gzochid_auth_system_identity (), properties);
    }
}

/* The main transaction worker for the queued task scheduling transaction. */

static void
enqueued_task_transactional_worker (GzochidApplicationContext *app_context,
				    gzochid_auth_identity *identity,
				    gpointer data)
{
  GError *err = NULL;
  GList *queued_task_oids = NULL;
  gzochid_application_state *state = NULL;

  initialize (app_context);
  
  g_object_get (app_context, "app-state", &state, NULL);
  queued_task_oids = state->queued_task_oids;
  
  while (queued_task_oids != NULL)
    {
      guint64 *task_id_ptr = queued_task_oids->data;
      gzochid_data_managed_reference *ref = gzochid_data_create_reference_to_oid
	(app_context, &gzochid_durable_application_task_handle_serialization,
	 *task_id_ptr);

      gzochid_data_dereference (ref, &err);

      if (err != NULL)
	gzochid_schedule_durable_task_handle (app_context, ref->obj, &err);

      if (err != NULL)
	{
	  /* If we failed to dereference or schedule the task, the transaction 
	     is likely in a bad state. Shouldn't keep trying with subsequent
	     tasks. */
	  
	  g_error_free (err);
	  break;
	}

      queued_task_oids = queued_task_oids->next;
    }
}

/* A "catch" task for the queued task scheduling transaction. */

static void
enqueued_task_catch_worker (GzochidApplicationContext *app_context,
			    gzochid_auth_identity *identity, gpointer data)
{
  gzochid_application_state *state = NULL;

  g_object_get (app_context, "app-state", &state, NULL);

  /* Log a warning if we failed to schedule the queued durable tasks. */
  
  g_warning ("Bootstrapping enqueued tasks for application %s failed.",
	     gzochid_application_context_get_name (app_context));

  g_mutex_lock (&state->mutex);
  state->bootstrap_stage = FAILED;
  g_mutex_unlock (&state->mutex);
}

/* A "cleanup" task for the queued task scheduling transaction. */

static void
enqueued_task_cleanup_worker (GzochidApplicationContext *app_context,
			      gzochid_auth_identity *identity, gpointer data)
{
  gzochid_application_state *state = NULL;

  g_object_get (app_context, "app-state", &state, NULL);

  g_mutex_lock (&state->mutex);
  
  /* Clear out all of the queued task oids, whether we were able to schedule
     them or not. */
  
  g_list_free_full (state->queued_task_oids, free);
  state->queued_task_oids = NULL;

  if (state->bootstrap_stage == STARTING)
    state->bootstrap_stage = STARTED;

  g_cond_broadcast (&state->cond);  
  g_mutex_unlock (&state->mutex);
}

/* Construct and return a `GzochidTask' that wraps a `gzochid_application_task'
   that schedules all of the durable tasks that have been enqueud prior to
   application startup. */

static GzochidTask *
create_enqueued_task_bootstrap_task (GzochidApplicationContext *app_context)
{
  gzochid_application_task *transactional_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     enqueued_task_transactional_worker, NULL, NULL);
  gzochid_application_task *catch_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (), enqueued_task_catch_worker,
     NULL, NULL);
  gzochid_application_task *cleanup_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (), enqueued_task_cleanup_worker,
     NULL, NULL);
  
  gzochid_transactional_application_task_execution *execution =
    gzochid_transactional_application_task_execution_new
    (transactional_task, catch_task, cleanup_task);

  gzochid_application_task *application_task = gzochid_application_task_new
    (app_context, gzochid_auth_system_identity (),
     gzochid_application_resubmitting_transactional_task_worker,
     (GDestroyNotify) gzochid_transactional_application_task_execution_free,
     execution);

  gzochid_application_task_unref (transactional_task);
  gzochid_application_task_unref (catch_task);
  gzochid_application_task_unref (cleanup_task);
  
  return gzochid_task_immediate_new
    (gzochid_application_task_thread_worker,
     (GDestroyNotify) gzochid_application_task_unref, application_task);
}

void
gzochid_start_task_processing (GzochidApplicationContext *app_context)
{
  gzochid_application_state *state = NULL;
  GzochidTaskQueue *task_queue = NULL;
  
  g_object_get
    (app_context, "app-state", &state, "task-queue", &task_queue, NULL);
  
  g_mutex_lock (&state->mutex);

  if (state->bootstrap_stage != NOT_STARTED)
    g_warning ("Task processing for application %s has already started.",
	       gzochid_application_context_get_name (app_context));
  else
    {
      /* Scheduling of enqueued durable tasks needs to happen in a 
	 transaction, and should run asynchronously with respect to this
	 function, which may be called in response to a notification from the
	 task server (if running in distributed mode). */
      
      gzochid_schedule_submit_task
	(task_queue, create_enqueued_task_bootstrap_task (app_context));

      state->bootstrap_stage = STARTING;
    }
  
  g_mutex_unlock (&state->mutex);
  g_object_unref (task_queue);
}

void
gzochid_wait_for_start (GzochidApplicationContext *app_context)
{
  gzochid_application_state *state = NULL;
  g_object_get (app_context, "app-state", &state, NULL);

  g_mutex_lock (&state->mutex);

  while (state->bootstrap_stage != STARTED && state->bootstrap_stage != FAILED)
    g_cond_wait (&state->cond, &state->mutex);

  g_mutex_unlock (&state->mutex);
}
