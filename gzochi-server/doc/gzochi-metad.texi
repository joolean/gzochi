\input texinfo	@c -*-texinfo-*-

@c %**start of header
@setfilename gzochi-metad.info
@include version-gzochi-metad.texi
@settitle GZOCHI-METAD MANUAL-EDITION @value{EDITION}
@defcodeindex op
@c %**end of header
     
@copying
This manual describes gzochi-metad, the gzochi meta server.

Copyright @copyright{} @value{UPDATED} Julian Graham.
     
@quotation
Permission is granted to copy, distribute and/or modify this document 
under the terms of the GNU Free Documentation License, Version 1.2 or 
any later version published by the Free Software Foundation; with no 
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A 
copy of the license is included in the section entitled ``GNU Free 
Documentation License''.
@end quotation
@end copying
     
@dircategory Servers
@direntry
* gzochi-metad: (gzochi-metad).	gzochi-metad, the gzochi meta server
@end direntry

@titlepage
@title gzochi-metad Manual
@subtitle Edition @value{EDITION}
@author Julian Graham

@c  The following two commands
@c  start the copyright page.
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage
     
@c So the toc is printed at the start.
@contents
     
@ifnottex
@node Top
@top The gzochi-metad Manual
     
@insertcopying
@end ifnottex

This reference manual documents gzochi-metad, the meta server
component of the gzochi massively multiplayer online game
development framework.

@menu

Getting started

* Introduction::
* Conceptual overview::
* Installation::
* Running gzochi-metad::

Administration

* Monitoring::

Appendices

* GNU Free Documentation License::

Indices

* Concept index::
@end menu

@contents

@node Introduction
@chapter Introduction

gzochi-metad is the meta server component for the gzochi massively
multiplayer online game development framework. As its name suggests,
it is responsible for supporting other servers: The gzochi meta
server coordinates access to shared resources across a
high-availability cluster of gzochi game application servers. When
a game is deployed to such a cluster, the failure of individual
application servers has no negative impact on game state, and the
player capacity of the game's infrastructure may be scaled
``horizontally'' by adding more application servers.

When a gzochid instance is connected to a meta server, its local
state---including its data store---becomes a proxy cache or
partition of the canonical game state managed by the meta server.
For example, each application server node maintains a table of
locally-connected game client sessions, and forwards messages for
remote sessions through the meta server.

The design and capabilities of the meta server are covered in
greater detail in the following section of this manual,
``Conceptual overview.''

@node Conceptual overview
@chapter Conceptual overview

The design of the meta server assumes that the hardware resource
under the most contention in a game application with many players is
the CPU, while network I/O and data storage throughput (as distinct
from data access latency) are not likely to be bottlenecks to
supporting more players interacting with a single logical game
state. As such, to support more players, a game application needs
more CPU cores to run its tasks---which means it needs to run across
multiple pieces of physical hardware.

The gzochid container is responsible for four major categories of
service: Data storage, task scheduling, session management, and
channel management. When multiple instances of gzochid (or
``application server nodes'') are clustered to serve a single game
application, the meta server takes responsibility for coordinating
those services across application server nodes to ensure that each
service continues to meet its consistency and reliability
guarantees.

@cindex data storage

To provide consistent access to data across application server
nodes, the meta server assumes control of the application's durable
storage. Application server nodes connected to a meta server
request intentional locks on individual keys or ranges of keys
within the store, and the meta server maintains a table of locks
granted to each application store. While an application server holds
a read or write lock for a particular key or range of keys, it can
execute transactions against a local proxy cache of the data
associated with the locked key(s), relaying changes back up to the
meta server as necessary.

The lock management protocol between the meta server and the gzochid
container nodes is cooperative; the nodes release their locks
voluntarily after a timeout has elapsed and all transactions against
locked regions of data have completed.

@cindex task scheduling

To distribute task execution across the cluster, the meta server
maintains a table of durably-scheduled tasks and the application
server node to which each task is currently assigned. Each
application server notifies the meta server when a new task is
submitted and when a task completes---or should no longer be
executed. The meta server in turn notifies application servers of
new task assignments, including those which arise when an
application server fails. When an application server cancels a task
assigned to a different server, the cancellation is relayed through
the meta server. 

@cindex session management

To distribute client session management across the cluster, the meta
server maintains a table of connected sessions and the application
servers node to which they are connected. When a task or callback
attempts an operation (like sending a message) on a seesion that
represents a client connected to a different application server
node than the one that executed the task, the server relays the
operation to the meta server, which in turn relays it to the server
to which the session is connected.

@cindex channel management

To distribute channel management across the cluster, the meta server
maintains a table of channels and their members on each application
server node. When a channel message is sent by a task or callback,
the application server on which that task was executed relays the
message to the meta server, which in turn relays it to any other
application server with at least one connected session that is a
member of the target channel.

@node Installation
@chapter Installation

See the @file{INSTALL} file included in the gzochi-server
distribution for detailed instructions on how to build gzochid and
gzochi-metad. In most cases, if you have the requisite toolchain and
dependences in place, you should simply be able to run

@example
./configure
make
make install
@end example

This will install the gzochid and gzochi-metad executables to a
standard location, depending on the installation prefix---on most
Unix-like systems, this will be @file{/usr/local}, with executable
files being copied to @file{/usr/local/bin}. Server configuration
files with the default settings will be installed to the @file{/etc}
directory under the installation prefix---by default,
@file{/usr/local/etc}.

The gzochi-metad configuration file will be processed prior to
installation to set references to the gzochi-metad data directory,
where the meta server stores game state data, to a location relative
to the installation prefix.
@xref{The meta server configuration file}, for more information.

@node Running gzochi-metad
@chapter Running gzochi-metad

The format for running the @command{gzochi-metad} program is:

@example
gzochid @var{option} @dots{}
@end example

With no options, @command{gzochi-metad} immediately begins listening
for connections from @command{gzochid} application server nodes. By
default, the monitoring web application is also started. 

In the absence of command line arguments, the port numbers on which
these servers listen for connections, as well as other aspects of 
their behavior, are modifiable via a configuration file (see below).

@command{gzochi-metad} supports the following options:

@table @option

@item --config
@itemx -c
@opindex --config
@opindex -c
Specify an alternate location for the meta server configuration
file.

@item --help
@itemx -h
@opindex --help
@opindex -h
Print an informative help message on standard output and exit
successfully.

@item --version
@itemx -v
@opindex --version
@opindex -v
Print the version number and licensing information of gzochi-metad
on standard output and then exit successfully.

@end table

@menu
* The meta server configuration file::
@end menu

@node The meta server configuration file
@section The meta server configuration file

@cindex gzochi-metad.conf

The gzochi-metad server configuration file is usually named 
@file{gzochi-metad.conf} and is installed (and searched for) by
default in the @file{/etc/} directory of the installation prefix. It
is an .ini-style configuration file, meaning it consists of several
named sections of key-value pairs, like so:

@example
[section]

key1 = value1
key2 = value2
@end example

The configuration options currently understood by gzochi-metad are
as follows, organized by section.

@emph{admin}

These settings control various aspects of gzochi-metad's
administrative and monitoring functionality.

@table @samp
@item module.httpd.enabled
Set to @code{true} to enable the monitoring web server.

@item module.httpd.port
The local port on which the monitoring web server should listen for
incoming HTTP connections.

@end table

@emph{meta}

These settings control the primary server module of gzochi-metad.

@table @samp
@item server.port
The local port on which to listen for incoming TCP connections from
gzochid application server nodes.

@item server.fs.data
The filesystem directory in which to store game state data for 
hosted game applications. The user associated with the gzochi-metad 
process must have read and write access to this directory, as the
server will attempt to create sub-directories rooted at this
location for each game, and will read and write data files in those
sub-directories.

@item storage.engine.dir
The parent directory for storage engine libraries. This path is used
to resolve the absolute location of the storage engine module 
specified by ``storage.engine'' (see below). The user associated 
with the gzochi-metad process must have read access to this
directory.

@item storage.engine
The name of the storage engine to use to store game application data.
If the value of this setting is given as ``mem'' then gzochi-metad's 
built-in B*tree-based storage engine will be used (note that this is
a transient data store, not suitable for production deployments); 
otherwise, this name is used in conjunction with the value of 
``storage.engine.dir'' to resolve the absolute location of a dynamic
library file that exports the gzochid storage engine interface.

Only the library file that corresponds to the named storage engine
will be loaded and inspected and boot time. All applications running
within the cluster share the same storage engine, although each
maintains its own databases.

@end table

@emph{log}

These settings control the system-wide logging behavior of gzochid.

@table @samp
@item priority.threshold
The lowest severity of log message that will be recorded in the logs
(both to console and to the server's log file). Valid values of this
settings are, in order of decreasing severity: @code{ERR}, 
@code{WARNING}, @code{NOTICE}, @code{INFO}, and @code{DEBUG}.

@end table

@node Monitoring
@chapter Monitoring

@cindex monitoring

The gzochi-metad server collects various bits of statistical
information and makes resources related to connected applicaiton
servers and active game applications available for reporting.

When the monitoring web server is enabled, it listens for HTTP 
connections on its configured port (8800 by default) and serves HTML
pages with information about the meta server and the application
servers it supports. Among other bits of information, this web
console displays each gzochid instance connected to the meta server
along with the total number of game client sessions connected in
turn to it.

@node GNU Free Documentation License
@appendix GNU Free Documentation License

@include fdl.texi

@node Concept index
@unnumbered Concept index

@printindex cp


@bye
